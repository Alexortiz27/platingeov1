/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.WorkerConsoleReportSpedd;


import com.backend.nsqworker.consumer.service.impl.MqConsumerByChannelServiceImplReportSpedd;
import java.util.Date;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

@SpringBootApplication
@PropertySource(
        ignoreResourceNotFound = false,
        value = "classpath:ingestaWorkers.properties")
@ComponentScan("com.backend.nsqworker.consumer.*")
@EntityScan("com.backend.ingest.entities")
@EnableJpaRepositories("com.backend.ingest.repositories")
@ComponentScan("com.backend.ingest.service.*")
public class WorkerReportSpeed implements CommandLineRunner {

    private final Logger log = LogManager.getLogger(WorkerReportSpeed.class);

    @Value("${nsq.topicIngest}")
    String topicIngestTrama;

    @Value("${nsq.produce.host}")
    String nsqAddress;

    @Value("${nsq.produce.port}")
    Integer nsqPort;
    @Value("${nsq.thread.count}")
    Integer nsqThreadCount;

    public static void main(String[] args) {
        new SpringApplicationBuilder(WorkerReportSpeed.class)
                .web(WebApplicationType.NONE) // .REACTIVE, .SERVLET
                .run(args);

    }
    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC")); 
        System.out.println("Spring boot application running in UTC timezone :" + new Date());
    }
    @Override
    public void run(String... args) throws Exception {
        log.info("==================WORKER mqConsumerReportSpedd OPERANDO===========");
    }

    @Bean(initMethod = "mqConsumerReportSpedd")
    public MqConsumerByChannelServiceImplReportSpedd mqConsumerReportSpedd() {
        return new MqConsumerByChannelServiceImplReportSpedd(topicIngestTrama, nsqAddress, nsqPort, nsqThreadCount);
    }


    @Bean(name = "mvcHandlerMappingIntrospector")
    public HandlerMappingIntrospector mvcHandlerMappingIntrospector() {
        return new HandlerMappingIntrospector();
    }
}
