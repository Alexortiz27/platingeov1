package com.backend.WorkerConsoleTramaWebSocket;

import com.backend.platin.consumer.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.backend.platin.util.NsqChannelConst;
import com.backend.nsqworker.consumer.model.NsqMessage;
import com.backend.ingest.dtos.TrackingTramaDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.dtos.TramaNsqDto;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.service.interfaces.ICompanyService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;

import java.util.Map;
import java.util.UUID;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplWebSocket implements MqConsumerService {
    @Autowired
    private EventRedisHsetService eventRedis;
    @Autowired
    ObjectMapper mapper;
    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    private final Logger log = LogManager.getLogger(MqConsumerByChannelServiceImplWebSocket.class);
    int tried = 0;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Value("${rest.uri.coords}")
    String uri;

    @Value("${token.Internal}")
    String tokenInternal;

    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;
    @Autowired
    private ICompanyService companyService;

    public MqConsumerByChannelServiceImplWebSocket() {
    }

    public MqConsumerByChannelServiceImplWebSocket(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);
        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.WEBSOCKET_WORKER_CHANNEL, (message) -> {
//            message = null;
            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();
                try {
                    nsqMessage.setAction(NsqChannelConst.WEBSOCKET_WORKER_CHANNEL);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker PosReport: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }
                try {
                    if (tried >= 3) {
                        tried = 0;
                        message.finished();
                        return;
                    }//
                    TramaInsDto trama = null;

                    trama = new Gson().fromJson(nsqMessage.getBody(), TramaInsDto.class);
                    String key = "vehicle-tracking:"+trama.getPlate();
                    String trackingPlate = eventRedis.findHgetTrackingPlate(key,"tracking");
                    if(trackingPlate!=null){
                        log.info("Tracking Operativo Vehicle:"+key);
                            if (trama.getPlate().equals(trackingPlate)) {
                                this.sendCoords(new TrackingTramaDto(trama.getPlate(), trama.getPosition().getLatitude(),
                                        trama.getPosition().getLongitude(), trama.getGpsDate(), trama.getSpeed(), trama.getOdometer()));
                            }
                    }

                    // Mensaje de confirmacion
                    message.finished();
                    log.info(nsqMessage.getBody());
                    return;
                } catch (Exception e) {
                    tried++;
                    log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
                    iErrorRepository.save(new ErrorLogs("Worker WEBSOCKET_WORKER_CHANNEL:" + e.getMessage(), "Error"));
                    log.info("Error Worker WEBSOCKET_WORKER_CHANNEL:" + e.getMessage());
                    e.printStackTrace();
                    e.getCause();
                    message.requeue();
                    return;
                }
            }
            return;
        });
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By Worker Channel El consumidor comenzó con éxito!");
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerByReportsChannel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void sendCoords(TrackingTramaDto trama) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String requestJson = new Gson().toJson(trama);

        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
        RestTemplate restTemplate = restTemplate();
        restTemplate.postForObject(uri + "/" + tokenInternal, entity, String.class);
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
