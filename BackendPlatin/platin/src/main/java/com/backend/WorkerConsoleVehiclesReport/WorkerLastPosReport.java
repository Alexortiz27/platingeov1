/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.WorkerConsoleVehiclesReport;

import com.backend.nsqworker.consumer.service.impl.MqConsumerLastPosReport;
import java.util.Date;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;


@SpringBootApplication
@PropertySource(
        ignoreResourceNotFound = false,
        value = "classpath:reportWorkers.properties")
@ComponentScan("com.backend.nsqworker.consumer.*")
@EntityScan("com.backend.ingest.entities")
@EnableJpaRepositories("com.backend.ingest.repositories")
@ComponentScan("com.backend.ingest.*")
public class WorkerLastPosReport implements CommandLineRunner {

    private final Logger log = LogManager.getLogger(WorkerLastPosReport.class);

    @Value("${nsq.ReportTopic}")
    String topic;

    @Value("${nsq.produce.host}")
    String nsqAddress;

    @Value("${nsq.produce.port}")
    Integer nsqPort;
    @Value("${nsq.thread.count}")
    Integer nsqThreadCount;

    public static void main(String[] args) {
        new SpringApplicationBuilder(WorkerLastPosReport.class)
                .web(WebApplicationType.NONE) // .REACTIVE, .SERVLET
                .run(args);

    }
    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT-5")); 
        System.out.println("Spring boot application running in GMT-5 timezone :" + new Date());
    }
    @Override
    public void run(String... args) throws Exception {
        log.info("==================WORKER mqConsumerByReportsChannel OPERANDO===========");
    }

    @Bean(initMethod = "mqConsumeLastPosReportMessage")
    public MqConsumerLastPosReport mqConsumeLastPosReportMessage() {
        return new MqConsumerLastPosReport(topic, nsqAddress, nsqPort, nsqThreadCount);
    }

    @Bean(name = "mvcHandlerMappingIntrospector")
    public HandlerMappingIntrospector mvcHandlerMappingIntrospector() {
        return new HandlerMappingIntrospector();
    }
}
