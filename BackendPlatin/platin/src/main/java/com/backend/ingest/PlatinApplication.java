package com.backend.ingest;
import java.util.Date;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EnableJpaRepositories()
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class PlatinApplication {
    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC")); 
        System.out.println("Spring boot application running in UTC timezone :" + new Date());
    }
	
	public static void main(String[] args) {
		SpringApplication.run(PlatinApplication.class, args);
                
	}
        
}
