package com.backend.ingest.authorization.token;

import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.repositories.IErrorLogsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

@Component
public class JwtEntryPoint implements AuthenticationEntryPoint {

    @Autowired
    private IErrorLogsRepository iErrorRepository;
    private final static Logger logger = LoggerFactory.getLogger(JwtEntryPoint.class);

    @Override
    public void commence(HttpServletRequest req, HttpServletResponse res, AuthenticationException e) throws IOException, ServletException {
        logger.error("fail en el método commence");
        res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "no AUTHORIZED");
        iErrorRepository.save(new ErrorLogs("Failed en el método commence " + e.getMessage(), "401"));

    }
}
