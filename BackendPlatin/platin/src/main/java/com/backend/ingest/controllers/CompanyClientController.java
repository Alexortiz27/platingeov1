/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.controllers;

import com.backend.ingest.dtos.CompanyClientInsDto;
import com.backend.ingest.dtos.CompanyClientListDto;
import com.backend.ingest.dtos.CompanyClientListPageDto;
import com.backend.ingest.dtos.CompanyClientPageDto;
import com.backend.ingest.dtos.CompanyClientShowDto;
import com.backend.ingest.dtos.CompanyClientUpdDto;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.entities.CompanyClient;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.ingest.service.interfaces.ICompanyClientService;
import com.backend.ingest.service.interfaces.ICompanyService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("api/v1/")
public class CompanyClientController {

    @Autowired
    private ICompanyClientService CompanyClientService;
    @Autowired
    private ICompanyService icompanyService;

    @Autowired
    private IErrorLogsRepository iErrorRepository;

    @GetMapping("/companiesClient")
    public ResponseEntity<Response> list() {

        List<CompanyClientListPageDto> listCompanyClient = null;

        try {
            listCompanyClient = CompanyClientService.findByStatus();

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar la Empresa:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al consultar la Empresa:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("Se consulto exitosamente la Empresa", HttpStatus.OK, listCompanyClient));
    }

    @GetMapping("/companiesClient/excel")
    public ResponseEntity<Response> listExcel(HttpServletResponse response) {
        try {

            CompanyClientService.excelExport(response);

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al exportar el excel de la empresa:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al exportar el excel de la empresa:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return null;
    }

    @GetMapping("/CompanyClient/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        CompanyClientListDto CompanyClient = null;

        try {
            CompanyClient = CompanyClientService.getId(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar la Empresa:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al consultar la Empresa: " + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("La consulta fue realizada exitosamente", HttpStatus.OK, CompanyClient));
    }

    @PostMapping("/CompanyClient")
    public ResponseEntity<Response> create(@Valid @RequestBody CompanyClientInsDto CompanyClientdto, BindingResult result) {

        List<CompanyClientListPageDto> CompanyClientNew = null;
        CompanyClientShowDto companyClient = null;
        try {
            boolean arroba = false;
            arroba = Util.validEmail(CompanyClientdto.getEmail());
            if (arroba == false) {
                iErrorRepository.save(new ErrorLogs("El email es invalido es requerido el @", "400"));
                return new ResponseEntity(new Response("El email es invalido es requerido el @", "400"), HttpStatus.BAD_REQUEST);
            }

            CompanyClientNew = CompanyClientService.findByName(CompanyClientdto.getName(), CompanyClientdto.getId_company());
            for (CompanyClientListPageDto companyClientListPageDto : CompanyClientNew) {
                if (companyClientListPageDto.getName().equals(CompanyClientdto.getName())
                        && companyClientListPageDto.getIdcompany().equals(CompanyClientdto.getId_company())) {
                    iErrorRepository.save(new ErrorLogs("El nombre de la EMV ya existe con esa empresa", "400"));
                    return new ResponseEntity(new Response("El nombre de la EMV ya existe con esa empresa", "400"), HttpStatus.BAD_REQUEST);

                }
            }
            Util.isEmptyField(result);

            companyClient = CompanyClientService.create(CompanyClientdto);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);

        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al Registrar la empresa" + " " + CompanyClientdto.getName() + ":" + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al Registrar la empresa" + " " + CompanyClientdto.getName() + ":" + ex.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("La empresa fue registrada con exito!", HttpStatus.OK, companyClient));
    }

    @PutMapping("/CompanyClient/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody CompanyClientUpdDto companyClientUpddto, BindingResult result,
            @PathVariable String id) {
        boolean arroba = false;
        arroba = Util.validEmail(companyClientUpddto.getEmail());
        if (arroba == false) {
            iErrorRepository.save(new ErrorLogs("El email es invalido es requerido el @", "400"));
            return new ResponseEntity(new Response("El email es invalido es requerido el @", "400"), HttpStatus.BAD_REQUEST);
        }
        List<CompanyClientListPageDto> listcompany = CompanyClientService.findNameDiferent(id,companyClientUpddto.getName(),companyClientUpddto.getId_company());
        if (!listcompany.isEmpty()) {
            iErrorRepository.save(new ErrorLogs("El nombre de la Empresa ya existe con esa EMV", "400"));
            return new ResponseEntity(new Response("El nombre de la Empresa ya existe con esa EMV", "400"), HttpStatus.BAD_REQUEST);
        }
        CompanyClientListDto companyClient = null;
        try {
            Util.isEmptyField(result);
            companyClient = CompanyClientService.update(id, companyClientUpddto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al actualizar la empresa: " + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al actualizar la empresa: " + ex.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("La empresa fue actualizada con exito!", HttpStatus.OK, companyClient));
    }

    @DeleteMapping("/CompanyClient/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {

        try {
            CompanyClientService.delete(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar la empresa" + e.getMessage(), "Error"));

            return new ResponseEntity(
                    new Response("Error al eliminar la empresa:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("La empresa fue eleminada con exito!", HttpStatus.OK));
    }

    @PostMapping("/companiesClientPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        try {
            Map<Object, Object> map = new HashMap<Object, Object>();
            List<Map<String, Object>> listCompanyClientPage = null;
            long total = CompanyClientService.countByStatus(params.getBusca());
            map = Util.totalPagesElemnt(total,params.getSize());
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listCompanyClientPage = CompanyClientService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)),params.getBusca());
            } else {
                listCompanyClientPage = CompanyClientService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())),params.getBusca());
                if (!params.getOrderAsc()) {
                    listCompanyClientPage = CompanyClientService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()),params.getBusca());
                }
            }
            return ResponseEntity.ok(new Response("La consulta de la empresa fue realizada con exito", HttpStatus.OK, listCompanyClientPage, map));
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar las empresas:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al consultar las empresas:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @GetMapping("/companiesClientByCompany/{company_id}")
    public ResponseEntity<Response> listByCompany(@PathVariable String company_id) {

        List<CompanyClientListDto> listCompanyClient = null;
        try {
            listCompanyClient = CompanyClientService.findByCompany(company_id);

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar la empresa:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al consultar la empresa:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta de la empresa fue realizada con exito!", HttpStatus.OK, listCompanyClient));
    }

}
