/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.controllers;

import com.backend.ingest.dtos.CompanyInDto;
import com.backend.ingest.dtos.CompanyShowDto;

import com.backend.ingest.dtos.CompanyUpdDto;

import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.User;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.ingest.service.interfaces.ICompanyService;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author FAMLETO
 */
@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("api/v1/")
public class CompanyController {

    @Autowired
    private IErrorLogsRepository iErrorRepository;
    @Autowired
    private ICompanyService companyService;
    @Autowired
    private IUserService userService;

    @GetMapping("/companies")
    public ResponseEntity<Response> list() {

        List<Map<String, Object>> listCompany = null;
        try {
            listCompany = companyService.findByStatus();

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar las EMV:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al consultar las EMV:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta de las EMV fue realizada con exito", HttpStatus.OK, listCompany));
    }

    @GetMapping("/companies/excel")
    public ResponseEntity<Response> listExcel(HttpServletResponse response) {
        List<CompanyShowDto> listCompany = null;
        try {
            companyService.excelExport(response);

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al exportar el excel de la EMV:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al exportar el excel de la EMV:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return null;
    }

    @GetMapping("/company/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        CompanyShowDto company = null;

        try {
            company = companyService.getId(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar la emv: " + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al consultar la emv: " + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta de la emv fue realizada con exito", HttpStatus.OK, company));
    }

    @PostMapping("/company")
    public ResponseEntity<Response> create(@Valid @RequestBody CompanyInDto companyDto, BindingResult result) {

        Company company = null;
        CompanyShowDto companyNew = null;
        try {
            boolean arroba = false;
            arroba = Util.validEmail(companyDto.getEmail());
            if (arroba == false) {
                iErrorRepository.save(new ErrorLogs("El email es invalido es requerido el @", "400"));
                return new ResponseEntity(new Response("El email es invalido es requerido el @", "400"), HttpStatus.BAD_REQUEST);
            }

            Util.isEmptyField(result);
            String ingestToken = UUID.randomUUID().toString().toUpperCase();
            company = new Company(companyDto.getName(), ingestToken, companyDto.getEmail(), companyDto.getAddress(), companyDto.getPhone(), companyDto.getCity(), companyDto.getCountry());

            User userd = userService.findById(companyDto.getUser_id());
            company.setUser(userd);
            companyNew = companyService.create(company);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al Registrar la EMV:" + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al Registrar la EMV:" + ex.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("La EMV fue registrada con exito!", HttpStatus.CREATED, companyNew));
    }

    @PutMapping("/company/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody CompanyUpdDto companyInDto, BindingResult result,
            @PathVariable String id) {
        boolean arroba = false;
        arroba = Util.validEmail(companyInDto.getEmail());
        if (arroba == false) {
            iErrorRepository.save(new ErrorLogs("El email es invalido es requerido el @", "400"));
            return new ResponseEntity(new Response("El email es invalido es requerido el @", "400"), HttpStatus.BAD_REQUEST);
        }

        List<Company> listcompany = companyService.findNameDiferent(id);
        if (listcompany == null) {
            iErrorRepository.save(new ErrorLogs("La EMV no existe en la base de datos", "400"));
            return new ResponseEntity(new Response("La EMV no existe en la base de datos", "400"), HttpStatus.BAD_REQUEST);

        }
        for (Company company : listcompany) {
            if (companyInDto.getName().equals(company.getName())) {
                iErrorRepository.save(new ErrorLogs("El nombre de la EMV ya existe", "400"));
                return new ResponseEntity(new Response("El nombre de la EMV ya existe", "400"), HttpStatus.BAD_REQUEST);
            } else if (companyInDto.getEmail().equals(company.getEmail())) {
                iErrorRepository.save(new ErrorLogs("El email de la EMV ya existe", "400"));
                return new ResponseEntity(new Response("El email de la EMV ya existe", "400"), HttpStatus.BAD_REQUEST);
            }
        }

        CompanyShowDto company = null;

        try {
            Util.isEmptyField(result);
            company = companyService.update(id, companyInDto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al Actualizar la EMV: " + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al Actualizar la EMV: " + ex.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("La EMV fue actualizada con exito!", HttpStatus.OK, company));
    }

    @DeleteMapping("/company/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {

        try {
            companyService.delete(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar la EMV: " + e.getMessage(), "Error"));
            return new ResponseEntity(
                    new Response("Error al eliminar la EMV: " + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("La EMV fue eliminada con exito", HttpStatus.OK));
    }

    @PostMapping("/companiesPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        try {
            Map<Object, Object> map = new HashMap<Object, Object>();
            List<Map<String, Object>> listCompanyPage = null;
            long total = companyService.countByStatus(params.getBusca());
            map = Util.totalPagesElemnt(total, params.getSize());
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listCompanyPage = companyService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)),params.getBusca());
                return ResponseEntity.ok(new Response("La EMV fue consultada con exito", HttpStatus.OK, listCompanyPage));
            } else {
                listCompanyPage = companyService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())),params.getBusca());
                if (!params.getOrderAsc()) {
                    listCompanyPage = companyService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()),params.getBusca());
                }
                return ResponseEntity.ok(new Response("La EMV fue consultada con exito", HttpStatus.OK, listCompanyPage, map));

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al Consultar la EMV:" + e.getMessage(), "Error"));
            return new ResponseEntity(
                    new Response("Error al Consultar la EMV:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/company-token/{token}")
    public ResponseEntity<Response> getCompanyByToken(@PathVariable String token) {
        String emvName = null;
        try {
            Company company = companyService.findByIngestToken(token);
            emvName = company.getName();
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al Actualizar la EMV: " + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al Actualizar la EMV: " + ex.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("La consulta de la emv fue realizada con exito!", HttpStatus.OK, emvName));
    }

}
