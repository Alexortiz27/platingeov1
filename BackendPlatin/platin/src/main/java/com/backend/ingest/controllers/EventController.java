package com.backend.ingest.controllers;

import com.backend.ingest.dtos.EventDto;
import com.backend.ingest.dtos.EventInsDto;
import com.backend.ingest.dtos.EventShowDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.interfaces.IVehicleService;
import com.backend.nsqworker.consumer.service.impl.MqConsumerLastPosReport;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.TokenInternalException;
import com.backend.platin.util.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("api/v1/")
public class EventController {

    @Autowired
    private IEventService eventService;
    @Autowired
    private IVehicleService vehicleService;

    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private IErrorLogsRepository iErrorRepository;

    @Value("${token.Internal}")
    String tokenInternal;
    private final Logger log = LogManager.getLogger(EventController.class);

    @GetMapping("/events")
    public ResponseEntity<Response> list() {

        List<EventShowDto> listEvent = null;
        try {
            listEvent = eventService.findByStatus();

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los Eventos:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al consultar los Eventos:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta de los Eventos fue realizada con exito", HttpStatus.OK, listEvent));
    }

    @GetMapping("/event/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        EventShowDto Event = null;

        try {
            Event = eventService.getById(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar el Evento:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al consultar el Evento:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta del Evento fue realizada con exito", HttpStatus.OK, Event));
    }

    @PostMapping("/event")
    public ResponseEntity<Response> create(@Valid @RequestBody EventInsDto eventDto, BindingResult result) {

        Event EventNew = null;
        EventInsDto eventShowDto = null;
        try {
            Util.isEmptyField(result);
            EventNew = new Event(eventDto.getType(), eventDto.getGeozone_id(), eventDto.getLatitude(),
                    eventDto.getLongitude(),
                    eventDto.getReceiveDate(), eventDto.getGpsDate(), eventDto.getSpedd(), eventDto.getIdTravel(), eventDto.getNameTravel(),eventDto.getOdometer());
            //Vehicle vehicledat = vehicleService.findById(eventDto.getVehicle_id());
            //EventNew.setVehicle_id(vehicledat);
            //eventService.save()
            eventShowDto = eventService.save(eventDto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al registrar el evento : " + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al registrar el evento : " + ex.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("El evento fue registrado con exito!", HttpStatus.OK, eventShowDto));
    }

    @PutMapping("/event/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody EventInsDto EventInDto, BindingResult result,
            @PathVariable String id) {

        EventShowDto Event = null;
        try {
            Util.isEmptyField(result);
            Event = eventService.update(id, EventInDto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al actualizar el evento: " + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al actualizar el evento:" + ex.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("El evento fue actualizado con exito!", HttpStatus.OK, Event));
    }

    @DeleteMapping("/event/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {

        try {
            eventService.delete(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar el evento:" + e.getMessage(), "Error"));
            return new ResponseEntity(
                    new Response("Error al eliminar el evento:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("El evento fue removido con exito!", HttpStatus.OK));
    }

    @PostMapping("/eventsPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        try {
            List<Map<String, Object>> listEventPage = null;
            Map<Object, Object> map = new HashMap<Object, Object>();
            long total = eventService.countByStatus(params.getBusca());
            map = Util.totalPagesElemnt(total, params.getSize());
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listEventPage = eventService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)),params.getBusca());
                return ResponseEntity.ok(new Response("Los eventos fue consultado con exito", HttpStatus.OK, listEventPage));
            } else {
                listEventPage = eventService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())),params.getBusca());
                if (!params.getOrderAsc()) {
                    listEventPage = eventService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()),params.getBusca());
                }
                return ResponseEntity.ok(new Response("Los eventos fue consultado con exito", HttpStatus.OK, listEventPage,map));

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los eventos:" + e.getMessage(), "Error"));
            return new ResponseEntity(
                    new Response("Error al consultar los eventos:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/alertEvent/{tokenInternalRest}")
    public ResponseEntity<?> sendAlertEvent(@PathVariable String tokenInternalRest, @Valid @RequestBody EventDto event, BindingResult result) {

        if (tokenInternal.equals(tokenInternalRest)) {
            template.convertAndSend("/topic/alertEvent", event);

        } else {
            log.info(new Response("El token es incorrecto", "Error"));
            return new ResponseEntity(new Response("El token es incorrecto", "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("Alerta enviada con exito!", HttpStatus.OK));

    }
}
