/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.controllers;

import com.backend.ingest.dtos.CoordInDto;
import com.backend.ingest.dtos.GeozoneCircleDto;
import com.backend.ingest.dtos.GeozoneCircleUpdDto;
import com.backend.ingest.dtos.GeozonePolygonDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.User;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.entities.Geozone;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.IActionService;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.ingest.service.interfaces.IGeozoneService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author FAMLETO
 */
@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("api/v1/")
public class GeozoneController {

    @Autowired
    private IGeozoneService geozoneService;
    @Autowired
    private IErrorLogsRepository iErrorRepository;
    @Autowired
    IUserService userService;
    @Autowired
    ObjectMapper mapper;
    CoordInDto[] coor;

    @Autowired
    private IActionService actionService;

    @PostMapping("/geozone/excel")
    public ResponseEntity<Response> excelImport(@RequestParam("file") MultipartFile file) throws IOException {
        try {
            geozoneService.excelImport(file);

        } catch (Exception e) {

            iErrorRepository.save(new ErrorLogs("Error al importar el excel:" + e.getMessage(), "Error"));

            return new ResponseEntity(new Response("Error al importar el excel:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("Importado el excel con exito", HttpStatus.OK));

    }

    @GetMapping("/geozones")
    public ResponseEntity<Response> list() {
        List<Geozone> listGeozone = null;

        try {
            listGeozone = geozoneService.findByStatus();
            if (listGeozone.isEmpty()) {
                throw new DataNotFoundException(com.backend.platin.util.Util.GEOZONE_NOT_FOUND);
            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar las geozonas:" + e.getMessage(), "Error"));

            return new ResponseEntity(new Response("Error al consultar las geozonas:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("La consulta de la geozone fue realizada con exito!", HttpStatus.OK, listGeozone));

    }

    @PostMapping("/geozonesPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        try {
            Map<Object, Object> map = new HashMap<Object, Object>();
            List<Map<String, Object>> listGeozonePage = null;
            long total = geozoneService.countByStatus(params.getBusca());
            map = Util.totalPagesElemnt(total, params.getSize());
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listGeozonePage = geozoneService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)), params.getBusca());
                return ResponseEntity.ok(new Response("La consulta de la geozone fue realizada con exito!", HttpStatus.OK, listGeozonePage,map));
            } else {
                listGeozonePage = geozoneService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())), params.getBusca());
                if (!params.getOrderAsc()) {
                    listGeozonePage = geozoneService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()), params.getBusca());
                }
                return ResponseEntity.ok(new Response("La consulta de la geozone fue realizada con exito!", HttpStatus.OK, listGeozonePage,map));

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar la geozona:" + e.getMessage(), "Error"));
            return new ResponseEntity(
                    new Response("Error al consultar la geozona:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/geozone/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        Geozone geozone = null;
        try {
            geozone = geozoneService.findById(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar la geozona: " + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al consultar la geozona:" + e.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("La consulta de la geozone fue realizada con exito!", HttpStatus.OK, geozone));
    }

    @PostMapping("/geozonecircle")
    public ResponseEntity<Response> createGeozoneCircle(@Valid @RequestBody GeozoneCircleDto geozone,
                                                        BindingResult result) {

        Geozone geozoneNew = null;

        try {
            Util.isEmptyField(result);
            geozoneNew = new Geozone(geozone.getCategory(), geozone.getRadius(), geozone.getType(), geozone.getCoords(), geozone.getName());
            User u = userService.findById(geozone.getUser_id());
            geozoneNew.setUser_id(u);
            geozoneNew = geozoneService.create(geozoneNew);

            actionService.create(Util.ACTION_CREATE, geozone.getSession_id(), Util.GEOZONE_TABLE_NAME, geozoneNew.getId());

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al Registrar la geozona:" + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al Registrar la geozona: " + ex.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("La geozona circular fue registrado con exito!", HttpStatus.OK, geozoneNew));
    }

    @PostMapping("/geozonepolygon")
    public ResponseEntity<Response> createGeozonePolygon(@Valid @RequestBody GeozonePolygonDto geozone,
                                                         BindingResult result) {

        Geozone geozoneNew = null;

        try {
            Util.isEmptyField(result);
            geozoneNew = new Geozone(geozone.getCategory(), 0, geozone.getType(), geozone.getCoords(), geozone.getName());
            User u = userService.findById(geozone.getUser_id());
            geozoneNew.setUser_id(u);
            geozoneNew = geozoneService.create(geozoneNew);

            actionService.create(Util.ACTION_CREATE, geozone.getSession_id(), Util.GEOZONE_TABLE_NAME, geozoneNew.getId());

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al Registrar la geozona: " + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al Registrar la geozona: " + ex.getMessage(), "Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("La geozona poligonal fue registrado con exito!", HttpStatus.OK, geozoneNew));
    }

    @PutMapping("/geozone/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody GeozoneCircleUpdDto geozoneDto, BindingResult result,
                                           @PathVariable String id) {

        Geozone geozone = null;

        try {
            Util.isEmptyField(result);

            Geozone listgeo = geozoneService.findNameDiferent(id, geozoneDto.getName());
            if (listgeo != null) {
                iErrorRepository.save(new ErrorLogs("El nombre de la Geozona ya existe", "400"));
                return new ResponseEntity(new Response("El nombre de la Geozona ya existe", "400"), HttpStatus.BAD_REQUEST);
            }


            geozone = geozoneService.update(id, geozoneDto);

            actionService.create(Util.ACTION_UPDATE, geozoneDto.getSession_id(), Util.GEOZONE_TABLE_NAME, geozone.getId());

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al actualizar la geozona:  " + ex.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al actualizar la geozona:  " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity
                .ok(new Response("La geozona fue actualizado con exito!", HttpStatus.OK, geozone));
    }

    @DeleteMapping("/geozone/{id}/{session_id}")
    public ResponseEntity<?> delete(@PathVariable String id, @PathVariable String session_id) {

        try {

            Geozone geozone = geozoneService.delete(id);

            actionService.create(Util.ACTION_DELETE, session_id, Util.GEOZONE_TABLE_NAME, geozone.getId());

        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar la geozona:" + e.getMessage(), "Error"));
            return new ResponseEntity(new Response("Error al eliminar la geozona: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("La geozona fue eliminado con exito!", HttpStatus.OK));
    }
}
