package com.backend.ingest.controllers;

import com.backend.ingest.dtos.MapInsDto;
import com.backend.ingest.dtos.MapUpdDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Maps;
import com.backend.ingest.entities.User;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.IActionService;

import java.util.*;

import javax.validation.Valid;

import com.backend.ingest.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.service.interfaces.IMapService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.platin.util.Util;

@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("/api/v1/")
public class MapController {
    @Autowired
    private IUserService userService;
    @Autowired
    private IMapService mapService;
    @Autowired
    private IErrorLogsRepository iErrorRepository;
    @Autowired
    private IActionService actionService;

    String messageExito = "La consulta de los Mapas fue realizada con exito!";
    String messageError = "Error al consultar los Mapas:";
    String messageResgistrar = "El mapa fue registrado con exito";
    String messageActualizar = "El mapa fue Actualizado con exito";
    String messageEliminado = "El mapa fue eliminado con exito!";
    String messageExiste = "El nombre del mapa ya existe";

    @GetMapping("/map/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        Maps map = null;

        try {
            map = mapService.getById(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs(messageError + e.getMessage(), "500"));
            return new ResponseEntity(new Response(messageError + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return ResponseEntity.ok(new Response(messageExito, HttpStatus.OK, map));
    }

    @GetMapping("/maps")
    public ResponseEntity<Response> list() {
        List<Maps> listMap = null;
        try {
            Calendar calendar = Calendar.getInstance();
            listMap = mapService.findByStatus();
            for (Maps listMa : listMap) {
                calendar.setTimeInMillis(listMa.getCreationDate().getTime() - 18000000);
                Date de = calendar.getTime();
                listMa.setCreationDate(de);
            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs(messageError + e.getMessage(), "500"));
            return new ResponseEntity(new Response(messageError + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response(messageExito, HttpStatus.OK, listMap));

    }

    @PostMapping("/mapsPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        try {
            Map<Object, Object> map = new HashMap<Object, Object>();
            List<Map<String, Object>> listMapsPage = null;
            long total = mapService.countByStatus(params.getBusca(),params.getUser_id());
            map = Util.totalPagesElemnt(total, params.getSize());
            if (params.getNameOrder() == null || params.getOrderAsc() == null
                    || params.getPage() == null || params.getSize() == null) {
                listMapsPage = mapService.findAllPage(
                        PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)),params.getBusca(),params.getUser_id());
                return ResponseEntity.ok(new Response(messageExito, HttpStatus.OK, listMapsPage));
            } else {
                listMapsPage = mapService.findAllPage(
                        PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())),params.getBusca(),params.getUser_id());
                if (!params.getOrderAsc()) {
                    listMapsPage = mapService.findAllPage(
                            PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder()).descending()),params.getBusca(),params.getUser_id());
                }
                return ResponseEntity.ok(new Response(messageExito, HttpStatus.OK, listMapsPage, map));

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs(messageError + e.getMessage(), "500"));
            return new ResponseEntity(new Response(messageError + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/map")
    public ResponseEntity<Response> create(@Valid @RequestBody MapInsDto mapInstDto, BindingResult result) {

        Maps mapsNew = null;

        try {
            Util.isEmptyField(result);
            mapsNew = mapService.create(mapInstDto);
            
            actionService.create(Util.ACTION_CREATE,mapInstDto.getSession_id() , Util.MAP_TABLE_NAME, mapsNew.getId());
            
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return ResponseEntity.ok(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST));

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs(messageError + ex.getMessage(), "500"));
            return new ResponseEntity(new Response(messageError + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response(messageResgistrar, HttpStatus.CREATED, mapsNew));
    }

    @PutMapping("/map/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody MapUpdDto mapUpdDto, BindingResult result,
            @PathVariable String id) {

        Maps mapsupdated = null;
        try {
            List<Maps> maplist = mapService.findNameDiferent(id);
            for (Maps map : maplist) {
                if (mapUpdDto.getName().equals(map.getName())) {
                    iErrorRepository.save(new ErrorLogs(messageExiste, "400"));
                    return new ResponseEntity(new Response(messageExiste, "400"), HttpStatus.BAD_REQUEST);
                }
            }

            Util.isEmptyField(result);
            mapsupdated = mapService.update(id, mapUpdDto);
            
            actionService.create(Util.ACTION_UPDATE,mapUpdDto.getSession_id(),Util.MAP_TABLE_NAME, mapsupdated.getId());
            
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs(messageError + ex.getMessage(), "500"));
            return new ResponseEntity(new Response(messageError + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response(messageExito, HttpStatus.OK, mapsupdated));
    }

    @DeleteMapping("/map/{id}/{session_id}")
    public ResponseEntity<Response> delete(@PathVariable String id,@PathVariable String session_id) {

        try {
           Maps map= mapService.delete(id);
           
           actionService.create(Util.ACTION_DELETE,session_id, Util.MAP_TABLE_NAME, map.getId());
           
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs(messageError + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response(messageError + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response(messageEliminado, HttpStatus.OK));

    }

}
