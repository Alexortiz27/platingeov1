/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.controllers;

import com.backend.ingest.dtos.MarkerDto;
import com.backend.ingest.dtos.MarkerUpdDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Marker;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.IActionService;
import com.backend.ingest.service.interfaces.IMarkerService;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.platin.util.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author FAMLETO
 */
@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("/api/v1/")
public class MarkerController {


    @Autowired
    private IMarkerService markerService;
    @Autowired
    private IErrorLogsRepository iErrorRepository;
    @Autowired
    private IActionService actionService;


    public String messagesApi(int pos, String type) {
        String message = null;
        switch (pos) {
            case 1:
                message = "El Marker fue " + type + " con exito";
                break;
            case 2:
                message = "La consulta del Marker fue realizada con exito!";
                break;
            case 3:
                message = "El nombre del Marker ya existe!";
                break;
            case 4:
                message = "Error al " + type + " el marker:";
                break;
        }
        return message;
    }

    @GetMapping("/markers")
    public ResponseEntity<Response> list() {

        List<Marker> listMarker = null;
        try {
            listMarker = markerService.findByStatus();
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs(messagesApi(4, "consultar") + e.getMessage(), "500"));
            return new ResponseEntity(new Response(messagesApi(4, "consultar") + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response(messagesApi(2, null), HttpStatus.OK, listMarker));
    }

    @GetMapping("/marker/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        Marker marker = null;

        try {
            marker = markerService.findById(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs(messagesApi(4, "consultar") + e.getMessage(), "500"));
            return new ResponseEntity(new Response(messagesApi(4, "consultar") + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response(messagesApi(2, null), HttpStatus.OK, marker));
    }

    @PostMapping("/marker")
    public ResponseEntity<Response> create(@Valid @Validated @RequestBody MarkerDto markerDto, BindingResult result) {

        Marker marker = null;

        try {
            if (result.hasErrors()) {
                List<ObjectError> allErrors = result.getAllErrors();
                for (ObjectError error : allErrors) {
                    System.out.println(error);
                }
            }
            Util.isEmptyField(result);

            marker = markerService.create(markerDto);
            
            actionService.create(Util.ACTION_CREATE, markerDto.getSession_id(),Util.MARKER_TABLE_NAME, marker.getId());
            
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs(messagesApi(4, "registrar") + ex.getMessage(), "500"));
            return new ResponseEntity(new Response(messagesApi(4, "registrar") + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response(messagesApi(1, "registrado"), HttpStatus.CREATED, marker));
    }
//

    @PutMapping("/marker/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody MarkerUpdDto markerUpddto, BindingResult result,
                                           @PathVariable String id) {

        Marker marker = null;

        try {
            List<Marker> maplist = markerService.findNameDiferent(id);
            for (Marker map : maplist) {
                if (markerUpddto.getName().equals(map.getName())) {
                    iErrorRepository.save(new ErrorLogs(messagesApi(3, null), "400"));
                    return new ResponseEntity(new Response(messagesApi(3, null), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
                }
            }

            Util.isEmptyField(result);
            marker = markerService.update(id, markerUpddto);
            
            actionService.create(Util.ACTION_UPDATE,markerUpddto.getSession_id(), Util.MARKER_TABLE_NAME, marker.getId());
            
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs(messagesApi(4, "eliminar") + ex.getMessage(), "500"));
            return new ResponseEntity(new Response(messagesApi(4, "eliminar") + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response(messagesApi(1, "actualizado"), HttpStatus.OK, marker));
    }
//

    @DeleteMapping("/marker/{id}/{session_id}")
    public ResponseEntity<?> delete(@PathVariable String id,@PathVariable String session_id) {

        try {
            markerService.delete(id);
            
            actionService.create(Util.ACTION_DELETE, session_id, Util.MARKER_TABLE_NAME, id);
            
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs(messagesApi(4, "eliminar") + e.getMessage(), "500"));
            return new ResponseEntity(new Response(messagesApi(4, "eliminar") + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response(messagesApi(1, "eliminado"), HttpStatus.OK));
    }

    @PostMapping("/markersPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {

        List<Map<String, Object>> listMarkerPage = null;
        Map<Object, Object> map = new HashMap<Object, Object>();
        long total = markerService.countByStatus(params.getBusca(), params.getUser_id());
        map = Util.totalPagesElemnt(total, params.getSize());
        try {
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listMarkerPage = markerService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)), params.getBusca(),params.getUser_id());
            } else {
                listMarkerPage = markerService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())), params.getBusca(),params.getUser_id());
                if (!params.getOrderAsc()) {
                    listMarkerPage = markerService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()), params.getBusca(),params.getUser_id());
                }

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs(messagesApi(4, "consultar") + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response(messagesApi(4, "consultar") + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response(messagesApi(2, "consultado"), HttpStatus.OK, listMarkerPage, map));

    }

}
