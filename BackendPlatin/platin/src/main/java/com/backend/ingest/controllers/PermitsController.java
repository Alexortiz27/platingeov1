package com.backend.ingest.controllers;

import com.backend.ingest.dtos.PermitsInsDto;
import com.backend.ingest.dtos.PermitsUpdDto;
import com.backend.ingest.entities.ErrorLogs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.entities.Permits;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.IPermitsService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.platin.util.Util;

@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("/api/v1/")
public class PermitsController {

    @Autowired
    private IPermitsService permitService;
    @Autowired
    private IErrorLogsRepository iErrorRepository;

    @GetMapping("/permit/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        Permits Rol = null;

        try {
            Rol = permitService.findById(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar Permisos:" + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar Permisos: " + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return ResponseEntity.ok(new Response("La consulta de Permisos fue realizada con exito!", HttpStatus.OK, Rol));
    }

    @GetMapping("/permits")
    public ResponseEntity<Response> list() {
        List<Permits> listRol = null;
        try {
            listRol = permitService.findByStatus();
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar Permisos: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar Permisos: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("La consulta de Permisos fue realizada con exito!", HttpStatus.OK, listRol));

    }

    @PostMapping("/permitsPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params
    ) {
        Page<Permits> listPermitsPage = null;
        try {
            if (params.getNameOrder() == null || params.getOrderAsc() == null
                    || params.getPage() == null || params.getSize() == null) {
                listPermitsPage = permitService.findAllPage(
                        PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)), params.getBusca());
            } else {
                listPermitsPage = permitService.findAllPage(
                        PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())), params.getBusca());
                if (!params.getOrderAsc()) {
                    listPermitsPage = permitService.findAllPage(
                            PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder()).descending()), params.getBusca());
                }

            }
            return ResponseEntity.ok(new Response("La consulta de Permisos fue realizada con exito!", HttpStatus.OK, listPermitsPage));

        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los permisos:" + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar los permisos:" + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/permit")
    public ResponseEntity<Response> create(@Valid @RequestBody PermitsInsDto permitsInstDto, BindingResult result) {

        Permits permitsNew = null;

        try {
            Util.isEmptyField(result);
            Optional<Permits> perexis = permitService.getByRolName(permitsInstDto.getNamePermists());
            if (!perexis.isEmpty()) {
                return new ResponseEntity(new Response("El Nombre del Permiso ya existe!", "400"),HttpStatus.BAD_REQUEST);
            }
            permitsNew = new Permits(permitsInstDto.getNamePermists(),permitsInstDto.getCreate_permits(),permitsInstDto.getEdit_permits(),permitsInstDto.getDelete_permits());
            permitsNew = permitService.create(permitsNew);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"),HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al registrar un permiso: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al registrar un permiso: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("El Permiso fue realiziado cone xito!", HttpStatus.CREATED, permitsNew));
    }

    @PutMapping("/permit/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody PermitsUpdDto permitsUpdDto, BindingResult result,
                                           @PathVariable String id) {

        Permits Permitsupdated = null;
        try {
            Util.isEmptyField(result);
            Permits permexis = permitService.findNameDiferent(id,Util.ACTIVE_STATUS_BIT,permitsUpdDto.getNamePermists());
            if(permexis!=null){
                iErrorRepository.save(new ErrorLogs("El nombre del permiso que desea actualizar ya existe en la bd", "400"));
                return new ResponseEntity(new Response("El nombre del permiso que desea actualizar ya existe en la bd", "400"), HttpStatus.BAD_REQUEST);
            }
            Permitsupdated = permitService.update(id, permitsUpdDto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al actualizar un permiso:" + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al actualizar un permiso: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("El Permiso fue actualizado cone xito!", HttpStatus.OK, Permitsupdated));
    }

    @DeleteMapping("/permit/{id}")
    public ResponseEntity<Response> delete(@PathVariable String id) {

        try {
            permitService.delete(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar un permiso: " + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response("Error al eliminar un permiso:" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("El Permiso fue eliminado cone xito!", HttpStatus.OK));

    }

}
