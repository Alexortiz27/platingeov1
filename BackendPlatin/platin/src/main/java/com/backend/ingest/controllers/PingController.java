package com.backend.ingest.controllers;

import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Permits;
import com.backend.ingest.redis.Config.RedisConfig;
import com.backend.ingest.service.interfaces.IPermitsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.backend.platin.helper.Response;
import com.github.brainlag.nsq.Connection;
import com.github.brainlag.nsq.NSQConfig;
import com.github.brainlag.nsq.NSQProducer;
import com.github.brainlag.nsq.exceptions.NSQException;
import io.lettuce.core.api.sync.RedisCommands;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;



@RestController
@CrossOrigin()
@RequestMapping("/api/")
public class PingController {
    
    @Value("${nsq.lookup.host}")
    private String nsqlookupd;

    @Autowired
    RedisCommands<String, String>  jedis;

    @Autowired
    private IPermitsService permitService;
    @Autowired
    NSQProducer nsqProducer;

    
    @GetMapping("/ping")
    public Map<String,?> pingv2() {
        Map<String, String> map = new HashMap<String, String>();
        String statusCache = "fail";
        String statusQueue = "fail";
        String statusDb = "fail";
 
        try {
             var client = HttpClient.newHttpClient();
             var request = HttpRequest.newBuilder(URI.create("http://"+nsqlookupd+":4161/ping")).build(); 
             var response = client.send(request,BodyHandlers.ofString());
             if (response.body().toString().equals("OK")){
                 statusQueue = "ok";
             }
         } catch (Exception e) { }

         try {
            String pong = jedis.ping();
            if(pong.contains("PONG")){
                statusCache = "ok";
            }
            //statusCache = "ok";
         } catch (Exception e) { }

        List<Permits> listRol = null;
        try {
            listRol = permitService.findByStatus();
            System.out.println(listRol);
            statusDb = "ok";
        } catch (Exception e) { }
         
         map.put("queue", statusQueue);
         map.put("cache", statusCache);
         map.put("db", statusDb);
         return  map;
    }

}
