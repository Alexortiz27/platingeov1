/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.controllers;



import com.backend.ingest.dtos.*;
import com.backend.ingest.dtos.Reports.jsons.ReportResponseDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Report;

import com.backend.ingest.entities.Trama;
import com.backend.ingest.service.interfaces.*;
import com.backend.platin.dtos.geojson.GeoJsonDto;
import com.backend.platin.dtos.geojson.MetaStandardLasPosDto;

import com.backend.platin.exceptions.GeneralException;
import com.backend.platin.helper.Response;
import com.backend.ingest.producer.Service.MqProductService;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.util.VehiclesSortByDate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.Callable;
import javax.validation.Valid;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.backend.platin.util.Util;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * @author FAMLETO
 */
@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("/api/v1/")
public class ReportsController {

    @Autowired
    MqProductService mqProductService;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    SimpMessagingTemplate template;
    @Value("${url.map}")
    private String urlMap;
    @Value("${geojson.path}")
    private String geojsonPath;

    @Value("${xls.path}")
    private String xlsPath;

    @Value("${rest.uri.nominatin}")
    private String nomatin_uri;
    @Autowired
    private IReportService reportService;

    @Autowired
    private IErrorLogsRepository iErrorRepository;
    @Value("${token.Internal}")
    String tokenInternal;
    private final Logger log = LogManager.getLogger(ReportsController.class);
    @Autowired
    private ITramaService tramaService;
    @Autowired
    private IVehicleService vehicleService;

    @Autowired
    private EventRedisHsetService redisService;
    @Autowired(required = true)
    private IEventService eventService;
    @Autowired
    private ICompanyService companyService;

    @PostMapping("/request-report")
    public ResponseEntity<Response> showReport(@Valid @RequestBody ReportDto report,
                                               BindingResult result) throws ParseException, JsonProcessingException {
        String json = "";
        ObjectMapper mapper = new ObjectMapper();

//        ReportNsqDto reportNsq = new ReportNsqDto(report.getPlate(), report.getStartDate(), report.getEndDate(), report.getTypeReport(), report.getCompany_id());
        Util.USER_ID = report.getUser_id();
        json = new Gson().toJson(report);
        try {

            mqProductService.createTopicBasicsReports(json);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al exportar el Reporte: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al exportar el Reporte: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("El reporte fue exportado con exito", HttpStatus.OK));

    }
    @GetMapping("/deferred")
    public DeferredResult<String> asyncDeferredResultMethod() throws InterruptedException {
        Map<Object, Object> deferredResultMap = new HashMap<Object, Object>();

        System.out.println("main method exec");

        // Crear objeto

        // Cuando el resultado de DeferredResult está vacío, la recepción no recibe una respuesta hasta que haya un valor para responder a la interfaz
        DeferredResult<String> deferredResult = new DeferredResult<>();

        deferredResultMap.put("task", deferredResult);

        System.out.println("main method end");
        return deferredResult;

    }
    @PostMapping("/request-report-api")
    public Callable<ResponseEntity<Response>> asyncCallableMethod(@Valid @RequestBody ReportDto report,
                                                                  BindingResult result) throws ParseException, JsonProcessingException {
        Callable<ResponseEntity<Response>> resultCall = new Callable<ResponseEntity<Response>>() {
            @Override
            public ResponseEntity<Response> call() throws InterruptedException {
                String uuid = UUID.randomUUID().toString();
                String geoUrlMap = urlMap + uuid + ".geojson";
                double totalDistance = 0;
                Instant start = Instant.now();
                //Util.USER_ID = report.getUser_id();
                try {
                    String keySetVehicle = "VehicleSetReport" + report.getPlate();
                    String vehicle = redisService.findGetTypeWorkersSet(keySetVehicle);
                    if (vehicle == null) {
                        IVehicleCompanyReportDto vehicles = vehicleService.vehiclefindByPlate(report.getPlate());
                            VehicleCompanyReportDto vehi = new VehicleCompanyReportDto(vehicles.getcode_osinergmin(), vehicles.getNameemv(), vehicles.getStatus(), vehicles.getNameclient());
                            vehicle = new Gson().toJson(vehi);
                        if (vehicle != null) {
                            redisService.createSetTypeWorkers(keySetVehicle, vehicle);
                        }
                    }
                    if (vehicle == null) {
                        throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
                    }
                    VehicleCompanyReportDto vehicleCompany = null;
                    vehicleCompany = new Gson().fromJson(vehicle, VehicleCompanyReportDto.class);
                    String[] keys = redisService.createReportRequest(Util.REQUESTED_STATUS, report, " ", " ");
                    if (report.getTypeUrl().equals("xls")) {
                        XSSFWorkbook wb = new XSSFWorkbook();
                        XSSFSheet sheett = wb.createSheet("Tramas");
                        CellStyle stylee = wb.createCellStyle();
                        String vehicle_status = vehicleCompany.getStatus() ? "Habilitado" : "Inhabilitado";
                        //Change status of report
                        String xlsUrlMap = urlMap + uuid + ".xls";
                        redisService.changeStatusReport(keys, report, xlsUrlMap, Util.PROCESSING_STATUS, geoUrlMap, 0, report.getPlate());
                        if (report.getTypeReport().equals(Util.STANDARD_LAST_POS_REPORT)) {
                            log.warn("tramas a procesar");
                            List<Trama> tramas = tramaService.getTramasByPlateAndGpsDate(report.getPlate(),
                                    report.getStartDate(),
                                    report.getEndDate());
                            totalDistance = getTotalDistance(tramas);
                            if (tramas.isEmpty())
                                throw new DataNotFoundException(Util.TRAMA_NOT_FOUND);

                            String[] excelHeader = Util.generateGeneralHeadersExcel(report.getTypeReport());
                            String[] excelBody = {"Fecha Gps", "Latitud", "Longitud", "Velocidad", "Fecha Ingesta"};

                            PositionDto position = new Gson().fromJson(tramas.get(0).getPosition(), PositionDto.class);
                            String[] locations = getLocationFromLatitudeAndLongitude(position);

                            Object[] exceldataLastPos = {report.getPlate(), vehicleCompany.getNameemv(),
                                    vehicleCompany.getNameclient(), vehicleCompany.getcode_osinergmin(), vehicle_status,
                                    geoUrlMap, Util.convertDateUtcGmtPeruString(new Date()), Util.convertDateUtcGmtPeruString(report.getStartDate()),
                                    Util.convertDateUtcGmtPeruString(report.getEndDate()), String.valueOf(totalDistance), locations[0], locations[1], locations[2]};
                            generateStandardLastPosReportExcel(uuid, excelHeader, exceldataLastPos, excelBody, xlsPath, tramas, sheett, wb, stylee);
                            Instant finishTime = Instant.now();
                            Duration timeElapsed = Duration.between(start, finishTime);
                            redisService.changeStatusReport(keys, report, xlsUrlMap, Util.COMPLETED_STATUS, "", timeElapsed.getSeconds(), report.getPlate());
                            ReportInDto reportObject = new ReportInDto("", xlsUrlMap, report.getTypeReport(), Util.COMPLETED_STATUS, report.getStartDate(),
                                    report.getEndDate(), true, report.getCompany_id());
                            reportService.create(reportObject);
                        }
                    } else {
                        keys = redisService.createReportRequest(Util.REQUESTED_STATUS, report, " ", " ");
                        log.warn("generando geojson");
                        List<Trama> geoTramas = new ArrayList<>();
                        //generate geojson file
                        GeoJsonDto geoJsonDto = new GeoJsonDto();
                        Util util = new Util();
                        String[][] body = null;

                        int rowaumentv1 = 0;
                        if (report.getTypeReport().equals(Util.STANDARD_LAST_POS_REPORT)) {
                            geoTramas = tramaService.getTramasByPlateAndGpsDate(report.getPlate(), report.getStartDate(), report.getEndDate());
                            totalDistance = getTotalDistance(geoTramas);
                            MetaStandardLasPosDto meta = new MetaStandardLasPosDto();
                            geoJsonDto.setMeta(meta);
                            body = new String[geoTramas.size()][14];
                        }
                        int columnCount;
                        geoJsonDto.setHeader(util.generateGeneralHeadersGeoJson(report.getTypeReport()));
                        redisService.changeStatusReport(keys, report, "", Util.PROCESSING_STATUS, geoUrlMap, 0, report.getPlate());
                        if (!report.getTypeReport().equals(Util.GLOBAL_REPORT)) {
                            for (int i = 0; i < geoTramas.size(); i++) {
                                columnCount = 0;
                                body[i][columnCount++] = report.getPlate();
                                body[i][columnCount++] = vehicleCompany.getNameemv();
                                body[i][columnCount++] = vehicleCompany.getNameclient();
                                body[i][columnCount++] = vehicleCompany.getcode_osinergmin();
                                body[i][columnCount++] = vehicleCompany.getStatus() ? Util.PARAM_ACTIVE : Util.PARAM_INACTIVE;
                                body[i][columnCount++] = geoUrlMap;
                                body[i][columnCount++] = Util.parseDateToPrinterFormatString(Util.convertDateUtcGmtPeru(new Date()));
                                body[i][columnCount++] = Util.parseDateToPrinterFormatString(Util.convertDateUtcGmtPeru(report.getStartDate()));
                                body[i][columnCount++] = Util.parseDateToPrinterFormatString(Util.convertDateUtcGmtPeru(report.getEndDate()));

                                if (report.getTypeReport().equals(Util.STANDARD_LAST_POS_REPORT)) {
                                    PositionDto positiondto = new Gson().fromJson(geoTramas.get(i).getPosition(), PositionDto.class);
                                    body[i][columnCount++] = String.valueOf(positiondto.getLatitude());
                                    body[i][columnCount++] = String.valueOf(positiondto.getLongitude());
                                    body[i][columnCount++] = String.valueOf(totalDistance);
                                    body[i][columnCount++] = Util.convertDateUtcGmtPeruString(geoTramas.get(i).getGpsDate());
                                    body[i][columnCount++] = String.valueOf(geoTramas.get(i).getSpeed());
                                }
                            }
                        }
                        geoJsonDto.setBody(body);
                        String jsonTramas = new Gson().toJson(geoJsonDto);
                        com.backend.platin.util.Util.generateDirIfDoesntExist(geojsonPath);
                        Util.generateGeoJsonReportFile(uuid, geojsonPath, jsonTramas);
                        log.warn("geojson generado");
                        Instant finishTime = Instant.now();
                        Duration timeElapsed = Duration.between(start, finishTime);
                        redisService.changeStatusReport(keys, report, "", Util.COMPLETED_STATUS, geoUrlMap, timeElapsed.getSeconds(), report.getPlate());
                        ReportInDto reportObject = new ReportInDto(geoUrlMap, "", report.getTypeReport(), Util.COMPLETED_STATUS, report.getStartDate(),
                                report.getEndDate(), true, report.getCompany_id());
                        reportService.create(reportObject);
                    }
                } catch (DataNotFoundException ex) {
                    iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
                    return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
                } catch (Exception ex) {
                    iErrorRepository.save(new ErrorLogs("Error al exportar el Reporte: " + ex.getMessage(), "500"));
                    return new ResponseEntity(new Response("Error al exportar el Reporte: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
                }

                return ResponseEntity.ok(new Response("El reporte fue exportado con exito", HttpStatus.OK));

            }

        };
        return resultCall;
    }
//    @PostMapping("/request-report-apiv1")
//    public ResponseEntity<Response> showReportApi(@Valid @RequestBody ReportDto report,
//                                                  BindingResult result) throws ParseException, JsonProcessingException {
//        String uuid = UUID.randomUUID().toString();
//        String geoUrlMap = urlMap + uuid + ".geojson";
//        double totalDistance = 0;
//        Instant start = Instant.now();
//
////        ReportNsqDto reportNsq = new ReportNsqDto(report.getPlate(), report.getStartDate(), report.getEndDate(), report.getTypeReport(), report.getCompany_id());
//        Util.USER_ID = report.getUser_id();
//        try {
//            String keySetVehicle = "VehicleSetReport" + report.getPlate();
//            String vehicle = redisService.findGetTypeWorkersSet(keySetVehicle);
//            if (vehicle == null) {
//                IVehicleCompanyReportDto vehicles = vehicleService.vehiclefindByPlate(report.getPlate());
//                VehicleCompanyReportDto vehi = new VehicleCompanyReportDto(vehicles.getcode_osinergmin(), vehicles.getNameemv(), vehicles.getStatus(), vehicles.getNameclient());
//                vehicle = new Gson().toJson(vehi);
//                if (vehicle != null) {
//                    redisService.createSetTypeWorkers(keySetVehicle, vehicle);
//                }
//            }
//            if (vehicle == null) {
//                throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
//            }
//            VehicleCompanyReportDto vehicleCompany = null;
//            vehicleCompany = new Gson().fromJson(vehicle, VehicleCompanyReportDto.class);
//            String[] keys = redisService.createReportRequest(Util.REQUESTED_STATUS, report, " ", " ");
//            if (report.getTypeUrl().equals("xls")) {
//                XSSFWorkbook wb = new XSSFWorkbook();
//                XSSFSheet sheett = wb.createSheet("Tramas");
//                CellStyle stylee = wb.createCellStyle();
//                String vehicle_status = vehicleCompany.getStatus() ? "Habilitado" : "Inhabilitado";
//                //Change status of report
//                String xlsUrlMap = urlMap + uuid + ".xls";
//                redisService.changeStatusReport(keys, report, xlsUrlMap, Util.PROCESSING_STATUS, geoUrlMap, 0, report.getPlate());
//                if (report.getTypeReport().equals(Util.STANDARD_LAST_POS_REPORT)) {
//                    log.warn("tramas a procesar");
//                    List<Trama> tramas = tramaService.getTramasByPlateAndGpsDate(report.getPlate(),
//                            report.getStartDate(),
//                            report.getEndDate());
//                    totalDistance = this.getTotalDistance(tramas);
//                    if (tramas.isEmpty())
//                        throw new DataNotFoundException(Util.TRAMA_NOT_FOUND);
//
//                    String[] excelHeader = Util.generateGeneralHeadersExcel(report.getTypeReport());
//                    String[] excelBody = {"Fecha Gps", "Latitud", "Longitud", "Velocidad", "Fecha Ingesta"};
//
//                    PositionDto position = new Gson().fromJson(tramas.get(0).getPosition(), PositionDto.class);
//                    String[] locations = this.getLocationFromLatitudeAndLongitude(position);
//
//                    Object[] exceldataLastPos = {report.getPlate(), vehicleCompany.getNameemv(),
//                            vehicleCompany.getNameclient(), vehicleCompany.getcode_osinergmin(), vehicle_status,
//                            geoUrlMap, Util.convertDateUtcGmtPeruString(new Date()), Util.convertDateUtcGmtPeruString(report.getStartDate()),
//                            Util.convertDateUtcGmtPeruString(report.getEndDate()), String.valueOf(totalDistance), locations[0], locations[1], locations[2]};
//                    this.generateStandardLastPosReportExcel(uuid, excelHeader, exceldataLastPos, excelBody, xlsPath, tramas, sheett, wb, stylee);
//                    Instant finishTime = Instant.now();
//                    Duration timeElapsed = Duration.between(start, finishTime);
//                    redisService.changeStatusReport(keys, report, xlsUrlMap, Util.COMPLETED_STATUS, "", timeElapsed.getSeconds(), report.getPlate());
//                    ReportInDto reportObject = new ReportInDto("", xlsUrlMap, report.getTypeReport(), Util.COMPLETED_STATUS, report.getStartDate(),
//                            report.getEndDate(), true, report.getCompany_id());
//                    reportService.create(reportObject);
//                }
//            } else {
//                keys = redisService.createReportRequest(Util.REQUESTED_STATUS, report, " ", " ");
//                log.warn("generando geojson");
//                List<ITramaDto> geoTramas = new ArrayList<>();
//                List<Trama> geoTramas = tramaService.findByPlateGpsDateBetweenCompanyIdAsc(report.getPlate(), report.getStartDate(), report.getEndDate());
//                totalDistance = this.getTotalDistance(geoTramas);
//                //generate geojson file
//                GeoJsonDto geoJsonDto = new GeoJsonDto();
//                Util util = new Util();
//                String[][] body = null;
//
//                int rowaumentv1 = 0;
//                if (report.getTypeReport().equals(Util.STANDARD_LAST_POS_REPORT)) {
//                    geoTramas = tramaService.findByPlateGpsDateBetweenCompanyIdAsc(report.getPlate(), report.getStartDate(), report.getEndDate());
//                    totalDistance = this.getTotalDistance(geoTramas);
//                    MetaStandardLasPosDto meta = new MetaStandardLasPosDto();
//                    geoJsonDto.setMeta(meta);
//                    body = new String[geoTramas.size()][14];
//                }
//                int columnCount;
//                geoJsonDto.setHeader(util.generateGeneralHeadersGeoJson(report.getTypeReport()));
//                redisService.changeStatusReport(keys, report, "", Util.PROCESSING_STATUS, geoUrlMap, 0, report.getPlate());
//                if (!report.getTypeReport().equals(Util.GLOBAL_REPORT)) {
//                    for (int i = 0; i < geoTramas.size(); i++) {
//                        columnCount = 0;
//                        body[i][columnCount++] = report.getPlate();
//                        body[i][columnCount++] = vehicleCompany.getNameemv();
//                        body[i][columnCount++] = vehicleCompany.getNameclient();
//                        body[i][columnCount++] = vehicleCompany.getcode_osinergmin();
//                        body[i][columnCount++] = vehicleCompany.getStatus() ? Util.PARAM_ACTIVE : Util.PARAM_INACTIVE;
//                        body[i][columnCount++] = geoUrlMap;
//                        body[i][columnCount++] = Util.parseDateToPrinterFormatString(Util.convertDateUtcGmtPeru(new Date()));
//                        body[i][columnCount++] = Util.parseDateToPrinterFormatString(Util.convertDateUtcGmtPeru(report.getStartDate()));
//                        body[i][columnCount++] = Util.parseDateToPrinterFormatString(Util.convertDateUtcGmtPeru(report.getEndDate()));
//
//                        if (report.getTypeReport().equals(Util.STANDARD_LAST_POS_REPORT)) {
//                            PositionDto positiondto = new Gson().fromJson(geoTramas.get(i).getPosition(), PositionDto.class);
//                            body[i][columnCount++] = String.valueOf(positiondto.getLatitude());
//                            body[i][columnCount++] = String.valueOf(positiondto.getLongitude());
//                            body[i][columnCount++] = String.valueOf(totalDistance);
//                            body[i][columnCount++] = Util.convertDateUtcGmtPeruString(geoTramas.get(i).getGpsDate());
//                            body[i][columnCount++] = String.valueOf(geoTramas.get(i).getSpeed());
//                        }
//                    }
//                }
//                geoJsonDto.setBody(body);
//                String jsonTramas = new Gson().toJson(geoJsonDto);
//                com.backend.platin.util.Util.generateDirIfDoesntExist(geojsonPath);
//                Util.generateGeoJsonReportFile(uuid, geojsonPath, jsonTramas);
//                log.warn("geojson generado");
//                Instant finishTime = Instant.now();
//                Duration timeElapsed = Duration.between(start, finishTime);
//                redisService.changeStatusReport(keys, report, "", Util.COMPLETED_STATUS, geoUrlMap, timeElapsed.getSeconds(), report.getPlate());
//                ReportInDto reportObject = new ReportInDto(geoUrlMap, "", report.getTypeReport(), Util.COMPLETED_STATUS, report.getStartDate(),
//                        report.getEndDate(), true, report.getCompany_id());
//                reportService.create(reportObject);
//            }
//        } catch (DataNotFoundException ex) {
//            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
//            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
//        } catch (Exception ex) {
//            iErrorRepository.save(new ErrorLogs("Error al exportar el Reporte: " + ex.getMessage(), "500"));
//            return new ResponseEntity(new Response("Error al exportar el Reporte: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//
//        return ResponseEntity.ok(new Response("El reporte fue exportado con exito", HttpStatus.OK));
//
//    }

    private void generateStandardLastPosReportExcel(String uuid, String[] excelHeader, Object[] exceldata,
                                                    String[] excelHeaderR, String xlsPath, List<Trama> tramas, XSSFSheet sheet,
                                                    XSSFWorkbook wb, CellStyle style) throws FileNotFoundException, IOException, DataNotFoundException, ParseException {

        int rowCount = 0;
        for (rowCount = 0; rowCount < excelHeader.length; rowCount++) {
            Row row = sheet.createRow(rowCount + 1);
            XSSFFont headerFont = wb.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            style.setFont(headerFont);
            Cell cell = row.createCell(0);
            cell.setCellValue(excelHeader[rowCount]);
            cell.setCellStyle(style);
            sheet.autoSizeColumn(0);
            sheet.setColumnWidth(0, 500 * 20 + 1000);
            Cell cell2 = row.createCell(1);
            cell2.setCellValue(exceldata[rowCount].toString());
            cell2.setCellStyle(style);
            sheet.autoSizeColumn(1);
            sheet.setColumnWidth(1, 500 * 50 + 1000);
            sheet.autoSizeColumn(2);
            sheet.setColumnWidth(2, 500 * 20 + 1000);
            sheet.autoSizeColumn(3);
            sheet.setColumnWidth(3, 500 * 20 + 1000);
            sheet.autoSizeColumn(4);
            sheet.setColumnWidth(4, 500 * 20 + 1000);
            sheet.autoSizeColumn(5);
            sheet.setColumnWidth(5, 500 * 20 + 1000);
            sheet.autoSizeColumn(6);
            sheet.setColumnWidth(6, 500 * 20 + 1000);
            sheet.autoSizeColumn(7);
            sheet.setColumnWidth(7, 500 * 20 + 1000);
        }

        generateBodySTANDARD_LAST_POS_REPORT(sheet, rowCount, excelHeaderR, tramas, style, wb);

        Util.generateDirIfDoesntExist(xlsPath);
        Util.generateExcelReportFile(wb, uuid, xlsPath);

    }

    private void generateBodySTANDARD_LAST_POS_REPORT(XSSFSheet sheet, int rowCount,
                                                      String[] excelHeaderR, List<Trama> tramas, CellStyle style, XSSFWorkbook wb) throws ParseException {

        rowCount = rowCount + 2;
        Row rowe = sheet.createRow(rowCount++);
//        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelHeaderR, style);

        for (Trama trama : tramas) {
            rowe = sheet.createRow(rowCount);

            PositionDto position = new Gson().fromJson(trama.getPosition(), PositionDto.class);
            Util.generateBodyType(wb, rowe, Util.convertDateUtcGmtPeruString(trama.getGpsDate()),
                    String.valueOf(position.getLatitude()), String.valueOf(position.getLongitude()), String.valueOf(trama.getSpeed()), Util.convertDateUtcGmtPeruString(trama.getReceiveDate()), null, null, null, null, null, Util.STANDARD_LAST_POS_REPORT);
            rowCount++;
        }
    }

    public String[] getLocationFromLatitudeAndLongitude(PositionDto position) {
        String[] location = new String[3];
        try {

            if (position != null && position.getLatitude() != 0 && position.getLongitude() != 0) {

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                String uriWithParameters = this.nomatin_uri + position.getLatitude() + "&lon=" + position.getLongitude() + "&format=jsonv2&addressdetails=1&extratags=1";
                RestTemplate restTemplate = restTemplate();
                ResponseEntity<String> result = restTemplate.getForEntity(uriWithParameters, String.class);

                if (result != null && result.getBody() != null) {
                    System.out.println("Cuerpo:" + result.getBody());
                    String jsonAddressString = result.getBody();

                    if (jsonAddressString != null) {

                        JsonObject jsonResult = new JsonParser().parse(jsonAddressString).getAsJsonObject();
                        if (jsonResult != null) {

                            JsonObject jsonAddress = jsonResult.get("address").getAsJsonObject();
                            if (jsonAddress != null && jsonAddress.get("amenity") != null) {
                                location[0] = jsonAddress.get("amenity").getAsString() + ", " + jsonAddress.get("road").getAsString();
                            } else if (jsonAddress != null && jsonAddress.get("road") != null) {
                                location[0] = " ";
                            }

                            if (jsonAddress != null && jsonAddress.get("city") != null) {
                                location[1] = jsonAddress.get("city").getAsString();
                            } else {
                                location[1] = " ";
                            }

                            if (jsonAddress != null && jsonAddress.get("state") != null) {
                                location[2] = jsonAddress.get("state").getAsString();
                            } else {
                                location[2] = " ";
                            }

                        } else {
                            for (int i = 0; i < location.length; i++) {
                                location[i] = " ";
                            }
                        }
                    } else {
                        for (int i = 0; i < location.length; i++) {
                            location[i] = " ";
                        }
                    }
                } else {
                    for (int i = 0; i < location.length; i++) {
                        location[i] = " ";
                    }
                }
            }
            return location;
        } catch (Exception ex) {
            location[0] = " ";
            location[1] = " ";
            location[2] = " ";
            return location;
        }
    }
    private double getTotalDistanceItrama(List<Trama> tramas) {
        int j = 1;
        double totalDistance = 0;
        if (tramas.size() > 0) {
            for (int i = 0; i < tramas.size(); i++) {
                String stPositionString = tramas.get(i).getPosition();
                PositionDto stPosition = new Gson().fromJson(stPositionString, PositionDto.class);
                String enPositionString = tramas.get(j).getPosition();
                PositionDto enPosition = new Gson().fromJson(enPositionString, PositionDto.class);
                if (j < tramas.size() - 1) {
                    j++;
                }
                double distance = Util.calculateDistance(stPosition.getLatitude(), stPosition.getLongitude(), enPosition.getLatitude(), enPosition.getLongitude());
                totalDistance = totalDistance + distance;
            }
        }

        return totalDistance;
    }
    private double getTotalDistance(List<Trama> tramas) {
        int j = 1;
        double totalDistance = 0;
        if (tramas.size() > 0) {
            for (int i = 0; i < tramas.size(); i++) {
                String stPositionString = tramas.get(i).getPosition();
                PositionDto stPosition = new Gson().fromJson(stPositionString, PositionDto.class);
                String enPositionString = tramas.get(j).getPosition();
                PositionDto enPosition = new Gson().fromJson(enPositionString, PositionDto.class);
                if (j < tramas.size() - 1) {
                    j++;
                }
                double distance = Util.calculateDistance(stPosition.getLatitude(), stPosition.getLongitude(), enPosition.getLatitude(), enPosition.getLongitude());
                totalDistance = totalDistance + distance;
            }
        }

        return totalDistance;
    }

    @PostMapping("/reportResponse/{tokenInternalRest}")
    public ResponseEntity<?> sendCoords(@PathVariable String tokenInternalRest, @Valid @RequestBody ReportResponseDto reportResponse, BindingResult result) {
        try {
            Util.isEmptyField(result);
            if (tokenInternal.equals(tokenInternalRest)) {
                template.convertAndSend("/topic/reportResponse", reportResponse);

            } else {
                log.info(new Response("El token es incorrecto", "401"));
                return new ResponseEntity(new Response("El token es incorrecto", "4001"), HttpStatus.INTERNAL_SERVER_ERROR);

            }
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs("Error al enviar coordenadas: " + ex.getMessage(), "400"));
            return new ResponseEntity(new Response("Error al enviar coordenadas: " + ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al enviar coordenadas: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al enviar coordenadas: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("Enviado con exito!", HttpStatus.OK));

    }

    @GetMapping("/reports")
    public ResponseEntity<Response> list() {

        List<Report> listReport = null;
        try {
            listReport = reportService.findByStatus();
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            iErrorRepository.save(new ErrorLogs("Error en la consulta de report: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error en la consulta de report:" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta de reportes fue realizado con exito", HttpStatus.OK, listReport));
    }

    @GetMapping("/report/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        Report report = null;

        try {
            report = reportService.findById(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar reportes: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar reportes: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("Reporte Consultado con exito", HttpStatus.OK, report));
    }

    @PostMapping("/report")
    public ResponseEntity<Response> create(@Valid @RequestBody ReportInDto reportDto, BindingResult result) {

        Report report = null;

        try {
            Util.isEmptyField(result);
            String ingestToken = UUID.randomUUID().toString().toUpperCase();

            report = reportService.create(reportDto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al registrar un reporte: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al registrar un reporte: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("El reporte fue registrado con exito!", HttpStatus.CREATED, report));
    }

    @PutMapping("/report/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody ReportInDto reportDto, BindingResult result,
                                           @PathVariable String id) {

        Report report = null;

        try {
            Util.isEmptyField(result);
            report = reportService.update(id, reportDto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al Actualizar un reporte: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al Actualizar un reporte: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("El reporte fue actualizado con exito!", HttpStatus.OK, report));
    }

    @DeleteMapping("/report/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {

        try {
            reportService.delete(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar un reporte: " + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response("Error al eliminar un reporte", HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("El reporte fue eliminado con exito!", HttpStatus.OK));
    }

    @PostMapping("/reportsPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        List<Map<String, Object>> listReportPage = null;
        Map<Object, Object> map = new HashMap<Object, Object>();
        try {
            long total = reportService.countByStatus(params.getBusca());
            map = Util.totalPagesElemnt(total, params.getSize());
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listReportPage = reportService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)), params.getBusca());
            } else {
                listReportPage = reportService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())), params.getBusca());
                if (!params.getOrderAsc()) {
                    listReportPage = reportService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()), params.getBusca());
                }

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar Reportes: " + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response("Error al consultar Reportes", HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("La consulta de reportes fue realizado con exito!", HttpStatus.OK, listReportPage, map));

    }

    @PostMapping("/last_pos")
    public ResponseEntity<Response> showLastPosReportPagination(@RequestBody Parameters params
    ) throws ParseException, JsonProcessingException, DataNotFoundException, GeneralException {
        List<LastPositionVehicleDto> lastPositionVehicles = null;
        try {
            List<String> plates = redisService.getListVehiclePopulation();
            switch (params.getType()) {
                case Util.LAST_POSITION_NOW:
                    lastPositionVehicles = vehicleService.showLastPositionVehicles(plates);
                    break;
                case Util.LAST_FIVE_MINUTES_OF_POSITION:
                    lastPositionVehicles = vehicleService.lastPositionVehiclesOfFiveMinutesAgo(plates);
                    break;
                case Util.LAST_TEN_MINUTES_OF_POSITION:
                    lastPositionVehicles = vehicleService.lastPositionVehiclesOfTenMinutesAgo(plates);
                    break;
                default:
                    lastPositionVehicles = vehicleService.lastPositionVehiclesMoreOfTenMinutesAgo(plates);
                    break;
            }
            Collections.sort(lastPositionVehicles, new VehiclesSortByDate());

        } catch (GeneralException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);

        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al localizar la ultima posición: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al localizar la ultima posición: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("La consulta de reportes fue realizado con exito!", HttpStatus.OK, lastPositionVehicles));

    }

    public static <T> Page<T> listConvertToPage1(List<T> list, Pageable pageable) {
        int start = (int) pageable.getOffset();
        int end = (start + pageable.getPageSize()) > list.size() ? list.size() : (start + pageable.getPageSize());
        return new PageImpl<T>(list.subList(start, end), pageable, list.size());
    }

    @GetMapping("/listReports/{user_id}")
    public ResponseEntity<Response> listOfReports(@PathVariable String user_id) {

        List<ReportStatusDto> reportStatusDto = null;
        try {

            reportStatusDto = redisService.getListOfReports(user_id);

        } catch (Exception e) {
            e.printStackTrace();
            iErrorRepository.save(new ErrorLogs("Error en la consulta de report: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error en la consulta de report:" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta de reportes fue realizado con exito", HttpStatus.OK, reportStatusDto));
    }
}
