package com.backend.ingest.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.ingest.dtos.RolUpdDto;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.dtos.RolInsDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Permits;
import com.backend.ingest.entities.Rol;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.IPermitsService;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.ingest.service.interfaces.IRolService;
import com.backend.ingest.service.services.PermitsService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.helper.Response;
import com.backend.platin.util.Util;
import java.util.HashSet;
import java.util.Set;

@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("/api/v1/")
public class RolController {

    @Autowired
    private IRolService RolService;

    @Autowired
    private PermitsService permitsService;

    @Autowired
    private IErrorLogsRepository iErrorRepository;

    @GetMapping("/rol/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {
        Rol Rol = null;

        try {
            Rol = RolService.findById(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar el rol: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar el rol:: " + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return ResponseEntity.ok(new Response("La consulta del rol fue realizada con exito!", HttpStatus.OK, Rol));
    }

    @GetMapping("/roles")
    public ResponseEntity<Response> list() {
        List<Rol> listRol = null;
        try {
            listRol = RolService.findByStatus();
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los roles: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar los roles:" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("La consulta de los roles fue realizada con exito!", HttpStatus.OK, listRol));

    }

    @PostMapping("/rolesPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params
    ) {
        Page<Rol> listRolPage = null;
        try {
            if (params.getNameOrder() == null || params.getOrderAsc() == null
                    || params.getPage() == null || params.getSize() == null) {
                listRolPage = RolService.findAllPage(
                        PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)),params.getBusca());
                return ResponseEntity.ok(new Response("La consulta de los roles fue realizada con exito!", HttpStatus.OK, listRolPage));
            } else {
                listRolPage = RolService.findAllPage(
                        PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())),params.getBusca());
                if (!params.getOrderAsc()) {
                    listRolPage = RolService.findAllPage(
                            PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder()).descending()),params.getBusca());
                }
                return ResponseEntity.ok(new Response("La consulta de los roles fue realizada con exito!", HttpStatus.OK, listRolPage));

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los roles" + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response("Error al consultar los roles" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/rol")
    public ResponseEntity<Response> create(@Valid @RequestBody RolInsDto rolInstDto, BindingResult result) {

        Rol RolNew = null;

        try {
            Util.isEmptyField(result);
            RolNew = new Rol(rolInstDto.getName());
            Set<Permits> permits = new HashSet<Permits>();
            for (String s : rolInstDto.getPermits()) {
                permits.add(permitsService.getByRolName(s).get());
            }
            RolNew.setPermits(permits);
            RolNew = RolService.create(RolNew);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al insertar el rol: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al insertar el rol: " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("El rol fue resgistrado con exito!", HttpStatus.CREATED, RolNew));
    }

    @PutMapping("/rol/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody RolUpdDto rolUpdDto, BindingResult result,
            @PathVariable String id) {
        Rol rolupdated = null;
        try {
            List<Rol> rollist = RolService.findNameDiferent(id);
            for (Rol rol : rollist) {
                if (rolUpdDto.getName().equals(rol.getName())) {
                    iErrorRepository.save(new ErrorLogs("El nombre del rol ya existe", "400"));
                    return new ResponseEntity(new Response("El nombre del rol ya existe", "400"), HttpStatus.BAD_REQUEST);
                }
            }

            Util.isEmptyField(result);
            rolupdated = RolService.update(id, rolUpdDto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Erro al actualizar el rol : " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Erro al actualizar el rol: " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("El rol fue actualizado con exito!", HttpStatus.OK, rolupdated));
    }

//    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/rol/{id}")
    public ResponseEntity<Response> delete(@PathVariable String id) {

        try {
            RolService.delete(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar el rol :" + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response("Error al eliminar el rol: " + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("El rol fue eliminado con exito!", HttpStatus.OK));

    }

}
