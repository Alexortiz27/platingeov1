/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.controllers;

import com.backend.ingest.dtos.SessionDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Session;
import com.backend.ingest.entities.User;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.ISessionService;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.platin.util.Util;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("api/v1/")
public class SessionController {

    @Autowired
    private ISessionService sessionService;
    @Autowired
    private IErrorLogsRepository iErrorRepository;
    @Autowired
    private IUserService userService;

    @GetMapping("/sessions")
    public ResponseEntity<Response> list() {

        List<Session> listSession = null;
        try {
            listSession = sessionService.findAll();
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar las sesiones:" + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar los sesiones:" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta de las sesiones fue realizada con exito!", HttpStatus.OK, listSession));
    }

    @GetMapping("/session/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        Session session = null;

        try {
            session = sessionService.findById(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar la sesion: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar la sesion: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La sesion fue consultada con exito", HttpStatus.OK, session));
    }
//

    @PostMapping("/session")
    public ResponseEntity<Response> create(@RequestBody SessionDto dtosesion) {

        Session session = null;

        try {

            session = sessionService.create(dtosesion);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al insertar la sesion : " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al insertar la sesion: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("La sesion fue creada con exito!", HttpStatus.CREATED, sessionService));
    }
//
//

    @PutMapping("/session")
    public ResponseEntity<Response> update(@RequestParam("id") String id, @RequestBody SessionDto sesi) {

        Session session = null;

        try {

            session = sessionService.update(id, sesi);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Erro al actualizar la sesion: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Erro al actualizar la sesion: " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("La sesion fue actualizada con exito!", HttpStatus.OK, session));
    }

    @PostMapping("/sessionsPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        Page<Session> listMarkerPage = null;
        try {
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listMarkerPage = sessionService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.USER_COMPLETE_NAMES_ORDER)),params.getBusca());
                return ResponseEntity.ok(new Response("La sesion fue consultada con exito", HttpStatus.OK, listMarkerPage));
            } else {
                listMarkerPage = sessionService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())),params.getBusca());
                if (!params.getOrderAsc()) {
                    listMarkerPage = sessionService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()),params.getBusca());
                }
                return ResponseEntity.ok(new Response("La sesion fue consultada con exito", HttpStatus.OK, listMarkerPage));

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar las sesiones: " + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response("Error al consultar las sesiones:" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
