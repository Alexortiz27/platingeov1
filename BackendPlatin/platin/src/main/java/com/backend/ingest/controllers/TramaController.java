/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.controllers;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Trama;
import com.backend.ingest.producer.Service.MqProductService;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.ICompanyService;
import com.backend.ingest.service.interfaces.ITramaService;
import com.backend.ingest.service.services.VehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.nsqworker.consumer.service.impl.MqConsumerByChannelServiceImplCalculateGeocerca;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.exceptions.GeneralException;
import com.backend.platin.helper.Response;
import com.backend.platin.util.GsonUTCDateAdapter;
import com.backend.platin.util.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.lettuce.core.RedisConnectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@CrossOrigin(origins = {"http://localhost", "http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("api/v1/")

public class TramaController {

    @Autowired
    private ITramaService tramaService;

    @Autowired
    VehicleService vehicleService;

    @Autowired
    private EventRedisHsetService eventRedis;

    @Autowired
    private IErrorLogsRepository iErrorRepository;

    @Autowired(required = true)
    MqProductService mqProductService;

    private final Logger log = LogManager.getLogger(MqConsumerByChannelServiceImplCalculateGeocerca.class);

    @Value("${token.Internal}")
    String tokenInternal;

    @Autowired
    private SimpMessagingTemplate template;

    @Value("${rest.uri.nominatin}")
    private String nomatin_uri;

    Pattern pattern;

    @Autowired
    private ICompanyService companyService;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    private EventRedisHsetService redisService;

    @GetMapping("/tramas")
    public ResponseEntity<Response> list() {
        Map<String, Object> response = new HashMap<>();
        List<Trama> listTrama = null;
        try {
            listTrama = tramaService.findByStatus();
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Failed to query getTramas: " + e.getMessage(), "500"));

            return new ResponseEntity(new Response("Failed to query getTramas" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("successfully getTrama", HttpStatus.OK, listTrama));

    }

    private Map<Object, Object> getAddresFromLatitudeLongitude(CoordInDto position) throws Exception {
        Map<Object, Object> addres = new HashMap<Object, Object>();
        try {
            if (position != null && position.getLatitude() != 0 && position.getLongitude() != 0) {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                String uriWithParameters = this.nomatin_uri + position.getLatitude() + "&lon=" + position.getLongitude() + "&format=jsonv2&addressdetails=1&extratags=1";
                RestTemplate restTemplate = restTemplate();
                ResponseEntity<String> result = restTemplate.getForEntity(uriWithParameters, String.class);

                if (result != null && result.getBody() != null) {
                    log.info("Cuerpo:" + result.getBody());
                    String jsonAddressString = result.getBody();
                    JsonObject jsonResult = new JsonParser().parse(jsonAddressString).getAsJsonObject();
                    if (jsonResult != null && jsonResult.has("address") && !jsonResult.get("address").isJsonNull()) {
                        JsonObject jsonAddress = jsonResult.get("address").getAsJsonObject();

                        String[] types = new String[]{"name", "display_name", "house_number", "road", "city", "region", "postcode", "country",
                                "country_code", "state", "town"};
                        for (String type : types) {
                            switch (type) {
                                case "name":
                                    Util.agregar(jsonResult, addres, type);
                                    break;
                                case "display_name":
                                    Util.agregar(jsonResult, addres, type);
                                    break;
                                default:
                                    Util.agregar(jsonAddress, addres, type);
                            }
                        }
                        return addres;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }

            } else {
                return null;
            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
            return null;

        }
    }

    @PostMapping("/coord-nominatin")
    public ResponseEntity<Response> coordNominatin(@Valid @RequestBody CoordInDto coor, BindingResult result) {
        Map<Object, Object> listAdrees = null;

        try {
            Util.isEmptyField(result);
            listAdrees = getAddresFromLatitudeLongitude(coor);
            if (listAdrees.isEmpty()) {
                iErrorRepository.save(new ErrorLogs("Las coordenadas no tiene direccion", "400"));
                return new ResponseEntity(new Response("Las coordenadas no tiene direccion", "400"), HttpStatus.BAD_REQUEST);

            }

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {

            iErrorRepository.save(new ErrorLogs("Error al consultar coord-nominatin: " + ex.getMessage(), "500"));
            ex.printStackTrace();
            return new ResponseEntity(new Response("Error al consultar coord-nominatin:" + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return new ResponseEntity(
                new Response("La direccion de las coordenadas fue enviado con exito!", HttpStatus.OK, listAdrees), HttpStatus.OK);
    }

    @GetMapping("/trama/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        Trama trama = null;

        try {
            trama = tramaService.findById(id);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Failed to query getIdTrama: " + e.getMessage(), "500"));

            return new ResponseEntity(new Response("Failed to query getIdTrama: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        if (trama == null) {
            iErrorRepository.save(new ErrorLogs("The Trama ID: ".concat(id.toString().concat(" does not exist in the database!")), "404"));

            return new ResponseEntity(new Response("The Trama ID: ".concat(id.toString().concat(" does not exist in the database!")),
                    HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(new Response("successfully getIdTrama", HttpStatus.OK, trama));
    }

    @PostMapping("/trama")
    public ResponseEntity<Object> createTrama(@Valid @RequestBody TramaInDto trama, BindingResult result) {
        List<Trama> listTrama = null;
        Trama tramaNew = null;
        Map<Object, Object> map = new HashMap<Object, Object>();
        Map<Object, Object> mapMeta = new HashMap<Object, Object>();
        try {
            Util.isEmptyField(result);
            if (!validateTrama(trama).isEmpty()) {
                List<ResponseTramaDTO> responseTramaDTO = validateTrama(trama);
                for (ResponseTramaDTO resp : responseTramaDTO) {
                    eventRedis.createHsetGlobal("platin:error:logs", resp.getId(), new Gson().toJson(resp));
                    iErrorRepository.save(new ErrorLogs(resp.getSuggestion(), "400"));
                }
                mapMeta.put("failure",responseTramaDTO.size());
                mapMeta.put("total",responseTramaDTO.size());
                map.put("data", responseTramaDTO);
                map.put("metadata",mapMeta);
                return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
            }
            trama.setReceiveDate(Util.convertDateUtcGmtPeru(new Date()));
            TramaInsDto tramaDto = new TramaInsDto(trama.getEvent(), trama.getPlate(),
                    trama.getPosition(), Util.parseStringToDatetime(trama.getGpsDate()), Double.parseDouble(trama.getSpeed().toString()), trama.getReceiveDate(),
                    trama.getTokenTrama(), trama.getOdometer());

            tramaDto.setRu_gps_date(String.valueOf(tramaDto.getGpsDate().getTime()));

            tramaNew = new Trama(trama.getEvent(), trama.getPlate(),
                    trama.getPosition(), Util.parseStringToDatetime(trama.getGpsDate()), Double.parseDouble(trama.getSpeed().toString()), trama.getReceiveDate(),
                    trama.getTokenTrama(), trama.getOdometer());

//            String tramaRedis ="{\"position\":{\"latitude\":"+trama.getPosition().getLatitude()+","+"\"longitude\":"+trama.getPosition().getLongitude()+","+"\"altitude\":"+trama.getPosition().getAltitude()+"},\"plate\":\""+trama.getPlate()+"\",\"speed\":"+trama.getSpeed()+",\"gpsDate\":\""+gpsredis+"\",\"event\":\""+trama.getEvent()+"\",\"tokenTrama\":\""+trama.getTokenTrama()+"\",\"odometer\":"+trama.getOdometer()+",\"receiveDate\":\""+reciredis+"\",\"creationDate\":\""+creatredis+"\",\"ru_gps_date\":\""+updateTrama.getRu_gps_date()+"\"}";
            String ingestaJson = new Gson().toJson(tramaDto);
            eventRedis.create(tramaNew, ingestaJson);
            mqProductService.sendTestMessageByTopicIngestTrama(ingestaJson);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (RedisConnectionException ex) {
            iErrorRepository.save(new ErrorLogs("Failed RedisConnectionException : " + ex.getMessage(), "500"));
            ex.printStackTrace();
            return new ResponseEntity(new Response("Failed to insert data trama: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Failed to query insert data trama : " + ex.getMessage(), "500"));
            ex.printStackTrace();
            return new ResponseEntity(new Response("Failed to insert data trama: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(
                new Response("The trama has been created successfully!", HttpStatus.OK, listTrama), HttpStatus.OK);
    }

    @PutMapping("/trama/{id}")
    public ResponseEntity<Response> update(@Valid
                                           @RequestBody TramaInsDto tramaDto, BindingResult result,
                                           @PathVariable String id) {

        Trama trama = null;

        try {
            Util.isEmptyField(result);
            trama = tramaService.update(id, tramaDto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Failed to query Update data trama : " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Failed to update data: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("The Trama has been updated successfully!", HttpStatus.OK, trama));
    }

    @DeleteMapping("/trama/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {

        try {
            tramaService.delete(id);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Failed to query delete trama : " + e.getMessage(), "500"));

            return new ResponseEntity(new Response("Failed to delete Trama", HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("The Trama successfully removed!", HttpStatus.OK));
    }

    @PostMapping("/coords/{tokenInternalRest}")
    public ResponseEntity<?> sendCoords(@PathVariable String tokenInternalRest, @Valid @RequestBody TrackingTramaDto trama, BindingResult result) {
        if (tokenInternal.equals(tokenInternalRest)) {
            template.convertAndSend("/topic/coords", trama);

        } else {
            log.info(new Response("Failed token sendCoords", "500"));

            return new ResponseEntity(new Response("Failed token sendCoords", HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("sendCoords successfully!", HttpStatus.OK));
    }

    @GetMapping("/trama/request-plate")
    public ResponseEntity<?> getTramaByPlate(@RequestParam("plate") String plate) {

        TramaRedisDto trama = null;
        try {

            if (plate != null && !plate.equals("")) {
                trama = redisService.findLastTramav2(plate);
            } else {
                iErrorRepository.save(new ErrorLogs("Datos Vacios getTramaByPlate", "400"));
                return new ResponseEntity(new Response("Datos Vacios", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

            }
        } catch (Exception ex) {
            return new ResponseEntity(new Response("Failed to getTramaByPlate", HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("successfully getTramaByPlate", HttpStatus.OK, trama));
    }

    @GetMapping("/error-logger-ingest")
    public ResponseEntity<Response> getListErrorLogger() {
        List<String> listError = null;

        try {
            listError = redisService.getListErrorLogger();
            if (listError.isEmpty()) {
                iErrorRepository.save(new ErrorLogs("La consulta no fue realizado", "400"));
                return new ResponseEntity(new Response("La consulta no fue realizado", "400"), HttpStatus.BAD_REQUEST);

            }

        } catch (Exception ex) {

            iErrorRepository.save(new ErrorLogs("Error al consultar la lista de errores de la ingesta: " + ex.getMessage(), "500"));
            ex.printStackTrace();
            return new ResponseEntity(new Response("Error al consultar la lista de errores de la ingesta: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return new ResponseEntity(
                new Response("La listas de errores de la ingesta fue enviado con exito!", HttpStatus.OK, listError), HttpStatus.OK);
    }

    @GetMapping("/delete-error-logger-ingest/{id}")
    public ResponseEntity<Response> deleteErrorLogger(@PathVariable String id) {
        try {
            //error
            redisService.DeleteHsetGloval("platin:error:logs", id);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar un error de la ingesta: " + ex.getMessage(), "500"));
            ex.printStackTrace();
            return new ResponseEntity(new Response("Error al eliminar un error de la ingesta: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(
                new Response("El error " + id + " de la ingesta fue eliminada con exito!", HttpStatus.OK), HttpStatus.OK);
    }

    private List<ResponseTramaDTO> validateTrama(TramaInDto trama) throws GeneralException, DataNotFoundException, ParseException, UnknownHostException {

        TramaShowDTO tramaShow = new TramaShowDTO(trama.getEvent(), trama.getPlate(), trama.getSpeed(), trama.getPosition(), trama.getGpsDate(), trama.getOdometer(), trama.getTokenTrama());

        ResponseTramaDTO[] ArrResponse = {checkPlateField(tramaShow), checkGpsDateFeature(tramaShow),checkEventField(tramaShow),checkPositionField(tramaShow), checkGpsDateField(tramaShow), checkTokenTramaField(tramaShow), checkSpeddField(tramaShow)};
        List<ResponseTramaDTO> listResponse = new ArrayList<>();

        for (ResponseTramaDTO responseTramaDTO : ArrResponse) {

            if (responseTramaDTO != null) {
                listResponse.add(responseTramaDTO);
            }
        }
        return listResponse;
    }


    private ResponseTramaDTO checkGpsDateFeature(TramaShowDTO trama) throws GeneralException, ParseException, UnknownHostException {
        Date gps = Util.parseStringToDatetime(trama.getGpsDate());
        Date gpsActual = new Date();
        long trest = gpsActual.getTime();
        long temp = gps.getTime();
        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
        Date now = new Date();

        if (temp > trest) {

            return new ResponseTramaDTO("no aceptamos tramas a futuro hemos recibido " + gps + " pero el formato establecido debe ser: YYYY-MM-DDTHH:mm:SS.sssZ," +
                    " un ejemplo es :" + gson.toJson(now) + "y debe ingresarse en UTC", HttpStatus.BAD_REQUEST, trama);
        }
        return null;
    }

    private ResponseTramaDTO checkEventField(TramaShowDTO trama) throws GeneralException, UnknownHostException {

        boolean isDifferent = false;
        String[] permitted_events = {"none", "acc_on", "acc_off", "battery_ct", "battery_dc", "sos"};

        for (String permitted_event : permitted_events) {
            if (permitted_event.equals(trama.getEvent())) {
                isDifferent = true;
                break;
            }
        }
        if (!isDifferent) {

            return new ResponseTramaDTO("Evento no válido " + trama.getEvent() + ", enviar uno de estos eventos validos:" + Arrays.toString(permitted_events), HttpStatus.BAD_REQUEST, trama);

        }
        return null;

    }

    private ResponseTramaDTO checkPlateField(TramaShowDTO trama) throws DataNotFoundException, UnknownHostException {

        if (!vehicleService.existPlate(trama.getPlate())) {

            return new ResponseTramaDTO(Util.VEHICLE_NOT_FOUND + Util.DATA_NO_FOUND, HttpStatus.BAD_REQUEST, trama);

        }
        return null;
    }


    private ResponseTramaDTO checkPositionField(TramaShowDTO trama) throws UnknownHostException {
        String[] positions = new String[3];

        positions[0] = String.valueOf(trama.getPosition().getLatitude());
        positions[1] = String.valueOf(trama.getPosition().getLongitude());
        positions[2] = String.valueOf(trama.getPosition().getAltitude());


        for (int i = 0; i < positions.length; i++) {
            String regex_decimal = "^-?[0-9]{1,3}(.[0-9]{1,15})?$";

            pattern = Pattern.compile(regex_decimal);
            Matcher matcher = pattern.matcher(positions[i]);

            if (!matcher.matches()) {

                return new ResponseTramaDTO("El campo " + positions[i] + " no es válido,  deberia enviar como maximo 3 digitos enteros y 15 decimales, ejemplo: -10.087457443458652", HttpStatus.BAD_REQUEST, trama);

            }

        }
        return null;
    }

    private ResponseTramaDTO checkGpsDateField(TramaShowDTO trama) throws GeneralException, UnknownHostException {
//        String regex_date = "/[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z/";
        String regex_date = "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z";
        pattern = Pattern.compile(regex_date);
        Matcher matcher = pattern.matcher(trama.getGpsDate());

        if (!matcher.matches()) {

            return new ResponseTramaDTO("El formato del campo gpsDate no es valido, hemos recibido " + trama.getGpsDate() + ", pero el formato establecido debe ser: YYYY-MM-DDTHH:mm:SS.sssZ", HttpStatus.BAD_REQUEST, trama);

        }
        return null;
    }

    private ResponseTramaDTO checkTokenTramaField(TramaShowDTO trama) throws UnknownHostException {

        if (!companyService.existsCompanyByTokenTramaAndStatus(trama.getTokenTrama())) {

            return new ResponseTramaDTO("El token " + Util.DATA_NO_FOUND, HttpStatus.BAD_REQUEST, trama);

        }
        return null;
    }

    private ResponseTramaDTO checkSpeddField(TramaShowDTO trama) throws UnknownHostException {
        if (trama.getSpeed() == null || trama.getSpeed().toString().isEmpty()) {
            return new ResponseTramaDTO("El campo speed no debe estar vacio", HttpStatus.BAD_REQUEST, trama);
        }
        if (trama.getSpeed().getClass() == String.class) {
            return new ResponseTramaDTO("El campo speed " + String.valueOf(trama.getSpeed()) + " no debe ser un String", HttpStatus.BAD_REQUEST, trama);
        }
        return null;

    }


}
