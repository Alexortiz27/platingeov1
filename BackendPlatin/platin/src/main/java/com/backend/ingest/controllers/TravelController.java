package com.backend.ingest.controllers;

import com.backend.ingest.dtos.TravelInsDto;
import com.backend.ingest.dtos.TravelShowDto;
import com.backend.ingest.dtos.TravelUpdDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Travel;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.ingest.service.interfaces.IGeozoneService;
import com.backend.ingest.service.interfaces.ITravelService;
import com.backend.ingest.service.interfaces.IVehicleService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("api/v1/")
public class TravelController {

    @Autowired
    private ITravelService travelService;

    @Autowired
    private IGeozoneService geozonaService;

    @Autowired
    private IVehicleService VehicleService;

    @Autowired
    private IErrorLogsRepository iErrorRepository;

    @GetMapping("/travels")
    public ResponseEntity<Response> list() {

        List<TravelShowDto> listEvent = null;
        try {
            listEvent = travelService.findByStatus();
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los viajes: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar los viajes:" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("Los Viajes fue consultado con exito", HttpStatus.OK, listEvent));
    }

    @GetMapping("/travel/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        TravelShowDto Travel = null;
        List<Travel> listEvent = null;

        try {
            Travel = travelService.getId(id);
//                        listEvent = travelService.findAllWithFieldsContaining("APRUEBA123456",Travel.getStartDate());
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar el viaje: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar el viaje: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("El Viaje fue consultado con exito", HttpStatus.OK, Travel));
    }

    @PostMapping("/travel")
    public ResponseEntity<Response> create(@Valid @RequestBody TravelInsDto travelInsDto, BindingResult result) {

        Travel TravelNew = null;
        TravelShowDto travel = null;
        try {
            Util.isEmptyField(result);

            TravelNew = new Travel(travelInsDto.getName(), travelInsDto.getStartDate(), null, travelInsDto.getStatusTravel(), travelInsDto.getSchudeledTime());
            Vehicle vehicleid = VehicleService.findById(travelInsDto.getVehicle_id());
            TravelNew.setVehicleTravel(vehicleid);
            Geozone geozonaTravel = geozonaService.findById(travelInsDto.getGeozona_id());
            TravelNew.setGeozonaTravel(geozonaTravel);
            travel = travelService.create(TravelNew);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(),"400"), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al registrar el Viaje : " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al registrar el Viaje : " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("El viaje fue registrado con exito!", HttpStatus.OK, travel));
    }

    @PutMapping("/travel/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody TravelUpdDto travelInDto, BindingResult result,
            @PathVariable String id) {
        Travel vehiclelist = travelService.findNameDiferent(id,travelInDto.getName());
        if(vehiclelist!=null){
            iErrorRepository.save(new ErrorLogs("El nombre del Viaje ya Existe", "400"));
            return new ResponseEntity(new Response("El nombre del Viaje ya Existe", "400"), HttpStatus.BAD_REQUEST);
        }
        TravelShowDto Travel = null;
        try {
            Util.isEmptyField(result);
            Travel = travelService.update(id, travelInDto);

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al actualizar el Viaje : " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al actualizar el Viaje :" + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity
                .ok(new Response("El viaje fue actualizado con exito!", HttpStatus.OK, Travel));
    }

    @DeleteMapping("/travel/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {

        try {
            travelService.delete(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar el Viaje : " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al eliminar el Viaje: " + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("El Viaje fue eliminado con exito!", HttpStatus.OK));
    }

    @PostMapping("/travelsPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        List<Map<String, Object>> listTravelPage = null;
        Map<Object, Object> map = new HashMap<Object, Object>();
        try {
            long total = travelService.countByStatus(params.getBusca());
            map = Util.totalPagesElemnt(total, params.getSize());
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listTravelPage = travelService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)),params.getBusca());
                return ResponseEntity.ok(new Response("Los Viajes fue consultado con exito!", HttpStatus.OK, listTravelPage,map));
            } else {
                listTravelPage = travelService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())),params.getBusca());
                if (!params.getOrderAsc()) {
                    listTravelPage = travelService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()),params.getBusca());
                }
                return ResponseEntity.ok(new Response("Los Viajes fue consultado con exito!", HttpStatus.OK, listTravelPage,map));

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los Viajes: " + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response("Error al consultar los Viajes:" + e.getMessage(),"500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
