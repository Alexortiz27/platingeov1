package com.backend.ingest.controllers;

import com.backend.ingest.dtos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.backend.ingest.authorization.token.JwtProvider;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.entities.Rol;
import com.backend.ingest.entities.Session;
import com.backend.ingest.entities.User;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.ingest.service.services.EmailService;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.ingest.service.services.RolService;
import com.backend.ingest.service.services.SessionService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.BadCredentialsException;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
public class UserController {

    @Lazy
    @Autowired
    PasswordEncoder passwordEncoder;

    @Lazy
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    IUserService userService;

    @Autowired
    RolService rolService;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    EmailService emailService;

    @Autowired
    private IErrorLogsRepository iErrorRepository;

    @Value("${spring.mail.username}")
    private String mailFrom;

    private static final String subject = "Cambio de Contraseña";

    @Autowired
    private SessionService sessionService;

    @Autowired
    EventRedisHsetService redisService;

    @Value("${token_time_alive}")
    private String token_time_alive;

    @PostMapping("/send-email")
    public ResponseEntity<Response> sendEmailTemplate(@Valid @RequestBody EmailValuesDTO dto, BindingResult bindingResult) {
        try {
            Util.isEmptyField(bindingResult);
            Optional<User> usuarioOpt = userService.getByEmail(dto.getMailTo());
            if (!usuarioOpt.isPresent()) {
                iErrorRepository.save(new ErrorLogs("No hay ningún usuario con esas credenciales.", "400"));
                return new ResponseEntity(new Response("No hay ningún usuario con esas credenciales.", "400"), HttpStatus.BAD_REQUEST);
            }
            User usuario = usuarioOpt.get();
            dto.setMailFrom(mailFrom);
            dto.setMailTo(usuario.getEmail());
            dto.setSubject(subject);
            dto.setUserName(usuario.getUser());
            UUID uuid = UUID.randomUUID();
            String tokenPassword = uuid.toString();
            dto.setTokenPassword(tokenPassword);
            usuario.setTokenPassword(tokenPassword);
            userService.create(usuario);
            emailService.sendEmail(dto);
            return ResponseEntity.ok(new Response("Le hemos enviado un correo electrónico: " + dto.getMailTo(), HttpStatus.OK));
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("No se pudo consultar el envío de correo electrónico: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("No se pudo consultar el envío de correo electrónico" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
    @PostMapping("/change-password")
    public ResponseEntity<Response> changePassword(@Valid @RequestBody ChangePasswordDTO dto, BindingResult bindingResult) {
        try {
            Util.isEmptyField(bindingResult);
            if (!dto.getPassword().equals(dto.getConfirmPassword())) {
                return new ResponseEntity(new Response("Las contraseñas no coinciden", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
            }
            Optional<User> usuarioOpt = userService.getByTokenPassword(dto.getTokenPassword());
            if (!usuarioOpt.isPresent()) {
                return new ResponseEntity(new Response("No hay ningún usuario con esas credenciales.", "400"), HttpStatus.BAD_REQUEST);
            }
            User usuario = usuarioOpt.get();
//            boolean exis = passwordEncoder.matches(dto.getOldPassword(), passwordEncoder.encode(usuario.getPassword()));
            String newPassword = passwordEncoder.encode(dto.getPassword());
            usuario.setPassword(newPassword);
            usuario.setTokenPassword(null);
            userService.create(usuario);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        } catch (BadCredentialsException ex) {
            iErrorRepository.save(new ErrorLogs("la contraseña anterior es incorrecta", "400"));
            return new ResponseEntity(new Response("la contraseña anterior es incorrecta", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("No se pudo consultar el cambio de contraseña: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("No se pudo consultar el cambio de contraseña: " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("Contraseña actualizada", HttpStatus.OK));

    }
    @PostMapping("/change-password-user")
    public ResponseEntity<Response> changePassword(@Valid @RequestBody ChangePasswordUserDTO dto, BindingResult bindingResult) {
        try {
            Util.isEmptyField(bindingResult);
            if (!dto.getPassword().equals(dto.getConfirmPassword())) {
                return new ResponseEntity(new Response("Las contraseñas no coinciden", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
            }
            Optional<User> usuarioOpt = userService.getByEmail(dto.getEmail());
            if (!usuarioOpt.isPresent()) {
                return new ResponseEntity(new Response("No hay ningún usuario con esas credenciales.", "400"), HttpStatus.BAD_REQUEST);
            }
            User usuario = usuarioOpt.get();
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(usuario.getEmail(), dto.getOldPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
//            boolean exis = passwordEncoder.matches(dto.getOldPassword(), passwordEncoder.encode(usuario.getPassword()));
            String newPassword = passwordEncoder.encode(dto.getPassword());
            usuario.setPassword(newPassword);
            userService.create(usuario);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        } catch (BadCredentialsException ex) {
            iErrorRepository.save(new ErrorLogs("la contraseña anterior es incorrecta", "400"));
            return new ResponseEntity(new Response("la contraseña anterior es incorrecta", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("No se pudo consultar el cambio de contraseña: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("No se pudo consultar el cambio de contraseña: " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("Contraseña actualizada", HttpStatus.OK));

    }

    @GetMapping("/users")
    public ResponseEntity<Response> list() {
        List<User> listUsers = null;
        try {
            listUsers = userService.findByStatus();
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los usuarios: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar los usuarios:" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta fue realizada con exito", HttpStatus.OK, listUsers));

    }

    @PostMapping("/usersPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        Page<User> listUsersPage = null;
        try {
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listUsersPage = userService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)), params.getBusca());
                return ResponseEntity.ok(new Response("La consulta fue realizada con exito", HttpStatus.OK, listUsersPage));
            } else {
                listUsersPage = userService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())), params.getBusca());
                if (!params.getOrderAsc()) {
                    listUsersPage = userService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()), params.getBusca());
                }
                return ResponseEntity.ok(new Response("La consulta fue realizada con exito", HttpStatus.OK, listUsersPage));

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los usuarios: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar los usuarios:" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        User listUser = null;
        try {
            listUser = userService.findById(id);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Erro al consultar el usuario:" + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Erro al consultar el usuario:" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        if (listUser == null) {
            iErrorRepository.save(new ErrorLogs("El user con el ID: ".concat(id.toString().concat(" No existe en la base de datos!")), "400"));

            return new ResponseEntity(new Response("El user con el ID: ".concat(id.toString().concat(" No existe en la base de datos!")),
                    "400"), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(new Response("La consulta de los usuarios fue realizada con exito!", HttpStatus.OK, listUser));
    }

    @PostMapping("/user")
    public ResponseEntity<Response> nuevo(@Valid @RequestBody UserInsDto nuevoUsuario, BindingResult bindingResult) {
        User usuario = null;
        boolean arroba = false;
        arroba = Util.validEmail(nuevoUsuario.getEmail());
        if (arroba == false) {
            iErrorRepository.save(new ErrorLogs("Correo electrónico no válido, obligatorio @", "400"));
            return new ResponseEntity(new Response("Correo electrónico no válido, obligatorio @", "400"), HttpStatus.BAD_REQUEST);
        }
        if (userService.existsByUser(nuevoUsuario.getUser()) == 1) {
            iErrorRepository.save(new ErrorLogs("Esa usuario ya existe", "400"));
            return new ResponseEntity(new Response("Esa usuario ya existe", "400"), HttpStatus.BAD_REQUEST);

        } else if (userService.existsByEmail(nuevoUsuario.getEmail()) == 1) {
            iErrorRepository.save(new ErrorLogs("El correo electrónico ya existe", "400"));
            return new ResponseEntity(new Response("El correo electrónico ya existe", "400"), HttpStatus.BAD_REQUEST);
        }

        try {

            Util.isEmptyField(bindingResult);

            usuario = new User(nuevoUsuario.getUser(), nuevoUsuario.getEmail(),
                    passwordEncoder.encode(nuevoUsuario.getPassword()), nuevoUsuario.getDisplayName(),
                    nuevoUsuario.getLastName(), nuevoUsuario.getFirstName());
            Set<Rol> roles = new HashSet<Rol>();
            for (String s : nuevoUsuario.getRoles()) {
                roles.add(rolService.getByRolName(s));
            }
            usuario.setRoles(roles);

            userService.create(usuario);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("No se pudo registrar el usuario: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("No se pudo registrar el usuario: " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("El usuario fue creado con exito", HttpStatus.OK, usuario));

    }

    @PostMapping("/login")
    public ResponseEntity<Response> loginUser(@Valid @RequestBody LoginUser loginUsuario, BindingResult bindingResult) {

        JwtDto jwtDto = null;
        User userObject = null;
        Session session=null;
        UserDto user=null;
        try {
            Util.isEmptyField(bindingResult);
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginUsuario.getEmail(), loginUsuario.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            userObject = userService.getByEmail(loginUsuario.getEmail()).get();
            String jwt = jwtProvider.generateToken(authentication);
            long mili = 3600000;
            Calendar calendarR = Calendar.getInstance();
            calendarR.setTimeInMillis(new Date().getTime() - mili);
            Date dateExpiritaion = calendarR.getTime();
            jwtDto = new JwtDto(jwt, token_time_alive, dateExpiritaion);

            session = sessionService.create(new SessionDto(userObject.getIdUser()));
//            user = new UserDto(userObject.getIdUser(),userObject.getUser(),userObject.getEmail(),userObject.getPassword(),
//                    userObject.getDisplayName(),userObject.getFirstName(),userObject.getLastName(),userObject.getTokenPassword(),
//                    userObject.getStatus(),userObject.getCreationDate(),session.getId());
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (BadCredentialsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response("Credenciales Incorrectas, " +
                    "Usted no está autorizado a entrar al Sistema. Por favor consulte con el administrador del sistema.", "400"), HttpStatus.BAD_REQUEST);
        } catch (InternalAuthenticationServiceException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response("Credenciales Incorrectas, " +
                    "Usted no está autorizado a entrar al Sistema. Por favor consulte con el administrador del sistema.", "400"), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            ex.printStackTrace();
            iErrorRepository.save(new ErrorLogs("Error en el login: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error en el login: " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("Login con exito", HttpStatus.OK, session, jwtDto));

    }

    @PutMapping("/user/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody UserUpdDto userUpdDto, BindingResult result,
                                           @PathVariable String id) {
        User userUpdated = null;
        boolean arroba = false;
        try {
            arroba = Util.validEmail(userUpdDto.getEmail());
            if (arroba == false) {
                iErrorRepository.save(new ErrorLogs("Correo electrónico no válido, obligatorio @", "400"));
                return new ResponseEntity(new Response("Correo electrónico no válido, obligatorio @", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
            }
            List<User> listUser = userService.findNameDiferent(id);
            for (User user : listUser) {
                if (userUpdDto.getUser().equals(user.getUser())) {
                    iErrorRepository.save(new ErrorLogs("Esa usuario ya existe", "400"));
                    return new ResponseEntity(new Response("Esa usuario ya existe", "400"), HttpStatus.BAD_REQUEST);
                } else if (userUpdDto.getEmail().equals(user.getEmail())) {
                    iErrorRepository.save(new ErrorLogs("El correo electrónico ya existe", "400"));
                    return new ResponseEntity(new Response("El correo electrónico ya existe", "400"), HttpStatus.BAD_REQUEST);
                }
            }

            Util.isEmptyField(result);
            userUpdated = userService.update(id, userUpdDto);
//            if (userUpdated.getLastName().equals("repeatedPassword")) {
//
//                return ResponseEntity
//                        .ok(new Response("Password same as above", HttpStatus.NOT_FOUND));
//            }

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al actualizar el usuario: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al actualizar el usuario: " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("El usuario fue consultado con exito!", HttpStatus.OK, userUpdated));
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<Response> delete(@PathVariable String id) {

        try {
            userService.delete(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar el usuario: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al eliminar el usuario: " + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("El usuario fue eliminado con exito!", HttpStatus.OK));

    }

    @PostMapping("/refresh")
    public ResponseEntity<Response> refresh(@RequestBody JwtDto jwtDto) throws ParseException {
        String token = jwtProvider.refreshToken(jwtDto);
        JwtDto jwt = new JwtDto(token);
        return ResponseEntity.ok(new Response("Actualizar Token", HttpStatus.OK, jwt));

    }
}
