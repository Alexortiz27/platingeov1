package com.backend.ingest.controllers;

import com.backend.ingest.dtos.VehicleInsDto;
import com.backend.ingest.dtos.VehicleListDto;
import com.backend.ingest.dtos.VehicleListPageDto;
import com.backend.ingest.dtos.VehicleShowDto;
import com.backend.ingest.dtos.VehicleUpdDto;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.dtos.parameters.Parameters;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.CompanyClient;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Trama;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.ICompanyClientService;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.helper.Response;
import com.backend.ingest.service.interfaces.IVehicleService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"http://192.168.0.15:8080", "http://osinergmin-dashboard.geolabs.pw/", "http://dashboard.osinergmin-agent-2021.com"})
@RequestMapping("api/v1/")
public class VehicleController {
    @Autowired
    private EventRedisHsetService eventRedis;
    @Autowired
    private IVehicleService VehicleService;
    @Autowired
    private ICompanyClientService companyClientService;

    @Autowired
    private IErrorLogsRepository iErrorRepository;

    @GetMapping("/vehicle/{id}")
    public ResponseEntity<Response> show(@PathVariable String id) {

        VehicleShowDto Vehicle = null;

        try {
            Vehicle = VehicleService.getId(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar el vehiculo: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar el vehiculo: " + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return ResponseEntity.ok(new Response("El vehiculo fue consultado con exito", HttpStatus.OK, Vehicle));
    }

    @GetMapping("/vehicles/excel")
    public ResponseEntity<Response> listExcel(HttpServletResponse response) {

        try {
            VehicleService.excelExport(response);

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al exportar el excel de los vehiculos" + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al exportar el excel de los vehiculos" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return null;
    }

    @PostMapping("/vehicle")
    public ResponseEntity<Response> create(@Valid @RequestBody VehicleInsDto vehicleInst, BindingResult result) {

        Vehicle VehicleNew = null;
        VehicleShowDto vehicleShowDto = null;
        try {
            Util.isEmptyField(result);
            Integer existsPlate = VehicleService.existsByPlate(vehicleInst.getPlate());
            if (existsPlate == 0) {
                VehicleNew = new Vehicle(vehicleInst.getPlate(), vehicleInst.getCategory(), vehicleInst.getcode_osinergmin(), vehicleInst.getStatus(), vehicleInst.getCreationDate());
                CompanyClient company = companyClientService.findById(vehicleInst.getId_company_client());
                VehicleNew.setCompanyClient(company);
                vehicleShowDto = VehicleService.create(VehicleNew);
            } else {
                iErrorRepository.save(new ErrorLogs("El vehiculo con la placa:" + " " + vehicleInst.getPlate() + " ya existe!", "400"));
                return new ResponseEntity(new Response("El vehiculo con la placa:" + " " + vehicleInst.getPlate() + " ya existe!", "400"), HttpStatus.BAD_REQUEST);

            }

        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al registrar el vehiculo: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al registrar el vehiculo: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new Response("El Vehiculo Fue registrado con exito!", HttpStatus.OK, vehicleShowDto));
    }

    @PutMapping("/vehicle/{id}")
    public ResponseEntity<Response> update(@Valid @RequestBody VehicleUpdDto VehicleUpdDto, BindingResult result,
                                           @PathVariable String id) {
        Vehicle vehiclelist = VehicleService.findNameDiferent(id, VehicleUpdDto.getPlate());
        if (vehiclelist != null) {
            iErrorRepository.save(new ErrorLogs("La placa del vehiculo ya existe", "400"));
            return new ResponseEntity(new Response("La placa del vehiculo ya existe", "400"), HttpStatus.BAD_REQUEST);
        }
        VehicleShowDto currentVehicle = null;
        try {
            Util.isEmptyField(result);
            currentVehicle = VehicleService.update(id, VehicleUpdDto);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (EmptyFieldsException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);

        } catch (Exception ex) {
            iErrorRepository.save(new ErrorLogs("Error al actualizar el vehiculo: " + ex.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al actualizar el vehiculo: " + ex.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity
                .ok(new Response("El vehiculo fue actualizado con exito!", HttpStatus.OK, currentVehicle));
    }

    @DeleteMapping("/vehicle/{id}")
    public ResponseEntity<Response> delete(@PathVariable String id) {

        try {
            VehicleService.delete(id);
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al eliminar el vehiculo: " + e.getMessage(), "500"));
            return new ResponseEntity(
                    new Response("Error al eliminar el vehiculo:" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("El vehiculo fue eliminado con exito!", HttpStatus.OK));
    }

    @GetMapping("/vehicles")
    public ResponseEntity<Response> list() {

        List<VehicleListDto> listVehicle = null;
        try {

            listVehicle = VehicleService.findByVehicleCompany();

        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los vehiculos: " + e.getMessage(), "500"));

            return new ResponseEntity(new Response("Error al consultar los vehiculos: " + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("Los vehiculos fue consultado con exito", HttpStatus.OK, listVehicle));
    }

    @PostMapping("/vehiclesPagination")
    public ResponseEntity<Response> findAllPagination(@RequestBody Parameters params) {
        Page<Vehicle> listVehiclePage = null;

        try {
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                listVehiclePage = VehicleService.findAllPage(PageRequest.of(Util.page, Util.size, Sort.by(Util.nameOrder)));
                return ResponseEntity.ok(new Response("Los vehiculos fue consultado con exito", HttpStatus.OK, listVehiclePage));
            } else {
                listVehiclePage = VehicleService
                        .findAllPage(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())));
                if (!params.getOrderAsc()) {
                    listVehiclePage = VehicleService.findAllPage(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()));
                }
                return ResponseEntity.ok(new Response("Los vehiculos fue consultado con exito", HttpStatus.OK, listVehiclePage));
            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los vehiculos: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar los vehiculos: " + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/vehiclesPaginationLink")
    public ResponseEntity<Response> findAllPaginationLink(@RequestBody Parameters params) {
        List<Map<String, Object>> listVehiclePage = null;
        Map<Object, Object> map = new HashMap<Object, Object>();
        try {
            long total = VehicleService.countByStatus(params.getBusca());
            map = Util.totalPagesElemnt(total, params.getSize());
            if (params.getNameOrder() == null || params.getOrderAsc() == null || params.getPage() == null
                    || params.getSize() == null) {
                iErrorRepository.save(new ErrorLogs("Parametros de Paginación Vacios", "400"));
                return new ResponseEntity(new Response("Parametros de Paginación Vacios", "400"), HttpStatus.BAD_REQUEST);
            } else {
                listVehiclePage = VehicleService
                        .findAllPageLink(PageRequest.of(params.getPage(), params.getSize(), Sort.by(params.getNameOrder())), params.getBusca());
                if (!params.getOrderAsc()) {
                    listVehiclePage = VehicleService.findAllPageLink(PageRequest.of(params.getPage(), params.getSize(),
                            Sort.by(params.getNameOrder()).descending()), params.getBusca());
                }

                return ResponseEntity.ok(new Response("Los vehiculos fue consultado con exito", HttpStatus.OK, listVehiclePage, map));

            }
        } catch (DataNotFoundException ex) {
            iErrorRepository.save(new ErrorLogs(ex.getMessage(), "400"));
            return new ResponseEntity(new Response(ex.getMessage(), "400"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los vehiculos: " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar los vehiculos: " + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/vehiclesByCompanyClient")
    public ResponseEntity<Response> listByCompany(@RequestParam(name = "company_client_id") String company_client_id
    ) {
        Map<String, Object> response = new HashMap<>();
        List<VehicleListDto> listVehicle = null;
        try {

            listVehicle = VehicleService.findByCompany(company_client_id);

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los vehiculos:  " + e.getMessage(), "500"));
            return new ResponseEntity(new Response("Error al consultar los vehiculos: " + e.getMessage(), "500"
            ), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(new Response("Los vehiculos fue consultado con exito", HttpStatus.OK, listVehicle));
    }

    @GetMapping("/vehicle-transmitting")
    public ResponseEntity<Response> showVehiclesByPlate() {

        try {
            VehicleService.showVehiclesTransmitting();
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar los vehiculos trasmitiendo: " + e.getMessage(), "500"));

            return new ResponseEntity(new Response("Error al consultar los vehiculos trasmitiendo:" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta fue realizado con exito", HttpStatus.OK));

    }

    @GetMapping("/vehicle-plate-companyclient-company")
    public ResponseEntity<Response> showVehiclesByPlateCompanyClient(@RequestParam(name = "plate") String plate) {
        Map<String, Object> vehicle = null;
        try {

            vehicle = VehicleService.findPlateEmvCompany(plate);
            if(vehicle.isEmpty()){
                iErrorRepository.save(new ErrorLogs("La placa del vehiculo no existe en la base de datos:" + plate, "400"));
                return new ResponseEntity(new Response("La placa del vehiculo no existe en la base de datos:" + plate, "400"), HttpStatus.INTERNAL_SERVER_ERROR);

            }
        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al consultar la placa: " + e.getMessage(), "500"));

            return new ResponseEntity(new Response("Error al consultar la placa:" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La consulta fue realizado con exito", HttpStatus.OK, vehicle));

    }

    @PostMapping("/vehicle-plate-tracking")
    public ResponseEntity<Response> showVehiclesByPlateWebSocket(@RequestParam(name = "plate") String plate) {
        try {
            String key = "vehicle-tracking:"+plate;
            String date = String.valueOf(new Date().getTime());
            eventRedis.createHsetvehicleWebSocket(key,"tracking",plate);

        } catch (Exception e) {
            iErrorRepository.save(new ErrorLogs("Error al enviar la placa: " + e.getMessage(), "500"));

            return new ResponseEntity(new Response("Error al enviar la placa:" + e.getMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return ResponseEntity.ok(new Response("La placa fue enviado con exito", HttpStatus.OK));

    }
}
