package com.backend.ingest.dtos;

import com.backend.platin.util.ValidPassword;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class ChangePasswordDTO {

//    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
//    @NotEmpty(message = "no puede estar vacío")
//    private String oldPassword;

    @ValidPassword
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String password;

    @ValidPassword
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String confirmPassword;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String tokenPassword;

    public ChangePasswordDTO() {
    }

    public ChangePasswordDTO(
            String password, String confirmPassword, String tokenPassword) {
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.tokenPassword = tokenPassword;
    }

//    public String getOldPassword() {
//        return oldPassword;
//    }
//
//    public void setOldPassword(String oldPassword) {
//        this.oldPassword = oldPassword;
//    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getTokenPassword() {
        return tokenPassword;
    }

    public void setTokenPassword(String tokenPassword) {
        this.tokenPassword = tokenPassword;
    }
}
