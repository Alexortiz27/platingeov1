package com.backend.ingest.dtos;

import com.backend.platin.util.ValidPassword;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class ChangePasswordUserDTO {

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String email;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String oldPassword;

    @ValidPassword
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String password;

    @ValidPassword
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String confirmPassword;


    public ChangePasswordUserDTO() {
    }


    public ChangePasswordUserDTO(String email, String oldPassword, String password, String confirmPassword) {
        this.email = email;
        this.oldPassword = oldPassword;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
