/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

/**
 *
 * @author FAMLETO
 */
public class CompaniesDto {

    /**
     * @return the company_id
     */
    public String getCompany_id() {
        return company_id;
    }

    /**
     * @param company_id the company_id to set
     */
    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    /**
     * @return the company_client_id
     */
    public String getCompany_client_id() {
        return company_client_id;
    }

    /**
     * @param company_client_id the company_client_id to set
     */
    public void setCompany_client_id(String company_client_id) {
        this.company_client_id = company_client_id;
    }
 
    private String company_id;
    private String company_client_id;
}
