/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;
import java.util.Date;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author FAMLETO
 */
public class CompanyClientBasicDto {
    
    private String id;

    private String name;

    private String email;

    private String ruc;

    private String address;

    private String phone;
    
    private String status;

    private String creationDate;

    
    public CompanyClientBasicDto(String id, String name, String email, String ruc, String address, String phone,boolean status,Date creationDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.status=status?Util.PARAM_STATUS_ACTIVE:Util.PARAM_STATUS_INACTIVE;
        this.creationDate=Util.parseDateToPrinterFormatString(creationDate);
    
    }
    
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the ruc
     */
    public String getRuc() {
        return ruc;
    }

    /**
     * @param ruc the ruc to set
     */
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
}
