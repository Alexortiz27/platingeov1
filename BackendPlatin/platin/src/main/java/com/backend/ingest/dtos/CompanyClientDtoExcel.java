/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Propietario
 */
@Data
public class CompanyClientDtoExcel extends BaseRowModel {

    @ExcelProperty(value = "name", index = 0)
    private String name;//destino

    @ExcelProperty(value = "email", index = 1)
    private String email;

    @ExcelProperty(value = "ruc", index = 2)
    private String ruc;

    @ExcelProperty(value = "address", index = 3)
    private String address;

    @ExcelProperty(value = "phone", index = 4)
    private String phone;

    @ExcelProperty(value = "EMV", index = 5)
    private String EMV;

    @ExcelProperty(value = "creationDate", index = 6)
    private Date creationDate;

    public CompanyClientDtoExcel() {
    }

    public CompanyClientDtoExcel(String name, String email, String ruc, String address, String phone, String EMV, Date creationDate) {
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.EMV = EMV;
        this.creationDate = creationDate;
    }



}
