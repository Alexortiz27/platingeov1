/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;
import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author FAMLETO
 */
public class CompanyClientInsDto {

    private String id;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String name;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String email;

    @Size(min = 11, message = "su tamaño requerido es de 11 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String ruc;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String address;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String phone;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String id_company;

    private String status;

    private String creationDate;

    public CompanyClientInsDto(String id, String name, String email, String ruc, String address, String phone) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;

    }

    public CompanyClientInsDto(String id, String name, String email, String ruc, String address, String phone, boolean status, Date creationDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.status = status ? Util.PARAM_STATUS_ACTIVE : Util.PARAM_STATUS_INACTIVE;

    }

    public CompanyClientInsDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId_company() {
        return id_company;
    }

    public void setId_company(String id_company) {
        this.id_company = id_company;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

}
