/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author FAMLETO
 */
public class CompanyClientListDto {

    private String id;

    @NotEmpty(message = "the name field cannot be empty")
    private String name;

    @NotEmpty(message = "the email field cannot be empty")
    private String email;

    @NotEmpty(message = "the ruc field cannot be empty")
    private String ruc;

    @NotEmpty(message = "the address field cannot be empty")
    private String address;

    @NotEmpty(message = "the phone field cannot be empty")
    private String phone;

    @NotEmpty(message = "the id_company field cannot be empty")
    private List<CompanyBasicDto> companies;
    
    private String status;

    private String creationDate;

    
    public CompanyClientListDto(String id, String name, String email, String ruc, String address, String phone, List<CompanyBasicDto> companies,boolean status,Date creationDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.companies = companies;
        this.status= status?Util.PARAM_STATUS_ACTIVE:Util.PARAM_STATUS_INACTIVE;
        this.creationDate=Util.parseDateToPrinterFormatString(creationDate);
        
    }

    public CompanyClientListDto(String name, String email, String ruc, String address, String phone, List<CompanyBasicDto> companies) {
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.companies = companies;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
     /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the companies
     */
    public List<CompanyBasicDto> getCompanies() {
        return companies;
    }

    /**
     * @param companies the companies to set
     */
    public void setCompanies(List<CompanyBasicDto> companies) {
        this.companies = companies;
    }

   
}
