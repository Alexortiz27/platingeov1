package com.backend.ingest.dtos;

import java.time.LocalDateTime;
import java.util.Date;

public interface CompanyClientListPageDto {

    public String getAddress();

    public String getId();

    public String getRuc();

    public String getName();

    public String getEmail();

    public String getPhone();

    public Boolean getStatus();

    public Date getCreation_date();

    public String getNamecompany();

    public String getIdcompany();
}
