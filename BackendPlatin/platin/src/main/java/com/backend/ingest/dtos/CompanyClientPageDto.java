package com.backend.ingest.dtos;

import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

@Data
public class CompanyClientPageDto {

    public Object address;

    public Object id;

    public Object ruc;

    public Object name;

    public Object email;

    public Object phone;

    public Object status;

    public Object creation_date;

    public Object namecompany;

    public Object idcompany;

    public CompanyClientPageDto(Object address, Object id, Object ruc, Object name, Object email, Object phone, Object status, Object creation_date, Object namecompany, Object idcompany) {
        this.address = address;
        this.id = id;
        this.ruc = ruc;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.status = status;
        this.creation_date = creation_date;
        this.namecompany = namecompany;
        this.idcompany = idcompany;
    }

    
}
