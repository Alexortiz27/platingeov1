/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;
import java.util.Date;

/**
 *
 * @author FAMLETO
 */
public class CompanyClientShowDto {

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

   

    private String id;

    private String name;

    private String email;

    private String ruc;

    private String address;

    private String phone;

    private String status;

    private String creationDate;
    
    private CompanyBasicDto company;

    public CompanyClientShowDto(String id, String name, String email, String ruc, String address, String phone) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;

    }

    public CompanyClientShowDto(String id, String name, String email, String ruc, String address, String phone, boolean status, Date creationDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.status = status ? Util.PARAM_STATUS_ACTIVE : Util.PARAM_STATUS_INACTIVE;
        this.creationDate=Util.parseDateToPrinterFormatString(creationDate);
    }

    public CompanyClientShowDto(String id, String name, String email, String ruc, String address, String phone, String status, String creationDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.status = status;
        this.creationDate = creationDate;
    }
    
    public CompanyClientShowDto(String id, String name, String email, String ruc, String address, String phone, boolean status, String creationDate,CompanyBasicDto company) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.status = status ? Util.PARAM_STATUS_ACTIVE : Util.PARAM_STATUS_INACTIVE;
        this.creationDate = creationDate;
        this.company=company;
    }
    
    

    public CompanyClientShowDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
     /**
     * @return the creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the company
     */
    public CompanyBasicDto getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(CompanyBasicDto company) {
        this.company = company;
    }

}
