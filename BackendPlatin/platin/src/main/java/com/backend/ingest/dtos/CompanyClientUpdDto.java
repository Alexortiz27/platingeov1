/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.UUID;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 *
 * @author FAMLETO
 */
public class CompanyClientUpdDto {

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String name;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String email;

    @Size(min = 11, message = "su tamaño requerido es de 11 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String ruc;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String address;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String phone;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String id_company;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId_company() {
        return id_company;
    }

    public void setId_company(String id_company) {
        this.id_company = id_company;
    }

}
