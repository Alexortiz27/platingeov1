/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.google.gson.Gson;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Propietario
 */
@Data
public class CompanyDtoExcel extends BaseRowModel {

    @ExcelProperty(value = "name", index = 0)
    private Object name;//destino

    @ExcelProperty(value = "token", index = 1)
    private Object token;

    @ExcelProperty(value = "email", index = 2)
    private Object email;

    @ExcelProperty(value = "address", index = 3)
    private Object address;

    @ExcelProperty(value = "phone", index = 4)
    private Object phone;

    @ExcelProperty(value = "city", index = 5)
    private Object city;

    @ExcelProperty(value = "country", index = 6)
    private Object country;

    @ExcelProperty(value = "creationDate", index = 7)
    private Object creationDate;

    public CompanyDtoExcel() {
    }

    public CompanyDtoExcel(Object name, Object token, Object email, Object address, Object phone, Object city, Object country, Object creationDate) {
        this.name = name;
        this.token = token;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.city = city;
        this.country = country;
        this.creationDate = creationDate;
    }

   

}
