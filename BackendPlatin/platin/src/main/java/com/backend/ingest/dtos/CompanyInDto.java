/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.ingest.entities.User;
import com.backend.platin.util.Util;
import com.backend.platin.validators.IUniqueNameCompany;
import com.backend.platin.validators.IUniqueNameCompanyEmail;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author FAMLETO
 */
public class CompanyInDto {

    private String id;

    @Column(unique = true)
    @IUniqueNameCompany
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String name;

    private String token;

    @Column(unique = true)
    @IUniqueNameCompanyEmail
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String email;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String address;

//    @Pattern(regexp = "^ 1 (3 | 4 | 5 | 7 | 8) \\ d {9} $", message = "Mobile phone number format error")
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String phone;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String city;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String country;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String user_id;

    private String status;

    private String creationDate;

    private List<CompanyClientShowDto> companiesClient;
    
    private User user;

    public CompanyInDto(String id, String name, String token, String email, String address, String phone,
            String city, String country, String user_id,
            boolean status, Date creationDate) {
        this.name = name;
        this.token = token;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.city = city;
        this.country = country;
        this.id = id;
        this.user_id = user_id;
        this.status = status ? Util.PARAM_STATUS_ACTIVE : Util.PARAM_STATUS_INACTIVE;
        this.creationDate = Util.parseDateToPrinterFormatString(creationDate);
    }

    public CompanyInDto(String id, String name, String token, String email, String address, String phone, String city,
            String country, boolean status, Date creationDate, List<CompanyClientShowDto> companiesClient,
            User user) {
        this.id = id;
        this.name = name;
        this.token = token;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.city = city;
        this.country = country;
        this.id = id;
        this.status = status ? Util.PARAM_STATUS_ACTIVE : Util.PARAM_STATUS_INACTIVE;
        this.creationDate = Util.parseDateToPrinterFormatString(creationDate);
        this.companiesClient = companiesClient;
        this.user = user;
    }

    public CompanyInDto() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the ingestToken
     */
    public String getIngestToken() {
        return token;
    }

    /**
     * @param ingestToken the ingestToken to set
     */
    public void setIngestToken(String ingestToken) {
        this.token = ingestToken;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the companiesClient
     */
    public List<CompanyClientShowDto> getCompaniesClient() {
        return companiesClient;
    }

    /**
     * @param companiesClient the companiesClient to set
     */
    public void setCompaniesClient(List<CompanyClientShowDto> companiesClient) {
        this.companiesClient = companiesClient;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
