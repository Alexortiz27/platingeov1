package com.backend.ingest.dtos;

import lombok.Data;

@Data
public class CompanyPageDto {

    public Object id;

    public Object name;

    public Object token;

    public Object email;

    public Object address;

    public Object phone;
    public Object city;
    public Object country;

    public Object status;

    public Object creation_date;

    public Object nameclient;

    public Object idclient;

    public int totalPages;

    public long totalElements;

    public CompanyPageDto(Object id, Object name, Object token, Object email, Object address, Object phone, Object city, Object country, Object status, Object creation_date, Object nameclient, Object idclient, int totalPages, long totalElements) {
        this.id = id;
        this.name = name;
        this.token = token;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.city = city;
        this.country = country;
        this.status = status;
        this.creation_date = creation_date;
        this.nameclient = nameclient;
        this.idclient = idclient;
        this.totalPages = totalPages;
        this.totalElements = totalElements;
    }

    
}
