/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.ingest.entities.User;
import com.backend.platin.util.Util;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author FAMLETO
 */
public class CompanyShowDto 
{

    private String id;
    
    @NotEmpty(message = "the name field cannot be empty")
    private String name;
  
    @NotEmpty(message = "the token field cannot be empty")
    private String token;
    
    @NotEmpty(message = "the email field cannot be empty")
    private String email;
    
    @NotEmpty(message = "the address field cannot be empty")
    private String address;
    
    @NotEmpty(message = "the phone field cannot be empty")
    private String phone;
    
    @NotEmpty(message = "the city field cannot be empty")
    private String city;
    
    @NotEmpty(message = "the country field cannot be empty")
    private String country;
    
    private String status;
    
    private String creationDate;
    
    private User user;
    
    private List<CompanyClientShowDto> companyClients;

    public CompanyShowDto(String id,String name, String token, String email, String address, String phone, String city, String country,
            boolean status,Date creationDate,List<CompanyClientShowDto> companyClientsDto,User user) {
        this.name = name;
        this.token = token;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.city = city;
        this.country = country;
        this.id= id;
        this.status= status?Util.PARAM_STATUS_ACTIVE:Util.PARAM_STATUS_INACTIVE;
        this.creationDate=Util.parseDateToPrinterFormatString(creationDate);
        this.companyClients=companyClientsDto;
        this.user= user;
    }
    
    
     
    public CompanyShowDto(){}


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the ingestToken
     */
    public String getIngestToken() 
    {
        return token;
    }

    /**
     * @param ingestToken the ingestToken to set
     */
    public void setIngestToken(String ingestToken) {
        this.token = ingestToken;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }
    
    
     /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
     /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the companyClientsDto
     */
    public List<CompanyClientShowDto> getCompanyClients() {
        return companyClients;
    }

    /**
     * @param companyClientsDto the companyClientsDto to set
     */
    public void setCompanyClients(List<CompanyClientShowDto> companyClientsDto) {
        this.companyClients = companyClientsDto;
    }


}
