/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import javax.validation.constraints.NotNull;

/**
 *
 * @author FAMLETO
 */
public class CoordInDto {
    
    @NotNull(message = "the latitude field cannot be empty")
    private double latitude;
      
    @NotNull(message = "the longitude field cannot be empty") 
    private double longitude;


    public CoordInDto(@NotNull(message = "the latitude field cannot be empty") double latitude,
			@NotNull(message = "the longitude field cannot be empty") double longitude) {
		
		this.latitude = latitude;
		this.longitude = longitude;
	}

 
    
    public CoordInDto(){}
    


    public double getLatitude() 
    {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
}
