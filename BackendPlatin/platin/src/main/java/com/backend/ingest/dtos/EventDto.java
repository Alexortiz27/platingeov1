package com.backend.ingest.dtos;

import com.backend.ingest.entities.*;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;

import com.backend.platin.util.Util;
import java.io.Serializable;

public class EventDto {

    private String type;

    private String vehicle_id;

    private String geozone_id;

    private double latitude;

    private double longitude;

    private String receiveDate;

    private String gpsDate;

    private double spedd;

    private String creationDate;

    private String idTravel;

    private String nameTravel;

    public EventDto() {

    }

    public EventDto(String type, String vehicle_id, String geozone_id, double latitude, double longitude, Date receiveDate, Date gpsDate, double spedd,
             String idTravel, String nameTravel) {

        this.type = type;
        this.vehicle_id = vehicle_id;
        this.geozone_id = geozone_id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.receiveDate = Util.parseDatetoNsqFormatString(receiveDate);
        this.gpsDate = Util.parseDatetoNsqFormatString(gpsDate);
        this.spedd = spedd;
        this.creationDate = Util.parseDatetoNsqFormatString(new Date());
        this.idTravel = idTravel;
        this.nameTravel = nameTravel;
    }

    public String getIdTravel() {
        return idTravel;
    }

    public void setIdTravel(String idTravel) {
        this.idTravel = idTravel;
    }

    public String getNameTravel() {
        return nameTravel;
    }

    public void setNameTravel(String nameTravel) {
        this.nameTravel = nameTravel;
    }

    public double getSpedd() {
        return spedd;
    }

    public void setSpedd(double spedd) {
        this.spedd = spedd;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getGeozone_id() {
        return geozone_id;
    }

    public void setGeozone_id(String geozone_id) {
        this.geozone_id = geozone_id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

}
