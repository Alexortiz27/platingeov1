package com.backend.ingest.dtos;

import java.util.Date;
import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

public class EventInsDto {

    @NotEmpty(message = "no puede estar vacío")
    private String type;

    @NotEmpty(message = "no puede estar vacío")
    private String vehicle_id;

    @NotEmpty(message = "no puede estar vacío")
    private String geozone_id;

    private double latitude;

    private double longitude;

    private Date receiveDate;

    private Date gpsDate;

    private double spedd;
    
    private Integer odometer;

    private String idTravel;

    private String nameTravel;

    public EventInsDto() {
    }

    public EventInsDto(String type, String vehicle_id, String geozone_id, double latitude, double longitude, Date receiveDate, Date gpsDate, double spedd,String idTravel, String nameTravel, Integer odometer) {
        this.type = type;
        this.vehicle_id = vehicle_id;
        this.geozone_id = geozone_id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.receiveDate = receiveDate;
        this.gpsDate = gpsDate;
        this.spedd = spedd;
        this.odometer = odometer;
        this.idTravel = idTravel;
        this.nameTravel = nameTravel;
    }

    public String getIdTravel() {
        return idTravel;
    }

    public void setIdTravel(String idTravel) {
        this.idTravel = idTravel;
    }

    public String getNameTravel() {
        return nameTravel;
    }

    public void setNameTravel(String nameTravel) {
        this.nameTravel = nameTravel;
    }

    public double getSpedd() {
        return spedd;
    }

    public void setSpedd(double spedd) {
        this.spedd = spedd;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public Date getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(Date gpsDate) {
        this.gpsDate = gpsDate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getGeozone_id() {
        return geozone_id;
    }

    public void setGeozone_id(String geozone_id) {
        this.geozone_id = geozone_id;
    }

    /**
     * @return the odometer
     */
    public Integer getOdometer() {
        return odometer;
    }

    /**
     * @param odometer the odometer to set
     */
    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

}
