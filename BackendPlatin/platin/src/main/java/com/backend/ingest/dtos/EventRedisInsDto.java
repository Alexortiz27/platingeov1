package com.backend.ingest.dtos;

import java.util.Date;
import javax.validation.constraints.NotEmpty;

public class EventRedisInsDto {

    private String type;

    private Date createDate;

    private String geozone_id;

    public EventRedisInsDto(String type, Date createDate, String geozone_id) {
        this.type = type;
        this.createDate = createDate;
        this.geozone_id = geozone_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getGeozone_id() {
        return geozone_id;
    }

    public void setGeozone_id(String geozone_id) {
        this.geozone_id = geozone_id;
    }

}
