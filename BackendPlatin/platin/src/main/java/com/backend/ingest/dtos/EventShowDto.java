/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.Date;

/**
 *
 * @author FAMLETO
 */
public class EventShowDto 
{

    private String id;
    
    private String type;

    private VehicleBasicDto vehicle;

    private String geozone_id;

    private double latitude;

    private double longitude;

    private Date receiveDate;

    private Date gpsDate;

    private double spedd;

    private Date creationDate;

    private String idTravel;

    private String nameTravel;

    public EventShowDto(String id,String type, VehicleBasicDto vehicle, String geozone_id, double latitude, double longitude, 
            Date receiveDate, Date gpsDate, double spedd, Date creationDate, String idTravel, String nameTravel) {
        this.id=id;
        this.type = type;
        this.vehicle = vehicle;
        this.geozone_id = geozone_id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.receiveDate = receiveDate;
        this.gpsDate = gpsDate;
        this.spedd = spedd;
        this.creationDate = creationDate;
        this.idTravel = idTravel;
        this.nameTravel = nameTravel;
    }
    
    
    
    
     /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the vehicle
     */
    public VehicleBasicDto getVehicle() {
        return vehicle;
    }

    /**
     * @param vehicle the vehicle to set
     */
    public void setVehicle(VehicleBasicDto vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * @return the geozone_id
     */
    public String getGeozone_id() {
        return geozone_id;
    }

    /**
     * @param geozone_id the geozone_id to set
     */
    public void setGeozone_id(String geozone_id) {
        this.geozone_id = geozone_id;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    
    /**
     * @return the spedd
     */
    public double getSpedd() {
        return spedd;
    }

    /**
     * @param spedd the spedd to set
     */
    public void setSpedd(double spedd) {
        this.spedd = spedd;
    }

 

    /**
     * @return the idTravel
     */
    public String getIdTravel() {
        return idTravel;
    }

    /**
     * @param idTravel the idTravel to set
     */
    public void setIdTravel(String idTravel) {
        this.idTravel = idTravel;
    }

    /**
     * @return the nameTravel
     */
    public String getNameTravel() {
        return nameTravel;
    }

    /**
     * @param nameTravel the nameTravel to set
     */
    public void setNameTravel(String nameTravel) {
        this.nameTravel = nameTravel;
    }

    /**
     * @return the receiveDate
     */
    public Date getReceiveDate() {
        return receiveDate;
    }

    /**
     * @param receiveDate the receiveDate to set
     */
    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    /**
     * @return the gpsDate
     */
    public Date getGpsDate() {
        return gpsDate;
    }

    /**
     * @param gpsDate the gpsDate to set
     */
    public void setGpsDate(Date gpsDate) {
        this.gpsDate = gpsDate;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
}
