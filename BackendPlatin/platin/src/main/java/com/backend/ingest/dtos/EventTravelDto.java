/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.Date;

/**
 *
 * @author Joshua Ly
 */
public interface EventTravelDto {
    Date getGps_Date();
    String getType();
    double getLatitude();
    double getLongitude();
     String getId_Travel();
    String getName_Travel();    
    double getSpedd();
    Integer getOdometer();
   
}
