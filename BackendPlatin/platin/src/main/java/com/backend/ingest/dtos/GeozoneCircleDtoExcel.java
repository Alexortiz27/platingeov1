/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.google.gson.Gson;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Propietario
 */
@Data
public class GeozoneCircleDtoExcel extends BaseRowModel {

    @ExcelProperty(value = "category", index = 0)
    private String category;//destino

    @ExcelProperty(value = "radius", index = 5)
    private double radius;

    @ExcelProperty(value = "iduser", index = 6)
    private String iduser;

    @ExcelProperty(value = "type", index = 2)
    private String type;

    private List<CoordInDto> coordss;

    private String coords;

    @ExcelProperty(value = "latitude", index = 3)
    private double latitude;

    @ExcelProperty(value = "longitude", index = 4)
    private double longitude;

    @ExcelProperty(value = "name", index = 1)
    private String name;

    public GeozoneCircleDtoExcel() {
    }

    public GeozoneCircleDtoExcel(String category, double radius,String iduser, String type, double latitude, double longitude, String name) {
        this.category = category;
        this.radius = radius;
        this.iduser=iduser;
        this.type = type;
        coordss.add(new CoordInDto(latitude, longitude));
        this.coords = new Gson().toJson(coords);
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

}
