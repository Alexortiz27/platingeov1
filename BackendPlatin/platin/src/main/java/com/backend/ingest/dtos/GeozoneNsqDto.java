/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 *
 * @author FAMLETO
 */
public class GeozoneNsqDto {


    private String category;//destino
    private String id;//destino

    private Double radius;

    private List<CoordInDto> coords;

    public GeozoneNsqDto(String category, String id, Double radius,List<CoordInDto> coords) {
        this.category = category;
        this.id = id;
        this.radius = radius;
        this.coords = coords;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public List<CoordInDto> getCoords() {
        return coords;
    }

    public void setCoords(List<CoordInDto> coords) {
        this.coords = coords;
    }
}
