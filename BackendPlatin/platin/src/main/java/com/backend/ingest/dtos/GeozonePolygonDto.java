/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.validators.IUniqueName;
import com.backend.platin.validators.IUniqueNameGeozone;
import java.util.List;
import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 *
 * @author FAMLETO
 */
public class GeozonePolygonDto {
    
    @NotEmpty(message = "cannot be empty")
    private String session_id;
    
    @Size(min = 2, message = "required size is 2 characters")
    @NotEmpty(message = "cannot be empty")
    private String category;

    @Size(min = 2, message = "required size is 2 characters")
    @NotEmpty(message = "cannot be empty")
    private String type;

    @NotEmpty(message = "cannot be empty")
    private List<CoordInDto> coords;

    @Column(unique = true)
    @IUniqueNameGeozone
    @Size(min = 2, message = "required size is 2 characters")
    @NotEmpty(message = "cannot be empty")
    private String name;

    @NotEmpty(message = "cannot be empty")
    private String user_id;

    public GeozonePolygonDto(String category, String type, List<CoordInDto> coords, String name,String user_id) {
        this.category = category;
        this.type = type;
        this.coords = coords;
        this.name = name;
        this.user_id=user_id;
    }

    public GeozonePolygonDto() {
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the coords
     */
    public List<CoordInDto> getCoords() {
        return coords;
    }

    /**
     * @param coords the coords to set
     */
    public void setCoords(List<CoordInDto> coords) {
        this.coords = coords;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the session_id
     */
    public String getSession_id() {
        return session_id;
    }

    /**
     * @param session_id the session_id to set
     */
    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

}
