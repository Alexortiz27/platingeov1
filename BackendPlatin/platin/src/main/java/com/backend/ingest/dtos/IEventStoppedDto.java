/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.Date;

/**
 *
 * @author Joshua Ly
 */
public interface IEventStoppedDto {
    Date getGps_Date();
    String getType();
}
