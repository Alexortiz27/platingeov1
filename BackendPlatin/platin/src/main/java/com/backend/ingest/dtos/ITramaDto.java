package com.backend.ingest.dtos;

public interface ITramaDto {
    String getPlate();
    String getPosition();
    String getGps_Date();
    double getSpeed();
    String getReceive_Date();

}
