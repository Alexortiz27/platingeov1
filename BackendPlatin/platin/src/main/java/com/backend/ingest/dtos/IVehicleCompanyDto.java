package com.backend.ingest.dtos;

import java.time.LocalDateTime;
import java.util.Date;


public interface IVehicleCompanyDto {

    public String getcode_osinergmin();

    public String getId();

    public String getPlate();

    public String getCategory();

    public Boolean getStatus();

    public Date getCreation_date();

    public String getNameclient();
    public String getIdclient();
    
    public String getCompanyName();
}
