package com.backend.ingest.dtos;

import java.util.Date;


public interface IVehicleCompanyExportarExcelDto {

    public String getcode_osinergmin();

    public String getId();

    public String getPlate();

    public String getCategory();

    public Date getCreation_date();

    public String getNameclient();

    public String getToken();

    public String getNameCompany();

    public Boolean getStatusVehicle();

    public Boolean getStatusCliente();

    public Boolean getStatusEmv();
}
