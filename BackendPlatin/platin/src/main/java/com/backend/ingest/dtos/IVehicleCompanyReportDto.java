package com.backend.ingest.dtos;

public interface IVehicleCompanyReportDto {

    public String getcode_osinergmin();

    public String getNameemv();

    public Boolean getStatus();

    public String getNameclient();

}