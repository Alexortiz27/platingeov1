package com.backend.ingest.dtos;

import java.util.Date;


public class JwtDto {
    private String token;
    private String token_time_alive;
    private Date expirationDate;

    public JwtDto() {
    }



    public JwtDto(String token, String token_time_alive, Date expirationDate) {
        this.token = token;
        this.token_time_alive = token_time_alive;
        this.expirationDate = expirationDate;
    }

    public JwtDto(String token) {
        this.token = token;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken_time_alive() {
        return token_time_alive;
    }

    public void setToken_time_alive(String token_time_alive) {
        this.token_time_alive = token_time_alive;
    }


}
