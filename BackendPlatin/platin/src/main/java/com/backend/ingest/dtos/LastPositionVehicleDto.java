/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.Date;

/**
 *
 * @author Joshua Ly
 */
public class LastPositionVehicleDto {
    

    private String plate;
    private String altitude;
    private String latitude;
    private String longitude;
    private String gpsDate;
    private String speed;
    private String odometer;
    private String recieveDate;

    public LastPositionVehicleDto(String plate, String altitude, String latitude, String longitude, String gpsDate, String speed, String odometer,String recieveDate) {
        this.plate = plate;
        this.altitude = altitude;
        this.latitude = latitude;
        this.longitude = longitude;
        this.gpsDate = gpsDate;
        this.speed = speed;
        this.odometer = odometer;
        this.recieveDate=recieveDate;
    }

    /**
     * @return the plate
     */
    public String getPlate() {
        return plate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }

    /**
     * @return the altitude
     */
    public String getAltitude() {
        return altitude;
    }

    /**
     * @param altitude the altitude to set
     */
    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the gpsDate
     */
    public String getGpsDate() {
        return gpsDate;
    }

    /**
     * @param gpsDate the gpsDate to set
     */
    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    /**
     * @return the odometer
     */
    public String getOdometer() {
        return odometer;
    }

    /**
     * @param odometer the odometer to set
     */
    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public String getRecieveDate() {
        return recieveDate;
    }

    public void setRecieveDate(String recieveDate) {
        this.recieveDate = recieveDate;
    }
}
