package com.backend.ingest.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class LoginUser {

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String email;

    @Size(min = 8, message = "su tamaño requerido es de 8 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String password;

    public LoginUser() {

    }

    public LoginUser(String email, String password) {

        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
