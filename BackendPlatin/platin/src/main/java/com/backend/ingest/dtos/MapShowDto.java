/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.Date;

/**
 *
 * @author FAMLETO
 */
public class MapShowDto {

    
    
    private String id;
  
    private String name;

    private String urlObject;

    private Boolean status;

    private Date creationDate;
    
    private CompanyInDto company;

    public MapShowDto(String id, String name, String urlObject, Boolean status, Date creationDate, CompanyInDto company) {
        this.id = id;
        this.name = name;
        this.urlObject = urlObject;
        this.status = status;
        this.creationDate = creationDate;
        this.company = company;
    }
    
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the urlObject
     */
    public String getUrlObject() {
        return urlObject;
    }

    /**
     * @param urlObject the urlObject to set
     */
    public void setUrlObject(String urlObject) {
        this.urlObject = urlObject;
    }

    /**
     * @return the status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the company
     */
    public CompanyInDto getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(CompanyInDto company) {
        this.company = company;
    }
}
