package com.backend.ingest.dtos;

import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class MapUpdDto {

    @NotEmpty(message = "cannot be empty")
    private String session_id;
    
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String name;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String urlObject;

    @NotEmpty(message = "cannot be empty")
    private String user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getUrlObject() {
        return urlObject;
    }

    public void setUrlObject(String urlObject) {
        this.urlObject = urlObject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MapUpdDto(String name, String urlObject) {
        this.name = name;
        this.urlObject = urlObject;
    }

    public MapUpdDto() {
    }

    /**
     * @return the session_id
     */
    public String getSession_id() {
        return session_id;
    }

    /**
     * @param session_id the session_id to set
     */
    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

 
}
