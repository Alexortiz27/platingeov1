/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.validators.IUniqueName;
import com.backend.platin.validators.IUniqueNameMarker;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author FAMLETO
 */
public class MarkerDto {

    @NotEmpty(message = "no puede estar vacío")
    private String session_id;

    @NotEmpty(message = "cannot be empty")
    private String user_id;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String geozone_id;

    @Column(unique = true)
    @IUniqueNameMarker
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String name;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String type;

    @NotNull(message = "no puede estar vacío")
    private Double latitude;

    @NotNull(message = "no puede estar vacío")
    private Double longitude;

    @NotNull(message = "no puede estar vacío")
    private Double radius;

//    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
//    @NotEmpty(message = "no puede estar vacío")
    private String zindex;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * @return the zindex
     */
    public String getZindex() {
        return zindex;
    }

    /**
     * @param zindex the zindex to set
     */
    public void setZindex(String zindex) {
        this.zindex = zindex;
    }

    /**
     * @return the geozone_id
     */
    public String getGeozone_id() {
        return geozone_id;
    }

    /**
     * @param geozone_id the geozone_id to set
     */
    public void setGeozone_id(String geozone_id) {
        this.geozone_id = geozone_id;
    }

    /**
     * @return the session_id
     */
    public String getSession_id() {
        return session_id;
    }

    /**
     * @param session_id the session_id to set
     */
    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
}
