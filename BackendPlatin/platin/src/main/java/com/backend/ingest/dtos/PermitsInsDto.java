package com.backend.ingest.dtos;

import java.util.Date;
import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PermitsInsDto {

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String namePermists;

    private Boolean create_permits;

    private Boolean edit_permits;

    private Boolean delete_permits;

    public String getNamePermists() {
        return namePermists;
    }

    public void setNamePermists(String namePermists) {
        this.namePermists = namePermists;
    }

    public PermitsInsDto(String namePermists) {
        this.namePermists = namePermists;
    }

    public PermitsInsDto() {
    }
    public Boolean getCreate_permits() {
        return create_permits;
    }

    public void setCreate_permits(Boolean create_permits) {
        this.create_permits = create_permits;
    }

    public Boolean getEdit_permits() {
        return edit_permits;
    }

    public void setEdit_permits(Boolean edit_permits) {
        this.edit_permits = edit_permits;
    }

    public Boolean getDelete_permits() {
        return delete_permits;
    }

    public void setDelete_permits(Boolean delete_permits) {
        this.delete_permits = delete_permits;
    }
}
