/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import javax.validation.constraints.NotEmpty;

/**
 *
 * @author FAMLETO
 */
public class PositionDto 
{
    
  
    @NotEmpty(message = "the latitude field cannot be empty")
    private double latitude;

    @NotEmpty(message = "the longitude field cannot be empty")
    private double longitude;
    
     @NotEmpty(message = "the altitude field cannot be empty")
    private double altitude;

    public PositionDto(@NotEmpty(message = "the latitude field cannot be empty") double latitude,
            @NotEmpty(message = "the longitude field cannot be empty") double longitude,
            @NotEmpty(message = "the altitude field cannot be empty") double altitude) {

        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude= altitude;
    }

    public PositionDto() {

    }

    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

   
}
