/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author FAMLETO
 */
public class ReportDto {

    @Size(min = 7, message = "required size is 7 characters")
    @NotEmpty(message = "cannot be empty")
    private String plate;

    @NotNull(message = "cannot be empty")
    private Date startDate;

    @NotNull(message = "cannot be empty")
    private Date endDate;

    @Size(min = 2, message = "required size is 2 characters")
    @NotEmpty(message = "cannot be empty")
    private String typeReport;

    @Size(min = 2, message = "required size is 2 characters")
    @NotEmpty(message = "cannot be empty")
    private String company_id;
    
    @NotEmpty(message = "cannot be empty")
    private String user_id;

    @NotEmpty(message ="the  typeUrl no puede estar vacío")
    private String typeUrl;

    public ReportDto() {
    }

    /**
     * @return the plate
     */
    public String getPlate() {
        return plate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the typeReport
     */
    public String getTypeReport() {
        return typeReport;
    }

    /**
     * @param typeReport the typeReport to set
     */
    public void setTypeReport(String typeReport) {
        this.typeReport = typeReport;
    }

    /**
     * @return the company_id
     */
    public String getCompany_id() {
        return company_id;
    }

    /**
     * @param company_id the company_id to set
     */
    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    /**
     * @return the user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTypeUrl() {
        return typeUrl;
    }

    public void setTypeUrl(String typeUrl) {
        this.typeUrl = typeUrl;
    }
}
