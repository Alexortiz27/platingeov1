/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author FAMLETO
 */
public class ReportInDto {

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String urlGeoJson;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String urlXls;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String type;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String estadoReport;

    @NotNull(message = "no puede estar vacío")
    private Date startDate;

    @NotNull(message = "no puede estar vacío")
    private Date endDate;
    
    @NotEmpty(message = "no puede estar vacío")
    private boolean accion;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String company_id;

    public ReportInDto(String urlGeoJson, String urlXls, String type, String estadoReport, Date startDate, Date endDate, boolean accion, String company_id) {
        this.urlGeoJson = urlGeoJson;
        this.urlXls = urlXls;
        this.type = type;
        this.estadoReport = estadoReport;
        this.startDate = startDate;
        this.endDate = endDate;
        this.accion = accion;
        this.company_id = company_id;
    }

    public String getEstadoReport() {
        return estadoReport;
    }

    public void setEstadoReport(String estadoReport) {
        this.estadoReport = estadoReport;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isAccion() {
        return accion;
    }

    public void setAccion(boolean accion) {
        this.accion = accion;
    }

    /**
     * @return the urlGeoJson
     */
    public String getUrlGeoJson() {
        return urlGeoJson;
    }

    /**
     * @param urlGeoJson the urlGeoJson to set
     */
    public void setUrlGeoJson(String urlGeoJson) {
        this.urlGeoJson = urlGeoJson;
    }

    /**
     * @return the urlXls
     */
    public String getUrlXls() {
        return urlXls;
    }

    /**
     * @param urlXls the urlXls to set
     */
    public void setUrlXls(String urlXls) {
        this.urlXls = urlXls;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the company_id
     */
    public String getCompany_id() {
        return company_id;
    }

    /**
     * @param company_id the company_id to set
     */
    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

}
