/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.util.Date;

/**
 *
 * @author Propietario
 */
@Data
public class ReportLocalizacionUnidaDtoExcel extends BaseRowModel {

    @ExcelProperty(value = "Fecha", index = 0)
    private String Fecha;//destino

    @ExcelProperty(value = "Latitud", index = 1)
    private String Latitud;

    @ExcelProperty(value = "Longitud", index = 2)
    private String Longitud;

    public ReportLocalizacionUnidaDtoExcel() {
    }

    public ReportLocalizacionUnidaDtoExcel(String fecha, String latitud, String longitud) {
        Fecha = fecha;
        Latitud = latitud;
        Longitud = longitud;
    }
}
