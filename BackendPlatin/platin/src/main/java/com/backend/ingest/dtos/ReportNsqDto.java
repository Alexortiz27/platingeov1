/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author FAMLETO
 */
public class ReportNsqDto {
    
    
    private String plate;
    
    private String startDate;
    
    private String endDate;
    
    private String typeReport;
    
    private String company_id;

        public ReportNsqDto(String plate, Date startDate, Date endDate, String typeReport, String company_id) {
        this.plate = plate;
        this.startDate = Util.parseDatetoNsqFormatString(startDate);
        this.endDate = Util.parseDatetoNsqFormatString(endDate);;
        this.typeReport = typeReport;
        this.company_id = company_id;
    }
    
    public ReportNsqDto()
    {
    
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
        
    /**
     * @return the plate
     */
    public String getPlate() {
        return plate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }

    /**
     * @return the startDate
     */





    /**
     * @return the typeReport
     */
    public String getTypeReport() {
        return typeReport;
    }

    /**
     * @param typeReport the typeReport to set
     */
    public void setTypeReport(String typeReport) {
        this.typeReport = typeReport;
    }

    /**
     * @return the company_id
     */
    public String getCompany_id() {
        return company_id;
    }

    /**
     * @param company_id the company_id to set
     */
    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

 
    
}
