/*
 * To change this license header, choose License Headers in Project Properties.S
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;
import java.util.Date;

/**
 *
 * @author Joshua Ly
 */
public class ReportStatusDto {

    /**
     * @return the elapsedTime
     */
    public long getElapsedTime() {
        return elapsedTime;
    }

    /**
     * @param elapsedTime the elapsedTime to set
     */
    public void setElapsedTime(long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    /**
     * @return the plate
     */
    public String getPlate() {
        return plate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    private String status;
    private String type;
    private String requestedDate;
    private String xlsurl;
    private String geourl;
    private long elapsedTime;
    private String plate;
    private Date startDate;
    private Date endDate;
    

    public ReportStatusDto(String status, String type, String url,String geourl,long elapsedTime,String plate,Date start,Date end) {
        this.status = status;
        this.type = type;
        this.requestedDate = Util.convertDateUtcGmtPeruString(new Date());
        this.xlsurl = url;
        this.geourl=geourl;
        this.plate= plate;
        this.elapsedTime=elapsedTime;
        this.startDate=start;
        this.endDate= end;
    }

    
     /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the requestedDate
     */
    public String getRequestedDate() {
        return requestedDate;
    }

    /**
     * @param requestedDate the requestedDate to set
     */
    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }

    /**
     * @return the geourl
     */
    public String getGeourl() {
        return geourl;
    }

    /**
     * @param geourl the geourl to set
     */
    public void setGeourl(String geourl) {
        this.geourl = geourl;
    }

    /**
     * @return the xlsurl
     */
    public String getXlsurl() {
        return xlsurl;
    }

    /**
     * @param xlsurl the xlsurl to set
     */
    public void setXlsurl(String xlsurl) {
        this.xlsurl = xlsurl;
    }

    
}
