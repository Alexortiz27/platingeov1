package com.backend.ingest.dtos.Reports;

import com.backend.ingest.dtos.ReportDto;
import com.backend.platin.exceptions.DataNotFoundException;
import com.github.brainlag.nsq.NSQMessage;

import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;

public interface IReport {

    void process(NSQMessage message, ReportDto report, Instant start) throws Exception;

    void generateExcelReport() throws IOException, ParseException;

    void generateGeoJsonReport() throws DataNotFoundException, ParseException, IOException;

}
