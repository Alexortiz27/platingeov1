package com.backend.ingest.dtos.Reports;

import com.backend.ingest.dtos.*;
import com.backend.ingest.dtos.Reports.jsons.ReportResponseDto;
import com.backend.ingest.entities.Trama;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.interfaces.ITramaService;
import com.backend.ingest.service.interfaces.IVehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import com.github.brainlag.nsq.NSQMessage;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class Report {

    ReportDto report;

    @Autowired
    ITramaService tramaService;

    @Autowired
    IEventService eventService;

    @Autowired
    EventRedisHsetService redisService;

    @Autowired
    private IVehicleService vehicleService;

    VehicleCompanyReportDto vehicleCompany;

    String vehicle_status;

    @Value("${url.map}")
    private String urlMap;

    String geoUrlMap;

    String xlsUrlMap;

    String uuid;

    NSQMessage message;

    List<EventStoppedDto> listEventStoppedCistern;

    XSSFWorkbook workbook;

    XSSFSheet sheet;

    CellStyle style;

    @Value("${xls.path}")
    String xlsPath;

    @Value("${geojson.path}")
    String geojsonPath;

    String [] keys;

    private final Logger log = LogManager.getLogger(Report.class);

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Value("${rest.uri.report}")
    String uri;

    @Value("${token.Internal}")
    String tokenInternal;

    List<String> excelDataHeader;

    Instant start;

    List<Trama> tramas;

    public void process(NSQMessage message, ReportDto report,Instant start) throws Exception {
        this.report=report;
        this.message= message;
        this.start=start;

        keys = redisService.createReportRequest(Util.REQUESTED_STATUS, report, " ", " ");
        getCompanyAndVehicle(message);
        vehicle_status = vehicleCompany.getStatus() ? "Habilitado" : "Inhabilitado";

        uuid = UUID.randomUUID().toString();
        geoUrlMap = urlMap + uuid + ".geojson";
        xlsUrlMap = urlMap + uuid + ".xls";
        //Generating stopped events for geojsons
        message.touch();
        listEventStoppedCistern = eventService.getStoppedEvents(Util.TYPESTOPPED, Util.TYPEMOVEMENT, report.getPlate(), report.getStartDate(), report.getEndDate());

        //Generating Tramas
        log.warn("tramas a procesar");
        message.touch();
        tramas = tramaService.getTramasByPlateAndGpsDate(report.getPlate(),
                report.getStartDate(),
                report.getEndDate());
        message.touch();

        if (tramas.isEmpty())
            throw new DataNotFoundException(Util.TRAMA_NOT_FOUND);

        log.warn(tramas.get(0).getGpsDate());

        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Tramas");
        style = workbook.createCellStyle();

    }

    private void getCompanyAndVehicle(NSQMessage message) throws Exception {
        String keySetVehicle = "VehicleSetReport" + report.getPlate();
        String vehicle = redisService.findGetTypeWorkersSet(keySetVehicle);
        if (vehicle == null) {
            message.touch();
            IVehicleCompanyReportDto vehicles = vehicleService.vehiclefindByPlate(report.getPlate());
            message.touch();
            VehicleCompanyReportDto vehi = new VehicleCompanyReportDto(vehicles.getcode_osinergmin(), vehicles.getNameemv(), vehicles.getStatus(), vehicles.getNameclient());
            vehicle = new Gson().toJson(vehi);

        }

        if (vehicle != null) {
            redisService.createSetTypeWorkers(keySetVehicle, vehicle);
        }else
        {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }

        vehicleCompany = new Gson().fromJson(vehicle, VehicleCompanyReportDto.class);
    }

    public String [] [] initializeGeoJson(String [] [] body) throws DataNotFoundException {

        for (int i = 0; i < body.length; i++) {
            int columnCount =0;
            body[i][columnCount++] = tramas.get(0).getPlate();
            Vehicle vehiclelist = vehicleService.findByPlate(tramas.get(0).getPlate());
            body[i][columnCount++] = vehicleCompany.getNameemv();
            body[i][columnCount++] = vehiclelist.getCompanyClient().getName();
            body[i][columnCount++] = vehiclelist.getcode_osinergmin();
            if (listEventStoppedCistern.size() > 0) {

                for (EventStoppedDto iEventStoppedDto : listEventStoppedCistern) {
                    if (iEventStoppedDto.getGps_Date().equals(tramas.get(i).getGpsDate())) {
                        if (iEventStoppedDto.getType().equals(Util.TYPESTOPPED)) {
                            columnCount=4;
                            body[i][columnCount++] = translateTypeEventToSpanish(Util.TYPESTOPPED);
                        } else {
                            columnCount=4;
                            body[i][columnCount++] = translateTypeEventToSpanish(Util.TYPEMOVEMENT);
                        }
                    } else {
                        columnCount=4;
                        body[i][columnCount++] =translateTypeEventToSpanish(Util.TYPEMOVEMENT);
                    }
                }
            } else {
                columnCount=4;
                body[i][columnCount++] = translateTypeEventToSpanish(Util.TYPEMOVEMENT);
            }
            body[i][columnCount++]=geoUrlMap;
            body[i][columnCount++]= new Date().toString();
            body[i][columnCount++]= Util.convertDateUtcGmtPeruString(report.getStartDate());
            body[i][columnCount++]= Util.convertDateUtcGmtPeruString(report.getEndDate());

        }

        return body;
    }

    public String translateTypeEventToSpanish(String typeEvent) {
        String type = null;
        switch (typeEvent) {
//            case Util.TYPERECOVERYTRASMISSION:
//                type = "RECUPERACIÓN";
//                break;
//            case Util.TYPELOSTTRASMISSION:
//                type = "PÉRDIDA";
//                break;
//            case Util.TYPETIMELAG:
//                type = "DEFASE DE TIEMPO";
//                break;
//            case Util.TYPEMAXIMUNSPEED:
//                type = "EXCESO DE VELOCIDAD";
//                break;
//            case Util.TYPESTARTOUT:
//                type = "EMPIEZA VIAJE";
//                break;
//            case Util.TYPEENDIN:
//                type = "TERMINA VIAJE";
//                break;
            case Util.TYPESTOPPED:
                type = "DETENIDO";
                break;
            case Util.TYPESTOPPEDFOUR:
                type = "DETENIDO POR 4 HORAS";
                break;
            case Util.TYPEMOVEMENT:
                type = "EN MOVIMIENTO";
                break;
            case Util.TYPEMOVEMENTFOUR:
                type = "EN MOVIMIENTO POR 4 HORAS";
                break;
//            case Util.TYPEINCOMEREPORT:
//                type = "DENTRO DE LA GEOZONA";
//                break;
//            case Util.TYPEEXITREPORT:
//                type = "FUERA DE LA GEOZONA";
//                break;
        }

        return type;
    }

    public void sendReportByWebSocket() {
        String xlsUrlMap = urlMap+ uuid + ".xls";
        String geoUrlMap = urlMap +uuid + ".geojson";
        String[] urls = new String[2];

        urls[0] = geoUrlMap;
        urls[1] = xlsUrlMap;

        ReportResponseDto reportResponse = new ReportResponseDto();
        reportResponse.setUrls(urls);
        reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_OK);
        reportResponse.setUser_id(report.getUser_id());

        this.sendReport(reportResponse);

    }

    private void sendReport(ReportResponseDto responseReport) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String requestJson = new Gson().toJson(responseReport);

        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
        RestTemplate restTemplate = restTemplate();
        restTemplate.postForObject(uri + "/" + tokenInternal, entity, String.class);
    }


    public String formatDuration(long duration) {
        long hours = TimeUnit.MILLISECONDS.toHours(duration);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) % 60;
//        long milliseconds = duration % 1000;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }



}
