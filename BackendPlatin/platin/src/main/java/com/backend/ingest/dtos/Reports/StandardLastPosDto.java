package com.backend.ingest.dtos.Reports;

import com.backend.ingest.dtos.ITramaDto;
import com.backend.ingest.dtos.PositionDto;
import com.backend.ingest.dtos.ReportDto;
import com.backend.ingest.entities.Trama;
import com.backend.platin.dtos.geojson.GeoJsonDto;
import com.backend.platin.dtos.geojson.MetaStandardLasPosDto;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import com.github.brainlag.nsq.NSQMessage;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class StandardLastPosDto extends Report implements  IReport {

    double totalDistance;

    private Logger log = LogManager.getLogger(StandardLastPosDto.class);

    @Value("${rest.uri.nominatin}")
    private String nomatin_uri;



    @Override
    public void process(NSQMessage message,ReportDto report,Instant start) throws Exception {
        super.process(message,report,start);
        this.start=start;


    }

    @Override
    public void generateExcelReport() throws IOException, ParseException {

        redisService.changeStatusReport(keys, report, xlsUrlMap, Util.PROCESSING_STATUS, "", 0, report.getPlate());

        List<String> headerTitle= getTitleHeader();

        List<String> excelTitleBody = getBodyTitle();

        totalDistance = this.getTotalDistance(tramas);
        PositionDto position = new Gson().fromJson(tramas.get(0).getPosition(), PositionDto.class);
        String[] locations = this.getLocationFromLatitudeAndLongitude(position);

        Object[] exceldataLastPos = {report.getPlate(), vehicleCompany.getNameemv(),
                vehicleCompany.getNameclient(), vehicleCompany.getcode_osinergmin(), vehicle_status,
                geoUrlMap, Util.convertDateUtcGmtPeruString(new Date()), Util.convertDateUtcGmtPeruString(report.getStartDate()),
                Util.convertDateUtcGmtPeruString(report.getEndDate()), String.valueOf(totalDistance), locations[0], locations[1], locations[2]};

        message.touch();
        this.generateStandardLastPosReportExcel(headerTitle, exceldataLastPos, excelTitleBody);
        message.touch();

        headerTitle=null;
        excelTitleBody=null;

        locations=null;
        exceldataLastPos=null;

        tramas.clear();
        listEventStoppedCistern.clear();

        Instant finishTime = Instant.now();
        Duration timeElapsed = Duration.between(start, finishTime);
        log.warn("duración: "+timeElapsed.getNano()+" NANOSEGUNDOS ");
        log.warn("duración: "+timeElapsed.getSeconds()+" SEGUNDOS ");
        redisService.changeStatusReport(keys, report, xlsUrlMap, Util.COMPLETED_STATUS, "", timeElapsed.getSeconds(), report.getPlate());
    }

    private List<String> getTitleHeader()
    {
        List<String> headerTitle= new ArrayList<>();
        headerTitle.add("Placa,");
        headerTitle.add("EMV");
        headerTitle.add("Cliente");
        headerTitle.add("Codigo Osignergmin");
        headerTitle.add("Estado de Vehículo");
        headerTitle.add("Url del Mapa");
        headerTitle.add("Fecha de Generación");
        headerTitle.add("Desde");
        headerTitle.add("Hasta");
        headerTitle.add("Kms Recorridos");
        headerTitle.add("Dirección");
        headerTitle.add("Distrito");
        headerTitle.add("Ciudad");

        return headerTitle;
    }

    private List<String> getBodyTitle()
    {
        List<String> bodyTitle= new ArrayList<>();
        bodyTitle.add("Fecha Gps");
        bodyTitle.add("Latitud");
        bodyTitle.add("Longitud");
        bodyTitle.add("Velocidad");
        bodyTitle.add("Fecha Ingesta");

        return bodyTitle;
    }

//    private  List<Object> getDataHeader(String [] locations)
//    {
//        List<Object> exceldataLastPos= new ArrayList<>();
//
//        exceldataLastPos.add(report.getPlate());
//
////        Object[] exceldataLastPos = {report.getPlate(), vehicleCompany.getNameemv(),
////                vehicleCompany.getNameclient(), vehicleCompany.getcode_osinergmin(), vehicle_status,
////                geoUrlMap, Util.convertDateUtcGmtPeruString(new Date()), Util.convertDateUtcGmtPeruString(report.getStartDate()),
////                Util.convertDateUtcGmtPeruString(report.getEndDate()), String.valueOf(totalDistance), locations[0], locations[1], locations[2]};
//
//    }

    private double getTotalDistance(List<Trama> tramas) {
        int j = 1;
        double totalDistance = 0;
        if (tramas.size() > 0) {
            for (int i = 0; i < tramas.size(); i++) {
                String stPositionString = tramas.get(i).getPosition();
                PositionDto stPosition = new Gson().fromJson(stPositionString, PositionDto.class);
                String enPositionString = tramas.get(j).getPosition();
                PositionDto enPosition = new Gson().fromJson(enPositionString, PositionDto.class);
                if (j < tramas.size() - 1) {
                    j++;
                }
                double distance = Util.calculateDistance(stPosition.getLatitude(), stPosition.getLongitude(), enPosition.getLatitude(), enPosition.getLongitude());
                totalDistance = totalDistance + distance;
            }
        }

        return totalDistance;
    }

    private String[] getLocationFromLatitudeAndLongitude(PositionDto position) {
        String[] location = new String[3];
        try {

            if (position != null && position.getLatitude() != 0 && position.getLongitude() != 0) {

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                String uriWithParameters = this.nomatin_uri + position.getLatitude() + "&lon=" + position.getLongitude() + "&format=jsonv2&addressdetails=1&extratags=1";
                RestTemplate restTemplate = restTemplate();
                ResponseEntity<String> result = restTemplate.getForEntity(uriWithParameters, String.class);

                if (result.getBody() != null) {
                    System.out.println("Cuerpo:" + result.getBody());

                    String jsonAddressString = result.getBody();
                    JsonObject jsonResult = new JsonParser().parse(jsonAddressString).getAsJsonObject();
                    JsonObject jsonAddress = jsonResult.get("address").getAsJsonObject();
                    String[] types = {Util.AMENITY_ADDRESS, Util.ROAD_ADDRESS, Util.CITY_ADDRESS, Util.STATE_ADDRESS};

                    for (String type : types) {
                        switch (type) {
                            case Util.AMENITY_ADDRESS:
                                location[0] = getAddress(jsonAddress);
                                break;
                            case Util.CITY_ADDRESS:
                                location[1] = getCity(jsonAddress);
                                break;
                            case Util.STATE_ADDRESS:
                                location[2] = getState(jsonAddress);
                                break;
                        }
                    }
                } else {
                    Arrays.fill(location, " ");
                }
            } else {
                Arrays.fill(location, " ");
            }

            return location;
        } catch (Exception ex) {
            Arrays.fill(location, " ");
            return location;
        }
    }

    private String getAddress(JsonObject jsonAddress) {
        String address = null;
        if (jsonAddress != null && jsonAddress.get("amenity") != null) {
            address = jsonAddress.get("amenity").getAsString() + ", " + jsonAddress.get("road").getAsString();
        } else if (jsonAddress != null && jsonAddress.get("road") != null) {
            address = jsonAddress.get("road").getAsString();
        } else {
            address = " ";
        }

        return address;
    }

    private String getCity(JsonObject jsonAddress) {
        String city = null;

        if (jsonAddress != null && jsonAddress.get("city") != null) {
            city = jsonAddress.get("city").getAsString();
        } else {
            city = " ";
        }

        return city;
    }

    private String getState(JsonObject jsonAddress) {
        String state = null;
        if (jsonAddress != null && jsonAddress.get("state") != null) {
            state = jsonAddress.get("state").getAsString();
        } else {
            state = " ";
        }

        return state;
    }


    private void generateStandardLastPosReportExcel(List<String> excelHeaderTitle, Object[] exceldataLastPos,
                                                    List<String> excelBodyTitle
                                                  ) throws IOException {
        int rowCount;
        for(rowCount = 0; rowCount < excelHeaderTitle.size(); rowCount++) {
            Row row = sheet.createRow(rowCount);
            row.createCell(0).setCellValue("" + excelHeaderTitle.get(rowCount));
            row.createCell(1).setCellValue("" + exceldataLastPos[rowCount]);
            row.getCell(0).setCellStyle(style);
            sheet.addMergedRegion(new CellRangeAddress(rowCount,rowCount,1,4));
        }

        generateBodySTANDARD_LAST_POS_REPORT(rowCount, excelBodyTitle);
        tramas.clear();
        Util.generateDirIfDoesntExist(xlsPath);
        Util.generateExcelReportFile(workbook, uuid, xlsPath);
        sheet=null;
        Instant finishTime = Instant.now();
        Duration timeElapsed = Duration.between(start, finishTime);
        redisService.changeStatusReport(keys, report, xlsUrlMap, Util.COMPLETED_STATUS, "", timeElapsed.getSeconds(), report.getPlate());
    }

    private void generateBodySTANDARD_LAST_POS_REPORT( int rowCount,
                                                      List<String> excelBodyTitlte) {

        rowCount = rowCount + 2;
        Row rowe = sheet.createRow(rowCount++);
        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelBodyTitlte, style);

        for (Trama trama : tramas)  {
            Row rowItem = sheet.createRow(rowCount);
            rowItem.createCell(0).setCellValue(Util.convertDateUtcGmtPeruString(trama.getGpsDate()));
            PositionDto position = new Gson().fromJson(trama.getPosition(), PositionDto.class);
            rowItem.createCell(1).setCellValue(String.valueOf(position.getLatitude()));
            rowItem.createCell(2).setCellValue(String.valueOf(position.getLongitude()));
            rowItem.createCell(3).setCellValue( String.valueOf(trama.getSpeed()));
            rowItem.createCell(4).setCellValue( Util.convertDateUtcGmtPeruString(trama.getCreationDate()));
            rowCount++;
        }
    }


    @Override
    public void generateGeoJsonReport() throws DataNotFoundException, ParseException, IOException {
        redisService.changeStatusReport(keys, report, "", Util.PROCESSING_STATUS, geoUrlMap, 0, report.getPlate());
        log.warn("Generando GEOJSON");
        //generate geojson file
        GeoJsonDto geoJsonDto = new GeoJsonDto();

        //Generating Meta
        MetaStandardLasPosDto meta = new MetaStandardLasPosDto();
        geoJsonDto.setMeta(meta);
        String [] [] body = new String[tramas.size()][14];

        //Generating header
        geoJsonDto.setHeader(Util.generateGeneralHeadersGeoJson(report.getTypeReport()));

        body=this.initializeGeoJson(body);

        //GENERATING GPS DATE
        //GENERATING LATITUDE AND LONGITUDE

        for(int i=0;i<tramas.size();i++) {
            int columnCount=9;
            PositionDto positiondto = new Gson().fromJson(tramas.get(i).getPosition(), PositionDto.class);
            body[i][columnCount++] = String.valueOf(positiondto.getLatitude());
            body[i][columnCount++] = String.valueOf(positiondto.getLongitude());
            body[i][columnCount++] = String.valueOf(totalDistance);
            body[i][columnCount++] = String.valueOf(tramas.get(i).getSpeed());
            body[i][columnCount] = Util.convertDateUtcGmtPeruString(tramas.get(i).getGpsDate());
        }

        tramas.clear();
        listEventStoppedCistern.clear();
        geoJsonDto.setBody(body);
        body= null;

        String jsonTramas = new Gson().toJson(geoJsonDto);

//        Generating GeoJson
        com.backend.platin.util.Util.generateDirIfDoesntExist(geojsonPath);
        Util.generateGeoJsonReportFile(uuid, geojsonPath, jsonTramas);
        log.warn("geojson generado");

        Instant finishTime = Instant.now();
        Duration timeElapsed = Duration.between(start, finishTime);
        redisService.changeStatusReport(keys, report, "", Util.COMPLETED_STATUS, geoUrlMap, timeElapsed.getSeconds(), report.getPlate());

    }

}
