package com.backend.ingest.dtos.Reports;

import com.backend.ingest.dtos.EventTravelDto;
import com.backend.ingest.dtos.ReportDto;
import com.backend.ingest.dtos.Reports.jsons.TravelListDto;
import com.backend.platin.dtos.geojson.GeoJsonDto;
import com.backend.platin.dtos.geojson.MetaTravelsReport;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import com.github.brainlag.nsq.NSQMessage;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class TravelReportDto extends Report implements IReport {

    private List<EventTravelDto> listEventTravels;
    private Logger log = LogManager.getLogger(StandardLastPosDto.class);
    private  List<TravelListDto> listTravelsDto;

    public void process(NSQMessage message, ReportDto report, Instant start) throws Exception {
        super.process(message, report,start);

        message.touch();
        listEventTravels = eventService.getTravelEvents(Util.TYPEENDIN, Util.TYPESTARTOUT, report.getPlate(), report.getStartDate(), report.getEndDate());
        if (listEventTravels.isEmpty()) {
            throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
        }

        listTravelsDto=generateBodyTRAVELREPORTList(listEventTravels);
    }

    @Override
    public void generateExcelReport() throws IOException {
        redisService.changeStatusReport(keys, report, xlsUrlMap, Util.PROCESSING_STATUS, "", 0, report.getPlate());

        String[] excelBodyTitle = {"ViajeId", "Nombre", "Inicio Coordenadas", "Inicio(Hora)", "Final Coordenadas", "Final(Hora)", "Tiempo", "Kilometros Recorridos", "Velocidad", "Odometro"};
        String[] excelHeaderTitle = Util.generateGeneralHeadersExcel(Util.TRAVEL_REPORT);
        message.touch();
        //this.generateTravelExcelReport(excelHeaderTitle, excelheaderData, excelBodyTitle);
        message.touch();
    }


    private void generateTravelExcelReport(String[] excelHeader, Object[] excelHeaderdata, String[] excelHeaderR
    ) throws IOException {
        int rowCount;
        for (rowCount = 0; rowCount < excelHeader.length; rowCount++) {
            Row row = sheet.createRow(rowCount + 1);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            style.setFont(headerFont);

            Cell cell = row.createCell(0);
            cell.setCellValue(excelHeader[rowCount]);
            cell.setCellStyle(style);
            sheet.autoSizeColumn(0);
            sheet.setColumnWidth(0, 500 * 20 + 1000);
            Cell cell2 = row.createCell(1);
            cell2.setCellValue(excelHeaderdata[rowCount].toString());
            cell2.setCellStyle(style);
            sheet.autoSizeColumn(1);
            sheet.setColumnWidth(1, 500 * 50 + 1000);
            sheet.autoSizeColumn(2);
            sheet.setColumnWidth(2, 500 * 20 + 1000);
            sheet.autoSizeColumn(3);
            sheet.setColumnWidth(3, 500 * 20 + 1000);
            sheet.autoSizeColumn(4);
            sheet.setColumnWidth(4, 500 * 20 + 1000);
            sheet.autoSizeColumn(5);
            sheet.setColumnWidth(5, 500 * 20 + 1000);
            sheet.autoSizeColumn(6);
            sheet.setColumnWidth(6, 500 * 20 + 1000);
            sheet.autoSizeColumn(7);
            sheet.setColumnWidth(7, 500 * 20 + 1000);
        }

        int travelQuantities = this.generateBodyTRAVELREPORT(rowCount, excelHeaderR);
        Row row = sheet.createRow(10);
        Cell cell = row.createCell(0);
        cell.setCellValue("Cantidad de Viajes");
        cell.setCellStyle(style);
        Cell cell2 = row.createCell(1);
        cell2.setCellValue(travelQuantities);
        cell2.setCellStyle(style);

        Util.generateDirIfDoesntExist(xlsPath);
        Util.generateExcelReportFile(workbook, uuid, xlsPath);

        Instant finishTime = Instant.now();
        Duration timeElapsed = Duration.between(start, finishTime);
        redisService.changeStatusReport(keys, report, xlsUrlMap, Util.COMPLETED_STATUS, "", timeElapsed.getSeconds(), report.getPlate());

    }

    private int generateBodyTRAVELREPORT(int rowCount, String[] excelHeaderR) {
        rowCount = rowCount + 3;
        Row rowe = sheet.createRow(rowCount++);
//        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelHeaderR, style);

        for (TravelListDto travelListDto : listTravelsDto) {
            rowe = sheet.createRow(rowCount);

            Util.generateBodyType(workbook, rowe, travelListDto.getTravel_id(), travelListDto.getNameTravel(),
                    travelListDto.getStartCoords(), travelListDto.getGpsDate(), travelListDto.getEndCoords(),
                    travelListDto.getGpsDateEnd(), travelListDto.getTimeTravels(), travelListDto.getKmdistance(),
                    travelListDto.getSpeed(), travelListDto.getOdometer(), Util.TRAVEL_REPORT);

            rowCount++;
        }

        int travelQuantities=listTravelsDto.get(0).getTravelQuantities();
        listTravelsDto.clear();
        return travelQuantities;
    }

    @Override
    public void generateGeoJsonReport() throws DataNotFoundException, ParseException, IOException {
        redisService.changeStatusReport(keys, report, "", Util.PROCESSING_STATUS, geoUrlMap, 0, report.getPlate());
        log.warn("Generando GEOJSON");
        //generate geojson file
        GeoJsonDto geoJsonDto = new GeoJsonDto();

        //Generating Meta
        MetaTravelsReport meta = new MetaTravelsReport();
        geoJsonDto.setMeta(meta);
        String[][] body = new String[listEventTravels.size()][19];

        geoJsonDto.setHeader(Util.generateGeneralHeadersGeoJson(report.getTypeReport()));

        body=this.initializeGeoJson(body);

        for(int i=0;i<listTravelsDto.size();i++)
        {
            int columnCount= 9;
            body[i][columnCount++]=listTravelsDto.get(i).getTravel_id();
            body[i][columnCount++]= listTravelsDto.get(i).getNameTravel();
            body[i][columnCount++]=listTravelsDto.get(i).getStartCoords();
            body[i][columnCount++]=listTravelsDto.get(i).getGpsDate();
            body[i][columnCount++]=listTravelsDto.get(i).getEndCoords();
            body[i][columnCount++]=listTravelsDto.get(i).getGpsDateEnd();
            body[i][columnCount++]=listTravelsDto.get(i).getTimeTravels();
            body[i][columnCount++]=listTravelsDto.get(i).getKmdistance();
            body[i][columnCount++]=listTravelsDto.get(i).getSpeed();
            body[i][columnCount]=listTravelsDto.get(i).getOdometer();

        }
        listTravelsDto.clear();
        geoJsonDto.setBody(body);
        body= null;

        String jsonTramas = new Gson().toJson(geoJsonDto);

        com.backend.platin.util.Util.generateDirIfDoesntExist(geojsonPath);
        Util.generateGeoJsonReportFile(uuid, geojsonPath, jsonTramas);
        log.warn("geojson generado");

        Instant finishTime = Instant.now();
        Duration timeElapsed = Duration.between(start, finishTime);
        redisService.changeStatusReport(keys, report, "", Util.COMPLETED_STATUS, geoUrlMap, timeElapsed.getSeconds(), report.getPlate());

    }

    private List<TravelListDto> generateBodyTRAVELREPORTList(List<EventTravelDto> event) {
        int rowv2 = 0;
        int rowv3 = 0;
        List<Object[]> listTravels = new ArrayList<>();
        List<TravelListDto> listTravelsDto = new ArrayList<>();
        int nvecesCantidadViajes = 0;
        for (int i = 0; i < event.size(); i++) {
            if (rowv2 < event.size()) {
                if (event.get(rowv2).getType().equals("ENDIN")) {
                    if (rowv2 + 1 < event.size()) {
                        if (event.get(rowv2).getType().equals("ENDIN") && event.get(rowv2 + 1).getType().equals("STARTOUT")) {
                            int odometer = event.get(rowv2).getOdometer() != null ? event.get(rowv2).getOdometer() : 0;
                            listTravels.add(new Object[]{event.get(rowv2).getId_Travel(), event.get(rowv2).getName_Travel(),
                                    event.get(rowv2).getLatitude() + "," + event.get(rowv2).getLongitude(),
                                    Util.convertDateUtcGmtPeruString(event.get(rowv2).getGps_Date()), "null", "null", "null", "null", event.get(rowv2).getSpedd(), odometer});
                        }
                        if (event.get(rowv2).getType().equals("ENDIN") && event.get(rowv2 + 1).getType().equals("ENDIN")) {
                            int odometer = event.get(rowv2).getOdometer() != null ? event.get(rowv2).getOdometer() : 0;
                            listTravels.add(new Object[]{event.get(rowv2).getId_Travel(), event.get(rowv2).getName_Travel(),
                                    event.get(rowv2).getLatitude() + "," + event.get(rowv2).getLongitude(),
                                    Util.convertDateUtcGmtPeruString(event.get(rowv2).getGps_Date()), "null", "null", "null", "null", event.get(rowv2).getSpedd(), odometer});
                        }
                    } else {
                        int odometer = event.get(rowv2).getOdometer() != null ? event.get(rowv2).getOdometer() : 0;
                        listTravels.add(new Object[]{event.get(rowv2).getId_Travel(), event.get(rowv2).getName_Travel(),
                                event.get(rowv2).getLatitude() + "," + event.get(rowv2).getLongitude(),
                                Util.convertDateUtcGmtPeruString(event.get(rowv2).getGps_Date()), "null", "null", "null", "null", event.get(rowv2).getSpedd(), odometer});
                    }
                }
                if (event.get(rowv2).getType().equals("STARTOUT")) {
                    if (rowv2 + 1 < event.size()) {
                        if (event.get(rowv2).getType().equals("STARTOUT") && event.get(rowv2 + 1).getType().equals("ENDIN")) {
                            Date gpsdate = event.get(rowv2 + 1).getGps_Date();
                            Date recievedate = event.get(rowv2).getGps_Date();
                            double timestap = recievedate.getTime() - gpsdate.getTime();
                            String formatDetenido = formatDuration(Double.valueOf(timestap).longValue());
                            double kmdistance = Util.calculateDistance(event.get(rowv2 + 1).getLatitude(), event.get(rowv2 + 1).getLongitude(),
                                    event.get(rowv2).getLatitude(), event.get(rowv2).getLongitude());
                            nvecesCantidadViajes++;
                            int odometer = event.get(rowv2).getOdometer() != null ? event.get(rowv2).getOdometer() : 0;
                            listTravels.add(new Object[]{event.get(rowv2 + 1).getId_Travel(), event.get(rowv2 + 1).getName_Travel(),
                                    event.get(rowv2 + 1).getLatitude() + "," + event.get(rowv2 + 1).getLongitude(),
                                    Util.convertDateUtcGmtPeruString(event.get(rowv2 + 1).getGps_Date()), event.get(rowv2).getLatitude() + "," + event.get(rowv2).getLongitude()
                                    , Util.convertDateUtcGmtPeruString(event.get(rowv2).getGps_Date()), formatDetenido, String.valueOf(kmdistance), event.get(rowv2).getSpedd(), odometer});
                            rowv3++;
                        }
                        if (event.get(rowv2).getType().equals("STARTOUT") && event.get(rowv2 + 1).getType().equals("STARTOUT")) {
                            int odometer = event.get(rowv2).getOdometer() != null ? event.get(rowv2).getOdometer() : 0;
                            listTravels.add(new Object[]{event.get(rowv2).getId_Travel(), event.get(rowv2).getName_Travel(),
                                    "null", "null", event.get(rowv2).getLatitude() + "," + event.get(rowv2).getLongitude(),
                                    Util.convertDateUtcGmtPeruString(event.get(rowv2 + 1).getGps_Date()), "null", "null", event.get(rowv2).getSpedd(), odometer});
                        }
                    } else {
                        int odometer = event.get(rowv2).getOdometer() != null ? event.get(rowv2).getOdometer() : 0;
                        listTravels.add(new Object[]{event.get(rowv2).getId_Travel(), event.get(rowv2).getName_Travel(),
                                "null", "null", event.get(rowv2).getLatitude() + "," + event.get(rowv2).getLongitude(),
                                Util.convertDateUtcGmtPeruString(event.get(rowv2).getGps_Date()), "null", "null", event.get(rowv2).getSpedd(), odometer});

                    }
                }
            } else {
                break;
            }
            if (rowv3 > 0) {
                rowv2 += 2;
                rowv3 = 0;
            } else {
                rowv2++;
            }
            int l = 0;
            listTravelsDto.add(new TravelListDto(listTravels.get(l)[0].toString(), listTravels.get(l)[1].toString(), listTravels.get(l)[2].toString(),
                    listTravels.get(l)[3].toString(), listTravels.get(l)[4].toString(), listTravels.get(l)[5].toString(),
                    listTravels.get(l)[6].toString(), listTravels.get(l)[7].toString(), listTravels.get(l)[8].toString(), listTravels.get(l)[9].toString(),nvecesCantidadViajes));
            listTravels.clear();
        }
        return listTravelsDto;
    }
}
