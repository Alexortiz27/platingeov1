package com.backend.ingest.dtos.Reports.jsons;

public class GeozoneDto {

    private String geozone_id;
    private String type;
    private  String gpsDate;
    private double speed;
    private Integer odometer;

    public GeozoneDto(String geozone_id, String type, String gpsDate, double speed, Integer odometer) {
        this.geozone_id = geozone_id;
        this.type = type;
        this.gpsDate = gpsDate;
        this.speed = speed;
        this.odometer = odometer;
    }

    public String getGeozone_id() {
        return geozone_id;
    }

    public void setGeozone_id(String geozone_id) {
        this.geozone_id = geozone_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public Integer getOdometer() {
        return odometer;
    }

    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }
}
