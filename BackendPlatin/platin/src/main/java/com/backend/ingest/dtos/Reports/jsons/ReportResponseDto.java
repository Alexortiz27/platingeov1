/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos.Reports.jsons;

/**
 *
 * @author FAMLETO
 */
public class ReportResponseDto {

    private String [] urls;
    
    private String response;
    
    private String user_id;

    private String headers;

    private String body;


      /**
     * @return the urls
     */
    public String[] getUrls() {
        return urls;
    }

    /**
     * @param urls the urls to set
     */
    public void setUrls(String[] urls) {
        this.urls = urls;
    }

    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return the user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }



}
