package com.backend.ingest.dtos.Reports.jsons;

public class StoppedDto
{
   private String creationDate;
   private String gpsDate;
   private double latitude;
   private double longitude;
   private double speed;
   private int odometer;
   private String type;

    public StoppedDto(String creationDate, String gpsDate, double latitude, double longitude, double speed, int odometer, String type) {
        this.creationDate = creationDate;
        this.gpsDate = gpsDate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.odometer = odometer;
        this.type = type;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
