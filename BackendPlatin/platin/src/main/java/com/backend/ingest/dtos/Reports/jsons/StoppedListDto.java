package com.backend.ingest.dtos.Reports.jsons;

import lombok.Data;

@Data
public class StoppedListDto {
    private String creationDate;
    private String gpsDate;
    private String gpsDateEnd;
    private String TimeStopped;
    private String latitude;
    private String longitude;
    private String speed;
    private String odometer;
private String nvecesStopped;
    public StoppedListDto(String creationDate, String latitude, String longitude, String gpsDate, String gpsDateEnd, String timeStopped, String speed, String odometer,String nvecesStopped) {
        this.creationDate = creationDate;
        this.gpsDate = gpsDate;
        this.gpsDateEnd = gpsDateEnd;
        TimeStopped = timeStopped;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.odometer = odometer;
        this.nvecesStopped=nvecesStopped;
    }

    public StoppedListDto() {
    }
}
