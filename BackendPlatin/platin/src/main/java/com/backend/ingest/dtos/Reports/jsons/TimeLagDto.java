package com.backend.ingest.dtos.Reports.jsons;

public class TimeLagDto
{
   private String receiveDate;
   private String gpsDate;
   private  double latitude;
   private double longitude;
   private  double speed;
   private int odometer;

    public TimeLagDto(String receiveDate, String gpsDate, double latitude, double longitude, double speed, int odometer) {
        this.receiveDate = receiveDate;
        this.gpsDate = gpsDate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.odometer = odometer;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }
}
