package com.backend.ingest.dtos.Reports.jsons;

public class TravelDto {

    private String gpsDate;
    private String type;
    private double latitude;
    private double longitude;
    private String travel_id;
    private String nameTravel;
    private double speed;
    private int  odometer;

    public TravelDto(String gpsDate, String type, double latitude, double longitude, String travel_id, String nameTravel, double speed, int odometer) {
        this.gpsDate = gpsDate;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.travel_id = travel_id;
        this.nameTravel = nameTravel;
        this.speed = speed;
        this.odometer = odometer;
    }

    public String getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTravel_id() {
        return travel_id;
    }

    public void setTravel_id(String travel_id) {
        this.travel_id = travel_id;
    }

    public String getNameTravel() {
        return nameTravel;
    }

    public void setNameTravel(String nameTravel) {
        this.nameTravel = nameTravel;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }
}
