package com.backend.ingest.dtos.Reports.jsons;

import lombok.Data;

@Data
public class TravelListDto {

    private String gpsDate;
    private String gpsDateEnd;
    private String type;
    private String startCoords;
    private String endCoords;
    private String travel_id;
    private String nameTravel;
    private String speed;
    private String  odometer;
    private String timeTravels;
    private String kmdistance;
    private  int travelQuantities;

    public TravelListDto(String travel_id, String nameTravel, String startCoords, String gpsDate,String endCoords,String gpsDateEnd,String timeTravels,String kmdistance, String speed, String odometer,int travelQuantities) {
        this.gpsDate = gpsDate;
        this.gpsDateEnd = gpsDateEnd;
        this.startCoords = startCoords;
        this.endCoords = endCoords;
        this.travel_id = travel_id;
        this.nameTravel = nameTravel;
        this.speed = speed;
        this.odometer = odometer;
        this.timeTravels = timeTravels;
        this.kmdistance = kmdistance;
        this.travelQuantities =travelQuantities;
    }


}
