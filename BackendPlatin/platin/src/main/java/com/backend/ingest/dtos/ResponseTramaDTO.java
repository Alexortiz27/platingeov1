package com.backend.ingest.dtos;

import com.backend.platin.exceptions.DataNotFoundException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.UUID;

public class ResponseTramaDTO {

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date timestamp = new Date();
    private String id = UUID.randomUUID().toString();
    private String request_ip = InetAddress.getLocalHost().getHostAddress();
    private String suggestion;
    private Object status;
    private Object content;

    public ResponseTramaDTO(String s, Object badRequest, Object trama) throws UnknownHostException {
        this.suggestion = s;
        this.status = badRequest;
        this.content = trama;
    }

    public ResponseTramaDTO(Object e, Object badRequest, Object tramaShow) throws UnknownHostException {
        this.suggestion = (String) e;
        this.status = badRequest;
        this.content = tramaShow;

    }


    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequest_ip() {
        return request_ip;
    }

    public void setRequest_ip(String request_ip) {
        this.request_ip = request_ip;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
