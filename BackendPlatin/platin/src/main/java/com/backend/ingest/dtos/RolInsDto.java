package com.backend.ingest.dtos;

import com.backend.platin.validators.IUniqueNameRol;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RolInsDto {

    @Column(unique = true)
    @IUniqueNameRol
    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String name;

    private Set<String> permits = new HashSet<>();

    public RolInsDto(String name, Set<String> permits) {
        this.name = name;
        this.permits = permits;
    }

    public Set<String> getPermits() {
        return permits;
    }

    public void setPermits(Set<String> permits) {
        this.permits = permits;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
