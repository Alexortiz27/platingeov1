/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;
import java.util.Date;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author FAMLETO
 */
public class TrackingTramaDto {


    private String plate;

    private String latitude;
    
    private String longitude;

    private String gpsDate;

    private double speed;

    private double odometer;
    
    public TrackingTramaDto(
             String plate,
            double latitude, double longitude,
            Date gpsDate, double speed,double odometer) {

        this.plate = plate;
        this.latitude=String.valueOf(latitude);
        this.longitude=String.valueOf(longitude);
        this.gpsDate = Util.parseDatetoRestClientFormatString(gpsDate);
        this.speed = speed;
        this.odometer= odometer;
    }

    public TrackingTramaDto() {

    }

    public String getPlate() {
        return plate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }

    
    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    /**
     * @return the odometer
     */
    public double getOdometer() {
        return odometer;
    }

    /**
     * @param odometer the odometer to set
     */
    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }

    /**
     * @return the gpsDate
     */
    public String getGpsDate() {
        return gpsDate;
    }

    /**
     * @param gpsDate the gpsDate to set
     */
    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }
    
      /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
