/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;

import java.util.Date;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;

/**
 * @author FAMLETO
 */
public class TramaInDto {

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "El evento "+Util.EMPTY_FIELD_MESSAGE)
    private String event;

    @Size(min = 7, message = "El tamaño requerido de la placa es de 7 caracteres")
    @NotEmpty(message = "La placa "+Util.EMPTY_FIELD_MESSAGE)
    private String plate;

    @NotNull(message = "La posición "+Util.EMPTY_FIELD_MESSAGE)
    private PositionDto position;
   
    @NotEmpty(message = "La fecha del gps "+Util.EMPTY_FIELD_MESSAGE)
    private String gpsDate;

//    @NotNull(message = "La velocidad "+Util.EMPTY_FIELD_MESSAGE)
//    @Digits(integer=3, fraction=2)
    private Object speed;

    private Date receiveDate;

    @Size(min = 2, message = "El tamaño requerido del Token es de 2 caracteres")
    @NotEmpty(message = "El token "+Util.EMPTY_FIELD_MESSAGE)
    private String tokenTrama;

    @NotNull(message = "El odometro "+Util.EMPTY_FIELD_MESSAGE)
//    @Max(value=10, message="El odometro debe ser máximo de 10 dígitos")
    private double odometer;

    private String ru_gps_date;

    public TramaInDto() {

    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }


    public String getRu_gps_date() {
        return ru_gps_date;
    }

    public void setRu_gps_date(String ru_gps_date) {
        this.ru_gps_date = ru_gps_date;
    }

    public String getTokenTrama() {
        return tokenTrama;
    }

    public void setTokenTrama(String tokenTrama) {
        this.tokenTrama = tokenTrama;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    public String getPlate() {
        return plate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }

    /**
     * @return the position
     */
    public PositionDto getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(PositionDto position) {
        this.position = position;
    }

    public Object getSpeed() {
        return speed;
    }

    public void setSpeed(Object speed) {
        this.speed = speed;
    }

    /**
     * @return the odometer
     */
    public double getOdometer() {
        return odometer;
    }

    /**
     * @param odometer the odometer to set
     */
    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }

    /**
     * @return the gpsDate
     */
    public String getGpsDate() {
        return gpsDate;
    }

    /**
     * @param gpsDate the gpsDate to set
     */
    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }

}
