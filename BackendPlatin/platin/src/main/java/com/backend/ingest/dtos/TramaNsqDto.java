/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.exceptions.MyJsonDateDeserializer;
import com.backend.platin.util.Util;
import com.backend.platin.validators.IUniqueDateUtc;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Date;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author FAMLETO
 */
public class TramaNsqDto {

    private String event;

    private String plate;

    private PositionDto position;

    private Date gpsDate;

    private Double speed;

    private Date receiveDate;

    private String tokenTrama;

    private double odometer;

    private String ru_gps_date;

    public TramaNsqDto(String event, String plate,
                       PositionDto position,
                       Date gpsDate, double speed, Date receiveDate, String tokenTrama, double odometer,String ru_gps_date
    ) {

        this.event = event;
        this.plate = plate;
        this.position = position;
        this.gpsDate = gpsDate;
        this.speed = speed;
        this.receiveDate = receiveDate;
        this.tokenTrama = tokenTrama;
        this.odometer = odometer;
        this.ru_gps_date=ru_gps_date;
    }
    public TramaNsqDto() {
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public PositionDto getPosition() {
        return position;
    }

    public void setPosition(PositionDto position) {
        this.position = position;
    }

    public Date getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(Date gpsDate) {
        this.gpsDate = gpsDate;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getTokenTrama() {
        return tokenTrama;
    }

    public void setTokenTrama(String tokenTrama) {
        this.tokenTrama = tokenTrama;
    }

    public double getOdometer() {
        return odometer;
    }

    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }

    public String getRu_gps_date() {
        return ru_gps_date;
    }

    public void setRu_gps_date(String ru_gps_date) {
        this.ru_gps_date = ru_gps_date;
    }
}
