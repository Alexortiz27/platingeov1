/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.platin.util.Util;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * @author FAMLETO
 */
public class TramaRediDto {


    private String event;


    private String plate;


    private PositionDto position;

    private String gpsDate;

    private double speed;

    private String receiveDate;

    private String tokenTrama;

    private double odometer;

    private String ru_gps_date;

    public TramaRediDto(String event, String plate,
                        PositionDto position,
                        String gpsDate, double speed, String receiveDate, String tokenTrama, double odometer, String ru_gps_date
    ) {

        this.event = event;
        this.plate = plate;
        this.position = position;
        this.gpsDate = gpsDate;
        this.speed = speed;
        this.receiveDate = receiveDate;
        this.tokenTrama = tokenTrama;
        this.odometer = odometer;
        this.ru_gps_date=ru_gps_date;
    }

    public TramaRediDto() {

    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }
    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public String getRu_gps_date() {
        return ru_gps_date;
    }

    public void setRu_gps_date(String ru_gps_date) {
        this.ru_gps_date = ru_gps_date;
    }

    public String getTokenTrama() {
        return tokenTrama;
    }

    public void setTokenTrama(String tokenTrama) {
        this.tokenTrama = tokenTrama;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    public String getPlate() {
        return plate;
    }

    public String getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }

    /**
     * @return the position
     */
    public PositionDto getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(PositionDto position) {
        this.position = position;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    /**
     * @return the odometer
     */
    public double getOdometer() {
        return odometer;
    }

    /**
     * @param odometer the odometer to set
     */
    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }

}
