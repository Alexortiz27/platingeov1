package com.backend.ingest.dtos;

import com.backend.platin.util.Util;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

public class TramaShowDTO implements Serializable{

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "El evento "+ Util.EMPTY_FIELD_MESSAGE)
    private String event;

    @Size(min = 7, message = "El tamaño requerido de la placa es de 7 caracteres")
    @NotEmpty(message = "La placa "+Util.EMPTY_FIELD_MESSAGE)
    private String plate;

//    @NotNull(message = "La velocidad "+Util.EMPTY_FIELD_MESSAGE)
//    @Digits(integer=3, fraction=2)
    private Object speed;

    @NotNull(message = "La posición "+Util.EMPTY_FIELD_MESSAGE)
    private PositionDto position;

    @NotEmpty(message = "La fecha del gps "+Util.EMPTY_FIELD_MESSAGE)
    private String gpsDate;

    @NotNull(message = "El odometer "+Util.EMPTY_FIELD_MESSAGE)
    private double odometer;

    @Size(min = 2, message = "El tamaño requerido del Token es de 2 caracteres")
    @NotEmpty(message = "El token "+Util.EMPTY_FIELD_MESSAGE)
    private String tokenTrama;


    public TramaShowDTO(String event, String plate, Object speed, PositionDto position, String gpsDate, double odometer, String tokenTrama) {
        this.event = event;
        this.plate = plate;
        this.speed = speed;
        this.position = position;
        this.gpsDate = gpsDate;
        this.odometer = odometer;
        this.tokenTrama = tokenTrama;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Object getSpeed() {
        return speed;
    }

    public void setSpeed(Object speed) {
        this.speed = speed;
    }

    public PositionDto getPosition() {
        return position;
    }

    public void setPosition(PositionDto position) {
        this.position = position;
    }

    public String getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(String gpsDate) {
        this.gpsDate = gpsDate;
    }

    public double getOdometer() {
        return odometer;
    }

    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }

    public String getTokenTrama() {
        return tokenTrama;
    }

    public void setTokenTrama(String tokenTrama) {
        this.tokenTrama = tokenTrama;
    }
}
