package com.backend.ingest.dtos;

import com.backend.platin.validators.IUniqueName;
import java.util.Date;
import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class TravelInsDto {

    //@Column(unique = false)
    @IUniqueName
    @Size(min = 2, message = "required size is 2 characters")
    @NotEmpty(message = "cannot be empty")
    private String name;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String vehicle_id;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String geozona_id;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String statusTravel;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String schudeledTime;

    private Date startDate;

    public TravelInsDto(String name, String vehicle_id, String geozona_id, String statusTravel, String schudeledTime, Date startDate) {
        this.name = name;
        this.vehicle_id = vehicle_id;
        this.geozona_id = geozona_id;
        this.statusTravel = statusTravel;
        this.schudeledTime = schudeledTime;

        this.startDate = startDate;
    }

    public TravelInsDto() {
    }

    public String getSchudeledTime() {
        return schudeledTime;
    }

    public void setSchudeledTime(String schudeledTime) {
        this.schudeledTime = schudeledTime;
    }

    public String getGeozona_id() {
        return geozona_id;
    }

    public void setGeozona_id(String geozona_id) {
        this.geozona_id = geozona_id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getStatusTravel() {
        return statusTravel;
    }

    public void setStatusTravel(String statusTravel) {
        this.statusTravel = statusTravel;
    }

}
