/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 *
 * @author Propietario
 */
public interface TravelInterfReportDto {

    public String getId();

    public String getName();

    public String getVehicle_travel_id();

    public String getStatus_travel();

    public String getSchudeled_time();

    public String getGeozona_id();

    public Date getStart_date();

    public Date getEnd_date();



}
