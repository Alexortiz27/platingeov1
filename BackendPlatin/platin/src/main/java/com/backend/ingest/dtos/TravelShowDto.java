/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Vehicle;
import com.backend.platin.util.Util;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author FAMLETO
 */
public class TravelShowDto {

    private String id;

    @Column(name = "startGeozone")
    private String startGeozone;

    @Column(name = "endGeozone")
    private String endGeozone;

    @Column(name = "name")
    @NotEmpty(message = "the name field cannot be empty")
    private String name;

    @Column(name = "startDate")
    private Date startDate;

    @Column(name = "endDate")
    private Date endDate;

    @NotEmpty(message = "the statusTravel field cannot be empty")
    @Column(name = "statusTravel")
    private String statusTravel;

    private String schudeledTime;

    @Column(name = "creationDate")
    private Date creationDate;

    @Column(name = "status")
    private boolean status;

    private VehicleDto vehicle;

    private Geozone geozonaTravel;

    public TravelShowDto() {

    }

    public TravelShowDto(String id, String startGeozone, String endGeozone, String name, Date startDate, Date endDate, String statusTravel, String schudeledTime, VehicleDto vehicle, Geozone geozonaTravel) {
        this.id = id;
        this.startGeozone=startGeozone;

        this.endGeozone= endGeozone;

        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.statusTravel = statusTravel;
        this.schudeledTime = schudeledTime;
        this.creationDate = new Date();
        this.status = Util.ACTIVE_STATUS;
        this.vehicle = vehicle;
        this.geozonaTravel = geozonaTravel;
    }

    public String getSchudeledTime() {
        return schudeledTime;
    }

    public void setSchudeledTime(String schudeledTime) {
        this.schudeledTime = schudeledTime;
    }

    public Geozone getGeozonaTravel() {
        return geozonaTravel;
    }

    public void setGeozonaTravel(Geozone geozonaTravel) {
        this.geozonaTravel = geozonaTravel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartGeozone() {
        return startGeozone;
    }

    public void setStartGeozone(String startGeozone) {
        this.startGeozone = startGeozone;
    }

    public String getEndGeozone() {
        return endGeozone;
    }

    public void setEndGeozone(String endGeozone) {
        this.endGeozone = endGeozone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatusTravel() {
        return statusTravel;
    }

    public void setStatusTravel(String statusTravel) {
        this.statusTravel = statusTravel;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the vehicle
     */
    public VehicleDto getVehicle() {
        return vehicle;
    }

    /**
     * @param vehicle the vehicle to set
     */
    public void setVehicle(VehicleDto vehicle) {
        this.vehicle = vehicle;
    }

}
