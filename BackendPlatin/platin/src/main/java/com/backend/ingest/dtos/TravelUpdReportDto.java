/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.Date;
import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author Propietario
 */
public class TravelUpdReportDto {

    private String id;

    @NotEmpty(message = "the name field cannot be empty")
    private String name;

    @NotEmpty(message = "the vehicle_id field cannot be empty")
    private String vehicle_id;

    @NotEmpty(message = "the statusTravel field cannot be empty")
    private String statusTravel;

    private String schudeledTime;

    @NotEmpty(message = "the geozona_id field cannot be empty")
    private String geozona_id;

    @Column(name = "startDate")
    private Date startDate;

    @Column(name = "endDate")
    private Date endDate;

    public TravelUpdReportDto() {
    }

    public TravelUpdReportDto(String id, String name, String vehicle_id, String geozona_id, String statusTravel,String schudeledTime,  Date startDate, Date endDate) {
        this.id = id;
        this.name = name;
        this.vehicle_id = vehicle_id;
        this.geozona_id = geozona_id;
        this.statusTravel = statusTravel;
        this.schudeledTime =schudeledTime;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getSchudeledTime() {
        return schudeledTime;
    }

    public void setSchudeledTime(String schudeledTime) {
        this.schudeledTime = schudeledTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGeozona_id() {
        return geozona_id;
    }

    public void setGeozona_id(String geozona_id) {
        this.geozona_id = geozona_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getStatusTravel() {
        return statusTravel;
    }

    public void setStatusTravel(String statusTravel) {
        this.statusTravel = statusTravel;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
