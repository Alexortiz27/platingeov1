/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import lombok.Data;

/**
 *
 * @author FAMLETO
 */
public interface TypeEvent {
    
    public String type();
}
