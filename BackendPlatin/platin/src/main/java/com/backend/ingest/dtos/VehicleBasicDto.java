/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.Date;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author FAMLETO
 */
public class VehicleBasicDto {

    
        private String id;

	private String plate;
	
	private String category;
	
	
	private String code_osinergmin;
	
	private Boolean status;

	private Date creationDate;

    public VehicleBasicDto(String id, String plate, String category, String code_osinergmin, Boolean status, Date creationDate) {
        this.id = id;
        this.plate = plate;
        this.category = category;

        this.code_osinergmin = code_osinergmin;
        this.status = status;
        this.creationDate = creationDate;
    }
        
        /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the plate
     */
    public String getPlate() {
        return plate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

  
    public String getCode_osinergmin() {
        return code_osinergmin;
    }

    /**
     * @param code_osinergmin the code_osinergmin to set
     */
    public void setCode_osinergmin(String code_osinergmin) {
        this.code_osinergmin = code_osinergmin;
    }

    /**
     * @return the status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
