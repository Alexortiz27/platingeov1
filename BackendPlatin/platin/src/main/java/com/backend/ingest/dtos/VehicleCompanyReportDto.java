package com.backend.ingest.dtos;

public class VehicleCompanyReportDto {

    String code_osinergmin;

    String nameemv;

    Boolean status;

     String nameclient;

    public VehicleCompanyReportDto(String code_osinergmin, String nameemv, Boolean status, String nameclient) {
        this.code_osinergmin = code_osinergmin;
        this.nameemv = nameemv;
        this.status = status;
        this.nameclient = nameclient;
    }

    public VehicleCompanyReportDto() {
    }

    public String getcode_osinergmin() {
        return code_osinergmin;
    }

    public void setcode_osinergmin(String code_osinergmin) {
        this.code_osinergmin = code_osinergmin;
    }

    public String getNameemv() {
        return nameemv;
    }

    public void setNameemv(String nameemv) {
        this.nameemv = nameemv;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getNameclient() {
        return nameclient;
    }

    public void setNameclient(String nameclient) {
        this.nameclient = nameclient;
    }
}