/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import java.util.Date;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author FAMLETO
 */
public class VehicleDto {

    private String id;

    @NotEmpty(message = "the plate field cannot be empty")
    private String plate;

    @NotEmpty(message = "the category field cannot be empty")
    private String category;



    @NotEmpty(message = "the code_osinergmin field cannot be empty")
    private String code_osinergmin;

    private Boolean status;

    private Date creationDate;

    private CompanyClientListDto companyClient;

    public VehicleDto(String id, @NotEmpty(message = "the plate field cannot be empty") String plate,
            @NotEmpty(message = "the category field cannot be empty") String category,

            @NotEmpty(message = "the code_osinergmin field cannot be empty") String code_osinergmin,
            CompanyClientListDto company, Boolean status,
            Date creationDate) {

        this.id = id;
        this.plate = plate;
        this.category = category;

        this.code_osinergmin = code_osinergmin;
        this.companyClient = company;
        this.status = status;
        this.creationDate = new Date();
    }

    public VehicleDto() {

    }

    public String getcode_osinergmin() {
        return code_osinergmin;
    }

    public void setcode_osinergmin(String code_osinergmin) {
        this.code_osinergmin = code_osinergmin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

  

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the companyClient
     */
    public CompanyClientListDto getCompanyClient() {
        return companyClient;
    }

    /**
     * @param companyClient the companyClient to set
     */
    public void setCompanyClient(CompanyClientListDto companyClient) {
        this.companyClient = companyClient;
    }

}
