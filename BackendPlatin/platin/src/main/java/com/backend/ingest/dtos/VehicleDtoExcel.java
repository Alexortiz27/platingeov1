/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.dtos;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Propietario
 */
@Data
public class VehicleDtoExcel extends BaseRowModel {

    @ExcelProperty(value = "plate", index = 0)
    private String plate;//destino

    @ExcelProperty(value = "category", index = 1)
    private String category;

    @ExcelProperty(value = "code_osinergmin", index = 2)
    private String code_osinergmin;

    @ExcelProperty(value = "nameCompany", index = 3)
    private String nameCompany;

    @ExcelProperty(value = "nameEMV", index = 4)
    private String nameEMV;

    @ExcelProperty(value = "tokenEmv", index = 5)
    private String tokenEmv;

    @ExcelProperty(value = "creationDate", index = 6)
    private Date creationDate;

    @ExcelProperty(value = "statusVehicle", index = 7)
    private String statusVehicle;

    @ExcelProperty(value = "statusCliente", index = 8)
    private String statusCliente;

    @ExcelProperty(value = "statusEmv", index = 9)
    private String statusEmv;

    public VehicleDtoExcel() {
    }

    public VehicleDtoExcel(String plate, String category, String code_osinergmin, String nameCompany, String nameEMV, String tokenEmv, Date creationDate
    ,String statusVehicle,String statusCliente,String statusEmv) {
        this.plate = plate;
        this.category = category;
        this.code_osinergmin = code_osinergmin;
        this.nameCompany = nameCompany;
        this.nameEMV = nameEMV;
        this.tokenEmv = tokenEmv;
        this.creationDate = creationDate;
        this.statusVehicle=statusVehicle;
        this.statusCliente=statusCliente;
        this.statusEmv=statusEmv;
    }



}
