package com.backend.ingest.dtos;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class VehicleInsDto {

    private String id;

    @Size(min = 7, message = "su tamaño requerido es de 7 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String plate;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String category;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String code_osinergmin;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String id_company_client;

    private Boolean status;

    private Date creationDate;

    public VehicleInsDto(String id, String plate,
            String category,
            String code_osinergmin,
            String id_Company_client, Boolean status,
            Date creationDate) {

        this.id = id;
        this.plate = plate;
        this.category = category;

        this.code_osinergmin = code_osinergmin;
        this.id_company_client = id_Company_client;
        this.status = status;
        this.creationDate = new Date();
    }

    public VehicleInsDto() {

    }

    public String getcode_osinergmin() {
        return code_osinergmin;
    }

    public void setcode_osinergmin(String code_osinergmin) {
        this.code_osinergmin = code_osinergmin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the id_company_client
     */
    public String getId_company_client() {
        return id_company_client;
    }

    /**
     * @param id_company_client the id_company_client to set
     */
    public void setId_company_client(String id_company_client) {
        this.id_company_client = id_company_client;
    }

}
