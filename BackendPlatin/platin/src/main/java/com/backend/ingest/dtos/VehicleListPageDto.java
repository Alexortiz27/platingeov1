package com.backend.ingest.dtos;

import java.util.Date;
import lombok.Data;

@Data
public class VehicleListPageDto {

    public Object code_osinergmin;

    public Object id;

    public Object plate;

    public Object category;

    public Object status;

    public Object creation_date;

    public Object nameClient;
    
    public Object company_clientid;

    public VehicleListPageDto(Object code_osinergmin, Object id, Object plate, Object category, Object status, Object creation_date, Object nameClient, Object company_clientid) {
        this.code_osinergmin = code_osinergmin;
        this.id = id;
        this.plate = plate;
        this.category = category;
        this.status = status;
        this.creation_date = creation_date;
        this.nameClient = nameClient;
        this.company_clientid = company_clientid;
    }
    
    
}
