package com.backend.ingest.dtos;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class VehicleUpdDto {

    private Integer id;

    @Size(min = 7, message = "su tamaño requerido es de 7 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String plate;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String category;


    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String code_osinergmin;

    @Size(min = 2, message = "su tamaño requerido es de 2 caracteres")
    @NotEmpty(message = "no puede estar vacío")
    private String id_company;

    private Boolean status;

    private Date creationDate;

    public VehicleUpdDto(String plate,String category,String code_osinergmin, String id_companyClient, Boolean status,
            Date creationDate) {

        this.plate = plate;
        this.category = category;

        this.code_osinergmin = code_osinergmin;
        this.id_company = id_companyClient;
        this.status = status;
        this.creationDate = new Date();
    }

    public VehicleUpdDto() {

    }

    public String getcode_osinergmin() {
        return code_osinergmin;
    }

    public void setcode_osinergmin(String code_osinergmin) {
        this.code_osinergmin = code_osinergmin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

 

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = new Date();
    }

    public String getId_company() {
        return id_company;
    }

    public void setId_company(String id_company) {
        this.id_company = id_company;
    }

}
