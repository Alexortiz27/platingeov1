/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.entities;

import com.backend.platin.util.Util;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(
            name = "uuid",
            strategy = "uuid2"
    )
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "ingestToken")
    private String ingestToken;

    @Column(name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "creationDate")
    private Date creationDate;

    @Column(name = "status")
    private boolean status;

//    
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "historialCompany",joinColumns = @JoinColumn(name = "company_id"),
//    inverseJoinColumns = @JoinColumn(name = "company_client_id"))
//    private List<CompanyClient> companyClient;
    @OneToMany(mappedBy = "company")
    private List<Historial_Company> historial = new ArrayList<>();
    
//    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
//    private List<Historial_Company> historial = new ArrayList<>();


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public Company(String name, String ingestToken, String email, String address, String phone, String city, String country) {
        this.name = name;
        this.ingestToken = ingestToken;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.city = city;
        this.country = country;
        this.creationDate = new Date();
        this.status = Util.ACTIVE_STATUS;
    }

    public Company() {

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
 
    }

    public void setUser_id(User user_id) {
        this.user = user_id;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the status
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the ingestToken
     */
    public String getIngestToken() {
        return ingestToken;
    }

    /**
     * @param ingestToken the ingestToken to set
     */
    public void setIngestToken(String ingestToken) {
        this.ingestToken = ingestToken;
    }

    public List<Historial_Company> getHistorial() {
        return historial;
    }

    /**
     * @param historial the historial to set
     */
    public void setHistorial(List<Historial_Company> historial) {
        this.historial = historial;
    }

}
