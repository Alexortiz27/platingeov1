package com.backend.ingest.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import com.backend.platin.util.Util;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;

@Entity
@Table(name = "companyClient")
public class CompanyClient {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(
            name = "uuid",
            strategy = "uuid2"
    )
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "ruc")
    private String ruc;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "creationDate")

    private Date creationDate;

    @Column(name = "status")
    private boolean status;

//    @NotNull
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "historialCompany" ,joinColumns = @JoinColumn(name = "company_client_id"),
//    inverseJoinColumns = @JoinColumn(name = "company_id"))
//    private Set<Company> company = new HashSet<>();
//      @OneToMany(fetch = FetchType.LAZY, mappedBy="companyClient", cascade = CascadeType.ALL )
//      private List<Historial_Company> historial = new ArrayList<>();
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @OneToMany(fetch = FetchType.EAGER, mappedBy="companyClient", cascade = CascadeType.ALL )
    private List<Historial_Company> historial = new ArrayList<>();

    public CompanyClient(String name, String email, String ruc, String address, String phone
    ) {
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.creationDate = new Date();
        this.status = Util.ACTIVE_STATUS;
    }

    public CompanyClient(String id, String name, String email, String ruc, String address, String phone, Date creationDate, boolean status) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.creationDate = creationDate;
        this.status = status;
    }

    public CompanyClient() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

//    public Set<Company> getCompany() {
//        return company;
//    }
//
//    public void setCompany(Set<Company> company) {
//        this.company = company;
//    }
    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the historial
     */
    public List<Historial_Company> getHistorial() {
        return historial;
    }

    /**
     * @param historial the historial to set
     */
    public void setHistorial(List<Historial_Company> historial) {
        this.historial = historial;
    }

}
