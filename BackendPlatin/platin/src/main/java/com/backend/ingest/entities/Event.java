package com.backend.ingest.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;

import com.backend.platin.util.Util;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;

    @Column(name = "type")
    @NotEmpty(message = "the type field cannot be empty")
    private String type;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "vehicle_id", nullable = false)
    private Vehicle vehicle_id;
    
//        
//    @Column(name = "vehicle_id")
//    @NotEmpty(message = "the vehicle_id field cannot be empty")
//    private String vehicle_id;

    @Column(name = "geozone_id")
    @NotEmpty(message = "the geozone_id field cannot be empty")
    private String geozone_id;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "receiveDate")
    private Date receiveDate;

    @Column(name = "gpsDate")
    private Date gpsDate;

    @Column(name = "spedd")
    private double spedd;
    
    @Column(name="odometer",nullable = true)
    private Integer odometer;

    @Column(name = "idTravel")
    private String idTravel;

    @Column(name = "nameTravel")
    private String nameTravel;

    @Column(name = "creationDate")
    private Date creationDate;

    @Column(name = "status")
    private boolean status;
    
    public Event() {

    }

    public Event(String type,String geozone_id, double latitude, double longitude, Date receiveDate, Date gpsDate, double spedd, String idTravel, String nameTravel,Integer odometer) {
        this.id = id;
        this.type = type;
        this.geozone_id = geozone_id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.receiveDate = receiveDate;
        this.gpsDate = gpsDate;
        this.spedd = spedd;
        this.odometer=odometer;
        this.idTravel = idTravel;
        this.nameTravel = nameTravel;
        this.creationDate = new Date();
        this.status = Util.ACTIVE_STATUS;
    }

    public String getIdTravel() {
        return idTravel;
    }

    public void setIdTravel(String idTravel) {
        this.idTravel = idTravel;
    }

    public String getNameTravel() {
        return nameTravel;
    }

    public void setNameTravel(String nameTravel) {
        this.nameTravel = nameTravel;
    }

    public double getSpedd() {
        return spedd;
    }

    public void setSpedd(double spedd) {
        this.spedd = spedd;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public Date getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(Date gpsDate) {
        this.gpsDate = gpsDate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Vehicle getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(Vehicle vehicle_id) {
        this.vehicle_id = vehicle_id;
    }



    public String getGeozone_id() {
        return geozone_id;
    }

    public void setGeozone_id(String geozone_id) {
        this.geozone_id = geozone_id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the odometer
     */
    public Integer getOdometer() {
        return odometer;
    }

    /**
     * @param odometer the odometer to set
     */
    public void setOdometer(Integer odometer) {
        this.odometer = odometer;
    }

}
