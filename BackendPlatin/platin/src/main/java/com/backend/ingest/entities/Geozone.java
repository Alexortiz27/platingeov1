/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.entities;

import com.backend.ingest.dtos.CoordInDto;
import com.backend.platin.util.Util;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.Gson;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

/**
 *9
 * @author FAMLETO
 */
@Entity
@Table(name = "geozone")
public class Geozone {
    
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(
            name = "uuid",
            strategy = "uuid2"
    )
    private String id;
    
    @Column(name="category")
    private String category;
    
    @Column(name="radius")
    private double radius;

    @Column(name="coords",columnDefinition = "NVARCHAR(MAX)")
    private String coords;
    
    @Column(name="type")
    private String type;
    
    @Column(name="name")
    private String name;
    
    @Column(name="creationDate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "America/Lima")
    private Date creationDate;
    
    @Column(name="status")
    private boolean status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idUser", nullable = true)
    private User user_id;

    public Geozone()
    {
    
    }
    
    public Geozone(String category, double radius,String type, List<CoordInDto> coordInDtos,String name) {
       this.category= category;
       this.radius=radius;
       this.type = type;
       this.coords = new Gson().toJson(coordInDtos);       
       this.creationDate=new Date();
       this.status= Util.ACTIVE_STATUS;
       this.name=name;
    }

    public void setCoords(String coords) {
        this.coords = coords;
    }

    public boolean isStatus() {
        return status;
    }

    public User getUser_id() {
        return user_id;
    }

    public void setUser_id(User user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }



    /**
     * @return the status
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the coords
     */
    public String getCoords() {
        return coords;
    }

    /**
     * @param coords the coords to set
     */
    public void setCoords(List<CoordInDto> coords)
    {
        this.coords = new Gson().toJson(coords);    
    }

    public String getType() {
            return type;
    }

    public void setType(String type) {
            this.type = type;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

   
    
}
