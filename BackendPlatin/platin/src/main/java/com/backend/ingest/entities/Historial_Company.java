/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author FAMLETO
 */

@Entity
@Table(name = "historial_company")
public class Historial_Company implements Serializable {

    @Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_client_id")
    private CompanyClient companyClient;
    
//    @ManyToOne
//    @JoinColumn(name = "company_id")
//    private Company company;
//
//    @ManyToOne
//    @JoinColumn(name = "company_client_id")
//    private CompanyClient companyClient;

   
    @Column(name = "creationDate")
    private Date creationDate;

    public Historial_Company() {
    }
    
    /**
     * @return the company
     */
    public Company getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(Company company) {
        this.company = company;
    }

    /**
     * @return the companyClient
     */
    public CompanyClient getCompanyClient() {
        return companyClient;
    }

    /**
     * @param companyClient the companyClient to set
     */
    public void setCompanyClient(CompanyClient companyClient) {
        this.companyClient = companyClient;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
