package com.backend.ingest.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class MainUser implements UserDetails {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String displayName;
	private String firstName;
	private String lastName;
	private String user;
	private String email;
	private String password;
	private Boolean status;
	private Date creationDate;
	private Collection<? extends GrantedAuthority> authorities;

	public MainUser(String displayName, String firstName, String lastName, String user, String email,
			String password, Boolean status, Date creationDate, Collection<? extends GrantedAuthority> authorities) {

		this.displayName = displayName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.user = user;
		this.email = email;
		this.password = password;
		this.status = status;
		this.creationDate = creationDate;
		this.authorities = authorities;
	}

	public MainUser() {
		
	}

	public static MainUser build(User User) {
		List<GrantedAuthority> authorities = User.getRoles().stream()
				.map(rol -> new SimpleGrantedAuthority(rol.getName())).collect(Collectors.toList());
		return new MainUser(User.getDisplayName(), User.getFirstName(), User.getLastName(), User.getUser(),
				User.getEmail(), User.getPassword(), User.getStatus(), User.getCreationDate(), authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return email;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

}
