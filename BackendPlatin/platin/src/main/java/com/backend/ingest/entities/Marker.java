/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.entities;

import com.backend.platin.util.Util;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author FAMLETO
 */
@Entity
@Table(name="marker")
public class Marker {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "geozoneId", nullable = false)
    private Geozone geozone;

    @Column(name="name")
    private String name;
    
    @Column(name="type")
    private String type;
    
    @Column(name="latitude")
    private double latitude;
    
    @Column(name="longitude")
    private double longitude;
    
    @Column(name="radius")
    private double radius ;
    
    @Column(name="zindex")
    private String zindex;
    
    @Column(name="status")
    private boolean status;
    
    @Column(name="creationDate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "America/Lima")
    private Date creationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idUser", nullable = true)
    private User user_id;

    public Marker(Geozone geozone,User user_id, String name, String type, double latitude, double longitude, double radius, String zindex) {
        this.geozone = geozone;
        this.user_id=user_id;
        this.name = name;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        this.zindex = zindex;
        this.status= Util.ACTIVE_STATUS;
        this.creationDate= new Date();
    }

    public Marker() {
    }

    public User getUser_id() {
        return user_id;
    }

    public void setUser_id(User user_id) {
        this.user_id = user_id;
    }
    
    
    

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * @return the zindex
     */
    public String getZindex() {
        return zindex;
    }

    /**
     * @param zindex the zindex to set
     */
    public void setZindex(String zindex) {
        this.zindex = zindex;
    }
    
    /**
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the geozone
     */
    public Geozone getGeozone() {
        return geozone;
    }

    /**
     * @param geozone the geozone to set
     */
    public void setGeozone(Geozone geozone) {
        this.geozone = geozone;
    }
    
}
