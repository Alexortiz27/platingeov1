package com.backend.ingest.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.GenericGenerator;

import com.backend.platin.util.Util;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "permits")
public class Permits {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    private Boolean status;

    @Column(name = "create_permits",nullable = true)
    private Boolean create_permits;

    @Column(name = "edit_permits",nullable = true)
    private Boolean edit_permits;

    @Column(name = "delete_permits",nullable = true)
    private Boolean delete_permits;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "America/Lima")
    private Date creationDate;

    public Permits(String name,Boolean create_permits,Boolean edit_permits,Boolean delete_permits) {
        this.name = name;
        this.status = Util.ACTIVE_STATUS;
        this.creationDate = new Date();
        this.create_permits=create_permits;
        this.edit_permits=edit_permits;
        this.delete_permits=delete_permits;
    }

    public Permits() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getCreate_permits() {
        return create_permits;
    }

    public void setCreate_permits(Boolean create_permits) {
        this.create_permits = create_permits;
    }

    public Boolean getEdit_permits() {
        return edit_permits;
    }

    public void setEdit_permits(Boolean edit_permits) {
        this.edit_permits = edit_permits;
    }

    public Boolean getDelete_permits() {
        return delete_permits;
    }

    public void setDelete_permits(Boolean delete_permits) {
        this.delete_permits = delete_permits;
    }
}
