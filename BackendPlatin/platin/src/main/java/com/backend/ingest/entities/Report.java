/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.entities;

import com.backend.platin.util.Util;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author FAMLETO
 */
@Entity
@Table(name = "report")
public class Report {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;

    @Column(name = "urlGeoJson")
    private String urlGeoJson;

    @Column(name = "urlXls")
    private String urlXls;

    @Column(name = "type")
    private String type;

    @Column(name = "estadoReport")
    private String estadoReport;

    @Column(name = "startDate")
    private Date startDate;

    @Column(name = "endDate")
    private Date endDate;

    @Column(name = "accion")
    private boolean accion;

    @Column(name = "status")
    private boolean status;

    @Column(name = "creationDate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "America/Lima")
    private Date creationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "companyid", nullable = false)
    private Company company;

    public Report(String urlGeoJson, String urlXls, String type, String estadoReport, Date startDate, Date endDate, boolean accion, Company company) {
        this.urlGeoJson = urlGeoJson;
        this.urlXls = urlXls;
        this.type = type;
        this.estadoReport = estadoReport;
        this.startDate = startDate;
        this.endDate = endDate;
        this.accion = accion;
        this.status = Util.ACTIVE_STATUS;
        this.creationDate = new Date();
        this.company = company;
    }



    public Report() {
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the urlGeoJson
     */
    public String getUrlGeoJson() {
        return urlGeoJson;
    }

    /**
     * @param urlGeoJson the urlGeoJson to set
     */
    public void setUrlGeoJson(String urlGeoJson) {
        this.urlGeoJson = urlGeoJson;
    }

    /**
     * @return the urlXls
     */
    public String getUrlXls() {
        return urlXls;
    }

    /**
     * @param urlXls the urlXls to set
     */
    public void setUrlXls(String urlXls) {
        this.urlXls = urlXls;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the company
     */
    public Company getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(Company company) {
        this.company = company;
    }

    public String getEstadoReport() {
        return estadoReport;
    }

    public void setEstadoReport(String estadoReport) {
        this.estadoReport = estadoReport;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isAccion() {
        return accion;
    }

    public void setAccion(boolean accion) {
        this.accion = accion;
    }

}
