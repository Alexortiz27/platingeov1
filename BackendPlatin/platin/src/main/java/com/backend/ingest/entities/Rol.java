package com.backend.ingest.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.GenericGenerator;

import com.backend.platin.util.Util;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "rol")
public class Rol {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;

    @NotEmpty(message = "it cant be empty")
    private String name;

    	@NotNull
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "rol_permits", joinColumns = @JoinColumn(name = "rol_id"),
    inverseJoinColumns = @JoinColumn(name = "permitsId"))
    private Set<Permits> permits = new HashSet<>();


    private Boolean status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "America/Lima")
    private Date creationDate;

    public Rol(String name) {
        this.name = name;
        this.status = Util.ACTIVE_STATUS;
        this.creationDate = new Date();
    }

    public Rol() {

    }

    public Set<Permits> getPermits() {
        return permits;
    }

    public void setPermits(Set<Permits> permits) {
        this.permits = permits;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

}
