/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.entities;

import com.backend.ingest.dtos.PositionDto;
import com.backend.platin.util.Util;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.Gson;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author FAMLETO
 */
@Entity
@Table(name = "trama")
public class Trama {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;

    @Column(name = "event")
    private String event;

    @Column(name = "plate")
    private String plate;

    @Column(name = "position")
    private String position;

    @Column(name = "gpsDate")
    private Date gpsDate;

    @Column(name = "speed")
    private double speed;

    @Column(name = "receiveDate")
    private Date receiveDate;

    @Column(name = "tokenTrama")
    private String tokenTrama;
    
    @Column(name = "status")
    private Boolean status;

    @Column(name = "creationDate")
    private Date creationDate;
    
    @Column(name="odometer")
    private double odometer;
  
    public Trama() {
    }

    public Trama(String event, String plate, PositionDto position, Date gpsDate, double speed, Date receiveDate,String tokenTrama,double odometer) {
        this.event = event;
        this.plate = plate;
        this.position = new Gson().toJson(position);
        this.gpsDate = new Timestamp(gpsDate.getTime());
        this.speed = speed;
        this.receiveDate = receiveDate;
        this.tokenTrama = tokenTrama;
        this.odometer=odometer;
        this.status = Util.ACTIVE_STATUS;
        this.creationDate = new Date();
      
    }
    
      public Trama(String event, String plate, PositionDto position, String gpsDate, double speed, Date receiveDate,String tokenTrama,double odometer) throws ParseException {
        this.event = event;
        this.plate = plate;
        this.position = new Gson().toJson(position);
        this.gpsDate = new Timestamp(Util.parseStringToDatetime(gpsDate).getTime());
        this.speed = speed;
        this.receiveDate = receiveDate;
        this.tokenTrama = tokenTrama;
        this.odometer=odometer;
        this.status = Util.ACTIVE_STATUS;
        this.creationDate = new Date();
      
    }
    
    
   

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getTokenTrama() {
        return tokenTrama;
    }

    public void setTokenTrama(String tokenTrama) {
        this.tokenTrama = tokenTrama;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the type
     */
    /**
     * @return the plate
     */
    public String getPlate() {
        return plate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }

    /**
     * @return the id_vehicle
     */
    public Date getGpsDate() {
        return gpsDate;
    }

    public void setGpsDate(Date gpsDate) {
        this.gpsDate = gpsDate;
    }

    /**
     * @return the status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    /**
     * @return the odometer
     */
    public double getOdometer() {
        return odometer;
    }

    /**
     * @param odometer the odometer to set
     */
    public void setOdometer(double odometer) {
        this.odometer = odometer;
    }

}
