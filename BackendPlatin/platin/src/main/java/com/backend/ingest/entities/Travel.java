package com.backend.ingest.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.GenericGenerator;

import com.backend.platin.util.Util;
import java.util.UUID;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
@Table(name = "travel")
public class Travel {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;

    @Column(name = "startGeozone")
    private String startGeozone;

    @Column(name = "endGeozone")
    private String endGeozone;

    @Column(name = "name")
    @NotEmpty(message = "the name field cannot be empty")
    private String name;

    @Column(name = "startDate")
    private Date startDate;

    @Column(name = "endDate")
    private Date endDate;

    @NotEmpty(message = "the statusTravel field cannot be empty")
    @Column(name = "statusTravel")
    private String statusTravel;

    @NotEmpty(message = "the schudeledTime field cannot be empty")
    @Column(name = "schudeledTime")
    private String schudeledTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "America/Lima")
    @Column(name = "creationDate")
    private Date creationDate;

    @Column(name = "status")
    private boolean status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "vehicleTravel_id", nullable = false)
    private Vehicle vehicleTravel;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "geozona_id", nullable = false)
    private Geozone geozonaTravel;

    public Travel() {

    }

    public Travel(String name, Date startDate, Date endDate, String statusTravel, String schudeledTime) {
        this.startGeozone = UUID.randomUUID().toString().toUpperCase();
        this.endGeozone = UUID.randomUUID().toString().toUpperCase();
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.statusTravel = statusTravel;
        this.schudeledTime = schudeledTime;
        this.creationDate = new Date();
        this.status = Util.ACTIVE_STATUS;
    }

    public String getSchudeledTime() {
        return schudeledTime;
    }

    public void setSchudeledTime(String schudeledTime) {
        this.schudeledTime = schudeledTime;
    }

    public Geozone getGeozonaTravel() {
        return geozonaTravel;
    }

    public void setGeozonaTravel(Geozone geozonaTravel) {
        this.geozonaTravel = geozonaTravel;
    }

    public Vehicle getVehicleTravel() {
        return vehicleTravel;
    }

    public void setVehicleTravel(Vehicle vehicleTravel) {
        this.vehicleTravel = vehicleTravel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartGeozone() {
        return startGeozone;
    }

    public void setStartGeozone(String startGeozone) {
        this.startGeozone = startGeozone;
    }

    public String getEndGeozone() {
        return endGeozone;
    }

    public void setEndGeozone(String endGeozone) {
        this.endGeozone = endGeozone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatusTravel() {
        return statusTravel;
    }

    public void setStatusTravel(String statusTravel) {
        this.statusTravel = statusTravel;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

}
