package com.backend.ingest.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.GenericGenerator;

import com.backend.platin.util.Util;
import com.backend.platin.util.ValidPassword;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "usuario")
public class User {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "idUser")
    private String idUser;

    @Column(name = "username")
    @NotEmpty(message = "the user field cannot be empty")
    private String user;

    @Column(name = "email")
    @NotEmpty(message = "the email field cannot be empty")
    private String email;
    
    @Column(name = "password")
    @NotEmpty(message = "the password field cannot be empty")
    private String password;

    @Column(name = "displayName")
    @NotEmpty(message = "the displayName field cannot be empty")
    private String displayName;

    @Column(name = "firstName")
    @NotEmpty(message = "the firstName field cannot be empty")
    private String firstName;

    @Column(name = "lastName")

    @NotEmpty(message = "the lastName field cannot be empty")
    private String lastName;

    private String tokenPassword;

    @Column(name = "status")
    private Boolean status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "America/Lima")
    @Column(name = "creationDate")
    private Date creationDate;

    @NotNull
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "usuario_rol", joinColumns = @JoinColumn(name = "usuario_id"),
            inverseJoinColumns = @JoinColumn(name = "rol_id"))
    private Set<Rol> roles = new HashSet<>();
    
  
    public User() {

    }

    public User(@NotEmpty(message = "the user field cannot be empty") String user,
            @NotEmpty(message = "the email field cannot be empty") String email,
            @NotEmpty(message = "the password field cannot be empty") String password,
            @NotEmpty(message = "the displayName field cannot be empty") String displayName,
            @NotEmpty(message = "the firstName field cannot be empty") String firstName,
            @NotEmpty(message = "the lastName field cannot be empty") String lastName) {
        this.user = user;
        this.email = email;
        this.password = password;
        this.displayName = displayName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = Util.ACTIVE_STATUS;
        this.creationDate = new Date();
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getTokenPassword() {
        return tokenPassword;
    }

    public void setTokenPassword(String tokenPassword) {
        this.tokenPassword = tokenPassword;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Rol> getRoles() {
        return roles;
    }

    public void setRoles(Set<Rol> roles) {
        this.roles = roles;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

}
