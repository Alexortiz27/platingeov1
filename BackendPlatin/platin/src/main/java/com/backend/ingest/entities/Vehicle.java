package com.backend.ingest.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;

import com.backend.platin.util.Util;

@Entity
@Table(name = "vehicle")
public class Vehicle {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;

    @Column(name = "plate")
    private String plate;

    @Column(name = "category")
    private String category;

    @Column(name = "code_osinergmin")
    private String code_osinergmin;

     @ManyToOne
   @JoinColumn(name = "companyClientid", nullable = false)

    private CompanyClient companyClient;

    private Boolean status;

    private Date creationDate;

    public Vehicle(@NotEmpty(message = "the plate field cannot be empty") String plate,
            @NotEmpty(message = "the category field cannot be empty") String category, String code_osinergmin,
            Boolean status,
            Date creationDate) {
        this.plate = plate;
        this.category = category;
        this.code_osinergmin = code_osinergmin;
        this.status = Util.ACTIVE_STATUS;
        this.creationDate = new Date();
    }

    public Vehicle() {
        super();
    }

    public String getcode_osinergmin() {
        return code_osinergmin;
    }

    public void setcode_osinergmin(String code_osinergmin) {
        this.code_osinergmin = code_osinergmin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the companyClient
     */
    public CompanyClient getCompanyClient() {
        return companyClient;
    }

    /**
     * @param companyClient the companyClient to set
     */
    public void setCompanyClient(CompanyClient companyClient) {
        this.companyClient = companyClient;
    }

}
