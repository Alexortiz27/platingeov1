package com.backend.ingest.producer.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NsqMessage implements Serializable {

    @SerializedName("id")
    @JsonProperty("id")
    private Long id;


    @SerializedName("action")
    @JsonProperty("action")
    private String action;

    @SerializedName("body")
    @JsonProperty("body")
    private String body;


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getAction() {
        return action;
    }


    public void setAction(String action) {
        this.action = action;
    }


    public String getBody() {
        return body;
    }


    public void setBody(String body) {
        this.body = body;
    }
}
