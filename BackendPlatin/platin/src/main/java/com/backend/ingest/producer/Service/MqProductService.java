package com.backend.ingest.producer.Service;


public interface MqProductService {

    String sendTestMessageByTopicIngestTrama(String body);
    String createTopicBasicsReports(String body);
    String sendLastPosReportMessage(String body );
   
}
