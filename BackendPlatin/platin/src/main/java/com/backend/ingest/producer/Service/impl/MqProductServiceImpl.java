package com.backend.ingest.producer.Service.impl;

import com.backend.ingest.producer.Service.MqProductService;
import com.github.brainlag.nsq.NSQProducer;
import com.github.brainlag.nsq.exceptions.NSQException;
import lombok.extern.slf4j.Slf4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeoutException;

@Slf4j
@Service
public class MqProductServiceImpl implements MqProductService {

    @Value("${nsq.topicIngest}")
    String topicIngestTrama;

    @Value("${nsq.ReportTopic}")
    String reportTopic;

    @Autowired
    NSQProducer nsqProducer;
    private final Logger log = LogManager.getLogger(MqProductServiceImpl.class);

    @Override
    public String sendTestMessageByTopicIngestTrama(String body) {
        try {

            nsqProducer.produce(topicIngestTrama, body.getBytes());
            log.info("¡Mensaje enviado a topicIngestTrama específico con éxito!:" + new Date());
            return "Success";
        } catch (NSQException e) {
            log.error("nsq Conexión anormal!msg={}", e.getMessage());
            return "Error:" + e.getMessage();
        } catch (TimeoutException e) {
            log.error("nsq Se agotó el tiempo de espera para enviar el mensaje!msg={}", e.getMessage());
            return "Error:" + e.getMessage();
        } catch (Exception e) {
            log.error("Ocurrió una excepción desconocida!", e);
            return "Error:" + e.getMessage();
        }
    }

    @Override
    public String createTopicBasicsReports(String body) {
        try {
            nsqProducer.produce(reportTopic, body.getBytes());
            log.info("¡Mensaje enviado al tópico de reporte con éxito!:" + new Date());
            return "Success";
        } catch (NSQException e) {
            log.error("nsq Conexión anormal!msg={}", e.getMessage());
            return "Error:" + e.getMessage();
        } catch (TimeoutException e) {
            log.error("nsq Se agotó el tiempo de espera para enviar el mensaje!msg={}", e.getMessage());
            return "Error:" + e.getMessage();
        } catch (Exception e) {
            log.error("Ocurrió una excepción desconocida!", e);
            return "Error:" + e.getMessage();
        }
    }


    @Override
    public String sendLastPosReportMessage(String body) {
        try {

            nsqProducer.produce(reportTopic, body.getBytes());
            log.info("¡Mensaje enviado a un canal en específico con éxito!:" + new Date());
            return "Success";
        } catch (NSQException e) {
            log.error("nsq Conexión anormal!msg={}", e.getMessage());
            return "Error:" + e.getMessage();
        } catch (TimeoutException e) {
            log.error("nsq Se agotó el tiempo de espera para enviar el mensaje!msg={}", e.getMessage());
            return "Error:" + e.getMessage();
        } catch (Exception e) {
            log.error("Ocurrió una excepción desconocida!", e);
            return "Error:" + e.getMessage();
        }
    }

}
