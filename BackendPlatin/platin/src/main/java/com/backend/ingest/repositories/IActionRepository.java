/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;


import com.backend.ingest.entities.Accion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Joshua Ly
 */
@Repository
public interface IActionRepository extends JpaRepository<Accion,String>{
    
    
}
