/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.dtos.CompanyClientListPageDto;
import com.backend.ingest.dtos.CompanyClientListPaginationDto;
import com.backend.ingest.entities.CompanyClient;
import com.backend.ingest.entities.Rol;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ICompanyClientRepository extends JpaRepository<CompanyClient, String> {
    Optional<CompanyClient> findByIdAndStatus(String id,Boolean status);

    
    List<CompanyClient> findByStatus(Boolean status);

    @Query(value = "SELECT cl.id,cl.address,cl.creation_date,cl.email,cl.name,cl.phone,cl.ruc,cl.status, c.id as idcompany,c.name as namecompany FROM company_client cl \n"
            + "inner join historial_company h on cl.id=h.company_client_id inner join company c on c.id=h.company_id where cl.name =:name and cl.status=1", nativeQuery = true)
    List<CompanyClientListPageDto> findByNameList(@Param("name") String name);

    @Query(value = "SELECT cl.id,cl.address,cl.creation_date,cl.email,cl.name,cl.phone,cl.ruc,cl.status, c.id as idcompany,c.name as namecompany FROM company_client cl \n"
            + "inner join historial_company h on cl.id=h.company_client_id inner join company c on c.id=h.company_id " +
            "where cl.id !=:id and cl.status=:status and c.status=:statusemv and cl.name=:name and c.id=:id_company", nativeQuery = true)
    List<CompanyClientListPageDto> findNameDiferent(@Param("id") String id, @Param("status") Integer status,@Param("statusemv") Integer statusemv,
                                                    @Param("name") String name,@Param("id_company") String id_company);

    @Query(value = "SELECT cl.id,cl.address,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,cl.creation_date)),120) as creation_date,cl.email,cl.name,cl.phone,cl.ruc,cl.status, c.id as idcompany,c.name as namecompany FROM company_client cl \n"
            + "inner join historial_company h on cl.id=h.company_client_id inner join company c on c.id=h.company_id where concat(convert(varchar,(cl.creation_date), 120),(cl.address),(cl.email),(cl.name),(cl.phone),(cl.ruc),(c.name)) like %:busca% and cl.status=:status and c.status=:statusemv", nativeQuery = true)
    List<Map<String, Object>> findAllWithFieldsContaining(Pageable pageable,@Param("busca") String busca, @Param("status") Integer status,@Param("statusemv") Integer statusemv);

    @Query(value = " SELECT CASE WHEN count(e.name) > 0 THEN 1 ELSE 0 END FROM company_client e inner join historial_company h on e.id=h.company_client_id inner join company c on c.id=h.company_id " +
            "where e.name =:name and e.status=:status and c.status=:statusemv", nativeQuery = true)
    Integer existsByName(@Param("name") String name,@Param("status") Integer status,@Param("statusemv") Integer statusemv);



    @Query(value ="SELECT COUNT(*) FROM company_client cl inner join historial_company h on cl.id=h.company_client_id inner join company c on c.id=h.company_id " +
            "where concat(convert(varchar,(cl.creation_date), 120),(cl.address),(cl.email),(cl.name),(cl.phone),(cl.ruc),(c.name)) like %:busca% and cl.status=:status and c.status=:statusemv", nativeQuery = true)
    long countByStatus(@Param("busca") String busca,@Param("status") boolean status,@Param("statusemv") boolean statuscl);

    /*
    @Query(value = "SELECT COUNT(*) FROM company_client cl \n" +
            "inner join historial_company h on cl.id=h.company_client_id " +
            "inner join company c on c.id=h.company_id where cl.status=:status and c.status=:statuscl", nativeQuery = true)
    long countByStatus(@Param("status") boolean status,@Param("statuscl") boolean statuscl);

     */

    @Query(value = "SELECT cl.id,cl.address,cl.creation_date,cl.email,cl.name,cl.phone,cl.ruc,cl.status, c.id as idcompany,c.name as namecompany FROM company_client cl \n"
            + "inner join historial_company h on cl.id=h.company_client_id inner join company c on c.id=h.company_id where cl.status=:status and c.status=:statusemv", nativeQuery = true)
    List<CompanyClientListPageDto> findAllStatus(@Param("status") boolean status,@Param("statusemv") boolean statusemv);
}
