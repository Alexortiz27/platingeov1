/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.dtos.ICompanyNameDto;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.Rol;
import com.backend.ingest.entities.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author FAMLETO
 */
public interface ICompanyRepository extends JpaRepository<Company, String> {

    @Query(value = "SELECT c.id,c.phone,c.email,c.address,c.name,c.city,c.country,c.ingest_token,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,c.creation_date)),120) as creation_date ,c.user_id from company c inner join usuario u on c.user_id=u.id_user  WHERE  " +
            "c.status=:status", nativeQuery = true)
    List<Map<String, Object>> findByStatus(@Param("status") Integer status);

    Optional<Company> findByIdAndStatus(String id, Boolean status);


    @Query(value = "SELECT * FROM company  WHERE id !=:id and status =:status", nativeQuery = true)
    List<Company> findNameDiferent(@Param("id") String id, @Param("status") Integer status);
//    Optional<Company> findById(String id);





    @Query(value = "SELECT c.id,c.phone,c.email,c.address,c.name,c.city,c.country,c.ingest_token,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,c.creation_date)),120) as creation_date ,c.user_id from company c inner join usuario u on c.user_id=u.id_user  WHERE  " +
            "concat((c.phone),(c.email),(c.address),(c.name),(c.city),(c.country),(c.ingest_token),convert(varchar,(c.creation_date), 120)) like %:busca% and c.status=:status", nativeQuery = true)
    List<Map<String, Object>> findAllWithFieldsContaining(Pageable pageable, @Param("status") Integer status, @Param("busca") String busca);

    @Query(value = "SELECT COUNT(*) from company c inner join usuario u on c.user_id=u.id_user  WHERE  " +
            "concat((c.phone),(c.email),(c.address),(c.name),(c.city),(c.country),(c.ingest_token),convert(varchar,(c.creation_date), 120)) like %:busca% and c.status=:status", nativeQuery = true)
    long countByStatus(@Param("status") boolean status, @Param("busca") String busca);

    /*
    long countByStatus(boolean status);

     */



    @Query(value = " SELECT CASE WHEN count(e.name) > 0 THEN 1 ELSE 0 END FROM company e where e.name =:name and e.status=:status", nativeQuery = true)
    Integer existsByNameAll(@Param("name") String name,@Param("status") boolean status);

    @Query(value = " SELECT CASE WHEN count(e.id) > 0 THEN 1 ELSE 0 END FROM company e where e.id =:id and e.status=:status", nativeQuery = true)
    Integer existsByIdAll(@Param("id") String id,@Param("status") boolean status);

    @Query(value = " SELECT CASE WHEN count(e.email) > 0 THEN 1 ELSE 0 END FROM company e where e.email =:email and e.status=:status", nativeQuery = true)
    Integer existsByEmailAll(@Param("email") String email,@Param("status") boolean status);

    Company findByUser(User user);
    
    Company findByIngestToken(String token); 
    
    @Query(value="SELECT name FROM Company where  ingest_Token=:token",nativeQuery=true)
    ICompanyNameDto getCompanyNameByToken(@Param("token")String tokenTrama);
    
    boolean existsCompanyByIngestTokenAndStatus(String token,boolean status);
    

}
