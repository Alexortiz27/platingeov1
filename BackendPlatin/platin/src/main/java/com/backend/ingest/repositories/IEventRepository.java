/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.Event;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;


public interface IEventRepository extends JpaRepository<Event, String> {
    Optional<Event> findByIdAndStatus(String id, Boolean status);

    @Modifying
    @Query(value = "insert into event (id,type,geozone_id,latitude,longitude,receive_date,gps_date,spedd,vehicle_id,id_travel,name_travel,odometer,status,creation_date) " +
            "VALUES (:id,:type,:geozone_id,:latitude,:longitude,:receive_date,:gps_date,:spedd,:vehicle_id,:id_travel,:name_travel,:odometer,:status,:creation_date)", nativeQuery = true)
    @Transactional
    void saveEvent(@Param("id") String id, @Param("type") String type, @Param("geozone_id") String geozone_id,
                @Param("latitude") double latitude, @Param("longitude") double longitude, @Param("receive_date") Date receive_date,
                @Param("gps_date") Date gps_date, @Param("spedd") double spedd, @Param("vehicle_id") String vehicle_id,
                @Param("id_travel") String id_travel, @Param("name_travel") String name_travel, @Param("odometer") Integer odometer
            , @Param("status") boolean status, @Param("creation_date") Date creation_date);

    List<Event> findByStatus(Boolean status);

    @Query(value = "SELECT ev.id,ev.type,ev.name_travel,ev.latitude,ev.longitude,ev.spedd," +
            "CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,ev.creation_date)),120) as creation_date," +
            "CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,ev.receive_date)),120) as receive_date," +
            "CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,ev.gps_date)),120) as gps_date," +
            "(SELECT name FROM  geozone WHERE id=ev.geozone_id)AS namegeozone,ve.plate from event ev " +
            "inner join vehicle ve on ev.vehicle_id = ve.id " +
            "where exists(select * from event inner join geozone ge on event.geozone_id = ge.id or event.geozone_id = 'null') and " +
            "concat((ve.plate),(ev.type),(ev.name_travel),convert(varchar,(ev.gps_date), 120),convert(varchar,(ev.receive_date), 120)," +
            "CAST(ev.spedd AS VARCHAR), (SELECT name FROM  geozone WHERE id=ev.geozone_id),CAST(ev.latitude AS VARCHAR)," +
            "CAST(ev.longitude AS VARCHAR)) like %:busca% and ev.status =:status", nativeQuery = true)
    List<Map<String, Object>> findAllWithFieldsContaining(Pageable pageable, @Param("status") Integer status, @Param("busca") String busca);

//    @Query(value = "SELECT EV.gps_date, EV.latitud, EV.longitud, ev.spedd from event EV INNER JOIN vehicle VE ON EV.vehicle_id = VE.ID  WHERE VE.PLATE =:plate AND EV.TYPE =:type AND EV.GPS_DATE >=:startDate and EV.GPS_DATE <=:endDate" +
//            " and EV.status=:status and VE.status=:statusve  ORDER BY EV.GPS_DATE DESC", nativeQuery = true)
//    List<Event> findByContainingTypeDate(@Param("plate") String plate, @Param("type") String type, @Param("startDate") Date startDate, @Param("endDate") Date endDate
//            , @Param("status") Integer status, @Param("statusve") Integer statusve);

    @Query(value = "SELECT * from event EV INNER JOIN vehicle VE ON EV.vehicle_id = VE.ID  WHERE VE.PLATE =:plate AND EV.TYPE IN(:type,:typeEnd) AND EV.GPS_DATE BETWEEN :startDate and :endDate " +
            "and EV.status=:status and VE.status=:statusve ORDER BY EV.GPS_DATE DESC", nativeQuery = true)
    List<Event> findByContainingTypeDateAll(@Param("type") String type, @Param("typeEnd") String typeEnd, @Param("plate") String plate, @Param("startDate") Date startDate, @Param("endDate") Date endDate
            , @Param("status") Integer status, @Param("statusve") Integer statusve);

//    List<Event> findByPlateAndTypeInAndGpsDateBetween(String plate, String[] types,Date startDate, Date endDate);

//    @Query(value = "from Event event INNER JOIN Vehicle vehicle ON  event.vehicle_id=vehicle.id  WHERE vehicle.plate =:plate AND event.type IN (:type) AND event.gpsDate BETWEEN :startDate and :endDate and event.status=:status and vehicle.status=:statusve")
//    List<Event> findByTypeAndPlateAndCreationDateBetween(@Param("type") String[] types, @Param("plate") String plate, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("status") boolean status, @Param("statusve") boolean statusve);

//    @Query(value = "from Event event INNER JOIN Vehicle vehicle ON  event.vehicle_id=vehicle.id  WHERE vehicle.plate =:plate AND event.creationDate BETWEEN :startDate and :endDate ")
//    List<Event> findByPlateAndCreationDateBetween(@Param("plate") String plate, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

//    @Query(value = "select e.type from Event event INNER JOIN Vehicle vehicle ON  event.vehicle_id=vehicle.id  WHERE vehicle.plate =:plate AND event.type IN (:type) AND event.gpsDate BETWEEN :startDate and :endDate and event.status=1 order by event.creationDate desc")
//    List<TypeEvent> findByTypeAndPlateAndCreationDateBetweenOrderByCreationDate(@Param("type") String [] types,@Param("plate") String plate, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

        @Query(value = "SELECT COUNT(*) from event ev inner join vehicle ve on ev.vehicle_id = ve.id " +
                "where exists(select * from event inner join geozone ge on event.geozone_id = ge.id or event.geozone_id = 'null') and " +
                "concat((ve.plate),(ev.type),(ev.name_travel),convert(varchar,(ev.gps_date), 120),convert(varchar,(ev.receive_date), 120)," +
                "CAST(ev.spedd AS VARCHAR),(SELECT name FROM  geozone WHERE id=ev.geozone_id),CAST(ev.latitude AS VARCHAR)," +
                "CAST(ev.longitude AS VARCHAR)) like %:busca% and ev.status =:status", nativeQuery = true)
    long countByStatus(@Param("status") Integer status, @Param("busca") String busca);


    @Query(value = "SELECT EV.gps_date, EV.type, EV.latitude, EV.longitude,EV.id_travel,EV.name_travel,EV.spedd,EV.odometer from event EV INNER JOIN vehicle VE ON EV.vehicle_id = VE.ID  WHERE VE.PLATE =:plate AND EV.TYPE IN(:type,:typeEnd) AND EV.GPS_DATE BETWEEN :startDate and :endDate " +
            "and EV.status=:status and VE.status=:statusve ORDER BY EV.GPS_DATE DESC", nativeQuery = true)
    List<EventTravelDto> getTravelEvents(@Param("type") String type, @Param("typeEnd") String typeEnd, @Param("plate") String plate, @Param("startDate") Date startDate, @Param("endDate") Date endDate
            , @Param("status") Integer status, @Param("statusve") Integer statusve);

    //
    @Query(value = "Select event.creation_date, event.gps_date, event.latitude, event.longitude, event.spedd, event.odometer, event.type "
            + " from Event event INNER JOIN Vehicle vehicle ON  event.vehicle_id=vehicle.id  "
            + " WHERE vehicle.plate =:plate AND event.type IN (:type)"
            + " AND event.gps_date BETWEEN :startDate and :endDate and event.status=:status and vehicle.status=:statusve"
            + " ORDER BY event.gps_date DESC", nativeQuery = true)
    List<EventStoppedDto> getStoppedEvent(@Param("type") String[] types, @Param("plate") String plate,
                                          @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("status")
                                                  boolean status, @Param("statusve") boolean statusve);

    @Query(value = "Select event.geozone_id,event.type,event.gps_date,event.spedd,event.odometer"
            + " from Event event INNER JOIN Vehicle vehicle ON  event.vehicle_id=vehicle.id  "
            + " WHERE vehicle.plate =:plate AND event.type IN (:type)"
            + " AND event.gps_date BETWEEN :startDate and :endDate and event.status=:status and vehicle.status=:statusve"
            + " ORDER BY event.gps_date DESC", nativeQuery = true)
    List<EventGeozoneDto> getGeozoneEvent(@Param("type") String[] types, @Param("plate") String plate, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("status") boolean status, @Param("statusve") boolean statusve);

    @Query(value = "SELECT EV.gps_date, EV.latitude, EV.longitude, ev.spedd, ev.odometer from event EV INNER JOIN vehicle VE ON EV.vehicle_id = VE.ID  WHERE VE.PLATE =:plate AND EV.TYPE =:type AND EV.GPS_DATE >=:startDate and EV.GPS_DATE <=:endDate" +
            " and EV.status=:status and VE.status=:statusve  ORDER BY EV.GPS_DATE DESC", nativeQuery = true)
    List<EventSpeedDto> getSpeedEvent(@Param("plate") String plate, @Param("type") String type, @Param("startDate") Date startDate, @Param("endDate") Date endDate
            , @Param("status") Integer status, @Param("statusve") Integer statusve);

    @Query(value = "SELECT EV.type, EV.gps_date,EV.latitude,ev.longitude, ev.spedd,ev.odometer from event EV INNER JOIN vehicle VE ON EV.vehicle_id = VE.ID  WHERE VE.PLATE =:plate AND EV.TYPE IN(:type,:typeEnd) AND EV.GPS_DATE BETWEEN :startDate and :endDate " +
            "and EV.status=:status and VE.status=:statusve ORDER BY EV.GPS_DATE DESC", nativeQuery = true)
    List<EventNoTransmissionDto> getNoTransmissionEvents(@Param("type") String type, @Param("typeEnd") String typeEnd, @Param("plate") String plate, @Param("startDate") Date startDate, @Param("endDate") Date endDate
            , @Param("status") Integer status, @Param("statusve") Integer statusve);


    @Query(value = "SELECT EV.receive_date, EV.gps_date, EV.latitude, EV.longitude, ev.spedd, ev.odometer from event EV INNER JOIN vehicle VE ON EV.vehicle_id = VE.ID  WHERE VE.PLATE =:plate AND EV.TYPE =:type AND EV.GPS_DATE >=:startDate and EV.GPS_DATE <=:endDate" +
            " and EV.status=:status and VE.status=:statusve  ORDER BY EV.GPS_DATE DESC", nativeQuery = true)
    List<EventTimeLagDto> getTimeLagEvents(@Param("plate") String plate, @Param("type") String type, @Param("startDate") Date startDate, @Param("endDate") Date endDate
            , @Param("status") Integer status, @Param("statusve") Integer statusve);

//
//    @Query(value = "Select  event.gps_date, event.type "
//            + " from Event event INNER JOIN Vehicle vehicle ON  event.vehicle_id=vehicle.id  "
//            + " WHERE vehicle.plate =:plate AND event.type IN (:type)"
//            + " AND event.gps_date BETWEEN :startDate and :endDate and event.status=:status and vehicle.status=:statusve"
//            + " ORDER BY event.gps_date DESC", nativeQuery = true)
//    List<IEventStoppedDto> findGps_DateAndTypeOfStoppedEvent(@Param("type") String[] types, @Param("plate") String plate,
//                                                             @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("status")
//                                                                     boolean status, @Param("statusve") boolean statusve);
}
