/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.dtos.IGeozoneNameDto;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Geozone;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author FAMLETO
 */
public interface IGeozoneRepository extends JpaRepository<Geozone, String> {

    List<Geozone> findByStatus(Boolean status);

    @Query(value = "SELECT g.id,(g.category),(g.coords),(g.radius) from geozone g inner join usuario u on u.id_user =g.id_user WHERE g.status=:status and g.type =:type", nativeQuery = true)
    List<Map<String, Object>> findByStatusAll(@Param("status") Integer status,@Param("type") String type);

    Optional<Geozone> findByIdAndStatus(String id, Boolean status);

    @Query(value = "SELECT * FROM geozone  WHERE id !=:id and status =:status and name=:name", nativeQuery = true)
    Optional<Geozone> findNameDiferent(@Param("id") String id, @Param("status") Integer status, @Param("name") String name);

    @Query(value = "SELECT cl.id,(cl.category),(cl.type), cl.name,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,cl.creation_date)),120) as creation_date FROM geozone cl inner join usuario u on u.id_user =cl.id_user " +
            "WHERE " +
            "concat(convert(varchar,(cl.creation_date), 120),(cl.name),(cl.category),(cl.type)) like %:busca% and cl.status =:status", nativeQuery = true)
    List<Map<String, Object>> findAllWithFieldsContaining(Pageable pageable, @Param("busca") String busca, @Param("status") Integer status);

    @Query(value = " SELECT CASE WHEN count(e.name) > 0 THEN 1 ELSE 0 END FROM geozone e where e.name =:name and e.status=:status", nativeQuery = true)

    Integer findByNameAll(@Param("name") String name,@Param("status") boolean status);
    
    @Query(value="SELECT name,type FROM GEOZONE WHERE id=:id and status=:status",nativeQuery=true)
    IGeozoneNameDto getNameAndTypeGeozoneById(@Param("id")String id,@Param("status")boolean status);



    @Query(value ="SELECT COUNT(*) FROM geozone cl inner join usuario u on u.id_user =cl.id_user " +
            "WHERE concat(convert(varchar,(cl.creation_date), 120),(cl.name),(cl.category),(cl.type)) like %:busca% and cl.status =:status", nativeQuery = true)
    long countByStatus(@Param("busca") String busca,@Param("status") boolean status);

    /*
    @Query(value = "SELECT COUNT(*) FROM geozone cl \n" +
            "inner join usuario c on c.id_user=cl.id_user where cl.status=:status and c.status=:statuscl", nativeQuery = true)
    long countByStatus(@Param("status") boolean status,@Param("statuscl") boolean statuscl);

     */

}
