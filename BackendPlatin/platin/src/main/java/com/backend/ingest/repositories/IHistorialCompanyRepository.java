/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.Historial_Company;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author FAMLETO
 */
public interface IHistorialCompanyRepository extends JpaRepository<Historial_Company,String> {
    
    
    List<Historial_Company> findByCompany(Company company);
}
