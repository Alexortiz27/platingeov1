package com.backend.ingest.repositories;

import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Maps;
import com.backend.ingest.entities.Permits;
import com.backend.ingest.entities.Rol;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IMapRepository extends JpaRepository<Maps, String> {
    Optional<Maps> findByIdAndStatus(String id, Boolean status);

    List<Maps> findByStatus(Boolean status);
    @Query(value = " SELECT CASE WHEN count(e.name) > 0 THEN 1 ELSE 0 END FROM map e where e.name =:name and e.status=:status", nativeQuery = true)
    Integer findByNameAll(@Param("name") String name,@Param("status") boolean status);

    @Query(value = "SELECT * FROM map  WHERE id !=:id and status=:status", nativeQuery = true)
    List<Maps> findNameDiferent(@Param("id") String id, @Param("status") Integer status);

    @Query(value = "SELECT m.id,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,m.creation_date)),120) as creation_date, m.name, m.status, m.url_object, m.id_user  FROM map m inner join usuario u on u.id_user =m.id_user WHERE concat(m.name,convert(varchar,(m.creation_date), 120),m.url_object)" +
            " like %:buscar% and m.status =:status and m.id_user=:user_id", nativeQuery = true)
    List<Map<String, Object>> findAllWithFieldsContaining(Pageable pageable, @Param("status") Integer status, @Param("buscar") String buscar, @Param("user_id") String user_id);


    @Query(value = "SELECT COUNT(*) FROM map m inner join usuario u on u.id_user =m.id_user WHERE concat(m.name,convert(varchar,(m.creation_date), 120),m.url_object)" +
            " like %:buscar% and m.status =:status and m.id_user=:user_id", nativeQuery = true)
    long countByStatus(@Param("status") boolean status, @Param("buscar") String buscar, @Param("user_id") String user_id);
    /*
    @Query(value = "SELECT COUNT(*) FROM map cl inner join usuario c on c.id_user=cl.id_user where cl.status=:status and c.status=:statuscl", nativeQuery = true)
    long countByStatus(@Param("status") boolean status,@Param("statuscl") boolean statuscl);

     */
}
