/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.entities.Maps;
import com.backend.ingest.entities.Marker;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author FAMLETO
 */
@Repository
public interface IMarkerRepository extends JpaRepository<Marker, String> {

    public List<Marker> findByStatus(boolean status);

    Optional<Marker> findByIdAndStatus(String id, Boolean status);

    @Query(value = "SELECT m.name,g.name as name_geozone,g.type as type_geozone,m.type,m.latitude,m.longitude,m.radius,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,m.creation_date)),120) as creation_date FROM marker m inner join usuario u on u.id_user=m.id_user inner join geozone g on m.geozone_id=g.id WHERE concat(m.name,g.name,g.type,m.type,m.latitude,m.longitude,m.radius,convert(varchar,(m.creation_date), 120)) " +
            "like %:busca% and m.status=:status and m.id_user=:user_id", nativeQuery = true)
    List<Map<String, Object>> findByStatusAll(Pageable pageable, @Param("busca") String busca, @Param("status") boolean status, @Param("user_id") String user_id);

    @Query(value = " SELECT CASE WHEN count(e.name) > 0 THEN 1 ELSE 0 END FROM marker e where e.name =:name and e.status=:status", nativeQuery = true)
    Integer findByNameAll(@Param("name") String name,@Param("status") boolean status);

    @Query(value = "SELECT * FROM marker  WHERE id !=:id and status=:status", nativeQuery = true)
    List<Marker> findNameDiferent(@Param("id") String id, @Param("status") Integer status);

    @Query(value = "SELECT COUNT(*) FROM marker m inner join usuario u on u.id_user=m.id_user inner join geozone g on m.geozone_id=g.id WHERE concat(m.name,g.name,g.type,m.type,m.latitude,m.longitude,m.radius,convert(varchar,(m.creation_date), 120)) " +
            "like %:busca% and m.status=:status and m.id_user=:user_id", nativeQuery = true)
    long countByStatus(@Param("busca") String busca, @Param("status") boolean status, @Param("user_id") String user_id);


    /*
    @Query(value = "SELECT COUNT(*) FROM marker cl \n" +
            "inner join usuario c on c.id_user=cl.id_user inner join geozone g on cl.geozone_id=g.id where cl.status=:status and c.status=:statuscl", nativeQuery = true)
    long countByStatus(@Param("status") boolean status,@Param("statuscl") boolean statuscl);

     */
}
