package com.backend.ingest.repositories;

import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Marker;
import com.backend.ingest.entities.Permits;
import com.backend.ingest.entities.Rol;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IPermitsRepository extends JpaRepository<Permits, String> {
    List<Permits> findByStatus(Boolean status);

    Optional<Permits> findByIdAndStatus(String id, Boolean status);

    Optional<Permits> findByNameAndStatus(String name,Boolean status);

    @Query(value = "SELECT * FROM permits p WHERE concat(p.name,convert(varchar,(p.creation_date), 120)) like %:busca% and p.status =:status", nativeQuery = true)
    Page<Permits> findAllWithFieldsContaining(Pageable pageable, @Param("status") Integer status, @Param("busca") String busca);

    @Query(value = "SELECT * FROM permits  WHERE id !=:id and status =:status and name=:name", nativeQuery = true)
    Permits findNameDiferent(@Param("id") String id,@Param("status") Integer status,@Param("name") String name);
}
