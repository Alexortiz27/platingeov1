/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.Marker;
import com.backend.ingest.entities.Report;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author FAMLETO
 */
@Repository
public interface IReportRepository extends JpaRepository<Report,String>{
    
    List<Report> findByStatus(boolean status);

    Optional<Report> findByIdAndStatus(String id, Boolean status);

//    Page<Report> findByStatus(Pageable pageable,  boolean status);

    @Query(value = "SELECT COUNT(*) FROM report p inner join company c on p.companyid=c.id WHERE concat(p.type,p.estado_report,c.name,convert(varchar,(p.creation_date), 120)) like %:busca% and p.status =:status", nativeQuery = true)
    long countByStatus(@Param("status") boolean status, @Param("busca") String busca);

    //long countByStatus(boolean status);


    @Query(value = "SELECT p.*,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,p.creation_date)),120) as creation_dateLima,c.name as namecompany FROM report p inner join company c on p.companyid=c.id WHERE concat(p.type,p.estado_report,c.name,convert(varchar,(p.creation_date), 120)) like %:busca% and p.status =:status", nativeQuery = true)
    List<Map<String, Object>>  findAllWithFieldsContaining(Pageable pageable, @Param("status") Integer status, @Param("busca") String busca);
}
