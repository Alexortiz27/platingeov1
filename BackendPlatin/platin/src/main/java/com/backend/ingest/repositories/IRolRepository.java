package com.backend.ingest.repositories;

import com.backend.ingest.entities.Rol;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IRolRepository extends JpaRepository<Rol, String> {

    List<Rol> findByStatus(Boolean status);

    Optional<Rol> findByIdAndStatus(String id,Boolean status);

    Rol findByNameAndStatus(String name,Boolean status);

    @Query(value = " SELECT CASE WHEN count(e.name) > 0 THEN 1 ELSE 0 END FROM rol e where e.name =:name and e.status=:status", nativeQuery = true)
    Integer findByNameAll(@Param("name") String name,@Param("status") boolean status);

    @Query(value = "SELECT * FROM rol WHERE id !=:id and status =:status", nativeQuery = true)
    List<Rol> findNameDiferent(@Param("id") String id, @Param("status") Integer status);

    @Query(value = "SELECT * FROM rol WHERE concat(name,convert(varchar,(creation_date), 120)) like %:buscar% and status =:status", nativeQuery = true)
    Page<Rol> findAllWithFieldsContaining(Pageable pageable, @Param("status") Integer status,@Param("buscar") String buscar);
}
