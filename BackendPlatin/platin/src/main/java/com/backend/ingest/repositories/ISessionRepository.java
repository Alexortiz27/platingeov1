/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.entities.Marker;
import com.backend.ingest.entities.Report;
import com.backend.ingest.entities.Rol;
import com.backend.ingest.entities.Session;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author FAMLETO
 */
@Repository
public interface ISessionRepository extends JpaRepository<Session, String> {
    //    Page<Session> findAll( Pageable pageable);
    Optional<Session> findByIdAndStatus(String id, Boolean status);

    @Query(value = "SELECT * FROM session WHERE concat(user_complete_names,convert(varchar,(creation_date), 120)) like %:buscar% and status =:status", nativeQuery = true)
    Page<Session> findAllWithFieldsContaining(Pageable pageable, @Param("status") Integer status, @Param("buscar") String buscar);
}
