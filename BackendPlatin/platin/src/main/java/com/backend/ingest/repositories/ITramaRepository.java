/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.dtos.ITramaDto;
import com.backend.ingest.entities.Trama;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author FAMLETO
 */
@Repository
public interface ITramaRepository extends JpaRepository<Trama, String> {

    List<Trama> findByStatus(Boolean status);

    @Query(value = "SELECT * FROM (SELECT GPS_DATE from TRAMA WHERE position =:position and status=:status ORDER BY GPS_DATE ASC ) WHERE rownum = 1", nativeQuery = true)
    Trama findAllWithFieldsContaining(@Param("status") Integer status, @Param("position") String position);

    //QUERY OF WORKER OF REPORTS
    List<Trama> findByPlateAndGpsDateBetweenOrderByGpsDate(String plate, Date startDate, Date endDate);

      @Query(value = "SELECT plate,position,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,gps_date)),120) as gps_date,speed,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,receive_date)),120) as receive_date FROM Trama where" +
            " plate=:plate AND gps_date BETWEEN :startDate AND :endDate AND status=:status order by gps_date ", nativeQuery = true)
    List<ITramaDto>  findByPlateGpsDateBetweenCompanyIdAsc(@Param("plate") String plate,
                                                         @Param("startDate") Date startDate,
                                                         @Param("endDate") Date endDate,
                                                         @Param("status") Integer status);

    @Query(value = "SELECT TOP 1 plate,position,gps_date,speed,receive_date FROM Trama where" +
            " plate=:plate AND gps_date BETWEEN :startDate AND :endDate AND status=:status order by gps_date ", nativeQuery = true)
    List<ITramaDto>  findByPlateGpsDateBetweenCompanyIdAscTop(@Param("plate") String plate,
                                                           @Param("startDate") Date startDate,
                                                           @Param("endDate") Date endDate,
                                                           @Param("status") Integer status);

}
