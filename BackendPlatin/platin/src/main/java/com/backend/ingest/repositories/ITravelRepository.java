/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.dtos.TravelInterfReportDto;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Session;
import com.backend.ingest.entities.Travel;
import com.backend.ingest.entities.Vehicle;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ITravelRepository extends JpaRepository<Travel, String> {

    List<Travel> findByStatus(Boolean status);
    Optional<Travel> findByIdAndStatus(String id, Boolean status);

    @Query(value = "SELECT t.id,t.name,ve.plate,g.name as namegeozona,g.type as typegeozona,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,t.creation_date)),120) as creation_date FROM travel t inner join vehicle ve on t.vehicle_travel_id=ve.id " +
            "inner join geozone g on t.geozona_id=g.id where concat(t.name,ve.plate,g.name,g.type,convert(varchar,(t.creation_date), 120)) like %:busca% and t.status =:status " +
            "and ve.status=:statusvehicle and g.status=:statusgeo", nativeQuery = true)
    List<Map<String, Object>> findAllPage(Pageable pageable, @Param("status") Integer status, @Param("busca") String busca, @Param("statusvehicle") Integer statusvehicle, @Param("statusgeo") Integer statusgeo);

    @Query(value = "  SELECT TOP 1 TR.id,TR.name,TR.status_travel,TR.schudeled_time,TR.start_date,TR.end_date,TR.VEHICLE_TRAVEL_ID,TR.geozona_id FROM TRAVEL TR INNER JOIN VEHICLE VE ON TR.VEHICLE_TRAVEL_ID = VE.ID WHERE VE.plate =:plate AND  CONVERT(DATETIME,CONCAT(CONVERT(varchar,GETDATE(),102),CONVERT(varchar, ' '+TR.schudeled_time) )) >=:gpsDate AND TR.status_travel !='end'  AND  TR.STATUS =1 order by TR.start_date desc",
            nativeQuery = true)
    List<TravelInterfReportDto> findAllWithFieldsContaining(@Param("plate") String plate, @Param("gpsDate") Date gpsDate);

    @Query(value = "SELECT TR.* FROM TRAVEL TR INNER JOIN VEHICLE VE ON TR.VEHICLE_TRAVEL_ID = VE.ID WHERE TR.VEHICLE_TRAVEL_ID =:idvehicle AND TR.STATUS =1 AND TR.status_travel='end' AND TR.START_DATE BETWEEN  :startDate AND :endDate",
            nativeQuery = true)
    List<Travel> findAllWithFieldsContainingPlateReport(@Param("idvehicle") String idvehicle, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query(value = " SELECT CASE WHEN count(tr.name) > 0 THEN 1 ELSE 0 END FROM travel tr inner join vehicle ve on tr.vehicle_travel_id = ve.id " +
            "inner join geozone g on tr.geozona_id=g.id " +
            "where tr.name =:name and tr.status=:status and ve.status=:statusve and g.status=:statusge", nativeQuery = true)
    Integer findByNameAll(@Param("name") String name, @Param("status") Integer status, @Param("statusve") Integer statusve, @Param("statusge") Integer statusge);

    @Query(value = "SELECT * FROM travel tr inner join vehicle ve on tr.vehicle_travel_id = ve.id " +
            "inner join geozone g on tr.geozona_id=g.id  " +
            "WHERE tr.id !=:id and tr.status =:status and ve.status=:statusve and g.status=:statusge and tr.name=:name", nativeQuery = true)
    Travel findNameDiferent(@Param("id") String id, @Param("status") Integer status, @Param("statusve") Integer statusve, @Param("statusge") Integer statusge, @Param("name") String name);

    @Query(value = "SELECT COUNT(*) FROM travel t inner join vehicle ve on t.vehicle_travel_id=ve.id " +
            "inner join geozone g on t.geozona_id=g.id where concat(t.name,ve.plate,g.name,g.type,convert(varchar,(t.creation_date), 120)) like %:busca% and t.status =:status " +
            "and ve.status=:statusvehicle and g.status=:statusgeo", nativeQuery = true)
    long countByStatus(@Param("status") boolean status, @Param("busca") String busca, @Param("statusvehicle") boolean statusvehicle, @Param("statusgeo") boolean statusgeo);


    /*
    @Query(value = "SELECT count(*) FROM travel t inner join vehicle ve on t.vehicle_travel_id=ve.id " +
            "inner join geozone g on t.geozona_id=g.id where t.status =:status " +
            "and ve.status=:statusvehicle and g.status=:statusgeo", nativeQuery = true)
    long countByStatus(@Param("status") boolean status,@Param("statusvehicle") boolean statusvehicle,@Param("statusgeo") boolean statusgeo);

     */
}
