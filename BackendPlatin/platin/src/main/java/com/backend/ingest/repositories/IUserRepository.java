package com.backend.ingest.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface IUserRepository extends JpaRepository<User, String> {

    Optional<User> findByUser(String user);

    @Query(value = "SELECT * FROM usuario  WHERE id_user =:id and status=:status", nativeQuery = true)
    User findByIdAndStatusAll(String id, Integer status);

    Optional<User> findByEmailAndStatus(String email,Boolean status);

    Optional<User> findByUserOrEmail(String nombreUsuario, String email);

    Optional<User> findByTokenPasswordAndStatus(String tokenPassword,Boolean status);

    @Query(value = " SELECT CASE WHEN count(e.username) > 0 THEN 1 ELSE 0 END FROM usuario e where e.username =:user and e.status=:status", nativeQuery = true)
    Integer existsByUserAndStatusAll(@Param("user") String user, @Param("status") Integer status);

    @Query(value = " SELECT CASE WHEN count(e.email) > 0 THEN 1 ELSE 0 END FROM usuario e where e.email =:email and e.status=:status", nativeQuery = true)
    Integer existsByEmailAll(@Param("email") String email, @Param("status") Integer status);

    boolean existsByPassword(String password);

    List<User> findByStatus(Boolean status);

    @Query(value = "SELECT * FROM usuario  WHERE id_user !=:id and status=:status", nativeQuery = true)
    List<User> findUserOrEmailDiferent(@Param("id") String id, @Param("status") Integer status);

    @Query(value = "SELECT * FROM usuario WHERE concat(first_name,last_name,username,email,display_name,convert(varchar,(creation_date), 120)) like %:busca% and status =:status", nativeQuery = true)
    Page<User> findAllWithFieldsContaining(Pageable pageable, @Param("status") Integer status, @Param("busca") String busca);

}
