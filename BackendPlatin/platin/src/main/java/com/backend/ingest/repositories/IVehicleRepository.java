/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.repositories;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.Vehicle;


import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IVehicleRepository extends JpaRepository<Vehicle, String> {

    List<Vehicle> findByStatus(Boolean status);

    Optional<Vehicle> findByIdAndStatus(String id, Boolean status);

    @Query(value = "SELECT ve.id,ve.category,ve.code_osinergmin,ve.creation_date,ve.plate,ve.status,cl.name as nameclient,cl.id as idclient FROM vehicle ve INNER JOIN company_client cl on ve.company_clientid=cl.id " +
            " INNER JOIN historial_company h on cl.id=h.company_client_id "
            + " INNER JOIN company c on c.id= h.company_id " +
            "WHERE ve.status =:status and cl.status=:statucl and c.status=:statusemv", nativeQuery = true)
    List<VehicleListDto> findByVehicleCompany(@Param("status") Integer status, @Param("statucl") Integer statucl, @Param("statusemv") Integer statusemv);

    @Query(value = "SELECT * FROM vehicle  WHERE id !=:id and status =:status and plate=:plate", nativeQuery = true)
    Vehicle findNameDiferent(@Param("id") String id, @Param("status") Integer status, @Param("plate") String plate);


    Optional<Vehicle> findByPlateAndStatus(@Param("plate") String plate, boolean status);

    @Query(value = "SELECT ve.id FROM vehicle ve WHERE ve.status =:status and ve.plate=:plate", nativeQuery = true)
    String findByPlateWorker(@Param("plate") String plate,@Param("status") boolean status);

    //    @Query(value = "SELECT ve.*,cl.name FROM vehicle ve INNER JOIN company_client cl on ve.company_clientid=cl.id WHERE ve.status =1", nativeQuery = true)
//    List<Map<String,Object>> findAllWithFieldsContaining(Pageable pageable);
//    @Query(value = "SELECT ve.id,ve.category,ve.code_osinergmin,ve.creation_date,ve.plate,ve.status,cl.name as nameclient,cl.id as idclient FROM vehicle ve INNER JOIN company_client cl on ve.company_clientid=cl.id WHERE ve.status =:status", nativeQuery = true)
//    Page<VehicleListDto> findAllWithFieldsContaining(Pageable pageable,boolean status);
    @Query(value = "SELECT * from vehicle WHERE status =:status", nativeQuery = true)
    Page<Vehicle> findAllWithFieldsContaining(Pageable pageable, @Param("status") boolean status);

    //
//    @Query(value = "SELECT * FROM VEHICLE ve INNER JOIN COMPANY co ON ve.company_id = co.id where ve.plate =:plate and ve.status =1 and co.status =1", nativeQuery = true)
//    List<Vehicle> findAllWithFieldsContainingPlateToken(@Param("plate") String plate);
//    @Query(value = "SELECT ve.id,ve.category,ve.code_osinergmin,ve.creation_date,ve.plate,ve.status,cl.name as nameclient,cl.id as idclient FROM vehicle ve INNER JOIN company_client cl on ve.company_clientid=cl.id WHERE ve.status =:status", nativeQuery = true)
//    Page<VehicleListDto> findAllWithFieldsContaining(Pageable pageable,@Param("status")boolean status);
    @Query(value = " SELECT CASE WHEN count(e.plate) > 0 THEN 1 ELSE 0 END FROM vehicle e inner join company_client cl on e.company_clientid=cl.id " +
            "INNER JOIN historial_company h on cl.id=h.company_client_id " +
            "INNER JOIN company c on c.id= h.company_id " +
            " where e.plate =:plate and e.status=:status and cl.status=:statucl and c.status=:statusemv", nativeQuery = true)
    Integer existsByPlateAndStatusAll(@Param("plate") String plate, @Param("status") Integer status, @Param("statucl") Integer statucl, @Param("statusemv") Integer statusemv);

    @Query(value = "SELECT ve.id,ve.category,ve.code_osinergmin,ve.creation_date,ve.plate,ve.status,cl.name as nameclient,cl.id as idclient FROM vehicle ve INNER JOIN company_client cl on ve.company_clientid=cl.id WHERE  cl.id=:companyClientId ", nativeQuery = true)
    List<VehicleListDto> findByCompanyClient(@Param("companyClientId") String company_client_id);


    @Query(value = "SELECT v.plate from vehicle v inner join trama t on v.plate=t.plate where v.status =:status group by v.plate", nativeQuery = true)
    List<VehicleCategoryDto> platePositionsTrama(@Param("status") boolean status);

    @Query(value = "SELECT ve.code_osinergmin,ve.status,cl.name as nameclient,c.name as nameemv " +
            "FROM vehicle ve INNER JOIN company_client cl on ve.company_clientid=cl.id " +
            "INNER JOIN historial_company h on cl.id=h.company_client_id " +
            " INNER JOIN company c on c.id= h.company_id "+
            "WHERE ve.plate=:plate and ve.status =:status", nativeQuery = true)
    IVehicleCompanyReportDto findByPlate(@Param("plate") String plate, @Param("status") boolean status);

    @Query(value = "SELECT ve.id,ve.category,ve.code_osinergmin,ve.creation_date,ve.plate,ve.status,cl.name as nameclient,"
            + " cl.id as idclient, c.name as CompanyName "
            + " FROM vehicle ve INNER JOIN company_client cl on ve.company_clientid=cl.id "
            + " INNER JOIN historial_company h on cl.id=h.company_client_id "
            + " INNER JOIN company c on c.id= h.company_id "
            + " WHERE ve.status =:status", nativeQuery = true)
    List<IVehicleCompanyDto> findByCompanyAndCompanyClient(@Param("status") boolean status);

//    @Query(value = "SELECT ve.id,ve.category,ve.code_osinergmin,ve.creation_date,ve.plate,ve.status,cl.name as nameclient,"
//            + " c.ingest_token as token, c.name as namecompany ,ve.status as statusvehicle, cl.status as statuscliente,c.status as statusemv"
//            + " FROM vehicle ve INNER JOIN company_client cl on ve.company_clientid=cl.id "
//            + " INNER JOIN historial_company h on cl.id=h.company_client_id "
//            + " INNER JOIN company c on c.id= h.company_id "
//            + " WHERE ve.status =:status and cl.status=:statusclient and c.status =:statuscompany", nativeQuery = true)
//    List<IVehicleCompanyExportarExcelDto> findByCompanyAndCompanyClientExportarExcel(@Param("status") boolean status,@Param("statusclient") boolean statusclient,@Param("statuscompany") boolean statuscompany);

    @Query(value = "SELECT ve.id,ve.category,ve.code_osinergmin,ve.creation_date,ve.plate,cl.name as nameclient,"
            + " c.ingest_token as token, c.name as namecompany ,ve.status as statusvehicle, cl.status as statuscliente,c.status as statusemv"
            + " FROM vehicle ve INNER JOIN company_client cl on ve.company_clientid=cl.id "
            + " INNER JOIN historial_company h on cl.id=h.company_client_id "
            + " INNER JOIN company c on c.id= h.company_id ", nativeQuery = true)
    List<IVehicleCompanyExportarExcelDto> findByCompanyAndCompanyClientExportarExcel();

    @Query(value = "SELECT ve.id,ve.plate,ve.code_osinergmin,ve.category,CONVERT(varchar,DATEADD(HH,-5,CONVERT(datetime2,ve.creation_date)),120) as creation_date,cl.name as nameclient,cl.id as idclient from vehicle ve inner join company_client cl on ve.company_clientid=cl.id INNER JOIN historial_company h on cl.id=h.company_client_id INNER JOIN company c on c.id= h.company_id" +
            " WHERE concat((ve.plate),(ve.code_osinergmin),(ve.category),(cl.name),convert(varchar,(ve.creation_date), 120)) like %:busca% and ve.status =:status and cl.status=:statusclient and c.status=:statusemv", nativeQuery = true)
    List<Map<String, Object>> findAllWithFieldsContaining(Pageable pageable, @Param("busca") String busca, @Param("status") boolean status, @Param("statusclient") boolean statusclient, @Param("statusemv") boolean statusemv);

    @Query(value = "SELECT c.id as idclient, cl.name as nameclient,c.name as namecompany, ve.category from vehicle ve " +
            "INNER JOIN company_client cl on ve.company_clientid=cl.id " +
            "INNER JOIN historial_company h on cl.id=h.company_client_id " +
            " INNER JOIN company c on c.id= h.company_id "
            + " WHERE ve.plate=:plate and ve.status =:status and cl.status=:statusclient and c.status =:statuscompany", nativeQuery = true)
    Map<String, Object> findPlateEmvCompany(@Param("plate") String plate, @Param("status") boolean status, @Param("statuscompany") boolean statuscompany, @Param("statusclient") boolean statusclient);


//    @Query(value = "SELECT ve.*,cl.name as nameclient,cl.id as idclient from vehicle ve inner join company_client cl on ve.company_clientid=cl.id"
//            + " WHERE ve.status =:status and   (ve.creation_date like %:creation_date% )", nativeQuery = true)
//    List<Map<String, Object>> findAllWithFieldsContainingDate(Pageable pageable, @Param("status") boolean status, @Param("creation_date") String creation_date);


    @Query(value = "SELECT count(*) from vehicle ve inner join company_client cl on ve.company_clientid=cl.id INNER JOIN historial_company h on cl.id=h.company_client_id INNER JOIN company c on c.id= h.company_id" +
            " WHERE concat((ve.plate),(ve.code_osinergmin),(ve.category),(cl.name),convert(varchar,(ve.creation_date), 120)) like %:busca% and ve.status =:status and cl.status=:statusclient and c.status=:statusemv", nativeQuery = true)
    long countByStatus(@Param("busca") String busca, @Param("status") boolean status, @Param("statusclient") boolean statuscl, @Param("statusemv") boolean statuscompany);


    /*
    @Query(value = "SELECT COUNT(*) FROM vehicle v inner join\n" +
            "    company_client cl on v.company_clientid = cl.id INNER JOIN historial_company h on cl.id=h.company_client_id " +
            "INNER JOIN company c on c.id= h.company_id" +
            " where v.status=:status and cl.status=:statuscl and c.status =:statuscompany", nativeQuery = true)
    long countByStatus(@Param("status") boolean status, @Param("statuscl") boolean statuscl, @Param("statuscompany") boolean statuscompany);

     */
    
    boolean existsVehicleByPlateAndStatus(String plate,boolean status);
 
}
