package com.backend.ingest.service;

public interface MqConsumerService {

	void mqConsumerSaveDatabaseTrama();
	void mqConsumerCalculateGeocerca();
	void mqConsumerReportSpedd();
	void mqConsumerReportTravels();
	void mqConsumeAndSendTramaWebSocket();
	void mqConsumeAndSendStoppedCisternFour();
	void mqConsumeAndSendStoppedCistern();
	void mqConsumeAndSendReportTransmissionLoss();
	void mqConsumeAndSendReportTimeLag();        
        void mqConsumerByReportsChannel();
        void mqConsumeLastPosReportMessage();
     

}
