/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.interfaces;

import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.GeneralException;

/**
 *
 * @author Joshua Ly
 */
public interface IActionService {
    
     void create(String name, String session_id, String table, String record_id) throws DataNotFoundException,GeneralException;
}
