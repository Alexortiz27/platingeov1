package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.CompanyClientInsDto;
import com.backend.ingest.dtos.CompanyClientListDto;
import com.backend.ingest.dtos.CompanyClientListPageDto;
import com.backend.ingest.dtos.CompanyClientListPaginationDto;
import com.backend.ingest.dtos.CompanyClientPageDto;
import com.backend.ingest.dtos.CompanyClientShowDto;
import com.backend.ingest.dtos.CompanyClientUpdDto;
import com.backend.ingest.entities.CompanyClient;
import com.backend.ingest.entities.Historial_Company;
import com.backend.platin.exceptions.DataNotFoundException;
import java.io.IOException;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ICompanyClientService {

    CompanyClientShowDto create(CompanyClientInsDto companyClient) throws DataNotFoundException;

    public void excelExport(HttpServletResponse response) throws Exception;

    CompanyClientListDto update(String id, CompanyClientUpdDto CompanyClient) throws DataNotFoundException;

    CompanyClient findById(String id) throws DataNotFoundException;

    List<CompanyClientListPageDto> findByStatus() throws DataNotFoundException;

    List<Map<String, Object>> findAllPage(Pageable pageable,String busca) throws DataNotFoundException;

    List<CompanyClientListDto> findAllPage() throws DataNotFoundException;

    List<CompanyClientListPageDto> findByName(String name, String company_id) throws DataNotFoundException;

    void delete(String id) throws DataNotFoundException;

    CompanyClientListDto getId(String id) throws DataNotFoundException;

    Integer existsByName(String name);

    List<CompanyClientListDto> findByCompany(String company_id) throws DataNotFoundException;

    List<CompanyClientListPageDto> findNameDiferent(String id,String name,String id_company);

    public long countByStatus(String busca);

}
