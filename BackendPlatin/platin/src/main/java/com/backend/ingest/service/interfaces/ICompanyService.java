/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.CompanyShowDto;
import com.backend.ingest.dtos.CompanyUpdDto;
import com.backend.ingest.dtos.ICompanyNameDto;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.User;
import com.backend.platin.exceptions.DataNotFoundException;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Pageable;

/**
 * @author FAMLETO
 */
public interface ICompanyService {

    CompanyShowDto create(Company company);

    List<Company> findNameDiferent(String id);

    CompanyShowDto update(String id, CompanyUpdDto company) throws DataNotFoundException;

    Company findById(String id) throws DataNotFoundException;

    public void excelExport(HttpServletResponse response) throws Exception;

    long countByStatus(String busca);

    List<Map<String, Object>> findByStatus() throws DataNotFoundException;

    List<Map<String, Object>> findAllPage(Pageable of, String busca) throws DataNotFoundException;

    void delete(String id) throws DataNotFoundException;

    CompanyShowDto getId(String id) throws DataNotFoundException;

    Company findByUser(User user) throws DataNotFoundException;

    Company findByIngestToken(String token) throws DataNotFoundException;

    ICompanyNameDto getCompanyNameByToken(String tokenTrama) throws DataNotFoundException;

    Integer existsByIdAll(String id);
    
    boolean existsCompanyByTokenTramaAndStatus(String token);
    
}
