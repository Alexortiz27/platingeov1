package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.Event;
import com.backend.platin.exceptions.DataNotFoundException;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Map;


public interface IEventService {

    Event create(Event event);

    EventInsDto save(EventInsDto event);

    EventShowDto update(String id, EventInsDto eventDto) throws DataNotFoundException;

    Event findById(String id) throws DataNotFoundException;

    List<EventShowDto> findByStatus() throws DataNotFoundException;

//    List<Event> findByContainingTypeDate(String plate, String type, Date startDate, Date endDate);

    List<Event> findByContainingTypeDateAll(String type, String typeEnd, String plate, Date startDate, Date endDate);

    void delete(String id) throws DataNotFoundException;

    List<Map<String, Object>> findAllPage(Pageable of, String busca) throws DataNotFoundException;

//    List<Event> findByTypeAndPlateAndCreationDateBetween(String[] types, String plate, Date startDate, Date endDate);

    EventShowDto getById(String id) throws DataNotFoundException;

//    List<Event> findByPlateAndCreationDateBetween(String vehicle_id, Date startDate, Date endDate);

    long countByStatus(String busca);

    List<EventTravelDto> getTravelEvents(String type, String typeEnd, String plate, Date startDate, Date endDate);

    List<EventStoppedDto> getStoppedEvents(String type, String typeEnd, String plate, Date startDate, Date endDate);

    List<EventGeozoneDto> getGeozoneEvents(String[] types, String plate, Date startDate, Date endDate);

    List<EventSpeedDto> getSpeedEvent(String plate, String type, Date startDate, Date endDate);

    List<EventNoTransmissionDto> getNoTransmissionEvents(String type, String typeEnd, String plate, Date startDate, Date endDate);

    List<EventTimeLagDto> getTimeLagEvents(String plate, String type, Date startDate, Date endDate);
    
//    List<IEventStoppedDto> findGps_DateAndTypeOfStoppedEvent( String[] types, String plate,Date startDate, Date endDate);
    
}
