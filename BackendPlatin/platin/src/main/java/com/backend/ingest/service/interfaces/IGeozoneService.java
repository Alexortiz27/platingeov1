/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.GeozoneCircleDto;
import com.backend.ingest.dtos.GeozoneCircleUpdDto;
import com.backend.ingest.dtos.IGeozoneNameDto;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.Geozone;
import com.backend.platin.exceptions.DataNotFoundException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author FAMLETO
 */
public interface IGeozoneService {

    Geozone create(Geozone geozone);

    public void excelImport(MultipartFile file) throws IOException, DataNotFoundException;

    Geozone findNameDiferent(String id, String name) throws DataNotFoundException;

    Geozone update(String id, GeozoneCircleUpdDto geozoneDto) throws DataNotFoundException;

    Geozone findById(String id) throws DataNotFoundException;

    List<Geozone> findAll() throws DataNotFoundException;

    List<Geozone> findByStatus() throws DataNotFoundException;

    List<Map<String, Object>> findByStatusAll() throws DataNotFoundException;

    Geozone delete(String id) throws DataNotFoundException;

    List<Map<String, Object>> findAllPage(Pageable of,String busca) throws DataNotFoundException;
    
    IGeozoneNameDto getNameAndTypeGeozoneById(String id) throws DataNotFoundException;

    long countByStatus(String busca);

}
