package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.MapInsDto;
import com.backend.ingest.dtos.MapShowDto;
import com.backend.ingest.dtos.MapUpdDto;
import com.backend.ingest.entities.Maps;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.platin.exceptions.DataNotFoundException;
import org.springframework.data.repository.query.Param;

public interface IMapService {

    Maps create(MapInsDto mapDto) throws DataNotFoundException;

    Maps update(String id, MapUpdDto mapspDto) throws DataNotFoundException;

    List<Maps> findNameDiferent(String id) throws DataNotFoundException;

    Maps findById(String id) throws DataNotFoundException;

    List<Maps> findAll() throws DataNotFoundException;

    List<Maps> findByStatus() throws DataNotFoundException;

    List<Map<String, Object>> findAllPage(Pageable of, String buscar, String user_id) throws DataNotFoundException;

    Maps delete(String id) throws DataNotFoundException;

    Maps getById(String id) throws DataNotFoundException;
    long countByStatus(String buscar, String user_id);
}
