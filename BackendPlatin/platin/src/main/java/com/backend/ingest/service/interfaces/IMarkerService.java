/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.MarkerDto;
import com.backend.ingest.dtos.MarkerUpdDto;
import com.backend.ingest.entities.Marker;
import com.backend.platin.exceptions.DataNotFoundException;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author FAMLETO
 */
public interface IMarkerService {

    List<Marker> findNameDiferent(String id);

    Marker create(MarkerDto markerDto) throws DataNotFoundException;

    Marker update(String id, MarkerUpdDto markerDto) throws DataNotFoundException;

    Marker findById(String id) throws DataNotFoundException;

    List<Marker> findAll()throws DataNotFoundException;

    List<Marker> findByStatus()throws DataNotFoundException;

    List<Map<String, Object>> findAllPage(Pageable pageable, String busca, String user_id)throws DataNotFoundException;

    void delete(String id) throws DataNotFoundException;

    long countByStatus(String busca, String user_id);
}
