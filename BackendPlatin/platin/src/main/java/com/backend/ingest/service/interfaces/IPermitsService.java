package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.PermitsUpdDto;
import com.backend.ingest.dtos.RolUpdDto;
import com.backend.ingest.entities.Permits;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.ingest.entities.Rol;
import com.backend.ingest.entities.User;
import com.backend.platin.exceptions.DataNotFoundException;
import org.springframework.data.repository.query.Param;

public interface IPermitsService {

    Permits create(Permits cliente);

    Permits update(String id, PermitsUpdDto permitspDto) throws DataNotFoundException;

    Permits findById(String id) throws DataNotFoundException;

    List<Permits> findAll() throws DataNotFoundException;

    List<Permits> findByStatus() throws DataNotFoundException;

    Page<Permits> findAllPage(Pageable of, String busca) throws DataNotFoundException;

    void delete(String id) throws DataNotFoundException;

    public Optional<Permits> getByRolName(String PermitsNombre);

    Permits findNameDiferent(String id, Integer status, String name);


}
