/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.ReportInDto;
import com.backend.ingest.entities.Report;
import com.backend.platin.exceptions.DataNotFoundException;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author FAMLETO
 */
public interface IReportService {
    Report create(ReportInDto reportDto)  throws DataNotFoundException;
    List<Report> findByStatus() throws DataNotFoundException;
    Report update(String id, ReportInDto reportDto) throws DataNotFoundException;
    Report findById(String id) throws DataNotFoundException;
    List<Report> findAll()throws DataNotFoundException;
    void delete(String id) throws DataNotFoundException;
    List<Map<String, Object>>  findAllPage(Pageable pageable, String busca) throws DataNotFoundException;
    long countByStatus(String busca);

}
