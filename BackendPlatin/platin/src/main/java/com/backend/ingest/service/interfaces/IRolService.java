package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.RolUpdDto;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.ingest.entities.Rol;
import com.backend.ingest.entities.User;
import com.backend.platin.exceptions.DataNotFoundException;

public interface IRolService {

    Rol create(Rol cliente);

    List<Rol> findNameDiferent(String id);

    Rol update(String id, RolUpdDto rolupDto) throws DataNotFoundException;

    Rol findById(String id) throws DataNotFoundException;

    List<Rol> findAll()throws DataNotFoundException;

    List<Rol> findByStatus()throws DataNotFoundException;

    Page<Rol> findAllPage(Pageable of,String busca)throws DataNotFoundException;

    void delete(String id) throws DataNotFoundException;

}
