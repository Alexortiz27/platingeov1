/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.MarkerDto;
import com.backend.ingest.dtos.SessionDto;
import com.backend.ingest.entities.Marker;
import com.backend.ingest.entities.Session;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.EmptyFieldsException;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author FAMLETO
 */
public interface ISessionService {
    
    Session create(SessionDto userCompleteNames)throws EmptyFieldsException;
    Session update(String id, SessionDto sesi) throws DataNotFoundException,EmptyFieldsException;
    Session findById(String id) throws DataNotFoundException;
    List<Session> findAll()throws DataNotFoundException,EmptyFieldsException;
    Page<Session> findAllPage(Pageable pageable,String buscar)throws DataNotFoundException,EmptyFieldsException;
 
    
}
