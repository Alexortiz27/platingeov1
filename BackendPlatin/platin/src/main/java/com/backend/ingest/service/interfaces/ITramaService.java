/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.ITramaDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.entities.Trama;
import com.backend.platin.exceptions.DataNotFoundException;

import java.util.Date;
import java.util.List;

/**
 *
 * @author FAMLETO
 */
public interface ITramaService {

    Trama create(Trama trama) throws DataNotFoundException;

    Trama update(String id, TramaInsDto tramaDto) throws DataNotFoundException;

    Trama findById(String id) throws DataNotFoundException;

    List<Trama> findAll();

    void delete(String id) throws DataNotFoundException;

    List<Trama> findByStatus();

    Trama findAllWithFieldsContaining(String position);

    List<Trama> getTramasByPlateAndGpsDate(String plate, Date startDate, Date endDate);

    List<ITramaDto> findByPlateGpsDateBetweenCompanyIdAsc(String plate, Date startDate, Date endDate);
    
   
}
