package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Travel;
import com.backend.ingest.entities.Vehicle;
import com.backend.platin.exceptions.DataNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

public interface ITravelService {

    TravelShowDto create(Travel event);

    Travel findNameDiferent(String id,String name);

    TravelShowDto update(String id, TravelUpdDto travelUpdDto) throws DataNotFoundException;

    Travel findById(String id) throws DataNotFoundException;

    List<TravelShowDto> findAll()throws DataNotFoundException;

    List<Map<String, Object>> findAllPage(Pageable of, String busca)throws DataNotFoundException;

    List<TravelShowDto> findByStatus()throws DataNotFoundException;

    List<TravelInterfReportDto> findAllWithFieldsContaining(String plate, Date gpsDate);

    List<Travel> findAllWithFieldsContainingPlateReport(String idvehicle, Date startDate, Date endDate);

    void delete(String id) throws DataNotFoundException;

    TravelShowDto getId(String id) throws DataNotFoundException;

    long countByStatus(String busca);

}
