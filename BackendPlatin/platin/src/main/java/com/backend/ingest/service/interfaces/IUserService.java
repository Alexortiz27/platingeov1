package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.UserUpdDto;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.backend.ingest.entities.User;
import com.backend.platin.exceptions.DataNotFoundException;

public interface IUserService {

    User create(User cliente);

    User update(String id, UserUpdDto userDto) throws DataNotFoundException;

    User findById(String id) throws DataNotFoundException;

    List<User> findAll()throws DataNotFoundException;

    void delete(String id) throws DataNotFoundException;

    List<User> findByStatus()throws DataNotFoundException;

    Page<User> findAllPage(Pageable of,String busca)throws DataNotFoundException;

    Optional<User> findByUserOrEmail(String nameOrEmail);

    Integer existsByUser(String nombreUsuario);

    Integer existsByEmail(String email);

    List<User> findNameDiferent(String id);

    Optional<User> getByTokenPassword(String tokenPassword);

    boolean existsByPassword(String password);

    Optional<User> getByEmail(String email);

}
