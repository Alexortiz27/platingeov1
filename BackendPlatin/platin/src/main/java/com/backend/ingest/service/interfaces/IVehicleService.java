/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.interfaces;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.CompanyClient;
import com.backend.ingest.entities.Vehicle;
import com.backend.platin.exceptions.DataNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.backend.platin.exceptions.GeneralException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author FAMLETO
 */
public interface IVehicleService {

    VehicleShowDto create(Vehicle company);

    Vehicle findNameDiferent(String id,String plate);

    public void excelExport(HttpServletResponse response) throws IOException;

    VehicleShowDto update(String id, VehicleUpdDto vehicleUpDto) throws DataNotFoundException;

    Vehicle findById(String id) throws DataNotFoundException;

    List<Vehicle> findAll() throws DataNotFoundException;

    List<Vehicle> findByStatus() throws DataNotFoundException;

    List<VehicleListDto> findByVehicleCompany() throws DataNotFoundException;

    Page<Vehicle> findAllPage(Pageable of) throws DataNotFoundException;

    Map<String, Object> findPlateEmvCompany(String plate);

    public List<Map<String, Object>> findAllPageLink(Pageable of, String busca) throws DataNotFoundException;

    void delete(String id) throws DataNotFoundException;

    Vehicle findByPlate(String plate) throws DataNotFoundException;

    String findByPlateWorker(String plate) throws DataNotFoundException;

    VehicleShowDto getId(String id) throws DataNotFoundException;

    Integer existsByPlate(String plate);

    List<VehicleListDto> findByCompany(String company_client_id);

    void showVehiclesTransmitting() throws DataNotFoundException;


    List<VehicleCategoryDto> platePositionsTrama();

    IVehicleCompanyReportDto vehiclefindByPlate(String plate) throws DataNotFoundException;

    public long countByStatus(String busca);
    
    boolean existPlate(String plate);
    
    List<LastPositionVehicleDto> showLastPositionVehicles(List<String>plates) throws DataNotFoundException, ParseException, GeneralException, Exception;
    
    List<LastPositionVehicleDto> lastPositionVehiclesOfFiveMinutesAgo(List<String>plates) throws Exception;
    
    List<LastPositionVehicleDto> lastPositionVehiclesOfTenMinutesAgo(List<String>plates) throws Exception;
    
    List<LastPositionVehicleDto> lastPositionVehiclesMoreOfTenMinutesAgo(List<String>plates) throws Exception;
    
    
}
