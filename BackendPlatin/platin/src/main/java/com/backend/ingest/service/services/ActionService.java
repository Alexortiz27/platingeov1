/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.services;


import com.backend.ingest.entities.Accion;
import com.backend.ingest.entities.Session;
import com.backend.ingest.entities.User;
import com.backend.ingest.repositories.IActionRepository;
import com.backend.ingest.repositories.ISessionRepository;
import com.backend.ingest.repositories.IUserRepository;
import com.backend.ingest.service.interfaces.IActionService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.GeneralException;
import com.backend.platin.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Joshua Ly
 */
@Service
public class ActionService implements IActionService{

    
    @Autowired
    private IActionRepository actionRepository;
    
    @Autowired
    private ISessionRepository sessionRepository;
    
    @Autowired
    private IUserRepository  userRepository;
    
    @Override
    public void create(String name, String session_id, String table, String record_id) throws DataNotFoundException,GeneralException
    {
        if(session_id==null)
        {
            throw new GeneralException("La sessión no existe");
        }
        Session session = sessionRepository.findById(session_id).orElse(null);
        
        if(session==null)
        {
            throw new DataNotFoundException(Util.SESSION_NOT_FOUND);
        }
        
        if(session.getUser_id()==null)
        {
            throw new DataNotFoundException(Util.USER_NOT_FOUND);
        }
        
        Accion action = new Accion(session,record_id,name,table);
        actionRepository.save(action);
       
    }
    
}
