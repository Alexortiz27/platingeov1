package com.backend.ingest.service.services;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.backend.ingest.dtos.CompanyBasicDto;
import com.backend.ingest.dtos.CompanyClientDtoExcel;
import com.backend.ingest.dtos.CompanyClientInsDto;
import com.backend.ingest.dtos.CompanyClientListDto;
import com.backend.ingest.dtos.CompanyClientListPageDto;
import com.backend.ingest.dtos.CompanyClientPageDto;
import com.backend.ingest.dtos.CompanyClientShowDto;
import com.backend.ingest.dtos.CompanyClientUpdDto;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.CompanyClient;
import com.backend.ingest.entities.Historial_Company;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.ingest.repositories.ICompanyClientRepository;
import com.backend.ingest.repositories.ICompanyRepository;
import com.backend.ingest.repositories.IHistorialCompanyRepository;
import com.backend.ingest.service.interfaces.ICompanyClientService;
import com.backend.platin.util.Util;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CompanyClientService implements ICompanyClientService {

    @Autowired
    private ICompanyClientRepository CompanyClientClientRepo;
    @Autowired
    private ICompanyRepository companyRepo;

    @Autowired
    private IHistorialCompanyRepository historialRepository;

    @Override
    public CompanyClientShowDto create(CompanyClientInsDto companyClient) throws DataNotFoundException {

        CompanyClient CompanyClientNew = new CompanyClient(companyClient.getName(), companyClient.getEmail(), companyClient.getRuc(), companyClient.getAddress(), companyClient.getPhone());
        CompanyClientNew = CompanyClientClientRepo.save(CompanyClientNew);

        Company company = companyRepo.findById(companyClient.getId_company()).get();

        if (company == null) {
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);
        }

        Historial_Company historial = new Historial_Company();
        historial.setCompany(company);
        historial.setCompanyClient(CompanyClientNew);
        historial.setCreationDate(new Date());
        this.historialRepository.save(historial);

        return new CompanyClientShowDto(CompanyClientNew.getId(), CompanyClientNew.getName(), CompanyClientNew.getEmail(),
                CompanyClientNew.getRuc(), CompanyClientNew.getAddress(), CompanyClientNew.getPhone(),
                CompanyClientNew.getStatus(), CompanyClientNew.getCreationDate().toString(), Util.parseCompanyToBasicDto(company));
    }

    @Override
    public void excelExport(HttpServletResponse response) throws Exception {
        List<CompanyClientListPageDto> list = CompanyClientClientRepo.findAllStatus(Boolean.TRUE,Util.ACTIVE_STATUS);

        if (list.isEmpty()) {
            throw new DataNotFoundException(Util.COMPANY_CLIENT_NOT_FOUND);
        }

        List<CompanyClientDtoExcel> listCompany = new ArrayList<>();
        for (CompanyClientListPageDto companyDtoExcel : list) {
            listCompany.add(new CompanyClientDtoExcel(companyDtoExcel.getName(), companyDtoExcel.getEmail(),
                    companyDtoExcel.getRuc(),
                    companyDtoExcel.getAddress(),
                    companyDtoExcel.getPhone(), companyDtoExcel.getNamecompany(),
                    companyDtoExcel.getCreation_date()));
        }
        String uuid = UUID.randomUUID().toString().toUpperCase();

        String fileName = "ListadoEMV-" + uuid;
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1") + ".xls");
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        Sheet sheet = new Sheet(1, 0, CompanyClientDtoExcel.class);
        // Establecer el ancho adaptativo
        sheet.setAutoWidth(Boolean.TRUE);
        sheet.setSheetName("Lista de EMV");
        writer.write(listCompany, sheet);
        writer.finish();
        out.flush();
        response.getOutputStream().close();
        out.close();
    }

    @Override
    public CompanyClientListDto update(String id, CompanyClientUpdDto CompanyClientDto) throws DataNotFoundException {
        CompanyClient companyClient = findById(id);

        if (companyClient == null || !companyClient.getStatus()) {
            throw new DataNotFoundException(Util.COMPANY_CLIENT_NOT_FOUND);
        }

        companyClient.setName(CompanyClientDto.getName());
        companyClient.setRuc(CompanyClientDto.getRuc());
        companyClient.setEmail(CompanyClientDto.getEmail());
        companyClient.setAddress(CompanyClientDto.getAddress());
        companyClient.setPhone(CompanyClientDto.getPhone());
        companyClient.setStatus(Util.ACTIVE_STATUS);

        List<Historial_Company> historials = companyClient.getHistorial();
        for (Historial_Company historial : historials) {
            if (historial.getCompany().getId().equals(CompanyClientDto.getId_company())) {
                historial.setCompany(historial.getCompany());

            } else {
                historial.setCompany(companyRepo.findById(CompanyClientDto.getId_company()).get());
            }
        }

        companyClient.setHistorial(historials);
        companyClient = CompanyClientClientRepo.save(companyClient);
        return this.parseCompanyToShowDto(companyClient);
    }

    @Override
    public CompanyClient findById(String id) throws DataNotFoundException {
        Optional<CompanyClient> CompanyClientOptional = CompanyClientClientRepo.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        CompanyClient CompanyClient = CompanyClientOptional.orElse(null);

        if (CompanyClient == null || !CompanyClient.getStatus()) {
            throw new DataNotFoundException(Util.COMPANY_CLIENT_NOT_FOUND);
        }

        return CompanyClientOptional.orElse(null);
    }

    @Override
    public List<CompanyClientListPageDto> findByStatus() throws DataNotFoundException {
        List<CompanyClientListPageDto> companiesClient = CompanyClientClientRepo.findAllStatus(Util.ACTIVE_STATUS,Util.ACTIVE_STATUS);

        if (companiesClient.isEmpty()) {
            throw new DataNotFoundException(Util.COMPANY_CLIENT_NOT_FOUND);
        }

        return companiesClient;
    }

    @Override
    public List<Map<String, Object>> findAllPage(Pageable pageable, String busca) throws DataNotFoundException {
        List<Map<String, Object>> companiesClient = null;
            companiesClient = CompanyClientClientRepo.findAllWithFieldsContaining(pageable,busca, Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT);
        if (companiesClient.isEmpty()) {
            throw new DataNotFoundException(Util.COMPANY_CLIENT_NOT_FOUND);
        }
        return companiesClient;
    }

    @Override
    public List<CompanyClientListDto> findAllPage() throws DataNotFoundException {

        List<CompanyClient> companiesClient = CompanyClientClientRepo.findByStatus(Util.ACTIVE_STATUS);

        if (companiesClient.isEmpty()) {
            throw new DataNotFoundException(Util.COMPANY_CLIENT_NOT_FOUND);
        }

        List<CompanyClientListDto> companiesClientDto = new ArrayList<CompanyClientListDto>();

        for (CompanyClient companyClient : companiesClient) {

            companiesClientDto.add(Util.parseCompanyToShowDto(companyClient));
        }

        return companiesClientDto;
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        CompanyClient CompanyClient = findById(id);

        if (CompanyClient == null || !CompanyClient.getStatus()) {
            throw new DataNotFoundException(Util.COMPANY_CLIENT_NOT_FOUND);
        }

        CompanyClient.setStatus(Util.INACTIVE_STATUS);

        CompanyClientClientRepo.save(CompanyClient);
    }

    @Override
    public CompanyClientListDto getId(String id) throws DataNotFoundException {
        Optional<CompanyClient> CompanyClientOptional = CompanyClientClientRepo.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        CompanyClient companyClient = CompanyClientOptional.orElse(null);

        if (companyClient == null || !companyClient.getStatus()) {
            throw new DataNotFoundException(Util.COMPANY_CLIENT_NOT_FOUND);
        }
        return Util.parseCompanyToShowDto(companyClient);
    }

    @Override
    public Integer existsByName(String name) {
        return CompanyClientClientRepo.existsByName(name,Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public List<CompanyClientListDto> findByCompany(String company_id) throws DataNotFoundException {

        Company company = companyRepo.findByIdAndStatus(company_id,Util.ACTIVE_STATUS).get();

        if (company == null) {
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);
        }
        List<Historial_Company> historials = historialRepository.findByCompany(company);
        List<CompanyClientListDto> companiesClientDto = new ArrayList<CompanyClientListDto>();

        for (Historial_Company historial : historials) {

            companiesClientDto.add(Util.parseCompanyToShowDto(historial.getCompanyClient()));
        }

        return companiesClientDto;
    }

    private CompanyClientListDto parseCompanyToShowDto(CompanyClient companyClient) {
        List<CompanyBasicDto> companies = new ArrayList<>();
        for (Historial_Company historial : companyClient.getHistorial()) {

            companies.add(Util.parseCompanyToBasicDto(historial.getCompany()));
        }

        CompanyClientListDto companyClientDto = new CompanyClientListDto(companyClient.getId(), companyClient.getName(), companyClient.getEmail(),
                companyClient.getRuc(), companyClient.getAddress(), companyClient.getPhone(), companies, companyClient.getStatus(), companyClient.getCreationDate());
        return companyClientDto;
    }

    @Override
    public List<CompanyClientListPageDto> findNameDiferent(String id,String name,String id_company) {
        return CompanyClientClientRepo.findNameDiferent(id,Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT,name,id_company);
    }

    @Override
    public List<CompanyClientListPageDto> findByName(String name, String company_id) throws DataNotFoundException {

        Optional<Company> companyOptional = companyRepo.findByIdAndStatus(company_id,Util.ACTIVE_STATUS);
        Company company = companyOptional.orElse(null);
        if (company == null) {
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);
        }
        return CompanyClientClientRepo.findByNameList(name);

    }

    @Override
    public long countByStatus(String busca) {
        return CompanyClientClientRepo.countByStatus(busca,Util.ACTIVE_STATUS,Util.ACTIVE_STATUS);
    }

}
