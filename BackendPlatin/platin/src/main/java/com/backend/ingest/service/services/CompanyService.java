/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.services;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.backend.ingest.dtos.CompanyDtoExcel;
import com.backend.ingest.dtos.CompanyShowDto;
import com.backend.ingest.dtos.CompanyUpdDto;
import com.backend.ingest.dtos.ICompanyNameDto;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.User;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.ingest.repositories.ICompanyRepository;
import com.backend.ingest.service.interfaces.ICompanyService;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.platin.util.Util;

import java.util.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author FAMLETO
 */
@Service
public class CompanyService implements ICompanyService {

    @Autowired
    private ICompanyRepository companyRepo;
    @Autowired
    private IUserService userService;

    @Override
    public CompanyShowDto create(Company company) {
        company = companyRepo.save(company);

        return Util.parseCompanyToShowDto(company);
    }



    @Override
    public CompanyShowDto update(String id, CompanyUpdDto companyDto) throws DataNotFoundException {

        Company company = findById(id);

        if (company == null || !company.getStatus()) {
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);
        }

        company.setName(companyDto.getName());
        company.setIngestToken(company.getIngestToken());
        company.setEmail(companyDto.getEmail());
        company.setAddress(companyDto.getAddress());
        company.setPhone(companyDto.getPhone());
        company.setCity(companyDto.getCity());
        company.setCountry(companyDto.getCountry());
        company.setStatus(Util.ACTIVE_STATUS);
        User userd = userService.findById(companyDto.getUser_id());
        company.setUser(userd);
        company = companyRepo.save(company);
        return Util.parseCompanyToShowDto(company);
    }

    @Override
    public Company findById(String id) throws DataNotFoundException {
        Optional<Company> companyOptional = companyRepo.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        Company company = companyOptional.orElse(null);

        if (company == null || !company.getStatus()) {
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);
        }

        return companyOptional.orElse(null);
    }

    @Override
    public void excelExport(HttpServletResponse response) throws Exception {
        List<Map<String, Object>> list = companyRepo.findByStatus(Util.ACTIVE_STATUS_BIT);

        if (list.isEmpty())
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);

        List<CompanyDtoExcel> listCompany = new ArrayList<>();
        for (Map<String, Object> companyDtoExcel : list) {
            listCompany.add(new CompanyDtoExcel(companyDtoExcel.get("name"), companyDtoExcel.get("ingest_token"),
                    companyDtoExcel.get("email"), companyDtoExcel.get("address"),
                    companyDtoExcel.get("phone"), companyDtoExcel.get("city"), companyDtoExcel.get("country"),
                    companyDtoExcel.get("creation_date")));

        }
        String uuid = UUID.randomUUID().toString().toUpperCase();

        String fileName = "ListadoCompany-" + uuid;
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1") + ".xls");
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        Sheet sheet = new Sheet(1, 0, CompanyDtoExcel.class);
        // Establecer el ancho adaptativo
        sheet.setAutoWidth(Boolean.TRUE);
        sheet.setSheetName("Lista de Company");
        writer.write(listCompany, sheet);
        writer.finish();
        out.flush();
        response.getOutputStream().close();
        out.close();
    }

    @Override
    public List<Map<String, Object>> findByStatus() throws DataNotFoundException {
        List<Map<String, Object>> companies = companyRepo.findByStatus(Util.ACTIVE_STATUS_BIT);

        if (companies.isEmpty())
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);

        return companies;
    }

    @Override
    public List<Map<String, Object>> findAllPage(Pageable pageable, String busca) throws DataNotFoundException {
        List<Map<String, Object>> companies = null;
            companies = companyRepo.findAllWithFieldsContaining(pageable, Util.ACTIVE_STATUS_BIT, busca);
        if (companies.isEmpty())
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);

        return companies;
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        Company company = findById(id);

        if (company == null || !company.getStatus()) {
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);
        }

        company.setStatus(Util.INACTIVE_STATUS);

        companyRepo.save(company);
    }

    @Override
    public CompanyShowDto getId(String id) throws DataNotFoundException {
        Optional<Company> companyOptional = companyRepo.findById(id);

        Company company = companyOptional.orElse(null);

        if (company == null || !company.getStatus()) {
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);
        }

        return Util.parseCompanyToShowDto(company);
    }

    @Override
    public Company findByUser(User user) throws DataNotFoundException {
        Company company = companyRepo.findByUser(user);

        if (company == null)
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);

        return company;
    }

    @Override
    public List<Company> findNameDiferent(String id) {
        return companyRepo.findNameDiferent(id,Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public Company findByIngestToken(String token) throws DataNotFoundException {
        Company company = companyRepo.findByIngestToken(token);

        if (company == null)
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);

        return company;
    }

    @Override
    public long countByStatus(String busca) {
        return companyRepo.countByStatus(Util.ACTIVE_STATUS, busca);
    }

    @Override
    public ICompanyNameDto getCompanyNameByToken(String tokenTrama) throws DataNotFoundException {
        ICompanyNameDto company= companyRepo.getCompanyNameByToken(tokenTrama);
        
        if(company==null)
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);
        
        return company;
    }

    @Override
    public Integer existsByIdAll(String id) {
        return companyRepo.existsByIdAll(id,Util.ACTIVE_STATUS);
    }

    @Override
    public boolean existsCompanyByTokenTramaAndStatus(String token) {
        return companyRepo.existsCompanyByIngestTokenAndStatus(token, Util.ACTIVE_STATUS);
    }

}
