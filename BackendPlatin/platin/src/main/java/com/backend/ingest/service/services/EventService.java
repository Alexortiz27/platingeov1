package com.backend.ingest.service.services;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.repositories.IEventRepository;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.interfaces.IVehicleService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EventService implements IEventService {

    @Autowired
    private IEventRepository EventRepository;
    @Autowired
    private IVehicleService vehicleService;

    @Override
    public Event create(Event Event) {

        return EventRepository.save(Event);
    }

    @Override
    public EventInsDto save(EventInsDto Event) {

        EventRepository.saveEvent(UUID.randomUUID().toString().toUpperCase(), Event.getType(), Event.getGeozone_id(), Event.getLatitude(),
                Event.getLongitude(), Util.convertDateUtcPeru(Event.getReceiveDate()), Event.getGpsDate(), Event.getSpedd(),
                Event.getVehicle_id(), Event.getIdTravel(), Event.getNameTravel(), Event.getOdometer(),
                Util.ACTIVE_STATUS, new Date());
        return Event;
    }

    @Override
    public Event findById(String id) throws DataNotFoundException {
        Optional<Event> EventOptional = EventRepository.findByIdAndStatus(id, Util.ACTIVE_STATUS);

        Event Event = EventOptional.orElse(null);
        if (Event == null || !Event.isStatus()) {
            throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
        }
        return EventOptional.orElse(null);
    }

    @Override
    public List<EventShowDto> findByStatus() throws DataNotFoundException {
        List<Event> events = EventRepository.findByStatus(Util.ACTIVE_STATUS);
        if (events.isEmpty()) {
            throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
        }
        List<EventShowDto> eventsDto = new ArrayList<>();
        for (Event event : events) {
            eventsDto.add(this.parseEventToShowDto(event));
        }
        return eventsDto;
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        Event Event = findById(id);

        if (Event == null || !Event.isStatus()) {
            throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
        }

        Event.setStatus(Util.INACTIVE_STATUS);

        EventRepository.save(Event);
    }

    @Override
    public List<Map<String, Object>> findAllPage(Pageable pageable, String busca) throws DataNotFoundException {

        List<Map<String, Object>> events = EventRepository.findAllWithFieldsContaining(pageable, Util.ACTIVE_STATUS_BIT, busca);

        if (events.isEmpty()) {
            throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
        }


        return events;
    }

//    @Override
//    public List<Event> findByContainingTypeDate(String plate, String type, Date startDate, Date endDate) {
//        return EventRepository.findByContainingTypeDate(plate, type, startDate, endDate, Util.ACTIVE_STATUS_BIT, Util.ACTIVE_STATUS_BIT);
//
//    }

    @Override
    public List<Event> findByContainingTypeDateAll(String type, String typeEnd, String plate, Date startDate, Date endDate) {
        return EventRepository.findByContainingTypeDateAll(type, typeEnd, plate, startDate, endDate, Util.ACTIVE_STATUS_BIT, Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public EventShowDto update(String id, EventInsDto eventDto) throws DataNotFoundException {
        Event event = findById(id);

        if (event == null || !event.isStatus()) {
            throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
        }
        event.setGeozone_id(eventDto.getGeozone_id());
        event.setType(eventDto.getType());
        event.setLatitude(eventDto.getLatitude());
        event.setLongitude(eventDto.getLongitude());
        event.setReceiveDate(eventDto.getReceiveDate());
        event.setGpsDate(eventDto.getGpsDate());
        event.setSpedd(eventDto.getSpedd());
        event.setIdTravel(eventDto.getIdTravel());
        event.setNameTravel(eventDto.getNameTravel());
        Vehicle vehicledat = vehicleService.findById(eventDto.getVehicle_id());
        event.setVehicle_id(vehicledat);

        event = EventRepository.save(event);

        return this.parseEventToShowDto(event);
    }
//
//    @Override
//    public List<Event> findByTypeAndPlateAndCreationDateBetween(String[] types, String plate, Date startDate, Date endDate) {
//        return EventRepository.findByTypeAndPlateAndCreationDateBetween(types, plate, startDate, endDate, Util.ACTIVE_STATUS, Util.ACTIVE_STATUS);
//    }

    private EventShowDto parseEventToShowDto(Event event) {

        VehicleBasicDto vehicle = new VehicleBasicDto(event.getVehicle_id().getId(), event.getVehicle_id().getPlate(),
                event.getVehicle_id().getCategory(),
                event.getVehicle_id().getcode_osinergmin(),
                event.getVehicle_id().getStatus(),
                event.getVehicle_id().getCreationDate());

        return new EventShowDto(event.getId(), event.getType(), vehicle, event.getGeozone_id(),
                event.getLatitude(), event.getLongitude(), event.getReceiveDate(), event.getGpsDate(), event.getSpedd(),
                event.getCreationDate(), event.getIdTravel(), event.getNameTravel());
    }

    @Override
    public EventShowDto getById(String id) throws DataNotFoundException {
        Optional<Event> EventOptional = EventRepository.findById(id);

        Event Event = EventOptional.orElse(null);
        if (Event == null || !Event.isStatus()) {
            throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
        }
        Event event = EventOptional.orElse(null);

        return this.parseEventToShowDto(event);
    }

//    @Override
//    public List<Event> findByPlateAndCreationDateBetween(String plate, Date startDate, Date endDate) {
//        return EventRepository.findByPlateAndCreationDateBetween(plate, startDate, endDate);
//    }

    @Override
    public long countByStatus(String busca) {
        return EventRepository.countByStatus(Util.ACTIVE_STATUS_BIT, busca);
    }

    @Override
    public List<EventTravelDto> getTravelEvents(String type, String typeEnd, String plate, Date startDate, Date endDate) {
        return EventRepository.getTravelEvents(type, typeEnd, plate, startDate, endDate, Util.ACTIVE_STATUS_BIT, Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public List<EventStoppedDto> getStoppedEvents(String type, String typeEnd, String plate, Date startDate, Date endDate) {
        String[] types = {type, typeEnd};
        return EventRepository.getStoppedEvent(types, plate, startDate, endDate, Util.ACTIVE_STATUS, Util.ACTIVE_STATUS);
    }

    @Override
    public List<EventGeozoneDto> getGeozoneEvents(String[] types, String plate, Date startDate, Date endDate) {
        return EventRepository.getGeozoneEvent(types, plate, startDate, endDate, Util.ACTIVE_STATUS, Util.ACTIVE_STATUS);
    }

    @Override
    public List<EventSpeedDto> getSpeedEvent(String plate, String type, Date startDate, Date endDate) {
        return EventRepository.getSpeedEvent(plate, type, startDate, endDate, Util.ACTIVE_STATUS_BIT, Util.ACTIVE_STATUS_BIT);

    }

    @Override
    public List<EventNoTransmissionDto> getNoTransmissionEvents(String type, String typeEnd, String plate, Date startDate, Date endDate) {
        return EventRepository.getNoTransmissionEvents(type, typeEnd, plate, startDate, endDate, Util.ACTIVE_STATUS_BIT, Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public List<EventTimeLagDto> getTimeLagEvents(String plate, String type, Date startDate, Date endDate) {
        return EventRepository.getTimeLagEvents(plate, type, startDate, endDate, Util.ACTIVE_STATUS_BIT, Util.ACTIVE_STATUS_BIT);

    }

//    @Override
//    public List<IEventStoppedDto> findGps_DateAndTypeOfStoppedEvent(String[] types, String plate, Date startDate, Date endDate) {
//        return EventRepository.findGps_DateAndTypeOfStoppedEvent(types, plate, startDate, endDate, Util.ACTIVE_STATUS, Util.ACTIVE_STATUS);
//    }

}
