/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.services;

import com.backend.ingest.dtos.CoordInDto;
import com.backend.ingest.dtos.GeozoneCircleUpdDto;
import com.backend.ingest.dtos.IGeozoneNameDto;
import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.User;
import com.backend.ingest.repositories.IGeozoneRepository;
import com.backend.ingest.service.interfaces.IGeozoneService;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author FAMLETO
 */
@Service
public class GeozoneService implements IGeozoneService {

    @Autowired
    private IGeozoneRepository geozoneRepository;

    @Autowired
    IUserService userService;

    @Override
    public Geozone create(Geozone geozone) {

        return geozoneRepository.save(geozone);
    }

    @Override
    public void excelImport(MultipartFile files) throws IOException, DataNotFoundException {
        XSSFWorkbook workbook = new XSSFWorkbook(files.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 0; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                XSSFRow row = worksheet.getRow(index);
                String category = row.getCell(0).getStringCellValue();
                double name = row.getCell(1).getNumericCellValue();
                int namee = (int) name;
                String namegeo = String.valueOf(namee);
                String type = row.getCell(2).getStringCellValue();
                double latitude =  Double.parseDouble(row.getCell(3).getRawValue());
                double longitude =  Double.parseDouble(row.getCell(4).getRawValue());
                double radius = row.getCell(5).getNumericCellValue();
                String userid = row.getCell(6).getStringCellValue();
                List<CoordInDto> coordInDtos = new ArrayList<>();
                coordInDtos.add(new CoordInDto(latitude, longitude));
                User usuario = userService.findById(userid);
                Geozone geo = new Geozone(category, radius, type, coordInDtos, namegeo);
                geo.setUser_id(usuario);
                geozoneRepository.save(geo);
            }
        }
    }

    @Override
    public Geozone update(String id, GeozoneCircleUpdDto geozoneDto) throws DataNotFoundException {
        Geozone geozone = findById(id);

        if (geozone == null || !geozone.getStatus()) {
            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
        }

        geozone.setCategory(geozoneDto.getCategory());
        geozone.setType(geozoneDto.getType());

        String category = geozone.getCategory().toUpperCase();

        if (category.equals(Util.POLYGON)) {
            geozone.setRadius(0);
        } else {
            geozone.setRadius(geozoneDto.getRadius());

        }

        geozone.setCoords(geozoneDto.getCoords());
        geozone.setName(geozoneDto.getName());
        User u = userService.findById(geozoneDto.getUser_id());
        geozone.setUser_id(u);
        return geozoneRepository.save(geozone);
    }

    @Override
    public Geozone findById(String id) throws DataNotFoundException {
        Optional<Geozone> geozoneOptional = geozoneRepository.findByIdAndStatus(id, Util.ACTIVE_STATUS);

        Geozone geozone = geozoneOptional.orElse(null);
        if (geozone == null || !geozone.getStatus()) {
            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
        }
        return geozoneOptional.orElse(null);
    }

    @Override
    public List<Geozone> findAll() throws DataNotFoundException {
        List<Geozone> listgeo = geozoneRepository.findAll();
        if (listgeo.isEmpty()) {
            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
        }
        return listgeo;
    }

    @Override
    public List<Geozone> findByStatus() throws DataNotFoundException {
        List<Geozone> listgeo = geozoneRepository.findByStatus(Util.ACTIVE_STATUS);
        if (listgeo.isEmpty()) {
            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);

        }
        return listgeo;
    }

    @Override
    public List<Map<String, Object>> findByStatusAll() throws DataNotFoundException {
        List<Map<String, Object>> listgeo = geozoneRepository.findByStatusAll(Util.ACTIVE_STATUS_BIT,"Restringida");
        if (listgeo.isEmpty()) {
            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);

        }
        return listgeo;
    }
    @Override
    public Geozone delete(String id) throws DataNotFoundException {
        Geozone geozone = findById(id);

        if (geozone == null || !geozone.getStatus()) {
            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
        }

        geozone.setStatus(Util.INACTIVE_STATUS);

        return geozoneRepository.save(geozone);
    }

    @Override
    public List<Map<String, Object>> findAllPage(Pageable pageable, String busca) throws DataNotFoundException {
        List<Map<String, Object>> listgeo = geozoneRepository.findAllWithFieldsContaining(pageable, busca, Util.ACTIVE_STATUS_BIT);
        if (listgeo.isEmpty()) {
            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
        }
        return listgeo;
    }

    @Override
    public Geozone findNameDiferent(String id, String name) throws DataNotFoundException {
        Optional<Geozone> listgeo = geozoneRepository.findNameDiferent(id, Util.ACTIVE_STATUS_BIT, name);
        Geozone geo = null;
        if (listgeo.isPresent()) {
            geo = listgeo.get();
        }
        return geo;
    }
    
    @Override
    public IGeozoneNameDto getNameAndTypeGeozoneById(String id) throws DataNotFoundException {
         IGeozoneNameDto geozone = geozoneRepository.getNameAndTypeGeozoneById(id, Util.ACTIVE_STATUS);

         if (geozone == null) {
             throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
         }
         return geozone;
    }

    @Override
    public long countByStatus(String busca) {
        return geozoneRepository.countByStatus(busca,Util.ACTIVE_STATUS);
    }

}
