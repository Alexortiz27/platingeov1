package com.backend.ingest.service.services;

import com.backend.ingest.dtos.CompanyClientShowDto;
import com.backend.ingest.dtos.CompanyInDto;
import com.backend.ingest.dtos.MapInsDto;
import com.backend.ingest.dtos.MapShowDto;
import com.backend.ingest.dtos.MapUpdDto;
import com.backend.ingest.entities.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.backend.ingest.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.backend.ingest.repositories.IMapRepository;
import com.backend.ingest.service.interfaces.ICompanyService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.ingest.service.interfaces.IMapService;

import com.backend.platin.util.Util;
import java.util.ArrayList;

@Service
public class MapService implements IMapService {

    @Autowired
    private IMapRepository mapRepo;
    @Autowired
    private IUserService userService;
    @Override
    public Maps create(MapInsDto mapDto) throws DataNotFoundException {

        Maps map = new Maps(mapDto.getName(), mapDto.getUrlObject());
        User u = userService.findById(mapDto.getUser_id());
        map.setUser_id(u);
        map = mapRepo.save(map);

        return map;
    }

    @Override
    public List<Maps> findNameDiferent(String id) {
        return mapRepo.findNameDiferent(id,Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public Maps update(String id, MapUpdDto mapupDto) throws DataNotFoundException {
        Maps map = findById(id);

        if (map == null || !map.getStatus()) {
            throw new DataNotFoundException(Util.MAP_NOT_FOUND);
        }
        map.setName(mapupDto.getName());
        map.setUrlObject(mapupDto.getUrlObject());
        User u = userService.findById(mapupDto.getUser_id());
        map.setUser_id(u);
        map = mapRepo.save(map);

        return map;
    }

    @Override
    public Maps findById(String id) throws DataNotFoundException {
        Optional<Maps> maps = mapRepo.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        Maps maplist = maps.orElse(null);

        if (maplist == null || !maplist.getStatus()) {
            throw new DataNotFoundException(Util.MAP_NOT_FOUND);
        }
        return maps.orElse(null);
    }

    @Override
    public List<Maps> findAll() throws DataNotFoundException {
        List<Maps> listMap = mapRepo.findAll();
        if (listMap.isEmpty()) {
            throw new DataNotFoundException(Util.MAP_NOT_FOUND);
        }
        return listMap;
    }

    @Override
    public List<Maps> findByStatus() throws DataNotFoundException {
        List<Maps> listMap = mapRepo.findByStatus(Util.ACTIVE_STATUS);
        if (listMap.isEmpty()) {
            throw new DataNotFoundException(Util.MAP_NOT_FOUND);
        }
        return listMap;

    }

    @Override
    public List<Map<String, Object>> findAllPage(Pageable pageable, String buscar, String user_id) throws DataNotFoundException {
        List<Map<String, Object>> listMap = mapRepo.findAllWithFieldsContaining(pageable, Util.ACTIVE_STATUS_BIT,buscar,user_id);
        if (listMap.isEmpty()) {
            throw new DataNotFoundException(Util.MAP_NOT_FOUND);
        }
        return listMap;
    }

    @Override
    public Maps delete(String id) throws DataNotFoundException {
        Maps map = findById(id);

        if (map == null || !map.getStatus()) {
            throw new DataNotFoundException(Util.MAP_NOT_FOUND);
        }

        map.setStatus(Util.INACTIVE_STATUS);

        return mapRepo.save(map);
    }

    @Override
    public Maps getById(String id) throws DataNotFoundException {
        Optional<Maps> maps = mapRepo.findById(id);

        Maps maplist = maps.orElse(null);

        if (maplist == null || !maplist.getStatus()) {
            throw new DataNotFoundException(Util.MAP_NOT_FOUND);
        }
        return maplist;
    }
    @Override
    public long countByStatus(String buscar,String user_id) {
        return mapRepo.countByStatus(Util.ACTIVE_STATUS,buscar,user_id);
    }
}
