/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.services;

import com.backend.ingest.dtos.MarkerDto;
import com.backend.ingest.dtos.MarkerUpdDto;
import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Maps;
import com.backend.ingest.entities.Marker;
import com.backend.ingest.entities.User;
import com.backend.ingest.repositories.IGeozoneRepository;
import com.backend.ingest.repositories.IMarkerRepository;
import com.backend.ingest.service.interfaces.IMarkerService;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author FAMLETO
 */
@Service
public class MarkerService implements IMarkerService {
    @Autowired
    private IUserService userService;
    @Autowired
    private IMarkerRepository markerRepository;

    @Autowired
    private IGeozoneRepository geozoneRepository;

    @Override
    public Marker create(MarkerDto markerDto) throws DataNotFoundException {

        Optional<Geozone> geozoneOptional = geozoneRepository.findById(markerDto.getGeozone_id());

        Geozone geozone = geozoneOptional.orElse(null);
        User u = userService.findById(markerDto.getUser_id());
        if (geozone == null) {
            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
        }

        return markerRepository.save(new Marker(geozone, u, markerDto.getName(), markerDto.getType(), markerDto.getLatitude(),
                markerDto.getLongitude(), markerDto.getRadius(), markerDto.getZindex()));
    }

    @Override
    public List<Marker> findNameDiferent(String id) {
        return markerRepository.findNameDiferent(id, Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public Marker update(String id, MarkerUpdDto markerDto) throws DataNotFoundException {
        Marker marker = findById(id);

        if (marker == null || !marker.isStatus()) {
            throw new DataNotFoundException(Util.MARKER_NOT_FOUND);
        }

        Optional<Geozone> geozoneOptional = geozoneRepository.findById(markerDto.getGeozone_id());

        Geozone geozone = geozoneOptional.orElse(null);
        User u = userService.findById(markerDto.getUser_id());
        if (geozone == null) {
            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
        }

        marker.setGeozone(geozone);
        marker.setName(markerDto.getName());
        marker.setType(markerDto.getType());
        marker.setLatitude(markerDto.getLatitude());
        marker.setLongitude(markerDto.getLongitude());
        marker.setRadius(markerDto.getRadius());
        marker.setCreationDate(new Date());
        marker.setStatus(Util.ACTIVE_STATUS);
        marker.setUser_id(u);

        return markerRepository.save(marker);
    }
//

    @Override
    public Marker findById(String id) throws DataNotFoundException {
        Optional<Marker> markerOptional = markerRepository.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        Marker marker = markerOptional.orElse(null);

        if (marker == null || !marker.isStatus()) {
            throw new DataNotFoundException(Util.MARKER_NOT_FOUND);
        }

        return markerOptional.get();
    }

    @Override
    public List<Marker> findAll() throws DataNotFoundException {
        List<Marker> listMap = markerRepository.findAll();
        if (listMap.isEmpty()) {
            throw new DataNotFoundException(Util.MARKER_NOT_FOUND);
        }
        return listMap;
    }
//

    @Override
    public List<Marker> findByStatus() throws DataNotFoundException {
        List<Marker> listMap = markerRepository.findByStatus(Util.ACTIVE_STATUS);
        if (listMap.isEmpty()) {
            throw new DataNotFoundException(Util.MARKER_NOT_FOUND);
        }
        return listMap;
    }
    @Override
    public long countByStatus(String busca, String user_id) {
        return markerRepository.countByStatus(busca, Util.ACTIVE_STATUS, user_id);
    }
    @Override
    public List<Map<String, Object>> findAllPage(Pageable pageable, String busca, String user_id) throws DataNotFoundException {
        List<Map<String, Object>> listMap = markerRepository.findByStatusAll(pageable, busca, Util.ACTIVE_STATUS,user_id);
        if (listMap.isEmpty()) {
            throw new DataNotFoundException(Util.MARKER_NOT_FOUND);
        }
        return listMap;
    }
//

    @Override
    public void delete(String id) throws DataNotFoundException {
        Marker marker = findById(id);

        if (marker == null || !marker.isStatus()) {
            throw new DataNotFoundException(Util.MARKER_NOT_FOUND);
        }

        marker.setStatus(Util.INACTIVE_STATUS);

        markerRepository.save(marker);
    }
}
