package com.backend.ingest.service.services;

import com.backend.ingest.dtos.PermitsUpdDto;
import com.backend.ingest.dtos.RolUpdDto;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Permits;
import com.backend.ingest.entities.Rol;
import com.backend.ingest.repositories.IPermitsRepository;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.ingest.repositories.IRolRepository;
import com.backend.ingest.service.interfaces.IPermitsService;
import com.backend.ingest.service.interfaces.IRolService;

import com.backend.platin.util.Util;

@Service
public class PermitsService implements IPermitsService {

    @Autowired
    private IPermitsRepository permitsRepo;

    @Override
    public Permits create(Permits cliente) {
        return permitsRepo.save(cliente);
    }

    @Override

    public Permits update(String id, PermitsUpdDto permitupDto) throws DataNotFoundException {
        Permits permits = findById(id);

        if (permits == null || !permits.getStatus()) {
            throw new DataNotFoundException(Util.PERMIT_NOT_FOUND);
        }

        permits.setName(permitupDto.getNamePermists());
        permits.setCreate_permits(permitupDto.getCreate_permits());
        permits.setEdit_permits(permitupDto.getEdit_permits());
        permits.setDelete_permits(permitupDto.getDelete_permits());
        return permitsRepo.save(permits);
    }

    @Override
    public Permits findById(String id) throws DataNotFoundException {
        Optional<Permits> permits = permitsRepo.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        Permits permitslist = permits.orElse(null);

        if (permitslist == null || !permitslist.getStatus()) {
            throw new DataNotFoundException(Util.PERMIT_NOT_FOUND);
        }
        return permits.orElse(null);
    }

    @Override
    public List<Permits> findAll() throws DataNotFoundException {
        List<Permits> listPermits = permitsRepo.findAll();
        if (listPermits.isEmpty()) {
            throw new DataNotFoundException(Util.PERMIT_NOT_FOUND);
        }
        return listPermits;
    }

    @Override
    public List<Permits> findByStatus() throws DataNotFoundException {
        List<Permits> listPermits = permitsRepo.findByStatus(Util.ACTIVE_STATUS);
        if (listPermits.isEmpty()) {
            throw new DataNotFoundException(Util.PERMIT_NOT_FOUND);
        }
        return listPermits;
    }

    public Optional<Permits> getByRolName(String PermitsNombre) {
        return permitsRepo.findByNameAndStatus(PermitsNombre,Util.ACTIVE_STATUS);
    }

    @Override
    public Permits findNameDiferent(String id, Integer status, String name) {
        return permitsRepo.findNameDiferent(id,status,name);
    }

    @Override
    public Page<Permits> findAllPage(Pageable pageable,String busca) throws DataNotFoundException {
        Page<Permits> listPermits = permitsRepo.findAllWithFieldsContaining(pageable, Util.ACTIVE_STATUS_BIT,busca);
        if (listPermits.isEmpty()) {
            throw new DataNotFoundException(Util.PERMIT_NOT_FOUND);
        }
        return listPermits;
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        Permits permits = findById(id);

        if (permits == null || !permits.getStatus()) {
            throw new DataNotFoundException(Util.PERMIT_NOT_FOUND);
        }

        permits.setStatus(Util.INACTIVE_STATUS);

        permitsRepo.save(permits);
    }

}
