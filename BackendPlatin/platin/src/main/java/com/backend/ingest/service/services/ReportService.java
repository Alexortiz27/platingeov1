/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.services;

import com.backend.ingest.dtos.ReportInDto;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.Report;
import com.backend.ingest.repositories.ICompanyRepository;
import com.backend.ingest.repositories.IReportRepository;
import com.backend.ingest.service.interfaces.IReportService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author FAMLETO
 */
@Service
public class ReportService implements IReportService {

    @Autowired
    private IReportRepository reportRepository;
    
    @Autowired ICompanyRepository companyRepository;
    
    
    @Override
    public Report create(ReportInDto reportDto)  throws DataNotFoundException
    {
        Company company = companyRepository.findById(reportDto.getCompany_id()).orElse(null);
        
        if(company==null)
            throw new DataNotFoundException(Util.COMPANY_NOT_FOUND);
        
        Report report = new Report(reportDto.getUrlGeoJson(), reportDto.getUrlXls(), reportDto.getType(),
                reportDto.getEstadoReport(),reportDto.getStartDate(),reportDto.getEndDate(),reportDto.isAccion(),
                company);
        return this.reportRepository.save(report);
    }

    @Override
    public Report update(String id, ReportInDto reportDto) throws DataNotFoundException {
        Report report = findById(id);

        if (report == null || !report.isStatus()) {
            throw new DataNotFoundException(Util.REPORT_NOT_FOUND);
        }

        report.setType(reportDto.getType());
        report.setUrlGeoJson(reportDto.getUrlGeoJson());
        report.setUrlXls(reportDto.getUrlXls());
        report.setEstadoReport(reportDto.getEstadoReport());
        report.setStartDate(reportDto.getStartDate());
        report.setEndDate(reportDto.getStartDate());
        report.setAccion(reportDto.isAccion());
        
        Company company = companyRepository.findById(reportDto.getCompany_id()).get();
        report.setCompany(company);

        report = reportRepository.save(report);
        return report;
    }

    @Override
    public Report findById(String id) throws DataNotFoundException {
        Optional<Report> reportOptional = reportRepository.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        Report report = reportOptional.orElse(null);

        if (report == null || !report.isStatus()) {
            throw new DataNotFoundException(Util.REPORT_NOT_FOUND);
        }

        return reportOptional.orElse(null);
    }

    @Override
    public List<Report> findAll() {
        return reportRepository.findAll();
    }

    @Override
    public List<Report> findByStatus() throws DataNotFoundException{
        
        List<Report> reports = reportRepository.findByStatus(Util.ACTIVE_STATUS);
        
        if(reports.isEmpty())
            throw new DataNotFoundException(Util.REPORT_NOT_FOUND);
        
        return reports;
    }

    @Override
    public List<Map<String, Object>>  findAllPage(Pageable pageable, String busca) throws DataNotFoundException{

        List<Map<String, Object>>  reports= reportRepository.findAllWithFieldsContaining(pageable, Util.ACTIVE_STATUS_BIT,busca);
        
        if(reports.isEmpty())
            throw new DataNotFoundException(Util.REPORT_NOT_FOUND);

         return reports;
    }

    @Override
    public long countByStatus(String busca) {
        return reportRepository.countByStatus(Util.ACTIVE_STATUS, busca);
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        Report report = findById(id);

        if (report == null || !report.isStatus()) {
            throw new DataNotFoundException(Util.REPORT_NOT_FOUND);
        }

        report.setStatus(Util.INACTIVE_STATUS);

        reportRepository.save(report);
    }
    
}
