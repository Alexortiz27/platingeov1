package com.backend.ingest.service.services;

import com.backend.ingest.dtos.RolUpdDto;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.backend.ingest.entities.Permits;
import com.backend.ingest.entities.Rol;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.ingest.repositories.IRolRepository;
import com.backend.ingest.service.interfaces.IRolService;

import com.backend.platin.util.Util;
import java.util.HashSet;
import java.util.Set;

@Service
public class RolService implements IRolService {

    @Autowired
    private IRolRepository rolRepo;
    @Autowired
    private PermitsService permitsService;

    @Override
    public Rol create(Rol cliente) {
        return rolRepo.save(cliente);
    }

    @Override
    public List<Rol> findNameDiferent(String id) {
        return rolRepo.findNameDiferent(id,Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public Rol update(String id, RolUpdDto rolupDto) throws DataNotFoundException {
        Rol rol = findById(id);

        if (rol == null || !rol.getStatus()) {
            throw new DataNotFoundException(Util.ROL_NOT_FOUND);
        }
        Set<Permits> permits = new HashSet<Permits>();
        for (String s : rolupDto.getPermits()) {
            permits.add(permitsService.getByRolName(s).get());
        }
        rol.setPermits(permits);
        rol.setName(rolupDto.getName());

        return rolRepo.save(rol);
    }

    @Override
    public Rol findById(String id) throws DataNotFoundException {
        Optional<Rol> clienteOptional = rolRepo.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        Rol rol = clienteOptional.orElse(null);

        if (rol == null || !rol.getStatus()) {
            throw new DataNotFoundException(Util.ROL_NOT_FOUND);
        }
        return clienteOptional.orElse(null);
    }

    @Override
    public List<Rol> findAll() throws DataNotFoundException {
        List<Rol> listrol = rolRepo.findAll();

        if (listrol.isEmpty()) {
            throw new DataNotFoundException(Util.ROL_NOT_FOUND);
        }
        return listrol;
    }

    @Override
    public List<Rol> findByStatus() throws DataNotFoundException {
        List<Rol> listrol = rolRepo.findByStatus(Util.ACTIVE_STATUS);

        if (listrol.isEmpty()) {
            throw new DataNotFoundException(Util.ROL_NOT_FOUND);
        }
        return listrol;
    }

    public Rol getByRolName(String rolNombre) throws DataNotFoundException {
        Rol roles =rolRepo.findByNameAndStatus(rolNombre,Util.ACTIVE_STATUS);
        if(roles==null){
            throw new DataNotFoundException(Util.ROL_NOT_FOUND);
        }
        return roles;
    }

    @Override
    public Page<Rol> findAllPage(Pageable pageable,String busca) throws DataNotFoundException {
        Page<Rol> listrol = rolRepo.findAllWithFieldsContaining(pageable, Util.ACTIVE_STATUS_BIT,busca);

        if (listrol.isEmpty()) {
            throw new DataNotFoundException(Util.ROL_NOT_FOUND);
        }
        return listrol;
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        Rol rol = findById(id);

        if (rol == null || !rol.getStatus()) {
            throw new DataNotFoundException(Util.ROL_NOT_FOUND);
        }

        rol.setStatus(Util.INACTIVE_STATUS);

        rolRepo.save(rol);
    }

}
