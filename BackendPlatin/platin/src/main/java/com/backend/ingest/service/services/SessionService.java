/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.services;

import com.backend.ingest.dtos.SessionDto;
import com.backend.ingest.entities.Session;
import com.backend.ingest.entities.User;
import com.backend.ingest.repositories.ISessionRepository;
import com.backend.ingest.service.interfaces.ISessionService;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.backend.platin.util.Util;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author FAMLETO
 */
@Service
public class SessionService implements ISessionService {

    @Autowired
    private ISessionRepository sessionRepository;
    @Autowired
    private IUserService userService;

    @Override
    public Session create(SessionDto se) throws EmptyFieldsException {
        Session sesion = new Session();
        try {
            if (se == null || se.getUser_id().equals("")) {
                throw new EmptyFieldsException("The field is empty");
            }
            Util.USER_ID = se.getUser_id();
            User userd = userService.findById(se.getUser_id());
            sesion.setUser_id(userd);
            sesion.setUserCompleteNames(userd.getDisplayName());
            sesion.setCreationDate(new Date());

        } catch (DataNotFoundException ex) {
            Logger.getLogger(SessionService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sessionRepository.save(sesion);
    }

    @Override
    public Session update(String id, SessionDto sesi) throws DataNotFoundException, EmptyFieldsException {
        Session session = findById(id);

        if (sesi == null || sesi.getUser_id().equals("")) {
            throw new EmptyFieldsException("The field is empty");
        }
        if (session == null) {
            throw new DataNotFoundException(Util.SESSION_NOT_FOUND);
        }

        User userd = userService.findById(sesi.getUser_id());
        session.setUser_id(userd);
        session.setUserCompleteNames(userd.getDisplayName());
        session.setCreationDate(new Date());
        return sessionRepository.save(session);
    }
//

    @Override
    public Session findById(String id) throws DataNotFoundException {
        Optional<Session> sessionOptional = sessionRepository.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        Session session = sessionOptional.orElse(null);

        if (session == null) {
            throw new DataNotFoundException(Util.SESSION_NOT_FOUND);
        }

        return sessionOptional.orElse(null);
    }

    @Override
    public List<Session> findAll() throws DataNotFoundException {
        List<Session> listsession = sessionRepository.findAll();
        if (listsession.isEmpty()) {
            throw new DataNotFoundException(Util.PERMIT_NOT_FOUND);
        }
        return listsession;
    }
//

    @Override
    public Page<Session> findAllPage(Pageable pageable,String buscar) throws DataNotFoundException {
        Page<Session> listsession = sessionRepository.findAllWithFieldsContaining(pageable,Util.ACTIVE_STATUS_BIT,buscar);
        if (listsession.isEmpty()) {
            throw new DataNotFoundException(Util.PERMIT_NOT_FOUND);
        }
        return listsession;
    }

}
