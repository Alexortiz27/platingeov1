/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.ingest.service.services;

import com.backend.ingest.dtos.ITramaDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.entities.Trama;
import com.backend.ingest.repositories.ITramaRepository;
import com.backend.ingest.service.interfaces.ITramaService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.Util;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author FAMLETO
 */
@Service
public class TramaService implements ITramaService {

    @Autowired
    private ITramaRepository tramaRepo;

    @Override
    public Trama create(Trama trama) {
        return tramaRepo.save(trama);
    }

    @Override
    public Trama update(String id, TramaInsDto tramaDto) throws DataNotFoundException {
        Trama currentTrama = findById(id);

        if (currentTrama == null || !currentTrama.getStatus()) {
            throw new DataNotFoundException(Util.TRAMA_NOT_FOUND);
        }

        currentTrama.setEvent(tramaDto.getEvent());
        currentTrama.setPlate(tramaDto.getPlate());
        currentTrama.setPosition(new Gson().toJson(tramaDto.getPosition()));
        currentTrama.setGpsDate(new Timestamp(tramaDto.getGpsDate().getTime()));
        currentTrama.setSpeed(tramaDto.getSpeed());
        currentTrama.setReceiveDate(tramaDto.getReceiveDate());
        currentTrama.setTokenTrama(tramaDto.getTokenTrama());

        return tramaRepo.save(currentTrama);

    }

    @Override
    public Trama findById(String id) throws DataNotFoundException {
        Optional<Trama> userOptional = tramaRepo.findById(id);
        Trama trama = userOptional.orElse(null);
        if (trama == null || !trama.getStatus()) {
            throw new DataNotFoundException(Util.TRAMA_NOT_FOUND);
        }
        return userOptional.orElse(null);
    }

    @Override
    public List<Trama> findAll() {
        return tramaRepo.findAll();
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        Trama trama = findById(id);

        trama.setStatus(Util.INACTIVE_STATUS);

        tramaRepo.save(trama);
    }

    @Override
    public List<Trama> findByStatus() {
        return tramaRepo.findByStatus(Util.ACTIVE_STATUS);
    }

    @Override
    public List<Trama> getTramasByPlateAndGpsDate(String plate, Date startDate, Date endDate) {
       
        return tramaRepo.findByPlateAndGpsDateBetweenOrderByGpsDate(plate, startDate, endDate);
    }

    @Override
    public Trama findAllWithFieldsContaining(String position) {
        return tramaRepo.findAllWithFieldsContaining(Util.ACTIVE_STATUS_BIT, position);
    }

    @Override
    public List<ITramaDto> findByPlateGpsDateBetweenCompanyIdAsc(String plate, Date startDate, Date endDate) {
        return tramaRepo.findByPlateGpsDateBetweenCompanyIdAsc(plate, startDate, endDate,Util.ACTIVE_STATUS_BIT);
    }

}
