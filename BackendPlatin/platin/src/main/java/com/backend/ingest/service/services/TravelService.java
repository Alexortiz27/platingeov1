package com.backend.ingest.service.services;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Historial_Company;
import com.backend.ingest.entities.Travel;
import com.backend.ingest.entities.Vehicle;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.ingest.repositories.ITravelRepository;
import com.backend.ingest.service.interfaces.ITravelService;
import com.backend.platin.util.Util;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TravelService implements ITravelService {

    @Autowired
    private ITravelRepository travelRepository;

    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private GeozoneService geozoneService;

    @Override
    public TravelShowDto create(Travel travel) {
        travel = travelRepository.save(travel);
        return this.parseTravleToDto(travel);
    }

    @Override
    public Travel findNameDiferent(String id,String name) {
        return travelRepository.findNameDiferent(id,Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT,name);
    }

    @Override
    public TravelShowDto update(String id, TravelUpdDto travelDto) throws DataNotFoundException {
        Travel travel = findById(id);

        if (travel == null || !travel.isStatus()) {
            throw new DataNotFoundException(Util.TRAVEL_NOT_FOUND);
        }
        travel.setName(travelDto.getName());
        travel.setEndGeozone(travel.getEndGeozone());
        travel.setStartGeozone(travel.getStartGeozone());
        travel.setStartDate(travelDto.getStartDate());
        travel.setEndDate(travelDto.getEndDate());
        travel.setStatusTravel(travelDto.getStatusTravel());
        Vehicle travelId = vehicleService.findById(travelDto.getVehicle_id());
        Geozone geozonaid = geozoneService.findById(travelDto.getGeozona_id());
        travel.setVehicleTravel(travelId);
        travel.setGeozonaTravel(geozonaid);
        travel.setSchudeledTime(travelDto.getSchudeledTime());
        return this.parseTravleToDto(travelRepository.save(travel));

    }

    @Override
    public Travel findById(String id) throws DataNotFoundException {
        Optional<Travel> EventOptional = travelRepository.findByIdAndStatus(id,Util.ACTIVE_STATUS);

        Travel Travel = EventOptional.orElse(null);
        if (Travel == null || !Travel.isStatus()) {
            throw new DataNotFoundException(Util.TRAVEL_NOT_FOUND);
        }
        return EventOptional.orElse(null);
    }

    @Override
    public List<TravelShowDto> findAll() throws DataNotFoundException {

        List<Travel> travels = travelRepository.findAll();
        if (travels.isEmpty()) {
            throw new DataNotFoundException(Util.TRAVEL_NOT_FOUND);
        }
        List<TravelShowDto> travelsDto = new ArrayList<>();
        for (Travel travel : travels) {
            travelsDto.add(this.parseTravleToDto(travel));
        }

        return travelsDto;
    }

    @Override
    public List<TravelShowDto> findByStatus() throws DataNotFoundException {
        List<Travel> travels = travelRepository.findByStatus(Util.ACTIVE_STATUS);
        if (travels.isEmpty()) {
            throw new DataNotFoundException(Util.TRAVEL_NOT_FOUND);
        }
        List<TravelShowDto> travelsDto = new ArrayList<>();
        for (Travel travel : travels) {
            travelsDto.add(this.parseTravleToDto(travel));
        }

        return travelsDto;
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        Travel Travel = findById(id);

        if (Travel == null || !Travel.isStatus()) {
            throw new DataNotFoundException(Util.TRAVEL_NOT_FOUND);
        }

        Travel.setStatus(Util.INACTIVE_STATUS);

        travelRepository.save(Travel);
    }

    @Override
    public List<TravelInterfReportDto> findAllWithFieldsContaining(String plate, Date gpsDate) {

        return travelRepository.findAllWithFieldsContaining(plate, gpsDate);

    }

    @Override
    public List<Map<String, Object>> findAllPage(Pageable of,String busca) throws DataNotFoundException {
        List<Map<String, Object>> travels = travelRepository.findAllPage(of, Util.ACTIVE_STATUS_BIT,busca,Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT);
        if (travels.isEmpty()) {
            throw new DataNotFoundException(Util.TRAVEL_NOT_FOUND);
        }
        return travels;
    }

    @Override
    public List<Travel> findAllWithFieldsContainingPlateReport(String idvehicle, Date startDate, Date endDate) {

        return travelRepository.findAllWithFieldsContainingPlateReport(idvehicle, startDate, endDate);
    }

    private TravelShowDto parseTravleToDto(Travel travel) {
        List<CompanyBasicDto> companies = new ArrayList<>();
        for (Historial_Company historial : travel.getVehicleTravel().getCompanyClient().getHistorial()) {

            companies.add(Util.parseCompanyToBasicDto(historial.getCompany()));
        }

        CompanyClientListDto companyClient = new CompanyClientListDto(travel.getVehicleTravel().getCompanyClient().getId(),
                travel.getVehicleTravel().getCompanyClient().getName(), travel.getVehicleTravel().getCompanyClient().getEmail(),
                travel.getVehicleTravel().getCompanyClient().getRuc(),
                travel.getVehicleTravel().getCompanyClient().getAddress(), travel.getVehicleTravel().getCompanyClient().getPhone(), companies,
                travel.getVehicleTravel().getCompanyClient().getStatus(), travel.getVehicleTravel().getCompanyClient().getCreationDate());

        Vehicle vehicle = travel.getVehicleTravel();
        VehicleDto vehicleDto = new VehicleDto(vehicle.getId(), vehicle.getPlate(), vehicle.getCategory(), vehicle.getcode_osinergmin(), companyClient, vehicle.getStatus(), vehicle.getCreationDate());

        TravelShowDto travelDto = new TravelShowDto(travel.getId(), travel.getStartGeozone(), travel.getEndGeozone(), travel.getName(), travel.getStartDate(), travel.getEndDate(), travel.getStatusTravel(), travel.getSchudeledTime(), vehicleDto, travel.getGeozonaTravel());
        return travelDto;
    }

    @Override
    public TravelShowDto getId(String id) throws DataNotFoundException {
        Travel Travel = findById(id);
        return this.parseTravleToDto(Travel);
    }

    @Override
    public long countByStatus(String busca) {
        return travelRepository.countByStatus(Util.ACTIVE_STATUS,busca,Util.ACTIVE_STATUS,Util.ACTIVE_STATUS);
    }

}
