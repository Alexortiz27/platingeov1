package com.backend.ingest.service.services;

import com.backend.ingest.dtos.UserUpdDto;
import com.backend.ingest.entities.Company;
import com.backend.ingest.entities.Rol;
import com.backend.ingest.entities.User;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.ingest.repositories.IRolRepository;
import com.backend.ingest.repositories.IUserRepository;
import com.backend.ingest.service.interfaces.ICompanyService;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.platin.helper.Response;
import com.backend.platin.util.RolName;
import com.backend.platin.util.Util;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Service
@Transactional
public class UserService implements IUserService {

    @Autowired
    IUserRepository userRepository;
    @Autowired
    private ICompanyService companynService;
    @Lazy
    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    IRolRepository rolRepo;
    @Autowired
    RolService rolService;
    @Autowired
    AuthenticationManager authenticationManager;

    public Optional<User> getByEmail(String email) {
        return userRepository.findByEmailAndStatus(email,Util.ACTIVE_STATUS);
    }

    @Override
    public List<User> findNameDiferent(String id) {
        return userRepository.findUserOrEmailDiferent(id,Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public Optional<User> findByUserOrEmail(String nameOrEmail) {
        return userRepository.findByUserOrEmail(nameOrEmail, nameOrEmail);
    }

    @Override
    public Optional<User> getByTokenPassword(String tokenPassword) {
        return userRepository.findByTokenPasswordAndStatus(tokenPassword,Util.ACTIVE_STATUS);
    }

    @Override
    public Integer existsByUser(String nombreUsuario) {
        return userRepository.existsByUserAndStatusAll(nombreUsuario,Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public Integer existsByEmail(String email) {
        return userRepository.existsByEmailAll(email,Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public boolean existsByPassword(String password) {
        return userRepository.existsByPassword(password);
    }

    @Override
    public User create(User cliente) {
        return userRepository.save(cliente);
    }

    @Override
    public User update(String id, UserUpdDto userUpdDto) throws DataNotFoundException {
        User currentUser = findById(id);

        if (currentUser == null && !currentUser.getStatus()) {
            throw new DataNotFoundException(Util.USER_NOT_FOUND);
        }
        currentUser.setFirstName(userUpdDto.getFirstName());
        currentUser.setLastName(userUpdDto.getLastName());
        currentUser.setDisplayName(userUpdDto.getDisplayName());
        currentUser.setEmail(userUpdDto.getEmail());
        currentUser.setPassword(passwordEncoder.encode(userUpdDto.getPassword()));
        Set<Rol> roles = new HashSet<Rol>();
        for (String s : userUpdDto.getRoles()) {
            roles.add(rolService.getByRolName(s));
        }
        currentUser.setRoles(roles);

        return userRepository.save(currentUser);
//        try {
//            Authentication authentication = authenticationManager.authenticate(
//                    new UsernamePasswordAuthenticationToken(currentUser.getEmail(), userUpdDto.getPassword()));
//            SecurityContextHolder.getContext().setAuthentication(authentication);
//            User currentUserv1 = new User();
//            currentUserv1.setLastName("repeatedPassword");
//            return currentUserv1;
//        } catch (BadCredentialsException ex) {
//            currentUser.setUser(userUpdDto.getUser());
//            currentUser.setFirstName(userUpdDto.getFirstName());
//            currentUser.setLastName(userUpdDto.getLastName());
//            currentUser.setDisplayName(userUpdDto.getDisplayName());
//            currentUser.setEmail(userUpdDto.getEmail());
//            currentUser.setPassword(passwordEncoder.encode(userUpdDto.getPassword()));
//            Set<Rol> roles = new HashSet<Rol>();
//            for (String s : userUpdDto.getRoles()) {
//                roles.add(rolService.getByRolName(s).get());
//            }
//            currentUser.setRoles(roles);
//
//            return userRepository.save(currentUser);
//
//        }
//        return userRepository.save(currentUser);
//        return currentUser;
    }

    @Override
    public User findById(String id) throws DataNotFoundException {
        User user = userRepository.findByIdAndStatusAll(id,Util.ACTIVE_STATUS_BIT);

        if (user == null || !user.getStatus()) {
            throw new DataNotFoundException(Util.USER_NOT_FOUND);
        }

        return user;
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        User user = findById(id);

        if (user == null || !user.getStatus()) {
            throw new DataNotFoundException(Util.USER_NOT_FOUND);
        }

        user.setStatus(Util.INACTIVE_STATUS);

        userRepository.save(user);
    }

    @Override
    public List<User> findByStatus() throws DataNotFoundException {
        List<User> listusers = userRepository.findByStatus(Util.ACTIVE_STATUS);
        if (listusers.isEmpty()) {
            throw new DataNotFoundException(Util.USER_NOT_FOUND);
        }
        return listusers;
    }

    @Override
    public List<User> findAll() throws DataNotFoundException {
        List<User> listusers = userRepository.findAll();
        if (listusers.isEmpty()) {
            throw new DataNotFoundException(Util.USER_NOT_FOUND);
        }
        return listusers;

    }

    @Override
    public Page<User> findAllPage(Pageable pageable,String busca) throws DataNotFoundException {
        Page<User> listusers = userRepository.findAllWithFieldsContaining(pageable, Util.ACTIVE_STATUS_BIT,busca);
        if (listusers.isEmpty()) {
            throw new DataNotFoundException(Util.USER_NOT_FOUND);
        }
        return listusers;
    }

}
