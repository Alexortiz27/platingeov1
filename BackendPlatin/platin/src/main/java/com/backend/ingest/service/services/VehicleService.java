package com.backend.ingest.service.services;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.CompanyClient;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.repositories.ICompanyClientRepository;
import com.backend.ingest.repositories.ICompanyRepository;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.ingest.repositories.IVehicleRepository;
import com.backend.ingest.service.interfaces.IVehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.exceptions.GeneralException;
import com.backend.platin.util.Util;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class VehicleService implements IVehicleService {

    @Autowired
    private IVehicleRepository vehicleRepo;

    @Autowired
    private ICompanyClientRepository companyClientRepo;
    
    @Autowired
    private ICompanyRepository companyRepository;
    
    @Autowired
    private EventRedisHsetService redisService;

    @Autowired
    private IVehicleService vehicleService;
    
    @Override
    public VehicleShowDto create(Vehicle vehicle) {

        return this.parseVehicleToShowDto(vehicleRepo.save(vehicle));
    }

    @Override
    public Vehicle findNameDiferent(String id, String plate) {
        return vehicleRepo.findNameDiferent(id, Util.ACTIVE_STATUS_BIT, plate);
    }

    @Override
    public void excelExport(HttpServletResponse response) throws IOException {
        List<IVehicleCompanyExportarExcelDto> list = vehicleRepo.findByCompanyAndCompanyClientExportarExcel();
        List<VehicleDtoExcel> listvehicle = new ArrayList<>();
        for (IVehicleCompanyExportarExcelDto vehicleDtoExcel : list) {
            String stave = String.valueOf(vehicleDtoExcel.getStatusVehicle());
            String stacl = String.valueOf(vehicleDtoExcel.getStatusCliente());
            String staemv = String.valueOf(vehicleDtoExcel.getStatusEmv());
            listvehicle.add(new VehicleDtoExcel(vehicleDtoExcel.getPlate(), vehicleDtoExcel.getCategory(),
                    vehicleDtoExcel.getcode_osinergmin(), vehicleDtoExcel.getNameclient(),
                    vehicleDtoExcel.getNameCompany(),
                    vehicleDtoExcel.getToken(),
                    vehicleDtoExcel.getCreation_date(),stave,stacl,staemv));
        }
        String uuid = UUID.randomUUID().toString().toUpperCase();

        String fileName = "ListadoVehicle-" + uuid;
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1") + ".xls");
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        Sheet sheet = new Sheet(1, 0, VehicleDtoExcel.class);
        // Establecer el ancho adaptativo
        sheet.setAutoWidth(Boolean.TRUE);
        sheet.setSheetName("Lista de Vehicle");
        writer.write(listvehicle, sheet);
        writer.finish();
        out.flush();
        response.getOutputStream().close();
        out.close();
    }

    @Override
    public VehicleShowDto update(String id, VehicleUpdDto vehicleDto) throws DataNotFoundException {

        Vehicle vehicle = findById(id);

        if (vehicle == null || !vehicle.getStatus()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }

        vehicle.setPlate(vehicleDto.getPlate());
        vehicle.setCategory(vehicleDto.getCategory());
        vehicle.setcode_osinergmin(vehicleDto.getcode_osinergmin());
        vehicle.setStatus(Util.ACTIVE_STATUS);

        Optional<CompanyClient> companyOptional = companyClientRepo.findById(vehicleDto.getId_company());
        CompanyClient companyClient = companyOptional.orElse(null);

        vehicle.setCompanyClient(companyClient);
        return this.parseVehicleToShowDto(vehicleRepo.save(vehicle));

    }

    @Override
    public Vehicle findById(String id) throws DataNotFoundException {
        Optional<Vehicle> VehicleOptional = vehicleRepo.findByIdAndStatus(id, Util.ACTIVE_STATUS);

        Vehicle vehicle = VehicleOptional.orElse(null);
        if (vehicle == null || !vehicle.getStatus()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }

        return VehicleOptional.orElse(null);
    }

    @Override
    public Vehicle findByPlate(String plate) throws DataNotFoundException {
        Optional<Vehicle> VehicleOptional = vehicleRepo.findByPlateAndStatus(plate, Util.ACTIVE_STATUS);

        Vehicle vehicle = VehicleOptional.orElse(null);
        if (vehicle == null || !vehicle.getStatus()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }

        return VehicleOptional.orElse(null);
    }

    @Override
    public String findByPlateWorker(String plate) throws DataNotFoundException {
       String idvehicle = vehicleRepo.findByPlateWorker(plate,Util.ACTIVE_STATUS);
        if(idvehicle.isEmpty()){
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }
        return idvehicle;
    }

    @Override
    public List<Vehicle> findAll() throws DataNotFoundException {
        List<Vehicle> vehicles = vehicleRepo.findAll();
        if (vehicles.isEmpty()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }
        return vehicles;
    }

    @Override
    public List<Vehicle> findByStatus() throws DataNotFoundException {

        List<Vehicle> vehicles = vehicleRepo.findByStatus(Util.ACTIVE_STATUS);
        if (vehicles.isEmpty()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }

        return vehicles;
    }

    @Override
    public List<VehicleListDto> findByVehicleCompany() throws DataNotFoundException {

        List<VehicleListDto> vehicles = vehicleRepo.findByVehicleCompany(Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT);
        if (vehicles.isEmpty()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }

        return vehicles;
    }

    @Override
    public Page<Vehicle> findAllPage(Pageable pageable) throws DataNotFoundException {

        Page<Vehicle> vehicle = vehicleRepo.findAllWithFieldsContaining(pageable, Util.ACTIVE_STATUS);

        if (vehicle.isEmpty()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }

        return vehicle;
    }

    @Override
    public Map<String, Object> findPlateEmvCompany(String plate) {
        return vehicleRepo.findPlateEmvCompany(plate,Util.ACTIVE_STATUS,Util.ACTIVE_STATUS,Util.ACTIVE_STATUS);
    }

    @Override
    public List<Map<String, Object>> findAllPageLink(Pageable pageable, String busca) throws DataNotFoundException {
        List<Map<String, Object>> listvehicle = new ArrayList<>();
        listvehicle = vehicleRepo.findAllWithFieldsContaining(pageable, busca, Util.ACTIVE_STATUS,Util.ACTIVE_STATUS,Util.ACTIVE_STATUS);
        if (listvehicle.isEmpty()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }
        return listvehicle;
    }

    @Override
    public long countByStatus(String busca) {
        return vehicleRepo.countByStatus(busca , Util.ACTIVE_STATUS,Util.ACTIVE_STATUS,Util.ACTIVE_STATUS);
    }

    @Override
    public void delete(String id) throws DataNotFoundException {
        Vehicle vehicle = findById(id);

        if (vehicle == null || !vehicle.getStatus()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }

        vehicle.setStatus(Util.INACTIVE_STATUS);
        vehicleRepo.save(vehicle);

    }

    @Override
    public VehicleShowDto getId(String id) {
        Optional<Vehicle> VehicleOptional = vehicleRepo.findById(id);

        Vehicle vehicle = VehicleOptional.orElse(null);
        if (vehicle == null || !vehicle.getStatus()) {
            try {
                throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
            } catch (DataNotFoundException ex) {
                Logger.getLogger(VehicleService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return this.parseVehicleToShowDto(vehicle);

    }

    private VehicleShowDto parseVehicleToShowDto(Vehicle vehicle) {
        CompanyClient companyClient = vehicle.getCompanyClient();

        CompanyClientBasicDto companyDto = new CompanyClientBasicDto(companyClient.getId(), companyClient.getName(),
                companyClient.getEmail(), companyClient.getRuc(), companyClient.getAddress(), companyClient.getPhone(),
                companyClient.getStatus(), companyClient.getCreationDate());
        VehicleShowDto vehicleDto = new VehicleShowDto(vehicle.getId(), vehicle.getPlate(), vehicle.getCategory(), vehicle.getcode_osinergmin(), companyDto, vehicle.getStatus(),
                vehicle.getCreationDate());

        return vehicleDto;
    }

    @Override
    public Integer existsByPlate(String plate) {
        return vehicleRepo.existsByPlateAndStatusAll(plate, Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT,Util.ACTIVE_STATUS_BIT);
    }

    @Override
    public List<VehicleListDto> findByCompany(String company_client_id) {

        List<VehicleListDto> vehicles = vehicleRepo.findByCompanyClient(company_client_id);

        return vehicles;

    }

    @Override
    public List<VehicleCategoryDto> platePositionsTrama() {
        return vehicleRepo.platePositionsTrama(Util.ACTIVE_STATUS);
    }

    @Override
    public void showVehiclesTransmitting() throws DataNotFoundException {
        List<IVehicleCompanyDto> vehicles = vehicleRepo.findByCompanyAndCompanyClient(Util.ACTIVE_STATUS);

        if (vehicles.isEmpty()) {
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }

        String[][] vehiclesTransmitting = new String[vehicles.size()][4];
        long duration = 0;
        int rowCount = 0;
        for (IVehicleCompanyDto vehicle : vehicles) {
            int columnCount = 0;
            vehiclesTransmitting[rowCount][columnCount++] = vehicle.getPlate();
            vehiclesTransmitting[rowCount][columnCount++] = vehicle.getNameclient();
            vehiclesTransmitting[rowCount][columnCount++] = vehicle.getCompanyName();

////            Trama trama = redisService.findLastTramav2(vehicle.getPlate());
//            if(trama==null)
//            trama = tramaService.findTopByPlateOrderByGpsDateDesc(vehicle.getPlate());

//            long diffInMillies = Math.abs(new Date().getTime() - trama.getGpsDate().getTime());
//            duration = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
//

//            if (duration >= 10) {
//                vehiclesTransmitting[rowCount][columnCount++] = Util.NO_TRANSMITTING_STATUS;
//            } else {
//                vehiclesTransmitting[rowCount][columnCount++] = Util.TRANSMITTING_STATUS;
//            }
        }

    }

    @Override
    public IVehicleCompanyReportDto vehiclefindByPlate(String plate) throws DataNotFoundException {
        IVehicleCompanyReportDto listvehicle  = vehicleRepo.findByPlate(plate, Util.ACTIVE_STATUS);
        if(listvehicle==null){
            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
        }
        return listvehicle;
    }

    @Override
    public boolean existPlate(String plate) {
        return vehicleRepo.existsVehicleByPlateAndStatus(plate, Util.ACTIVE_STATUS);
    }
    
    @Override
    public List<LastPositionVehicleDto> showLastPositionVehicles(List<String> plates) throws Exception {

        List<LastPositionVehicleDto> lastPositionVehicles = new ArrayList<>();

        if (plates.isEmpty()) {
            throw new GeneralException("No se han encontrado placas");
        }

        for (String plate :plates) {

            TramaRedisDto trama = redisService.findLastTramav2(plate);
            if (trama != null) {
                // ONLY FOR TESTING
                /* Date date = Util.convertDateUtcGmtPeru(new Date());
                double now = date.getTime(); */
                double now = new Date().getTime();
                double gpsDate =Double.valueOf(trama.getRu_gps_date());
                double substract = now - gpsDate;
                int subs = (int) substract;
                if (subs < 300000) {

                PositionDto positionDto = trama.getPosition();

                LastPositionVehicleDto lastPositionVehice = new LastPositionVehicleDto(
                        trama.getPlate(), String.valueOf(positionDto.getAltitude()),
                        String.valueOf(positionDto.getLatitude()), String.valueOf(positionDto.getLongitude()), trama.getGpsDate(),
                        String.valueOf(trama.getSpeed()), String.valueOf(trama.getOdometer()),trama.getReceiveDate());
                lastPositionVehicles.add(lastPositionVehice);
                }
            }
        }

        return lastPositionVehicles;
    }

    @Override
    public List<LastPositionVehicleDto> lastPositionVehiclesOfFiveMinutesAgo(List<String> plates) throws Exception {
        
        List<LastPositionVehicleDto> lastPositionVehicles = new ArrayList<>();

        if (plates.isEmpty()) {
            throw new GeneralException("No se ha encontrado placas ");
        }
        
        for (String plate: plates) {
            TramaRedisDto trama = redisService.findLastTramav2(plate);

            if (trama != null) {

                double now = new Date().getTime();
                double gpsDate =Double.valueOf(trama.getRu_gps_date());
                double substract = now - gpsDate;
                int subs = (int) substract;
                if (subs >= 300000 && subs <= 360000) {

                    PositionDto positionDto = trama.getPosition();
                    LastPositionVehicleDto lastPositionVehice = new LastPositionVehicleDto(
                            trama.getPlate(),String.valueOf(positionDto.getAltitude()),
                            String.valueOf(positionDto.getLatitude()), String.valueOf(positionDto.getLongitude()), trama.getGpsDate(),
                            String.valueOf(trama.getSpeed()), String.valueOf(trama.getOdometer()),trama.getReceiveDate());
                    lastPositionVehicles.add(lastPositionVehice);
                }

            }
        }

        return lastPositionVehicles;
    }
    
    @Override
    public List<LastPositionVehicleDto> lastPositionVehiclesOfTenMinutesAgo(List<String> plates) throws Exception {
        
        List<LastPositionVehicleDto> lastPositionVehicles = new ArrayList<>();

        if (plates.isEmpty()) {
            throw new GeneralException("No se han encontrado placas ");
        }

        for (String plate: plates) {
            TramaRedisDto trama = redisService.findLastTramav2(plate);

            if (trama != null) {

                double now = new Date().getTime();
                double gpsDate =Double.valueOf(trama.getRu_gps_date());
                double substract = now - gpsDate;
                int subs = (int) substract;
                if (subs >= 600000 && subs <= 660000) {

                    PositionDto positionDto = trama.getPosition();
                    LastPositionVehicleDto lastPositionVehice = new LastPositionVehicleDto(
                            trama.getPlate(), String.valueOf(positionDto.getAltitude()),
                            String.valueOf(positionDto.getLatitude()), String.valueOf(positionDto.getLongitude()), trama.getGpsDate(),
                            String.valueOf(trama.getSpeed()), String.valueOf(trama.getOdometer()),trama.getReceiveDate());
                    lastPositionVehicles.add(lastPositionVehice);
                }

            }
        }

        return lastPositionVehicles;
    }
    
    @Override
     public List<LastPositionVehicleDto> lastPositionVehiclesMoreOfTenMinutesAgo(List<String> plates) throws Exception {
        
        List<LastPositionVehicleDto> lastPositionVehicles = new ArrayList<>();

        if (plates.isEmpty()) {
            throw new GeneralException("No se ha encontrado placas ");
        }

        for (String plate: plates) {
            TramaRedisDto trama = redisService.findLastTramav2(plate);

            if (trama != null) {

                double now = new Date().getTime();
                double gpsDate =Double.valueOf(trama.getRu_gps_date());
                double substract = now - gpsDate;
                int subs = (int) substract;
                if (subs >= 660000) {
                    PositionDto positionDto = trama.getPosition();
                    LastPositionVehicleDto lastPositionVehice = new LastPositionVehicleDto(
                            trama.getPlate(),  String.valueOf(positionDto.getAltitude()),
                            String.valueOf(positionDto.getLatitude()), String.valueOf(positionDto.getLongitude()), trama.getGpsDate(),
                            String.valueOf(trama.getSpeed()), String.valueOf(trama.getOdometer()),trama.getReceiveDate());
                    lastPositionVehicles.add(lastPositionVehice);
                }

            }
        }
        return lastPositionVehicles;
    }
 
}
