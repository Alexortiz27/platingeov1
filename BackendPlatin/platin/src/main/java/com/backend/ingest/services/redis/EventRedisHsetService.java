package com.backend.ingest.services.redis;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.Trama;
import com.backend.platin.util.Util;
import com.google.gson.Gson;
import io.lettuce.core.Range;
import io.lettuce.core.ScoredValue;
import io.lettuce.core.api.sync.RedisCommands;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.SocketException;
import java.util.*;
import java.util.stream.Collectors;

@Service()
public class EventRedisHsetService {

    @Autowired
    RedisCommands<String, String> jedis;

    public void createHsetGlobal(String hsetkey, String field, String type) throws Exception {
        this.jedis.hset(hsetkey, field, type);

        //long time = Double.valueOf(43200).longValue();
        //this.jedis.expire(hsetkey, time);
    }
    public void createHsetvehicleWebSocket(String hsetkey, String field, String type) throws Exception {
        this.jedis.hset(hsetkey, field, type);
        long time = Double.valueOf(43200).longValue();
        this.jedis.expire(hsetkey, time);
    }

    public void createSetTypeWorkers(String hsetkey, double gps) throws Exception {

        String gpsdate = String.valueOf(gps);
        this.jedis.set(hsetkey, gpsdate);
        long time = Double.valueOf(86400).longValue();
        this.jedis.expire(hsetkey, time);
    }

    public void createSetTypeWorkers(String hsetkey, String vehicle) throws Exception {
        this.jedis.set(hsetkey, vehicle);
        long time = Double.valueOf(600).longValue();
        this.jedis.expire(hsetkey, time);
    }

    public void createHsetTravels(TravelUpdReportDto travel,String plate) throws Exception {
        String hsetkey = "eventoTravels:" + plate;
        String travelJsonString = new Gson().toJson(travel);

        this.jedis.hset(hsetkey, "travelStartEnd", travelJsonString);
        long time = Double.valueOf(86400).longValue();
        this.jedis.expire(hsetkey, time);

    }

    public TramaInsDto createOutInCalculateGeocerca(TramaInsDto trama, EventRedisInsDto type) throws Exception {
        String hsetkey = "eventOutIn:" + trama.getPlate();

        double timestamp = (double) trama.getGpsDate().getTime();
        String tramaJsonString = new Gson().toJson(type);

        this.jedis.zadd(hsetkey, timestamp, tramaJsonString);
        return trama;
    }

    public void createReportTrasmission(TramaInsDto trama) throws Exception {
        String hsetkey = "lastPositions:" + trama.getPlate();
        double timestamp = (double) trama.getGpsDate().getTime();
        String gpsdate = String.valueOf(timestamp);
        this.jedis.zadd(hsetkey, timestamp, trama.getPlate() + ":" + gpsdate);

    }

    public TramaInsDto createOutInTravels(TramaInsDto trama, EventRedisInsDto type) throws Exception {

        String hsetkey = "eventOutInReport:" + trama.getPlate();

        double timestamp = (double) trama.getGpsDate().getTime();
        String tramaJsonString = new Gson().toJson(type);

        this.jedis.zadd(hsetkey, timestamp, tramaJsonString);

        return trama;
    }

//    public List<TramaInsDto> find(String hsetkey) throws Exception {
//
//        Map<String, String> list = this.jedis.hgetall(hsetkey);
//
//        List<String> objectListHset = new ArrayList<String>();
//        for (String l : list.values()) {
//            objectListHset.add(l);
//        }
//        List<Object> objectListSet = new ArrayList<Object>(objectListHset);
//        List<TramaInsDto> castedListSet = (List<TramaInsDto>) (List) objectListSet;
//
//        return castedListSet;
//
//    }
//    public String findHGetStoppedCistern(String hsetkey, String field) throws Exception {
//
//        String movementStatus = this.jedis.hget(hsetkey, field);
//
//        return movementStatus;
//
//    }

    public String findGetTypeWorkersSet(String hsetkey) throws Exception {

        return this.jedis.get(hsetkey);

    }

    public String findGetTravelsHset(String hsetkey, String field) throws Exception {

        return this.jedis.hget(hsetkey, field);

    }
    public String findHgetTrackingPlate(String hsetkey, String field) throws Exception {

        return this.jedis.hget(hsetkey, field);

    }

    public void DeleteTypeDelWorkers(String hsetkey) throws Exception {

        this.jedis.del(hsetkey);

    }

    public void DeleteTravelsStartEnd(String hsetkey) throws Exception {

        this.jedis.hdel(hsetkey, "travelStartEnd");

    }
    public void DeleteHsetGloval(String hsetkey,String field) throws Exception {

        this.jedis.hdel(hsetkey, field);

    }
//    public List<EventRedisInsDto> GetEventOutInTravels(String redisKey, TramaInsDto tramadto) throws Exception {
//        Calendar calendarR = Calendar.getInstance();
//        calendarR.setTime(tramadto.getGpsDate());
//        calendarR.add(Calendar.MINUTE, -10);
//        Date fechaSalidaR = calendarR.getTime();
//        double end = (double) tramadto.getGpsDate().getTime();
//        double start = (double) fechaSalidaR.getTime();
//
//        Range<Double> range = Range.create(start, end);
//
//        List<String> aList = this.jedis.zrangebyscore(redisKey, range);
//
//        EventRedisInsDto event = null;
//        List<EventRedisInsDto> castedList = new ArrayList<>();
//        for (String smember : aList) {
//            event = new Gson().fromJson(smember, EventRedisInsDto.class);
//            castedList.add(new EventRedisInsDto(event.getType(), event.getCreateDate(), event.getGeozone_id()));
//        }
//        return castedList;
//
//    }

    public List<ScoredValue<String>> findGetTrasmission(TramaInsDto tramadto) throws Exception {

        String hsetkey = "lastPositions:" + tramadto.getPlate();
        Range<Double> range = Range.create(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
        return this.jedis.zrangebyscoreWithScores(hsetkey, range);

    }

    public Trama create(Trama trama) throws SocketException {
        String redisKey = "tramas:" + trama.getPlate();

        double timestamp = (double) trama.getGpsDate().getTime();
        String tramaJsonString = new Gson().toJson(trama);
        this.jedis.zadd(redisKey, timestamp, tramaJsonString);

        return trama;
    }
    public Trama create(Trama trama,String tra) throws SocketException {
        String redisKey = "tramas:" + trama.getPlate();
        double timestamp = (double) trama.getGpsDate().getTime();
        this.jedis.zadd(redisKey, timestamp, tra);

        return trama;
    }

    public List<TramaInsDto> findStoppedZrange(String redisKey, TramaInsDto tramadto, String type, int minHour) throws Exception {
        Calendar calendarR = Calendar.getInstance();
        calendarR.setTime(tramadto.getGpsDate());
        if (type.equals("MINUTE")) {
            calendarR.add(Calendar.MINUTE, minHour);
        } else {
            calendarR.add(Calendar.HOUR, minHour);
        }
        Date fechaSalidaR = calendarR.getTime();

        double start = (double) fechaSalidaR.getTime();

        Range<Double> range = Range.create(start, Double.POSITIVE_INFINITY);
        List<String> smembers = this.jedis.zrangebyscore(redisKey, range);

        List<TramaInsDto> castedList = new ArrayList<>();
        for (String smember : smembers) {
            TramaInsDto tramaNew= Util.parseJsonToTramaDto(smember);
            castedList.add(new TramaInsDto(tramaNew.getEvent(), tramaNew.getPlate(), tramaNew.getPosition(), tramaNew.getGpsDate(),
                    tramaNew.getSpeed(), tramaNew.getReceiveDate(), tramaNew.getTokenTrama(), tramaNew.getOdometer()));
        }
        return castedList;

    }

    public TramaRedisDto findLastTramav2(String plate) throws  Exception {
        String redisKey = "tramas:" + plate;

        List<String> tramas = this.jedis.zrange(redisKey, -1, -1);
        if (tramas.isEmpty()) {
            return null;
        } else {

            return new Gson().fromJson(tramas.get(0), TramaRedisDto.class);
        }

    }

    public String[] createReportRequest(String status, ReportDto report, String xlsUrl,String geoUrl) {
        
        String redisKey = "user/reports/" + report.getUser_id();

        Map<String, String> listReportStatus = this.jedis.hgetall(redisKey);
        List<String> fieldsReportStatus= new ArrayList<>(listReportStatus.keySet());
        
        if (fieldsReportStatus.size() >= 10) {
            
            String oldField = fieldsReportStatus.get(0);
            this.jedis.hdel(redisKey,oldField);
        }
        
        String hash_report = report.getPlate() + "/" + report.getStartDate() + "/" + report.getEndDate() + "/"+Util.convertDateUtcGmtPeruString(new Date())+"/" + report.getTypeReport();

        String report_data = new Gson().toJson(new ReportStatusDto(status, report.getTypeReport(), xlsUrl,geoUrl,0,report.getPlate(),report.getStartDate(),report.getEndDate()));

        this.jedis.hset(redisKey, hash_report, report_data);
        
        long time = Double.valueOf(43200).longValue();
        this.jedis.expire(redisKey, time);
        
        String [] keys = new String [2];
        keys[0]=redisKey;
        keys[1]=hash_report;
        
        return keys;

    }
    
    public void changeStatusReport(String [] keys,ReportDto report,String xlsUrl,String status,String geoUrl,long timeElapsed,String plate)
    {
         String report_data = new Gson().toJson(new ReportStatusDto(status, report.getTypeReport(), xlsUrl,geoUrl,timeElapsed,plate,report.getStartDate(),report.getEndDate()));
         
          this.jedis.hset(keys[0], keys[1], report_data);
    }
    
    
    public List<ReportStatusDto> getListOfReports(String user_id) {
        String redisKey = "user/reports/" + user_id;

        Map<String, String> listReportStatus = this.jedis.hgetall(redisKey);

        List<String> listReportStatusString = new ArrayList<>(listReportStatus.values());

        List<ReportStatusDto> listReportStatusDto = new ArrayList<>();

        for (String json : listReportStatusString) {
            ReportStatusDto reportStatusDto = new Gson().fromJson(json, ReportStatusDto.class);

            listReportStatusDto.add(reportStatusDto);
        }

        return listReportStatusDto;

    }

    public List<String> getListErrorLogger() {

        String redisKey = "platin:error:logs";
        Map<String, String> mapErrorLogs = this.jedis.hgetall(redisKey);

        List<String> listErrorLogs = mapErrorLogs.values().stream().collect(Collectors.toList());
        return listErrorLogs;
    }
    public List<String> getListVehiclePopulation() {

        String redisKey = "rb:last_position_populate";
        Map<String, String> mapListPlate = this.jedis.hgetall(redisKey);

        return mapListPlate.keySet().stream().collect(Collectors.toList());
    }

    public List<String> getAllPlates()
    {
        Map<String, String> mapPlates=this.jedis.hgetall("vehicle");
        return mapPlates.keySet().stream().collect(Collectors.toList());
    }

}
