package com.backend.nsqworker.consumer.service.impl;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.producer.Model.NsqMessage;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.services.GeozoneService;
import com.backend.ingest.service.services.VehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.consumer.util.Util;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.NsqChannelConst;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.brainlag.nsq.NSQConfig;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.NSQMessage;
import com.github.brainlag.nsq.exceptions.NSQException;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vividsolutions.jts.geom.Coordinate;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.logging.Level;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.springframework.context.annotation.ComponentScan;

@Service
@Slf4j
@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplCalculateGeocerca implements MqConsumerService {

    @Autowired(required = true)
    private GeozoneService geozoneService;

    @Autowired(required = true)
    private EventRedisHsetService eventRedis;

    @Autowired(required = true)
    private VehicleService vehicleService;

    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;

    @Autowired(required = true)
    private IEventService eventService;
    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    private final Logger log = LogManager.getLogger(MqConsumerByChannelServiceImplCalculateGeocerca.class);
    int tried = 0;

    @Value("${rest.uri.alertEvent}")
    String uri;

    @Value("${token.Internal}")
    String tokenInternal;

    public MqConsumerByChannelServiceImplCalculateGeocerca() {
    }

    public MqConsumerByChannelServiceImplCalculateGeocerca(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        log.info("CORRECTO CONSUMO mqConsumerCalculateGeocerca: ");
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);
//        NSQConfig config = new NSQConfig();
//        config.setMsgTimeout(600000);
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.CHANNEL_CAL_GEOZONA, (message) -> {
            if (message == null) {
                return;
            }
            String msg = new String(message.getMessage());
            NsqMessage nsqMessage = new NsqMessage();
            try {
                nsqMessage.setAction(NsqChannelConst.CHANNEL_CAL_GEOZONA);
                nsqMessage.setBody(msg);
                nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
            } catch (Exception e) {
                iErrorRepository.save(new ErrorLogs("Worker CalculateGeocerca: El mensaje no se pudo convertir, hay un problema", "500"));
                log.error("El mensaje no se pudo convertir, hay un problema");
                message.finished();
                return;
            }
            try {
                if (tried >= 3) {
                    tried = 0;
                    message.finished();
                    return;
                }
                // Consumo específico channel
                String idGeocerca;
                String category;
                double radius;
                Util u = new Util();
                TramaInsDto tramaNew = null;
                tramaNew = new Gson().fromJson(nsqMessage.getBody(), TramaInsDto.class);
                ArrayList<Coordinate> points = new ArrayList<Coordinate>();
                double latitudeTrama = tramaNew.getPosition().getLatitude();
                double longitudeTrama = tramaNew.getPosition().getLongitude();
                String idvehicle = u.VehicleSet(tramaNew,eventRedis,vehicleService,message);
                String keySetGeozona = "geozonasSet";
                String listGeozona = eventRedis.findGetTypeWorkersSet(keySetGeozona);
                if (listGeozona == null) {
                    message.touch();
                    List<Map<String, Object>> geozones = geozoneService.findByStatusAll();
                    message.touch();
                    listGeozona = new Gson().toJson(geozones);
                    if (listGeozona != null) {
                        eventRedis.createSetTypeWorkers(keySetGeozona, listGeozona);
                    }
                }
                if (listGeozona == null) {
                    throw new DataNotFoundException(com.backend.platin.util.Util.GEOZONE_NOT_FOUND);
                }
                //Assigning Trama's Position to an Object Coordinate
                Coordinate coord = new Coordinate(latitudeTrama, longitudeTrama);
                JsonArray jsonArray = new JsonParser().parse(listGeozona).getAsJsonArray();
                for (JsonElement jsonElement : jsonArray) {
                    points.clear();
                    category = jsonElement.getAsJsonObject().get("category").getAsString();
                    radius = Double.parseDouble(jsonElement.getAsJsonObject().get("radius").getAsString());
                    idGeocerca = jsonElement.getAsJsonObject().get("id").getAsString();
                    String coordgeo = jsonElement.getAsJsonObject().get("coords").getAsString();
                    //log.info("COMIENZO DE CALCULATE GEOZONA:"+new Date()+"===IDGEOZONA:"+idGeocerca);
                    message.touch();
                    this.calculateGeo(message, idvehicle, radius, category, coordgeo, points, u, idGeocerca, tramaNew, coord, latitudeTrama, longitudeTrama);
                    //log.info("TERMINO DE CALCULATE GEOZONA:"+new Date()+"===IDGEOZONA:"+idGeocerca);
                    message.touch();
                }
                log.info("Consumo de mensajes CHANNEL_CAL_GEOZONA específicos: " + nsqMessage.getBody());

                // Mensaje de confirmacion
                message.finished();
                return;
            } catch (DataNotFoundException ex) {
                log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                log.warn("DataNotFoundException Worker CalculateGeocerca:" + ex.getMessage());
                iErrorRepository.save(new ErrorLogs("Worker CalculateGeocerca:" + ex.getMessage(), "400"));
                message.finished();
                return;

            } catch (Exception e) {
                tried++;
                log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
                log.warn("Error Worker CalculateGeocerca:" + e.getMessage());
                iErrorRepository.save(new ErrorLogs("Worker CalculateGeocerca:" + e.getMessage(), "Error"));
                message.requeue();
                return;
            }
        });
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By CHANNEL_CAL_GEOZONA El consumidor CalculateGeocercaTrama comenzó con éxito!");

    }

    public void SaveEvent(String type, TramaInsDto tramaNew, String idvehicle, String idGeocerca, double latitudeTrama, double longitudeTrama) throws DataNotFoundException {
        EventInsDto eventNew = null;
        eventNew = new EventInsDto(type, idvehicle, idGeocerca, latitudeTrama, longitudeTrama,
                tramaNew.getReceiveDate(), tramaNew.getGpsDate(),
                tramaNew.getSpeed(), "null", "null", (int) tramaNew.getOdometer());
//        Vehicle vehiclelist = vehicleService.findById(idvehicle);
//        eventNew.setVehicle_id(vehiclelist);
        eventNew = eventService.save(eventNew);
        com.backend.platin.util.Util.sendAlertEventInst(eventNew, uri, tokenInternal);
    }

    public void calculateGeo(NSQMessage message, String idvehicle, double radius, String category, String loc, ArrayList<Coordinate> points, Util u,
                             String idGeocerca, TramaInsDto tramaNew, Coordinate coord, double longitudeTrama, double latitudeTrama) throws Exception {
        JsonArray jsonArrayCoords = new JsonParser().parse(loc).getAsJsonArray();
        if (category.equals("polygon")) {
            String hsetkey = "eventOutInSet:" + tramaNew.getPlate() + idGeocerca;
            String typeInOut = eventRedis.findGetTypeWorkersSet(hsetkey);
            message.touch();
            u.coordPolygon(points, jsonArrayCoords);
            message.touch();
            //boolean outPolygon = u.isPointPolygon(points, coord);
            boolean outPolygon = u.coordPolygonPath2D(jsonArrayCoords, coord);
            message.touch();
            if (outPolygon == true) {
                if (isNull(typeInOut)) {
                    log.info("ENTRO DENTRO POLYGON DE LA GEOZONA:" + hsetkey);
                    log.info("POLYGONO NUEVO METODO:" + outPolygon);
                    eventRedis.createSetTypeWorkers(hsetkey, tramaNew.getGpsDate().getTime());
                    this.SaveEvent(com.backend.platin.util.Util.TYPEINCOMEREPORT, tramaNew, idvehicle, idGeocerca, latitudeTrama, longitudeTrama);
                }
            } else {
                if (!isNull(typeInOut)) {
                    log.info("SALIO POLYGON DE LA GEOZONA:" + hsetkey);
                    log.info("POLYGONO NUEVO METODO SALIO:" + outPolygon);
                    eventRedis.DeleteTypeDelWorkers(hsetkey);
                    this.SaveEvent(com.backend.platin.util.Util.TYPEEXITREPORT, tramaNew, idvehicle, idGeocerca, latitudeTrama, longitudeTrama);
                }
            }
        } else if (category.equals("circle")) {
            String hsetkeyCircle = "eventOutInSetCircle:" + tramaNew.getPlate() + idGeocerca;
            String typeInOutCircle = eventRedis.findGetTypeWorkersSet(hsetkeyCircle);
            message.touch();
            CoordInDto coordi = u.coordCircle(loc);
            boolean outCircle = u.isPointInCircle(coordi.getLatitude(), coordi.getLongitude(), radius, latitudeTrama, longitudeTrama);
            message.touch();
            if (outCircle == true) {
                if (isNull(typeInOutCircle)) {
                    log.info("ENTRO DENTRO CIRCLE DE LA GEOZONA:" + hsetkeyCircle);
                    eventRedis.createSetTypeWorkers(hsetkeyCircle, tramaNew.getGpsDate().getTime());
                    this.SaveEvent(com.backend.platin.util.Util.TYPEINCOMEREPORT, tramaNew, idvehicle, idGeocerca, latitudeTrama, longitudeTrama);
                }
            } else {
                if (!isNull(typeInOutCircle)) {
                    log.info("SALIO CIRCLE DE LA GEOZONA:" + hsetkeyCircle);
                    eventRedis.DeleteTypeDelWorkers(hsetkeyCircle);
                    this.SaveEvent(com.backend.platin.util.Util.TYPEEXITREPORT, tramaNew, idvehicle, idGeocerca, latitudeTrama, longitudeTrama);
                }
            }
        }
    }

    public boolean isNull(Object valueField) {
        if (valueField == null || (valueField.getClass().equals(String.class) && ((String) valueField).isEmpty())) {
            return true;
        }
        return false;
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerByReportsChannel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
