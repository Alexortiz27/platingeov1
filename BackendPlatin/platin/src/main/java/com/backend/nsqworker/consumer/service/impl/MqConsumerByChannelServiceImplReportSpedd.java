package com.backend.nsqworker.consumer.service.impl;

import com.backend.ingest.dtos.EventInsDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.dtos.TramaNsqDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.producer.Model.NsqMessage;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.services.VehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.NsqChannelConst;
import com.backend.platin.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;

import java.util.UUID;
import java.util.concurrent.Executors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.springframework.context.annotation.ComponentScan;

@Service
@Slf4j
@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplReportSpedd implements MqConsumerService {

    @Autowired(required = true)
    private VehicleService vehicleService;

    @Autowired(required = true)
    private IEventService eventService;

    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;

    @Autowired(required = true)
    private EventRedisHsetService eventRedis;

    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    int tried = 0;

    @Value("${rest.uri.alertEvent}")
    String uri;

    @Value("${token.Internal}")
    String tokenInternal;

    @Value("${value.spedd}")
    double valueSpedd;

    @Autowired
    EventRedisHsetService redisService;

    public MqConsumerByChannelServiceImplReportSpedd() {
    }

    public MqConsumerByChannelServiceImplReportSpedd(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

    @Override
    public void mqConsumerReportSpedd() {

        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);
        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.CHANNEL_REPORT_SPEED, (message) -> {
//            message = null;
            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();
                try {
                    nsqMessage.setAction(NsqChannelConst.CHANNEL_REPORT_SPEED);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker Spedd: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }

                try {
                    if (tried >= 3) {
                        tried = 0;
                        message.finished();
                        return;
                    }

                    TramaInsDto tramaNew = null;

                    tramaNew = new Gson().fromJson(nsqMessage.getBody(), TramaInsDto.class);
                    String idvehicle = com.backend.platin.consumer.util.Util.VehicleSet(tramaNew,eventRedis,vehicleService,message);

                    if (tramaNew.getSpeed() >= valueSpedd) {
                        EventInsDto eventNew = null;
                        eventNew = saveEventSpeed(tramaNew, idvehicle);
                        com.backend.platin.util.Util.sendAlertEventInst(eventNew, uri, tokenInternal);
                    }
                    log.info("Consumo de mensajes específicos CHANNEL_REPORT_SPEED: " + nsqMessage.getBody());
                    // Mensaje de confirmacion
                    message.finished();
                    return;
                } catch (DataNotFoundException ex) {
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.warn("DataNotFoundException Worker Spedd:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker Spedd:" + ex.getMessage(), "400"));
                    message.finished();
                    return;
                } catch (Exception e) {
                    tried++;
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
                    log.warn("Error Worker Spedd:" + e.getMessage());
                    e.printStackTrace();
                    iErrorRepository.save(new ErrorLogs("Worker Spedd:" + e.getMessage(), "Error"));
                    message.requeue();
                    return;
                }
            }
            return;
        });
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By CHANNEL_REPORT_SPEED El consumidor comenzó con éxito!");
    }

    //
//
    public EventInsDto saveEventSpeed(TramaInsDto tramaNew, String idvehicle) throws DataNotFoundException {
        EventInsDto eventNew = null;
        eventNew = new EventInsDto(Util.TYPEMAXIMUNSPEED,
                idvehicle,
                "null",
                tramaNew.getPosition().getLatitude(),
                tramaNew.getPosition().getLongitude(),
                tramaNew.getReceiveDate(), tramaNew.getGpsDate(), tramaNew.getSpeed(), "null", "null", (int) tramaNew.getOdometer());

        //Vehicle vehicledat = vehicleService.findById(idvehicle);
        //eventNew.setVehicle_id(vehicledat);
        eventNew = eventService.save(eventNew);
        return eventNew;
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerByReportsChannel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
