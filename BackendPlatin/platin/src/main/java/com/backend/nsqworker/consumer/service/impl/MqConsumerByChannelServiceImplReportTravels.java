package com.backend.nsqworker.consumer.service.impl;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Travel;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.producer.Model.NsqMessage;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.interfaces.ITravelService;
import com.backend.ingest.service.services.GeozoneService;
import com.backend.ingest.service.services.VehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.consumer.util.Util;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.NsqChannelConst;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vividsolutions.jts.geom.Coordinate;

import java.util.*;
import java.util.concurrent.Executors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.springframework.context.annotation.ComponentScan;

@Service
@Slf4j
@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplReportTravels implements MqConsumerService {

    @Autowired(required = true)
    private ITravelService travelService;

    @Autowired(required = true)
    private GeozoneService geozoneService;
    //
    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;

    @Autowired(required = true)
    private EventRedisHsetService eventRedis;
    //
    @Autowired(required = true)
    private VehicleService vehicleService;

    @Autowired(required = true)
    private IEventService eventService;
    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    int tried = 0;

    @Value("${rest.uri.alertEvent}")
    String uri;

    @Value("${token.Internal}")
    String tokenInternal;

    public MqConsumerByChannelServiceImplReportTravels() {
    }

    public MqConsumerByChannelServiceImplReportTravels(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

    @Override
    public void mqConsumerReportTravels() {
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);
        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.CHANNEL_REPORTTRAVELS, (message) -> {
//            message = null;
            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();
                try {
                    nsqMessage.setAction(NsqChannelConst.CHANNEL_REPORTTRAVELS);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker Travels: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }

                try {
                    if (tried >= 3) {
                        tried = 0;
                        log.info("Consumo CHANNEL_REPORTTRAVELS de mensajes específicos: " + nsqMessage.getBody());

                        message.finished();
                        return;
                    }
                    ArrayList<Coordinate> points = new ArrayList<Coordinate>();
                    String idGeocerca;
                    TramaInsDto tramaNew = null;
                    tramaNew = new Gson().fromJson(nsqMessage.getBody(), TramaInsDto.class);
                    Calendar calendarR = Calendar.getInstance();
                    calendarR.setTime(tramaNew.getGpsDate());
                    calendarR.add(Calendar.MINUTE, -10);
                    calendarR.add(Calendar.HOUR, -5);//estar por ver
                    Date fechaSalidaR = calendarR.getTime();
                    log.info("FECHA:"+fechaSalidaR);
                    List<TravelInterfReportDto> travelListClass = travelService.findAllWithFieldsContaining(tramaNew.getPlate(), fechaSalidaR);
                    if (travelListClass.isEmpty()) {
                        log.info(" TRAVEL_NOT_FOUND");
//                        throw new DataNotFoundException(com.backend.platin.util.Util.TRAVEL_NOT_FOUND);
                    }
                    String idVehicleTrama = com.backend.platin.consumer.util.Util.VehicleSet(tramaNew,eventRedis,vehicleService,message);
                    Util u = new Util();
                    String keySetGeozona = "geozonasSetTravels";
                    String listGeozona = eventRedis.findGetTypeWorkersSet(keySetGeozona);
                    if (listGeozona == null) {
                        List<Map<String, Object>> geozones = geozoneService.findByStatusAll();
                        listGeozona = new Gson().toJson(geozones);
                        if (listGeozona != null) {
                            eventRedis.createSetTypeWorkers(keySetGeozona, listGeozona);
                        }
                    }
                    if (listGeozona == null) {
                        log.info(" GEOZONE_NOT_FOUND");
                        throw new DataNotFoundException(com.backend.platin.util.Util.GEOZONE_NOT_FOUND);
                    }
                    String hsetkeyTravels = "eventoTravels:" + tramaNew.getPlate();
                    int sizeTravel = travelListClass.size() - 1;
                    TravelUpdReportDto travelDtoObject = new TravelUpdReportDto();
                    String travelIdGeozona = null;
                    String travelStatusEndStart = null;
                    if (!travelListClass.isEmpty()) {
                         travelDtoObject = new TravelUpdReportDto(
                                travelListClass.get(sizeTravel).getId(),
                                travelListClass.get(sizeTravel).getName(),
                                travelListClass.get(sizeTravel).getVehicle_travel_id(),
                                travelListClass.get(sizeTravel).getGeozona_id(),
                                travelListClass.get(sizeTravel).getStatus_travel(),
                                travelListClass.get(sizeTravel).getSchudeled_time(),
                                travelListClass.get(sizeTravel).getStart_date(),
                                travelListClass.get(sizeTravel).getEnd_date());
                        travelIdGeozona = travelDtoObject.getGeozona_id();
                        travelStatusEndStart = travelDtoObject.getStatusTravel();
                    }
                    String travelRedis = eventRedis.findGetTravelsHset(hsetkeyTravels, "travelStartEnd");
                    if (travelListClass.isEmpty() && travelRedis!=null) {
                        travelDtoObject = new Gson().fromJson(travelRedis, TravelUpdReportDto.class);
                        travelIdGeozona = travelDtoObject.getGeozona_id();
                        travelStatusEndStart = travelDtoObject.getStatusTravel();
                    }
                    double latitudeTrama = tramaNew.getPosition().getLatitude();
                    double longitudeTrama = tramaNew.getPosition().getLongitude();
                    Coordinate coord = new Coordinate(latitudeTrama, longitudeTrama);
                    if(travelStatusEndStart!=null) {
                        if (!travelStatusEndStart.equals("end")) {
                            JsonArray jsonArray = new JsonParser().parse(listGeozona).getAsJsonArray();
                            for (JsonElement jsonElement : jsonArray) {
                                points.clear();
                                String category = jsonElement.getAsJsonObject().get("category").getAsString();
                                double radius = Double.parseDouble(jsonElement.getAsJsonObject().get("radius").getAsString());
                                idGeocerca = jsonElement.getAsJsonObject().get("id").getAsString();
                                String coordgeo = jsonElement.getAsJsonObject().get("coords").getAsString();
                                if (idGeocerca.equals(travelIdGeozona)) {
                                    this.Calculategeo(hsetkeyTravels, travelIdGeozona, idVehicleTrama, travelDtoObject, category, radius, coordgeo, points, u, idGeocerca, tramaNew, coord, longitudeTrama, latitudeTrama);
                                }
                            }
                        }
                    }
                    log.info("Consumo CHANNEL_REPORTTRAVELS de mensajes específicos: " + nsqMessage.getBody());
                    // Mensaje de confirmacion
                    message.finished();
                    return;
                } catch (DataNotFoundException ex) {
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.warn("DataNotFoundException Worker CalculateGeocerca:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker CalculateGeocerca:" + ex.getMessage(), "400"));
                    message.finished();
                    return;
                } catch (Exception e) {
                    tried++;
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
                    log.warn("Error Worker CalculateGeocerca:" + e.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker Travels:" + e.getMessage(), "Error"));
                    message.requeue();
                    return;
                }
            }
            return;
        });
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By testChannel El consumidor comenzó con éxito!");
    }

    public void saveEventTravels(String type, TramaInsDto tramaNew, String idVehicleTrama,
                                 String idGeocerca, double latitudeTrama, double longitudeTrama, TravelUpdReportDto travelDtoObject) throws DataNotFoundException, Exception {
        EventInsDto eventNew = null;
        eventNew = new EventInsDto(type,
                idVehicleTrama,
                idGeocerca,
                latitudeTrama,
                longitudeTrama,
                tramaNew.getReceiveDate(),
                tramaNew.getGpsDate(),
                tramaNew.getSpeed(),
                travelDtoObject.getId(),
                travelDtoObject.getName(),
                (int) tramaNew.getOdometer());

        //Vehicle vehicledat = vehicleService.findById(idVehicleTrama);
        //eventNew.setVehicle_id(vehicledat);
        eventNew = eventService.save(eventNew);
        com.backend.platin.util.Util.sendAlertEventInst(eventNew, uri, tokenInternal);
    }

    public void Calculategeo(String hsetkeyTravels, String travelIdGeozone, String idVehicleTrama, TravelUpdReportDto travelDtoObject, String category, double radius, String loc, ArrayList<Coordinate> points, Util u,
                             String idGeocerca, TramaInsDto tramaNew, Coordinate coord, double longitudeTrama, double latitudeTrama) throws Exception {
        JsonArray jsonArrayy = new JsonParser().parse(loc).getAsJsonArray();
        if (category.equals("polygon")) {
            String hsetkey = "eventoSetTravels:" + tramaNew.getPlate() + idGeocerca;
            String typeInOut = eventRedis.findGetTypeWorkersSet(hsetkey);
            u.coordPolygon(points, jsonArrayy);
            boolean outPolygon = u.isPointPolygon(points, coord);
            if (outPolygon == true) {
                if (typeInOut == null) {
                    log.info("ENTRO DENTRO POLYGON DE LA GEOZONA:" + hsetkey);
                    eventRedis.createSetTypeWorkers(hsetkey, tramaNew.getGpsDate().getTime());
                    this.saveEventTravels(com.backend.platin.util.Util.TYPEENDIN, tramaNew,
                            idVehicleTrama, idGeocerca, latitudeTrama, longitudeTrama, travelDtoObject);
                    if (travelDtoObject.getStatusTravel().equals("created")) {
                        travelService.update(travelDtoObject.getId(), new TravelUpdDto(travelDtoObject.getName(), idVehicleTrama, travelIdGeozone,
                                "start", travelDtoObject.getSchudeledTime(),
                                tramaNew.getGpsDate(), travelDtoObject.getEndDate()));
                        eventRedis.createHsetTravels(travelDtoObject,tramaNew.getPlate());
                    }
                }
            } else {
                if (typeInOut != null) {
                    log.info("SALIO POLYGON DE LA GEOZONA:" + hsetkey);
                    eventRedis.DeleteTypeDelWorkers(hsetkey);
                    this.saveEventTravels(com.backend.platin.util.Util.TYPESTARTOUT, tramaNew,
                            idVehicleTrama, idGeocerca, latitudeTrama, longitudeTrama, travelDtoObject);
                    if (travelDtoObject.getStatusTravel().equals("start")) {
                        travelService.update(travelDtoObject.getId(), new TravelUpdDto(travelDtoObject.getName(), idVehicleTrama, travelIdGeozone,
                                "end", travelDtoObject.getSchudeledTime(),
                                travelDtoObject.getStartDate(), tramaNew.getGpsDate()
                        ));
                        eventRedis.DeleteTravelsStartEnd(hsetkeyTravels);
                    }
                }
            }
        } else if (category.equals("circle")) {
            String hsetkeyCircle = "eventoSetTravelsCircle:" + tramaNew.getPlate() + idGeocerca;
            String typeInOutCircle = eventRedis.findGetTypeWorkersSet(hsetkeyCircle);
            CoordInDto coordi = u.coordCircle(loc);
            double y1 = longitudeTrama;
            double x1 = latitudeTrama;
            double y2 = coordi.getLatitude();
            double x2 = coordi.getLongitude();
            boolean outCircle = u.isPointInCircle(y2, x2, radius, x1, y1);
            if (outCircle == true) {
                if (typeInOutCircle == null) {
                    log.info("ENTRO DENTRO CIRCLE DE LA GEOZONA:" + hsetkeyCircle);
                    eventRedis.createSetTypeWorkers(hsetkeyCircle, tramaNew.getGpsDate().getTime());
                    this.saveEventTravels(com.backend.platin.util.Util.TYPEENDIN, tramaNew,
                            idVehicleTrama, idGeocerca, latitudeTrama, longitudeTrama, travelDtoObject);

                    if (travelDtoObject.getStatusTravel().equals("created")) {
                        travelService.update(travelDtoObject.getId(), new TravelUpdDto(travelDtoObject.getName(), idVehicleTrama, travelIdGeozone,
                                "start", travelDtoObject.getSchudeledTime(),
                                tramaNew.getGpsDate(), travelDtoObject.getEndDate()));
                        eventRedis.createHsetTravels(travelDtoObject,tramaNew.getPlate());
                    }

                }
            } else {
                if (typeInOutCircle != null) {
                    log.info("SALIO CIRCLE DE LA GEOZONA:" + hsetkeyCircle);
                    eventRedis.DeleteTypeDelWorkers(hsetkeyCircle);
                    this.saveEventTravels(com.backend.platin.util.Util.TYPESTARTOUT, tramaNew,
                            idVehicleTrama, idGeocerca, latitudeTrama, longitudeTrama, travelDtoObject);
                    if (travelDtoObject.getStatusTravel().equals("start")) {
                        travelService.update(travelDtoObject.getId(), new TravelUpdDto(travelDtoObject.getName(), idVehicleTrama, travelIdGeozone,
                                "end", travelDtoObject.getSchudeledTime(),
                                travelDtoObject.getStartDate(), tramaNew.getGpsDate()
                        ));
                        eventRedis.DeleteTravelsStartEnd(hsetkeyTravels);
                    }
                }
            }
        }
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerByReportsChannel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
