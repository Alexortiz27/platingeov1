package com.backend.nsqworker.consumer.service.impl;


import com.backend.platin.util.NsqChannelConst;
import com.backend.nsqworker.consumer.model.NsqMessage;
import com.backend.ingest.dtos.ReportDto;
import com.backend.ingest.dtos.ReportInDto;
import com.backend.ingest.dtos.ReportNsqDto;
import com.backend.ingest.dtos.Reports.jsons.ReportResponseDto;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.services.ReportService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.GeneralException;
import com.backend.platin.util.Util;

import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;

import java.util.UUID;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.ResourceAccessException;


@Service
@Slf4j

@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplReportsChannel implements MqConsumerService {

    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    private final Logger log = LogManager.getLogger(MqConsumerByChannelServiceImplReportsChannel.class);

   
    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;

    @Autowired
    private ReportService reportService;

  
    ReportNsqDto reportNsq;

    String stackMessage = null;
    String stackMessageReport = null;

    ReportDto report;
  
    public MqConsumerByChannelServiceImplReportsChannel() {
    }

    public MqConsumerByChannelServiceImplReportsChannel(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }
    int tried = 0;

    @Override
    public void mqConsumerByReportsChannel() {
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);

        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.REPORTS_CHANNEL, (message) -> {
            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();
                try {
                    nsqMessage.setAction(NsqChannelConst.REPORTS_CHANNEL);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }

                try {
                    if (tried >= 3) {
                        tried = 0;
                        stackMessageReport = "¡Los registros seleccionados no existe en la base de datos!";
                        message.finished();

                        ReportInDto reportObject = new ReportInDto("No Disponible", "No Disponible", report.getTypeReport(), "No Disponible", report.getStartDate(),
                                report.getEndDate(), false, report.getCompany_id());
                        reportService.create(reportObject);

                        ReportResponseDto reportResponse = new ReportResponseDto();
                        reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_ERROR + ": " + stackMessage);
                 
                        return;
                    }

                    report = new Gson().fromJson(nsqMessage.getBody(), ReportDto.class);
                    if (report != null && report.getPlate() != null && !report.getPlate().isEmpty()
                            && report.getStartDate() != null
                            && report.getEndDate() != null
                            && report.getCompany_id() != null && !report.getCompany_id().isEmpty()) {
                        


                    } else {

                        throw new GeneralException("Hay campos vacios para el reporte");
                    }

                   
                } catch (ResourceAccessException re) {
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + re.getMessage(), "Error"));
//                    CellStyle style = style = workbook.createCellStyle();
//                    XSSFFont font = workbook.createFont();
//                    font.setFontHeight(14);
//                    style.setFont(font);
//                    this.generateGlobalReport(message, style, workbook);
                    stackMessage = re.getMessage();
                    re.printStackTrace();
                   
                } catch (DataNotFoundException ex) {
                    tried++;
                    log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.info("DataNotFoundException Worker ReportChannel:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error 400"));
                    ex.printStackTrace();
                    stackMessage = ex.getMessage();
                    stackMessageReport = "¡Los registros seleccionados no existe en la base de datos!";
                    log.info(ex.getMessage());
                    message.requeue();
                    return;

//                } catch (IOException ex) {
//                    tried++;
//                    log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
//                    log.info("IOException Worker ReportChannel:" + ex.getMessage());
//                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
//                    ex.printStackTrace();
//                    stackMessage = ex.getMessage();
//                    log.info(ex.getMessage());
//                    message.requeue();
//                    return;
//                } catch (DataException ex) {
//                    tried++;
//                    log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
//                    log.info("DataException Worker ReportChannel:" + ex.getMessage());
//                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
//                    ex.printStackTrace();
//                    stackMessage = ex.getMessage();
//                    log.info(ex.getMessage());
//                    message.requeue();
//                    return;
//
//                } catch (ParseException ex) {
//                    tried++;
//                    log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
//                    log.info("ParseException Worker ReportChannel:" + ex.getMessage());
//                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
//                    ex.printStackTrace();
//                    stackMessage = ex.getMessage();
//                    stackMessageReport = "¡Los registros seleccionados no existe en la base de datos!";
//                    log.info(ex.getMessage());
//

//                } catch (IOException ex) {
//                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
//
//                    ex.printStackTrace();
//                    message.requeue();
//                    stackMessage = ex.getMessage();
//                    tried++;
//
//                    return;
//                } catch (ParseException ex) {
//                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
//
//                    ex.printStackTrace();
//                    message.requeue();
//                    stackMessage = ex.getMessage();
//                    stackMessageReport = "¡Los registros seleccionados no existe en la base de datos!";
//                    tried++;
//
//                    return;
//                
                 
                } catch (Exception ex) {
                    tried++;
                    log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.info("Error Worker ReportChannel:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:"+ex.getMessage(), "Error"));
                    ex.printStackTrace();
                    stackMessage = ex.getMessage();
                    log.info(ex.getMessage());
                    message.requeue();
                    return;
                }
            }
            return;
        });
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By REPORTS_CHANNEL El consumidor mqConsumerByReportsChannel comenzó con éxito!");
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
//    private void getExcelGlobalReport(List<Event> listEvent, int rowCount, List<Trama> tramas, Row row, CellStyle style) throws ParseException, DataNotFoundException {
//
//        rowCount = rowCount + 3;
//        for (Trama tramaObject : tramas) {
//            int columnCount = 0;
//            row = sheet.createRow(rowCount);
//
//            PositionDto position = new Gson().fromJson(tramaObject.getPosition(), PositionDto.class);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(position.getLatitude()), style, sheet, creationHelper);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(position.getLongitude()), style, sheet, creationHelper);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(position.getAltitude()), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(tramaObject.getSpeed()), style, sheet, creationHelper);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(tramaObject.getGpsDate()), style, sheet, creationHelper);
//            rowCount++;
//
//        }
//
//        rowCount = rowCount - tramas.size();
//
//        int length = 0;
//        if (listEvent.size() > tramas.size()) {
//            length = tramas.size();
//        } else {
//            length = listEvent.size();
//        }
//        int counter = 0;
//
//        if (listEvent.size() > 0) {
//            int i = 0;
//            length = counter + length;
//            if (listEvent.size() < tramas.size()) {
//                while (counter < length) {
//
//                    int columnCount = 5;
//                    row = sheet.getRow(rowCount);
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, this.translateTypeEventToSpanish(listEvent.get(i).getType()), style, sheet, creationHelper);
//
//                    if (listEvent.get(i).getGeozone_id() != null && !listEvent.get(i).getGeozone_id().equals("")
//                            && !listEvent.get(i).getGeozone_id().equals("null")) {
//
//                        Geozone geozone = geozoneService.findById(listEvent.get(i).getGeozone_id());
//
//                        if (geozone == null) {
//                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
//                        }
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, geozone.getName(), style, sheet, creationHelper);
//                    } else {
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, " ", style, sheet, creationHelper);
//                    }
//
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(listEvent.get(i).getCreationDate()), style, sheet, creationHelper);
//                    rowCount++;
//                    counter++;
//                    i++;
//                }
//            } else {
//                int a = 0;
//                for (a = 0; a < tramas.size(); a++) {
//
//                    int columnCount = 5;
//                    row = sheet.getRow(rowCount);
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, this.translateTypeEventToSpanish(listEvent.get(a).getType()), style, sheet, creationHelper);
//
//                    if (listEvent.get(a).getGeozone_id() != null && !listEvent.get(a).getGeozone_id().equals("")
//                            && !listEvent.get(a).getGeozone_id().equals("null")) {
//
//                        Geozone geozone = geozoneService.findById(listEvent.get(a).getGeozone_id());
//
//                        if (geozone == null) {
//                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
//                        }
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, geozone.getName(), style, sheet, creationHelper);
//                    } else {
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, " ", style, sheet, creationHelper);
//                    }
//
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(listEvent.get(a).getCreationDate()), style, sheet, creationHelper);
//                    rowCount++;
//
//                }
//                int substract = listEvent.size() - tramas.size();
//
//                for (int x = a + 1; x < substract; x++) {
//                    int columnCount = 5;
//                    row = sheet.createRow(rowCount);
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, this.translateTypeEventToSpanish(listEvent.get(x).getType()), style, sheet, creationHelper);
//
//                    if (listEvent.get(i).getGeozone_id() != null && !listEvent.get(i).getGeozone_id().equals("")
//                            && !listEvent.get(i).getGeozone_id().equals("null")) {
//
//                        Geozone geozone = geozoneService.findById(listEvent.get(i).getGeozone_id());
//
//                        if (geozone == null) {
//                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
//                        }
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, geozone.getName(), style, sheet, creationHelper);
//                    } else {
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, " ", style, sheet, creationHelper);
//                    }
//
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(listEvent.get(i).getCreationDate()), style, sheet, creationHelper);
//                    rowCount++;
//                }
//
//            }
//        }
//
//    }
//
//    private GeoJsonDto getGeoJsonGlobalReport(List<Event> listEvent, List<Trama> tramas, String typeReport) throws DataNotFoundException {
//
//        MetaGlobalDto meta = new MetaGlobalDto();
//        GeoJsonDto geoJsonDto = new GeoJsonDto();
//        geoJsonDto.setMeta(meta);
//
//        Util util = new Util();
//        geoJsonDto.setHeader(util.generateGeneralHeadersGeoJson(typeReport));
//
//        String[][] body = new String[tramas.size()][8];
//        int j = 0;
//        for (int i = 0; i < tramas.size(); i++) {
//            PositionDto position = new Gson().fromJson(tramas.get(i).getPosition(), PositionDto.class);
//            body[i][0] = String.valueOf(position.getLatitude());
//            body[i][1] = String.valueOf(position.getLongitude());
//            body[i][2] = String.valueOf(position.getAltitude());
//
//            body[i][3] = String.valueOf(tramas.get(i).getSpeed());
//            body[i][4] = String.valueOf(tramas.get(i).getGpsDate());
//            if (listEvent.size() > 0) {
//                //List of Events
//                if (j == listEvent.size()) {
//                    body[i][5] = null;
//                    body[i][6] = null;
//                    body[i][7] = null;
//
//                } else {
//                    body[i][5] = this.translateTypeEventToSpanish(listEvent.get(j).getType());
//
//                    if (listEvent.get(j).getGeozone_id() != null && !listEvent.get(j).getGeozone_id().equals("")
//                            && !listEvent.get(j).getGeozone_id().equals("null")) {
//
//                        Geozone geozone = geozoneService.findById(listEvent.get(j).getGeozone_id());
//                        if (geozone == null) {
//                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
//                        }
//                        body[i][6] = geozone.getName();
//                    } else {
//                        body[i][6] = " ";
//                    }
//
//                    body[i][7] = listEvent.get(j).getCreationDate().toString();
//                    j++;
//                }
//            }
//            geoJsonDto.setBody(body);
//        }
//        return geoJsonDto;
//
//    }

    private String translateTypeEventToSpanish(String typeEvent) {
        String type = null;
        switch (typeEvent) {
            case Util.TYPERECOVERYTRASMISSION:
                type = "RECUPERACIÓN";
                break;
            case Util.TYPELOSTTRASMISSION:
                type = "PÉRDIDA";
                break;
            case Util.TYPETIMELAG:
                type = "DEFASE DE TIEMPO";
                break;
            case Util.TYPEMAXIMUNSPEED:
                type = "EXCESO DE VELOCIDAD";
                break;
            case Util.TYPESTARTOUT:
                type = "EMPIEZA VIAJE";
                break;
            case Util.TYPEENDIN:
                type = "TERMINA VIAJE";
                break;
            case Util.TYPESTOPPED:
                type = "DETENIDO";
                break;
            case Util.TYPESTOPPEDFOUR:
                type = "DETENIDO POR 4 HORAS";
                break;
            case Util.TYPEMOVEMENT:
                type = "EN MOVIMIENTO";
                break;
            case Util.TYPEMOVEMENTFOUR:
                type = "EN MOVIMIENTO POR 4 HORAS";
                break;
            case Util.TYPEINCOMEREPORT:
                type = "DENTRO DE LA GEOZONA";
                break;
            case Util.TYPEEXITREPORT:
                type = "FUERA DE LA GEOZONA";
                break;
        }

        return type;
    }

   

//    private void generateGlobalReport(NSQMessage message, CellStyle style, XSSFWorkbook workbook) {
//
//        try {
//            Row currentRow = sheet.getRow(rowCount);
//            com.backend.platin.util.Util.writeCell(currentRow, 1, " ", style, sheet, creationHelper);
//
//            rowCount++;
//            currentRow = sheet.getRow(rowCount);
//            com.backend.platin.util.Util.writeCell(currentRow, 1, " ", style, sheet, creationHelper);
//
//            Vehicle vehicle = vehicleService.findByPlate(report.getPlate());
//            listEvent = eventService.findByPlateAndCreationDateBetween(report.getPlate(), report.getStartDate(), report.getEndDate());
//            this.getExcelGlobalReport(listEvent, rowCount, tramas, currentRow, style);
//            com.backend.platin.util.Util.generateExcelReportFile(workbook, uuid, xlsPath);
//
//            GeoJsonDto geoJsonDto = this.getGeoJsonGlobalReport(listEvent, tramas, topic);
//            String jsonTramas = new Gson().toJson(geoJsonDto);
//            Util.generateGeoJsonReportFile(uuid, geojsonPath, jsonTramas);
//
//        } catch (Exception ex) {
//            iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
//            tried++;
//
//            java.util.logging.Logger.getLogger(MqConsumerByChannelServiceImplReportsChannel.class.getName()).log(Level.SEVERE, null, ex);
//            message.requeue();
//            return;
//        }
//
//    }
//
//    private void generateHeadersVehiclesExcelReport(List<Trama> tramas, String geoUrlMap, CellStyle style) throws ParseException, DataNotFoundException {
//        rowCount = 1;
//        for (Trama trama : tramas) {
//            int columnCount = 0;
//            Row row = sheet.getRow(rowCount);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, trama.getPlate(), style, sheet, creationHelper);
////          
//            Vehicle vehicle = vehicleService.findByPlate(trama.getPlate());
//            Company company = companyService.findByIngestToken(trama.getTokenTrama());
//            com.backend.platin.util.Util.writeCell(row, columnCount++, company.getName(), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, vehicle.getCompanyClient().getName(), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, vehicle.getcode_osinergmin(), style, sheet, creationHelper);
//
//            String vehicle_status = vehicle.getStatus() ? "Habilitado" : "Inhabilitado";
//            com.backend.platin.util.Util.writeCell(row, columnCount++, vehicle_status, style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, geoUrlMap, style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(new Date()), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(report.getStartDate()), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(report.getEndDate()), style, sheet, creationHelper);
//        }
//
//    }

//    private void generateLastPosExcelReport(String plate, CellStyle style) throws ParseException {
//        if (!plate.equals(Util.PARAM_ALL)) {
//            rowCount++;
//            Row currentRow = sheet.getRow(rowCount);
//
//            rowCount = rowCount + 3;
//            Row row = sheet.createRow(rowCount);
//            totalDistance = this.getTotalDistance(tramas);
//            com.backend.platin.util.Util.writeCell(currentRow, 1, String.valueOf(totalDistance), style, sheet, creationHelper);
//
//            for (Trama tramaObject : tramas) {
//                int columnCount = 0;
//                row = sheet.createRow(rowCount++);
//
////                              GENERATING GPS DATE 
//                com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(tramaObject.getGpsDate()), style, sheet, creationHelper);
////                              GENERATING LATITUDE AND LONGITUDE (STANDARD_LAST_POS_REPORT)
//                PositionDto position = new Gson().fromJson(tramaObject.getPosition(), PositionDto.class);
//                com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(position.getLatitude()), style, sheet, creationHelper);
//                com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(position.getLongitude()), style, sheet, creationHelper);
//
//            }
//        } else {
//            rowCount = 2;
//            Row currentRow = sheet.getRow(rowCount);
//            totalDistance = this.getTotalDistance(tramas);
//            com.backend.platin.util.Util.writeCell(currentRow, 1, String.valueOf(totalDistance), style, sheet, creationHelper);
//
//        }
//
//    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
