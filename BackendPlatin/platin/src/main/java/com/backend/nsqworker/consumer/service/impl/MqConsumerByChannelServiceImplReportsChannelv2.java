package com.backend.nsqworker.consumer.service.impl;

import com.backend.ingest.dtos.*;
import com.backend.ingest.dtos.Reports.StandardLastPosDto;
import com.backend.ingest.dtos.Reports.TravelReportDto;
import com.backend.ingest.dtos.Reports.jsons.*;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.service.interfaces.ICompanyService;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.interfaces.ITramaService;
import com.backend.ingest.service.interfaces.IVehicleService;
import com.backend.ingest.service.services.GeozoneService;
import com.backend.ingest.service.services.ReportService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.nsqworker.consumer.model.NsqMessage;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.NsqChannelConst;
import com.backend.platin.util.Util;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplReportsChannelv2 implements MqConsumerService {

    @Autowired
    private ITramaService tramaService;

    @Autowired
    private GeozoneService geozoneService;

    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    private Logger log = LogManager.getLogger(MqConsumerByChannelServiceImplReportsChannelv2.class);

    @Autowired
    private IEventService eventService;

    @Value("${url.map}")
    private String urlMap;

    @Autowired
    private IVehicleService vehicleService;

    @Autowired
    ICompanyService companyServi;

    @Value("${rest.uri.report}")
    String uri;

    @Value("${token.Internal}")
    String tokenInternal;

    @Autowired
    EventRedisHsetService redisService;

    @Autowired
    private IErrorLogsRepository iErrorRepository;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    private ReportService reportService;

    String user_id;

    @Autowired
    StandardLastPosDto standardLastPosDto;

    @Autowired
    TravelReportDto travelReportDto;
    public MqConsumerByChannelServiceImplReportsChannelv2() {
    }

    public MqConsumerByChannelServiceImplReportsChannelv2(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

    int tried = 0;


    @Override
    public void mqConsumerByReportsChannel() {

        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);

        // Set the timeout
//       NSQConfig config= new NSQConfig();
//       config.setMsgTimeout(Util.NSQ_MSG_TIMEOUT);
        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.REPORTS_CHANNEL, (message) -> {

            Instant start = Instant.now();

            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();

                try {
                    nsqMessage.setAction(NsqChannelConst.REPORTS_CHANNEL);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }

                String stackMessage = null;
                String[] keys = null;
                ReportDto report = null;
                try {

                    String uuid = UUID.randomUUID().toString();
                    report = new Gson().fromJson(nsqMessage.getBody(), ReportDto.class);

                    if (tried >= 3) {
                        tried = 0;

                        ReportInDto reportObject = new ReportInDto("No Disponible", "No Disponible", report.getTypeReport(), "No Disponible", report.getStartDate(),
                                report.getEndDate(), false, report.getCompany_id());
                        reportService.create(reportObject);
                        ReportResponseDto reportResponse = new ReportResponseDto();
                        reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_ERROR + ": " + stackMessage);
                        reportResponse.setUser_id(report.getUser_id());
                        this.sendReport(reportResponse);
                        if (keys != null)
                            redisService.changeStatusReport(keys, report, " ", Util.FAILED_STATUS, " ", 0, report.getPlate());

                        message.finished();
                        return;
                    }

//                    List<Event> listEvent = null;
                    List<EventTravelDto> listEventTravels;
                    List<EventStoppedDto> listEventStoppedCistern;
                    List<EventGeozoneDto> listEventInOut ;
                    List<EventSpeedDto> listSpeedEvent ;
                    List<EventNoTransmissionDto> listEventTrasmission ;
                    List<EventTimeLagDto> listEventDesfase;

                    if (report != null && report.getPlate() != null && !report.getPlate().isEmpty()
                            && report.getStartDate() != null
                            && report.getEndDate() != null
                            && report.getCompany_id() != null && !report.getCompany_id().isEmpty()
                            && report.getUser_id() != null && !report.getUser_id().isEmpty()
                            && report.getTypeUrl() != null && !report.getTypeUrl().isEmpty()) {

                        log.warn("placa: " + report.getPlate());
                        log.warn("fecha inicio: " + report.getStartDate());
                        log.warn("fecha fin: " + report.getEndDate());
                        log.warn("compañía: " + report.getCompany_id());
                        log.warn("user id: " + report.getUser_id());
                        log.warn("type: " + report.getTypeReport());

                        String keySetVehicle = "VehicleSetReport" + report.getPlate();
                        String vehicle = redisService.findGetTypeWorkersSet(keySetVehicle);
                        if (vehicle == null) {
                            message.touch();
                            IVehicleCompanyReportDto vehicles = vehicleService.vehiclefindByPlate(report.getPlate());
                            message.touch();
                            VehicleCompanyReportDto vehi = new VehicleCompanyReportDto(vehicles.getcode_osinergmin(), vehicles.getNameemv(), vehicles.getStatus(), vehicles.getNameclient());
                            vehicle = new Gson().toJson(vehi);

                        }
                        if (vehicle != null) {
                            redisService.createSetTypeWorkers(keySetVehicle, vehicle);
                        }else
                        {
                            throw new DataNotFoundException(Util.VEHICLE_NOT_FOUND);
                        }

                        VehicleCompanyReportDto vehicleCompany = new Gson().fromJson(vehicle, VehicleCompanyReportDto.class);

                        log.warn("vehiculo procesado");
                        //set requested Report
                        keys = redisService.createReportRequest(Util.REQUESTED_STATUS, report, " ", " ");

                        String geoUrlMap = urlMap + uuid + ".geojson";

                        String header = null;
                        String body = null;

                        String vehicle_status = vehicleCompany.getStatus() ? "Habilitado" : "Inhabilitado";

                        Object[] exceldata = null;

                        //Change status of report
                        String xlsUrlMap = urlMap + uuid + ".xls";
                        redisService.changeStatusReport(keys, report, xlsUrlMap, Util.PROCESSING_STATUS, geoUrlMap, 0, report.getPlate());

                        user_id = report.getUser_id();

                        if (report.getTypeReport().equals(Util.NO_TRANSMISSION_REPORT) || report.getTypeReport().equals(Util.GEOZONES_REPORT)
                                || report.getTypeReport().equals(Util.TIMELAG_REPORT) || report.getTypeReport().equals(Util.STOPPED_REPORT) || report.getTypeReport().equals(Util.STOPPED_REPORTFOUR)
                                || report.getTypeReport().equals(Util.TRAVEL_REPORT)) {
                            exceldata = new Object[]{report.getPlate(), vehicleCompany.getNameemv(),
                                    vehicleCompany.getNameclient(), vehicleCompany.getcode_osinergmin(), vehicle_status,
                                    geoUrlMap, Util.convertDateUtcGmtPeruString(new Date()), Util.convertDateUtcGmtPeruString(report.getStartDate()),
                                    Util.convertDateUtcGmtPeruString(report.getEndDate())};
                        }

                        switch (report.getTypeReport())
                        {
                            case Util.STANDARD_LAST_POS_REPORT:
                                standardLastPosDto.process(message,report,start);
                                if(report.getTypeUrl().equals("xls"))
                                    standardLastPosDto.generateExcelReport();
                                else
                                    standardLastPosDto.generateGeoJsonReport();

                                standardLastPosDto.sendReportByWebSocket();
                                break;

                            case Util.TRAVEL_REPORT:
                                 travelReportDto.process(message,report,start);
                                 if(report.getTypeUrl().equals("xls"))
                                     travelReportDto.generateExcelReport();
                                 else
                                     travelReportDto.generateGeoJsonReport();

                                 travelReportDto.sendReportByWebSocket();
                                break;

                            case Util.SPEED_REPORT:
                                message.touch();
                                listSpeedEvent = eventService.getSpeedEvent(report.getPlate(), Util.TYPEMAXIMUNSPEED, report.getStartDate(), report.getEndDate());
                                if (listSpeedEvent.isEmpty()) {
                                    throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
                                }
//                                this.sendReport(uuid, header, body);
                                break;

                            case Util.NO_TRANSMISSION_REPORT:
                                message.touch();
                                listEventTrasmission = eventService.getNoTransmissionEvents(Util.TYPERECOVERYTRASMISSION, Util.TYPELOSTTRASMISSION, report.getPlate(), report.getStartDate(), report.getEndDate());
                                if (listEventTrasmission.isEmpty()) {
                                    throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
                                }

//                                String[] excelHeader = Util.generateGeneralHeadersExcel(report.getTypeReport());
//                                String[] excelBody = {"Fecha Velocidad", "Latitud", "Longitud", "Velocidad", "Odometro"};

//                                message.touch();
//                                this.generateSpeedExcelReport(uuid, excelHeader, exceldataSpped, excelBody, xlsPath, listSpeedEvent, sheett, wb, stylee);
//                                message.touch();

                                header = new Gson().toJson(exceldata);

                                List<NoTransmissionDto> eventNoTransmissions = new ArrayList<>();
                                for (EventNoTransmissionDto eventNoTransmission : listEventTrasmission) {
                                    NoTransmissionDto noTransmission = new NoTransmissionDto(eventNoTransmission.getType(),Util.convertDateUtcGmtPeruString(eventNoTransmission.getGps_Date()),eventNoTransmission.getLatitude(),eventNoTransmission.getLongitude(),eventNoTransmission.getSpedd(),eventNoTransmission.getOdometer());
                                    eventNoTransmissions.add(noTransmission);
                                }
                                body = new Gson().toJson(eventNoTransmissions);
                                this.sendReport(uuid, header, body);
                                break;

                            case Util.STOPPED_REPORT:
                                message.touch();
                                listEventStoppedCistern = eventService.getStoppedEvents(Util.TYPESTOPPED, Util.TYPEMOVEMENT, report.getPlate(), report.getStartDate(), report.getEndDate());
                                if (listEventStoppedCistern.isEmpty()) {
                                    throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
                                }

                                header = new Gson().toJson(exceldata);
                                //generateBodySTOPPEDCISTERNList(listEventStoppedCistern);
//                            List<StoppedDto> stoppedDtos = new ArrayList<>();
//                            for (EventStoppedDto eventStopped : listEventStoppedCistern) {
//                                StoppedDto stoppedDto = new StoppedDto(Util.convertDateUtcGmtPeruString(eventStopped.getCreation_Date()), Util.convertDateUtcGmtPeruString(eventStopped.getGps_Date()),
//                                        eventStopped.getLatitude(), eventStopped.getLongitude(),
//                                        eventStopped.getSpedd(), eventStopped.getOdometer(), eventStopped.getType());
//                                stoppedDtos.add(stoppedDto);
//                            }
                                List<StoppedListDto> ver = generateBodySTOPPEDCISTERNList(listEventStoppedCistern,Util.TYPESTOPPED, Util.TYPEMOVEMENT);
                                body = new Gson().toJson(ver);

                                this.sendReport(uuid, header, body);

//                                String[] excelBody = {"Fecha", "Latitud", "Longitud", "Inicio detenido", "Final detenido", "Tiempo definido", "Velocidad", "Odometro"};
//                                String[] excelHeader = Util.generateGeneralHeadersExcel(Util.STOPPED_REPORT);
//                                message.touch();
//                                this.generateStoppedExcelReport(uuid, excelHeader, exceldata, excelBody, xlsPath, listEventStoppedCistern, sheett, wb, stylee, creationHelper);
//                                message.touch();
                                break;

                            case Util.STOPPED_REPORTFOUR:
                                message.touch();
                                listEventStoppedCistern = eventService.getStoppedEvents(Util.TYPESTOPPEDFOUR, Util.TYPEMOVEMENTFOUR, report.getPlate(), report.getStartDate(), report.getEndDate());
                                if (listEventStoppedCistern.isEmpty()) {
                                    throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
                                }

                                header = new Gson().toJson(exceldata);
                                List<StoppedListDto> stoppedDtos = generateBodySTOPPEDCISTERNList(listEventStoppedCistern,Util.TYPESTOPPEDFOUR, Util.TYPEMOVEMENTFOUR);
                                body = new Gson().toJson(stoppedDtos);

                                this.sendReport(uuid, header, body);
//                                String[] excelBody = {"Fecha", "Latitud", "Longitud", "Inicio detenido", "Final detenido", "Tiempo definido", "Velocidad", "Odometro"};
//                                String[] excelHeader = Util.generateGeneralHeadersExcel(Util.STOPPED_REPORTFOUR);
//                                message.touch();
//                                this.generateStoppedFourExcelReport(uuid, excelHeader, exceldata, excelBody, xlsPath, listEventStoppedCistern, sheett, wb, stylee, creationHelper);
//                                message.touch();
                                break;

                            case Util.GEOZONES_REPORT:
                                String[] types = {Util.TYPEINCOMEREPORT, Util.TYPEEXITREPORT};
                                message.touch();
                                listEventInOut = eventService.getGeozoneEvents(types, report.getPlate(), report.getStartDate(), report.getEndDate());
                                if (listEventInOut.isEmpty()) {
                                    throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
                                }

//                                String[] excelBody = {"Fecha Geozona", "Tipo", "Geozona", "Velocidad", "Odometro"};
//                                String[] excelHeader = Util.generateGeneralHeadersExcel(Util.GEOZONES_REPORT);
//                                message.touch();
//                                this.generateGeozoneExcelReport(uuid, excelHeader, exceldata, excelBody, xlsPath, listEventInOut, sheett, wb, stylee, creationHelper);
//                                message.touch();
                                header = new Gson().toJson(exceldata);

                                List<GeozoneDto> geozones = new ArrayList<>();
                                for (EventGeozoneDto eventGeozone : listEventInOut) {
                                    GeozoneDto geozoneDto = new GeozoneDto(eventGeozone.getGeozone_id(),
                                            eventGeozone.getType(), Util.convertDateUtcGmtPeruString(eventGeozone.getGps_Date()),
                                            eventGeozone.getSpedd(), eventGeozone.getOdometer());
                                    geozones.add(geozoneDto);
                                }
                                body = new Gson().toJson(geozones);

                                this.sendReport(uuid, header, body);
                                break;

                            case Util.TIMELAG_REPORT:
                            message.touch();
                            listEventDesfase = eventService.getTimeLagEvents(report.getPlate(), Util.TYPETIMELAG, report.getStartDate(), report.getEndDate());
                            if (listEventDesfase.isEmpty()) {
                                throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
                            }

                            header = new Gson().toJson(exceldata);

                            List<TimeLagDto> timeLagDtos = new ArrayList<>();
                            for (EventTimeLagDto eventTimeLag : listEventDesfase) {
                                TimeLagDto timeLagDto = new TimeLagDto(Util.convertDateUtcGmtPeruString(eventTimeLag.getReceive_Date()),
                                        Util.convertDateUtcGmtPeruString(eventTimeLag.getGps_Date()), eventTimeLag.getLatitude(),
                                        eventTimeLag.getLongitude(), eventTimeLag.getSpedd(), eventTimeLag.getOdometer());

                                timeLagDtos.add(timeLagDto);
                            }
                            body = new Gson().toJson(timeLagDtos);

                            this.sendReport(uuid, header, body);

//                                String[] excelBody = {"Fecha de Recepción", "Fecha de Gps", "Latitud", "Longitud", "Velocidad", "Odometro"};
//                                String[] excelHeader = Util.generateGeneralHeadersExcel(Util.REPORTTIMELAG);
//                                message.touch();
//                                this.generateTimeLagExcelReport(uuid, excelHeader, exceldata, excelBody, xlsPath, listEventDesfase, sheett, wb, stylee, creationHelper);
//                                message.touch();
                            break;

                        }

//                        if (report.getTypeUrl().equals("xls")) {
//

                       //else if (report.getTypeReport().equals(Util.GLOBAL_REPORT)) {
//                                listEvent = eventService.findByPlateAndCreationDateBetween(report.getPlate(), report.getStartDate(), report.getEndDate());
//                                if (listEvent.isEmpty()) {
//                                    throw new DataNotFoundException(Util.EVENT_NOT_FOUND);
//                                }
//                                String[] excelHeader = Util.generateGeneralHeadersExcel(report.getTypeReport());
//                                String[] excelBody = {"Latitud", "Longitud", "Altitud", "Velocidad", "Fecha Gps", "Evento", "Geozona", "Fecha y Hora del mensaje"};
//
//                                Object[] exceldataGloval = {report.getPlate(), vehicleCompany.getNameemv(),
//                                        vehicleCompany.getNameclient(), vehicleCompany.getcode_osinergmin(), vehicle_status,
//                                        geoUrlMap, Util.convertDateUtcGmtPeruString(new Date()), Util.convertDateUtcGmtPeruString(report.getStartDate()),
//                                        Util.convertDateUtcGmtPeruString(report.getEndDate())};
////                                Util.generarExcelCabecera(uuid,excelHeader, exceldataGloval, excelBody, xlsPath, tramas, listEvent, sheett, wb, stylee, creationHelper, report.getTypeReport(), geozoneService);
//                            }
//                            log.warn("excel generado");

//                            Instant finishTime = Instant.now();
//                            Duration timeElapsed = Duration.between(start, finishTime);
//                            redisService.changeStatusReport(keys, report, xlsUrlMap, Util.COMPLETED_STATUS, "", timeElapsed.getSeconds(), report.getPlate());

//                            this.sendReport(uuid);

//                            log.warn("Consumo de mensajes REPORTS_CHANNEL específicos: " + nsqMessage.getBody());
//                            message.finished();
                        //Create a new row in Report Table in Database
//                            ReportInDto reportObject = new ReportInDto("", xlsUrlMap, report.getTypeReport(), Util.COMPLETED_STATUS, report.getStartDate(),
//                                    report.getEndDate(), true, report.getCompany_id());
//                            reportService.create(reportObject);

                        // *******GEJSON REPORT*****
//                        } else {
//                            keys = redisService.createReportRequest(Util.REQUESTED_STATUS, report, " ", " ");
//                            log.warn("generando geojson");
//
//                            message.touch();
//                            List<ITramaDto> geoTramas = tramaService.findByPlateGpsDateBetweenCompanyIdAsc(report.getPlate(), report.getStartDate(), report.getEndDate());
//                            message.touch();
//
//                            //generate geojson file
//                            GeoJsonDto geoJsonDto = new GeoJsonDto();
//
//                            String[][] body = null;
//
//                            int rowaumentv1 = 0;
//                            log.warn("instanciando body");
//                            if (report.getTypeReport().equals(Util.SPEED_REPORT)) {
//
//                                rowaumentv1 = Math.max(geoTramas.size(), listSpeedEvent.size());
//                            }
//                            if (report.getTypeReport().equals(Util.NO_TRANSMISSION_REPORT)) {
//
//                                rowaumentv1 = Math.max(geoTramas.size(), listEventTrasmission.size());
//                            }
//                            if (report.getTypeReport().equals(Util.REPORTTIMELAG)) {
//
//                                rowaumentv1 = Math.max(geoTramas.size(), listEventDesfase.size());
//                            }
//                            if (report.getTypeReport().equals(Util.GEOZONES_REPORT)) {
//
//                                rowaumentv1 = Math.max(geoTramas.size(), listEventInOut.size());
//                            }
//                            if (report.getTypeReport().equals(Util.STOPPED_REPORT)) {
//
//                                rowaumentv1 = Math.max(geoTramas.size(), listEventStoppedCistern.size());
//                            }
//                            if (report.getTypeReport().equals(Util.STOPPED_REPORTFOUR)) {
//
//                                rowaumentv1 = Math.max(geoTramas.size(), listEventStoppedCistern.size());
//                            }
//                            if (report.getTypeReport().equals(Util.TRAVEL_REPORT)) {
//
//                                rowaumentv1 = Math.max(geoTramas.size(), listEventTravels.size());
//                            }
//                            if (report.getTypeReport().equals(Util.GENERAL_REPORT)) {
//                                MetaDto meta = new MetaDto();
//                                geoJsonDto.setMeta(meta);
//                                body = new String[geoTramas.size()][9];
//                            } else if (report.getTypeReport().equals(Util.STANDARD_LAST_POS_REPORT)) {
//                                MetaStandardLasPosDto meta = new MetaStandardLasPosDto();
//                                geoJsonDto.setMeta(meta);
//                                body = new String[geoTramas.size()][14];
//                            } else if (report.getTypeReport().equals(Util.SPEED_REPORT)) {
//                                MetaSpedd meta = new MetaSpedd();
//                                geoJsonDto.setMeta(meta);
//                                body = new String[rowaumentv1][13];
//                            } else if (report.getTypeReport().equals(Util.NO_TRANSMISSION_REPORT)) {
//                                MetaTrasmission meta = new MetaTrasmission();
//                                geoJsonDto.setMeta(meta);
//                                body = new String[rowaumentv1][14];
//                            } else if (report.getTypeReport().equals(Util.REPORTTIMELAG)) {
//                                MetaTimeLag meta = new MetaTimeLag();
//                                geoJsonDto.setMeta(meta);
//                                body = new String[rowaumentv1][14];
//                            } else if (report.getTypeReport().equals(Util.GEOZONES_REPORT)) {
//
//                                MetaGeozone meta = new MetaGeozone();
//                                geoJsonDto.setMeta(meta);
//                                body = new String[rowaumentv1][13];
//
//                            } else if (report.getTypeReport().equals(Util.STOPPED_REPORT)) {
//                                MetaStoppedCistern meta = new MetaStoppedCistern();
//                                geoJsonDto.setMeta(meta);
//                                body = new String[rowaumentv1][16];
//                            } else if (report.getTypeReport().equals(Util.STOPPED_REPORTFOUR)) {
//                                MetaStoppedCistern meta = new MetaStoppedCistern();
//                                geoJsonDto.setMeta(meta);
//                                body = new String[rowaumentv1][16];
//                            } else if (report.getTypeReport().equals(Util.TRAVEL_REPORT)) {
//                                MetaTravelsReport meta = new MetaTravelsReport();
//                                geoJsonDto.setMeta(meta);
//                                body = new String[rowaumentv1][18];
//                            }
//
//                            int columnCount;
//                            geoJsonDto.setHeader(Util.generateGeneralHeadersGeoJson(report.getTypeReport()));
//
//                            redisService.changeStatusReport(keys, report, "", Util.PROCESSING_STATUS, geoUrlMap, 0, report.getPlate());
//                            if (!report.getTypeReport().equals(Util.GLOBAL_REPORT)) {
//
//                                String[] types = {Util.TYPESTOPPED, Util.TYPEMOVEMENT};
//                                List<IEventStoppedDto> listEventStopped = eventService.findGps_DateAndTypeOfStoppedEvent(types, report.getPlate(), report.getStartDate(), report.getEndDate());
//
//                                for (int i = 0; i < geoTramas.size(); i++) {
//                                    columnCount = 0;
//                                    body[i][columnCount++] = geoTramas.get(0).getPlate();
//                                    Vehicle vehiclelist = vehicleService.findByPlate(geoTramas.get(0).getPlate());
////                                    body[i][columnCount++] = vehicle.getCompanyClient().getHistorial().get(0).getCompany().getName();
//                                    body[i][columnCount++] = vehicleCompany.getNameemv();
//                                    body[i][columnCount++] = vehiclelist.getCompanyClient().getName();
//                                    body[i][columnCount++] = vehiclelist.getcode_osinergmin();
//                                    if (listEventStopped.size() > 0) {
//                                        columnCount = 4;
//                                        for (IEventStoppedDto iEventStoppedDto : listEventStopped) {
//                                            if (iEventStoppedDto.getGps_Date().equals(geoTramas.get(i).getGps_Date())) {
//                                                if (iEventStoppedDto.getType().equals("STOPPED")) {
//                                                    columnCount = 4;
//                                                    body[i][columnCount++] = "DETENIDO";
//                                                } else {
//                                                    columnCount = 4;
//                                                    body[i][columnCount++] ="EN MOVIMIENTO";
//                                                }
//                                            } else {
//                                                columnCount = 4;
//                                                body[i][columnCount++] = "EN MOVIMIENTO";
//                                            }
//                                        }
//                                    } else {
//                                        columnCount = 4;
//                                        body[i][columnCount++] = "EN MOVIMIENTO";
//                                    }
//                                    columnCount = 5;
////                                body[i][columnCount++] = vehicle.getStatus() ? Util.PARAM_STATUS_ACTIVE : Util.PARAM_STATUS_INACTIVE;
//                                    body[i][columnCount++] = geoUrlMap;
//                                    body[i][columnCount++] = Util.parseDateToPrinterFormatString(Util.convertDateUtcGmtPeru(new Date()));
//                                    body[i][columnCount++] = Util.parseDateToPrinterFormatString(Util.convertDateUtcGmtPeru(report.getStartDate()));
//                                    body[i][columnCount++] = Util.parseDateToPrinterFormatString(Util.convertDateUtcGmtPeru(report.getEndDate()));
//
//                                    if (report.getTypeReport().equals(Util.STANDARD_LAST_POS_REPORT)) {
//                                        //GENERATING GPS DATE
////
//                                        //GENERATING LATITUDE AND LONGITUDE
//                                        PositionDto positiondto = new Gson().fromJson(geoTramas.get(i).getPosition(), PositionDto.class);
//                                        body[i][columnCount++] = String.valueOf(positiondto.getLatitude());
//                                        body[i][columnCount++] = String.valueOf(positiondto.getLongitude());
//                                        body[i][columnCount++] = String.valueOf(totalDistance);
//                                        body[i][columnCount++] = Util.convertStringUtcGmtPeruString(geoTramas.get(i).getGps_Date());
//                                        body[i][columnCount++] = String.valueOf(geoTramas.get(i).getSpeed());
//                                    }
//                                }
//                            }
//
//                            if (report.getTypeReport().equals(Util.SPEED_REPORT)) {
////
//                                for (int i = 0; i < listSpeedEvent.size(); i++) {
//                                    int columnCountn = 9;
//
//                                    body[i][columnCountn++] = Util.convertDateUtcGmtPeruString(listSpeedEvent.get(i).getGps_Date());
//                                    body[i][columnCountn++] = String.valueOf(listSpeedEvent.get(i).getLatitude());
//                                    body[i][columnCountn++] = String.valueOf(listSpeedEvent.get(i).getLongitude());
//                                    body[i][columnCountn++] = String.valueOf(listSpeedEvent.get(i).getSpedd());
//                                }
//
//                            }
//                            if (report.getTypeReport().equals(Util.NO_TRANSMISSION_REPORT)) {
//                                for (int i = 0; i < listEventTrasmission.size(); i++) {
//                                    int columnCountnn = 9;
//
//                                    body[i][columnCountnn++] = Util.convertDateUtcGmtPeruString((listEventTrasmission.get(i).getGps_Date()));
//                                    body[i][columnCountnn++] = String.valueOf(listEventTrasmission.get(i).getType());
//                                    body[i][columnCountnn++] = String.valueOf(listEventTrasmission.get(i).getLatitude());
//                                    body[i][columnCountnn++] = String.valueOf(listEventTrasmission.get(i).getLongitude());
//                                    body[i][columnCountnn++] = String.valueOf(listEventTrasmission.get(i).getSpedd());
//                                }
//                            }
//                            if (report.getTypeReport().equals(Util.REPORTTIMELAG)) {
//
//                                for (int i = 0; i < listEventDesfase.size(); i++) {
//                                    int columnCountnnn = 9;
//
//                                    body[i][columnCountnnn++] = String.valueOf(Util.convertDateUtcGmtPeru(listEventDesfase.get(i).getReceive_Date()));
//                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventDesfase.get(i).getGps_Date());
//                                    body[i][columnCountnnn++] = String.valueOf(listEventDesfase.get(i).getLatitude());
//                                    body[i][columnCountnnn++] = String.valueOf(listEventDesfase.get(i).getLongitude());
//                                    body[i][columnCountnnn++] = String.valueOf(listEventDesfase.get(i).getSpedd());
//                                }
//
//                            }
//                            if (report.getTypeReport().equals(Util.GEOZONES_REPORT)) {
//
//                                for (int i = 0; i < listEventInOut.size(); i++) {
//                                    int columnCountnnn = 9;
//
//                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventInOut.get(i).getGps_Date());
//
//                                    Geozone geozone = geozoneService.findById(listEventInOut.get(i).getGeozone_id());
//                                    body[i][columnCountnnn++] = geozone.getType();
//                                    body[i][columnCountnnn++] = geozone.getName();
//                                    body[i][columnCountnnn++] = String.valueOf(listEventInOut.get(i).getSpedd());
//                                }
//
//                            }
//                            int j = 0;
//                            if (report.getTypeReport().equals(Util.STOPPED_REPORT)) {
//                                int iv2 = 0;
//
//                                for (int i = 0; i < listEventStoppedCistern.size(); i++) {
//                                    int columnCountnnn = 9;
//                                    if (j < listEventStoppedCistern.size()) {
//
//                                        body[i][columnCountnnn++] = String.valueOf(Util.convertDateUtcGmtPeru(listEventStoppedCistern.get(j).getCreation_Date()));
//                                        body[i][columnCountnnn++] = String.valueOf(listEventStoppedCistern.get(j).getLatitude());
//                                        body[i][columnCountnnn++] = String.valueOf(listEventStoppedCistern.get(j).getLongitude());
//                                        if (listEventStoppedCistern.get(j).getType().equals("STOPPED")) {
//                                            if (j + 1 < listEventStoppedCistern.size()) {
//                                                if (listEventStoppedCistern.get(j).getType().equals("STOPPED") && listEventStoppedCistern.get(j + 1).getType().equals("MOVEMENT")) {
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                }
//                                                if (listEventStoppedCistern.get(j).getType().equals("STOPPED") && listEventStoppedCistern.get(j + 1).getType().equals("STOPPED")) {
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                }
//                                            } else {
//                                                body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                body[i][columnCountnnn++] = "null";
//                                                body[i][columnCountnnn++] = "null";
//                                            }
//
//                                        }
//                                        if (listEventStoppedCistern.get(j).getType().equals("MOVEMENT")) {
//                                            if (j + 1 < listEventStoppedCistern.size()) {
//                                                if (listEventStoppedCistern.get(j).getType().equals("MOVEMENT") && listEventStoppedCistern.get(j + 1).getType().equals("STOPPED")) {
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j + 1).getGps_Date());
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                    Date gpsdate = listEventStoppedCistern.get(j + 1).getGps_Date();
//                                                    Date recievedate = listEventStoppedCistern.get(j).getGps_Date();
//                                                    double timestap = recievedate.getTime() - gpsdate.getTime();
//
//                                                    String formatDetenido = formatDuration(Double.valueOf(timestap).longValue());
//                                                    body[i][columnCountnnn++] = formatDetenido;
//                                                    iv2++;
//                                                }
//                                                if (listEventStoppedCistern.get(j).getType().equals("MOVEMENT") && listEventStoppedCistern.get(j + 1).getType().equals("MOVEMENT")) {
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                    body[i][columnCountnnn++] = "null";
//                                                }
//                                            } else {
//                                                body[i][columnCountnnn++] = "null";
//                                                body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                body[i][columnCountnnn++] = "null";
//                                            }
//
//                                        }
//                                        if (iv2 > 0) {
//                                            j += 2;
//                                            iv2 = 0;
//                                        } else {
//                                            j++;
//                                        }
//
//
//                                    }
//                                    body[i][columnCountnnn++] = String.valueOf(listEventStoppedCistern.get(i).getSpedd());
//                                }
//                            }
//                            if (report.getTypeReport().equals(Util.STOPPED_REPORTFOUR)) {
//                                int iv2 = 0;
//
//                                for (int i = 0; i < listEventStoppedCistern.size(); i++) {
//                                    int columnCountnnn = 9;
//                                    if (j < listEventStoppedCistern.size()) {
//
//                                        body[i][columnCountnnn++] = String.valueOf(Util.convertDateUtcGmtPeru(listEventStoppedCistern.get(j).getCreation_Date()));
//                                        body[i][columnCountnnn++] = String.valueOf(listEventStoppedCistern.get(j).getLatitude());
//                                        body[i][columnCountnnn++] = String.valueOf(listEventStoppedCistern.get(j).getLongitude());
//
//                                        if (listEventStoppedCistern.get(j).getType().equals("STOPPEDFOUR")) {
//                                            if (j + 1 < listEventStoppedCistern.size()) {
//                                                if (listEventStoppedCistern.get(j).getType().equals("STOPPEDFOUR") && listEventStoppedCistern.get(j + 1).getType().equals("MOVEMENTFOUR")) {
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                }
//                                                if (listEventStoppedCistern.get(j).getType().equals("STOPPEDFOUR") && listEventStoppedCistern.get(j + 1).getType().equals("STOPPEDFOUR")) {
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                }
//                                            } else {
//                                                body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                body[i][columnCountnnn++] ="null";
//                                                body[i][columnCountnnn++] = "null";
//                                            }
//
//                                        }
//                                        if (listEventStoppedCistern.get(j).getType().equals("MOVEMENTFOUR")) {
//                                            if (j + 1 < listEventStoppedCistern.size()) {
//                                                if (listEventStoppedCistern.get(j).getType().equals("MOVEMENTFOUR") && listEventStoppedCistern.get(j + 1).getType().equals("STOPPEDFOUR")) {
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j + 1).getGps_Date());
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                    Date gpsdate = listEventStoppedCistern.get(j + 1).getGps_Date();
//                                                    Date recievedate = listEventStoppedCistern.get(j).getGps_Date();
//                                                    double timestap = recievedate.getTime() - gpsdate.getTime();
//
//                                                    String formatDetenido = formatDuration(Double.valueOf(timestap).longValue());
//                                                    body[i][columnCountnnn++] = formatDetenido;
//                                                    iv2++;
//                                                }
//                                                if (listEventStoppedCistern.get(j).getType().equals("MOVEMENTFOUR") && listEventStoppedCistern.get(j + 1).getType().equals("MOVEMENTFOUR")) {
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                    body[i][columnCountnnn++] ="null";
//                                                }
//                                            } else {
//                                                body[i][columnCountnnn++] = "null";
//                                                body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventStoppedCistern.get(j).getGps_Date());
//                                                body[i][columnCountnnn++] = "null";
//                                            }
//
//                                        }
//                                        if (iv2 > 0) {
//                                            j += 2;
//                                            iv2 = 0;
//                                        } else {
//                                            j++;
//                                        }
//
//
//                                    }
//                                    body[i][columnCountnnn++] = String.valueOf(listEventStoppedCistern.get(i).getSpedd());
//                                }
//                            }
//                            if (report.getTypeReport().equals(Util.TRAVEL_REPORT)) {
//                                int iv2 = 0;
//
//                                for (int i = 0; i < listEventTravels.size(); i++) {
//                                    int columnCountnnn = 9;
//                                    if (j < listEventTravels.size()) {
//
//                                        if (listEventTravels.get(j).getType().equals("ENDIN")) {
//                                            if (j + 1 < listEventTravels.size()) {
//                                                if (listEventTravels.get(j).getType().equals("ENDIN") && listEventTravels.get(j + 1).getType().equals("STARTOUT")) {
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j).getId_Travel();
//                                                    body[i][columnCountnnn++] =listEventTravels.get(j).getName_Travel();
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j).getLatitude() + "," + listEventTravels.get(j).getLongitude();
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventTravels.get(j).getGps_Date());
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = String.valueOf(listEventTravels.get(j).getName_Travel());
//
//                                                }
//                                                if (listEventTravels.get(j).getType().equals("ENDIN") && listEventTravels.get(j + 1).getType().equals("ENDIN")) {
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j).getId_Travel();
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j).getName_Travel();
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j).getLatitude() + "," + listEventTravels.get(j).getLongitude();
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventTravels.get(j).getGps_Date());
//                                                    body[i][columnCountnnn++] ="null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] ="null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                }
//                                            } else {
//                                                body[i][columnCountnnn++] =listEventTravels.get(j).getId_Travel();
//                                                body[i][columnCountnnn++] =listEventTravels.get(j).getName_Travel();
//                                                body[i][columnCountnnn++] = listEventTravels.get(j).getLatitude() + "," + listEventTravels.get(j).getLongitude();
//                                                body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventTravels.get(j).getGps_Date());
//                                                body[i][columnCountnnn++] = "null";
//                                                body[i][columnCountnnn++] = "null";
//                                                body[i][columnCountnnn++] = "null";
//                                                body[i][columnCountnnn++] = "null";
//                                            }
//
//                                        }
//                                        if (listEventTravels.get(j).getType().equals("STARTOUT")) {
//                                            if (j + 1 < listEventTravels.size()) {
//                                                if (listEventTravels.get(j).getType().equals("STARTOUT") && listEventTravels.get(j + 1).getType().equals("ENDIN")) {
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j + 1).getId_Travel();
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j + 1).getName_Travel();
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j + 1).getLatitude() + "," + listEventTravels.get(j + 1).getLongitude();
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventTravels.get(j + 1).getGps_Date());
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j).getLatitude() + "," + listEventTravels.get(j).getLongitude();
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventTravels.get(j).getGps_Date());
//
//                                                    Date gpsdate = listEventTravels.get(j + 1).getGps_Date();
//                                                    Date recievedate = listEventTravels.get(j).getGps_Date();
//                                                    double timestap = recievedate.getTime() - gpsdate.getTime();
//
//                                                    String formatDetenido = formatDuration(Double.valueOf(timestap).longValue());
//                                                    body[i][columnCountnnn++] = formatDetenido;
//
//                                                    double kmdistance = com.backend.platin.util.Util.calculateDistance(listEventTravels.get(j + 1).getLatitude(), listEventTravels.get(j + 1).getLongitude(),
//                                                            listEventTravels.get(j).getLatitude(), listEventTravels.get(j).getLongitude());
//                                                    body[i][columnCountnnn++] = String.valueOf(kmdistance);
//                                                    iv2++;
//                                                }
//                                                if (listEventTravels.get(j).getType().equals("STARTOUT") && listEventTravels.get(j + 1).getType().equals("STARTOUT")) {
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j).getId_Travel();
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j).getName_Travel();
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = listEventTravels.get(j).getLatitude() + "," + listEventTravels.get(j).getLongitude();
//                                                    body[i][columnCountnnn++] = Util.convertDateUtcGmtPeruString(listEventTravels.get(j).getGps_Date());
//                                                    body[i][columnCountnnn++] = "null";
//                                                    body[i][columnCountnnn++] = "null";
//                                                }
//                                            } else {
//                                                body[i][columnCountnnn++] = listEventTravels.get(j).getId_Travel();
//                                                body[i][columnCountnnn++] =listEventTravels.get(j).getName_Travel();
//                                                body[i][columnCountnnn++] = "null";
//                                                body[i][columnCountnnn++] = "null";
//                                                body[i][columnCountnnn++] = listEventTravels.get(j).getLatitude() + "," + listEventTravels.get(j).getLongitude();
//                                                body[i][columnCountnnn++] = String.valueOf(Util.convertDateUtcGmtPeruString(listEventTravels.get(j).getGps_Date()));
//                                                body[i][columnCountnnn++] = String.valueOf("null");
//                                                body[i][columnCountnnn++] = String.valueOf("null");
//                                            }
//
//                                        }
//                                        if (iv2 > 0) {
//                                            j += 2;
//                                            iv2 = 0;
//                                        } else {
//                                            j++;
//                                        }
//
//
//                                    }
//                                }
//                            }
//                            geoJsonDto.setBody(body);
//                            String jsonTramas = new Gson().toJson(geoJsonDto);
////                            if (report.getTypeReport().equals(Util.GLOBAL_REPORT)) {
////                                geoJsonDto = this.getGeoJsonGlobalReport(listEvent, tramas, report.getTypeReport());
////                                jsonTramas = new Gson().toJson(geoJsonDto);
////                            }
//
//                            //Generating GeoJson
//                            com.backend.platin.util.Util.generateDirIfDoesntExist(geojsonPath);
//                            Util.generateGeoJsonReportFile(uuid, geojsonPath, jsonTramas);
//                            log.warn("geojson generado");


//                        if(report.getTypeUrl().equals("xls"))
//                            redisService.changeStatusReport(keys, report, xlsUrlMap, Util.COMPLETED_STATUS, "", timeElapsed.getSeconds(), report.getPlate());
//                        else
//                            redisService.changeStatusReport(keys, report, "", Util.COMPLETED_STATUS, geoUrlMap, timeElapsed.getSeconds(), report.getPlate());

                        log.warn("Consumo de mensajes REPORTS_CHANNEL específicos: " + nsqMessage.getBody());
                        message.finished();
                        //Create a new row in Report Table in Database
                        ReportInDto reportObject = new ReportInDto(geoUrlMap, "", report.getTypeReport(), Util.COMPLETED_STATUS, report.getStartDate(),
                                report.getEndDate(), true, report.getCompany_id());
                        reportService.create(reportObject);

                        return;
//                        }

                    } else {
                        tried++;
                    }

                } catch (DataNotFoundException ex) {
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.warn("DataNotFoundException Worker ReportChannel:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error 400"));
                    ex.printStackTrace();
                    stackMessage = ex.getMessage();

                    ReportResponseDto reportResponse = new ReportResponseDto();
                    reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_ERROR + ": " + stackMessage);
                    reportResponse.setUser_id(report.getUser_id());
                    this.sendReport(reportResponse);

                    redisService.changeStatusReport(keys, report, " ", Util.FAILED_STATUS, " ", 0, report.getPlate());

                    message.finished();
                    return;

                } catch (IOException ex) {

                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.warn("IOException Worker ReportChannel:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
                    ex.printStackTrace();
                    stackMessage = ex.getMessage();

                    redisService.changeStatusReport(keys, report, " ", Util.FAILED_STATUS, " ", 0, report.getPlate());

                    ReportResponseDto reportResponse = new ReportResponseDto();
                    reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_ERROR + ": " + stackMessage);
                    reportResponse.setUser_id(report.getUser_id());

                    this.sendReport(reportResponse);

                    message.finished();
                    return;
                } catch (DataException ex) {
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.warn("DataException Worker ReportChannel:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
                    ex.printStackTrace();
                    stackMessage = ex.getMessage();

                    ReportResponseDto reportResponse = new ReportResponseDto();
                    reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_ERROR + ": " + stackMessage);
                    reportResponse.setUser_id(report.getUser_id());
                    this.sendReport(reportResponse);

                    redisService.changeStatusReport(keys, report, " ", Util.FAILED_STATUS, " ", 0, report.getPlate());
                    message.finished();
                    return;

                } catch (ParseException ex) {
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.warn("ParseException Worker ReportChannel:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
                    ex.printStackTrace();
                    stackMessage = ex.getMessage();

                    ReportResponseDto reportResponse = new ReportResponseDto();
                    reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_ERROR + ": " + stackMessage);
                    reportResponse.setUser_id(report.getUser_id());
                    this.sendReport(reportResponse);

                    redisService.changeStatusReport(keys, report, " ", Util.FAILED_STATUS, " ", 0, report.getPlate());
                    message.finished();
                    return;

                } catch (Exception ex) {
                    tried++;
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.warn("Error Worker ReportChannel:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
                    ex.printStackTrace();
                    stackMessage=ex.getMessage();
                    message.requeue();
                    return;
                }
            }
        });

        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By REPORTS_CHANNEL El consumidor mqConsumerByReportsChannel comenzó con éxito!");
    }



    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private String formatDuration(long duration) {
        long hours = TimeUnit.MILLISECONDS.toHours(duration);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) % 60;
//        long milliseconds = duration % 1000;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public void sendReport(ReportResponseDto responseReport) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String requestJson = new Gson().toJson(responseReport);

        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
        RestTemplate restTemplate = restTemplate();
        restTemplate.postForObject(uri + "/" + tokenInternal, entity, String.class);
    }

//    private void getExcelGlobalReport(List<Event> listEvent, int rowCount, List<ITramaDto> tramas, Row row, CellStyle style) throws ParseException, DataNotFoundException {
//
//        rowCount = rowCount + 3;
//        for (ITramaDto tramaObject : tramas) {
//            int columnCount = 0;
//            row = sheet.createRow(rowCount);
//
//            PositionDto position = new Gson().fromJson(tramaObject.getPosition(), PositionDto.class);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(position.getLatitude()), style, sheet, creationHelper);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(position.getLongitude()), style, sheet, creationHelper);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(position.getAltitude()), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, String.valueOf(tramaObject.getSpeed()), style, sheet, creationHelper);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertStringUtcGmtPeruString(tramaObject.getGps_Date()), style, sheet, creationHelper);
//            rowCount++;
//
//        }
//
//        rowCount = rowCount - tramas.size();
//
//        int length = 0;
//        if (listEvent.size() > tramas.size()) {
//            length = tramas.size();
//        } else {
//            length = listEvent.size();
//        }
//        int counter = 0;
//
//        if (listEvent.size() > 0) {
//            int i = 0;
//            length = counter + length;
//            if (listEvent.size() < tramas.size()) {
//                while (counter < length) {
//
//                    int columnCount = 5;
//                    row = sheet.getRow(rowCount);
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, this.translateTypeEventToSpanish(listEvent.get(i).getType()), style, sheet, creationHelper);
//
//                    if (listEvent.get(i).getGeozone_id() != null && !listEvent.get(i).getGeozone_id().equals("")
//                            && !listEvent.get(i).getGeozone_id().equals("null")) {
//
//                        Geozone geozone = geozoneService.findById(listEvent.get(i).getGeozone_id());
//
//                        if (geozone == null) {
//                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
//                        }
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, geozone.getName(), style, sheet, creationHelper);
//                    } else {
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, " ", style, sheet, creationHelper);
//                    }
//
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(listEvent.get(i).getCreationDate()), style, sheet, creationHelper);
//                    rowCount++;
//                    counter++;
//                    i++;
//                }
//            } else {
//                int a = 0;
//                for (a = 0; a < tramas.size(); a++) {
//
//                    int columnCount = 5;
//                    row = sheet.getRow(rowCount);
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, this.translateTypeEventToSpanish(listEvent.get(a).getType()), style, sheet, creationHelper);
//
//                    if (listEvent.get(a).getGeozone_id() != null && !listEvent.get(a).getGeozone_id().equals("")
//                            && !listEvent.get(a).getGeozone_id().equals("null")) {
//
//                        Geozone geozone = geozoneService.findById(listEvent.get(a).getGeozone_id());
//
//                        if (geozone == null) {
//                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
//                        }
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, geozone.getName(), style, sheet, creationHelper);
//                    } else {
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, " ", style, sheet, creationHelper);
//                    }
//
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(listEvent.get(a).getCreationDate()), style, sheet, creationHelper);
//                    rowCount++;
//
//                }
//                int substract = listEvent.size() - tramas.size();
//
//                for (int x = a + 1; x < substract; x++) {
//                    int columnCount = 5;
//                    row = sheet.createRow(rowCount);
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, this.translateTypeEventToSpanish(listEvent.get(x).getType()), style, sheet, creationHelper);
//
//                    if (listEvent.get(i).getGeozone_id() != null && !listEvent.get(i).getGeozone_id().equals("")
//                            && !listEvent.get(i).getGeozone_id().equals("null")) {
//
//                        Geozone geozone = geozoneService.findById(listEvent.get(i).getGeozone_id());
//
//                        if (geozone == null) {
//                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
//                        }
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, geozone.getName(), style, sheet, creationHelper);
//                    } else {
//                        com.backend.platin.util.Util.writeCell(row, columnCount++, " ", style, sheet, creationHelper);
//                    }
//
//                    com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(listEvent.get(i).getCreationDate()), style, sheet, creationHelper);
//                    rowCount++;
//                }
//
//            }
//        }
//
//    }

//    private GeoJsonDto getGeoJsonGlobalReport(List<Event> listEvent, List<ITramaDto> tramas, String typeReport) throws DataNotFoundException {
//
//        MetaGlobalDto meta = new MetaGlobalDto();
//        GeoJsonDto geoJsonDto = new GeoJsonDto();
//        geoJsonDto.setMeta(meta);
//
//        Util util = new Util();
//        geoJsonDto.setHeader(util.generateGeneralHeadersGeoJson(typeReport));
//
//        String[][] body = new String[tramas.size()][8];
//        int j = 0;
//        for (int i = 0; i < tramas.size(); i++) {
//            PositionDto position = new Gson().fromJson(tramas.get(i).getPosition(), PositionDto.class);
//            body[i][0] = String.valueOf(position.getLatitude());
//            body[i][1] = String.valueOf(position.getLongitude());
//            body[i][2] = String.valueOf(position.getAltitude());
//
//            body[i][3] = String.valueOf(tramas.get(i).getSpeed());
//            body[i][4] = String.valueOf(tramas.get(i).getGps_Date());
//            if (listEvent.size() > 0) {
//                //List of Events
//                if (j == listEvent.size()) {
//                    body[i][5] = null;
//                    body[i][6] = null;
//                    body[i][7] = null;
//
//                } else {
//                    body[i][5] = this.translateTypeEventToSpanish(listEvent.get(j).getType());
//
//                    if (listEvent.get(j).getGeozone_id() != null && !listEvent.get(j).getGeozone_id().equals("")
//                            && !listEvent.get(j).getGeozone_id().equals("null")) {
//
//                        Geozone geozone = geozoneService.findById(listEvent.get(j).getGeozone_id());
//                        if (geozone == null) {
//                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
//                        }
//                        body[i][6] = geozone.getName();
//                    } else {
//                        body[i][6] = " ";
//                    }
//
//                    body[i][7] = listEvent.get(j).getCreationDate().toString();
//                    j++;
//                }
//            }
//            geoJsonDto.setBody(body);
//        }
//        return geoJsonDto;
//
//    }

//    private String translateTypeEventToSpanish(String typeEvent) {
//        String type = null;
//        switch (typeEvent) {
//            case Util.TYPERECOVERYTRASMISSION:
//                type = "RECUPERACIÓN";
//                break;
//            case Util.TYPELOSTTRASMISSION:
//                type = "PÉRDIDA";
//                break;
//            case Util.TYPETIMELAG:
//                type = "DEFASE DE TIEMPO";
//                break;
//            case Util.TYPEMAXIMUNSPEED:
//                type = "EXCESO DE VELOCIDAD";
//                break;
//            case Util.TYPESTARTOUT:
//                type = "EMPIEZA VIAJE";
//                break;
//            case Util.TYPEENDIN:
//                type = "TERMINA VIAJE";
//                break;
//            case Util.TYPESTOPPED:
//                type = "DETENIDO";
//                break;
//            case Util.TYPESTOPPEDFOUR:
//                type = "DETENIDO POR 4 HORAS";
//                break;
//            case Util.TYPEMOVEMENT:
//                type = "EN MOVIMIENTO";
//                break;
//            case Util.TYPEMOVEMENTFOUR:
//                type = "EN MOVIMIENTO POR 4 HORAS";
//                break;
//            case Util.TYPEINCOMEREPORT:
//                type = "DENTRO DE LA GEOZONA";
//                break;
//            case Util.TYPEEXITREPORT:
//                type = "FUERA DE LA GEOZONA";
//                break;
//        }
//
//        return type;
//    }


    private void sendReport(String uuid, String header, String body) {
        String xlsUrlMap = uuid + ".xls";
        String geoUrlMap = uuid + ".geojson";
        String[] urls = new String[2];

        urls[0] = geoUrlMap;
        urls[1] = xlsUrlMap;

        ReportResponseDto reportResponse = new ReportResponseDto();
        reportResponse.setUrls(urls);
        reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_OK);
        reportResponse.setHeaders(header);
        reportResponse.setBody(body);
        reportResponse.setUser_id(user_id);

        this.sendReport(reportResponse);

    }

//    private void generateGlobalReport(NSQMessage message, CellStyle style, XSSFWorkbook workbook) {
//
//        try {
//            Row currentRow = sheet.getRow(rowCount);
//            com.backend.platin.util.Util.writeCell(currentRow, 1, " ", style, sheet, creationHelper);
//
//            rowCount++;
//            currentRow = sheet.getRow(rowCount);
//            com.backend.platin.util.Util.writeCell(currentRow, 1, " ", style, sheet, creationHelper);
//
//            Vehicle vehicle = vehicleService.findByPlate(report.getPlate());
//            //listEvent = eventService.findByPlateAndCreationDateBetween(report.getPlate(), report.getStartDate(), report.getEndDate());
//           // this.getExcelGlobalReport(listEvent, rowCount, tramas, currentRow, style);
//            com.backend.platin.util.Util.generateExcelReportFile(workbook, uuid, xlsPath);
//
//           // GeoJsonDto geoJsonDto = this.getGeoJsonGlobalReport(listEvent, tramas, topic);
//           // String jsonTramas = new Gson().toJson(geoJsonDto);
//           // Util.generateGeoJsonReportFile(uuid, geojsonPath, jsonTramas);
//
//        } catch (Exception ex) {
//            iErrorRepository.save(new ErrorLogs("Worker ReportChannel:" + ex.getMessage(), "Error"));
//            tried++;
//
//            java.util.logging.Logger.getLogger(MqConsumerByChannelServiceImplReportsChannelv2.class.getName()).log(Level.SEVERE, null, ex);
//            message.requeue();
//            return;
//        }
//
//    }

//    private void generateHeadersVehiclesExcelReport(List<Trama> tramas,IVehicleCompanyReportDto vehicleCompany, String geoUrlMap, CellStyle style) throws ParseException, DataNotFoundException {
//        rowCount = 1;
//        for (Trama trama : tramas) {
//            int columnCount = 0;
//            Row row = sheet.getRow(rowCount);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, trama.getPlate(), style, sheet, creationHelper);
//            com.backend.platin.util.Util.writeCell(row, columnCount++, vehicleCompany.getNameemv(), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, vehicleCompany.getNameclient(), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, vehicleCompany.getcode_osinergmin(), style, sheet, creationHelper);
//
//            String vehicle_status = vehicleCompany.getStatus() ? "Habilitado" : "Inhabilitado";
//            com.backend.platin.util.Util.writeCell(row, columnCount++, vehicle_status, style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, geoUrlMap, style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(new Date()), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(report.getStartDate()), style, sheet, creationHelper);
//
//            com.backend.platin.util.Util.writeCell(row, columnCount++, Util.convertDateUtcGmtPeru(report.getEndDate()), style, sheet, creationHelper);
//        }
//
//    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//
//
//    private void generateSpeedExcelReport(String uuid, String[] excelHeader, Object[] exceldata,
//                                          String[] excelHeaderR, String xlsPath, List<EventSpeedDto> even, XSSFSheet sheet,
//                                          XSSFWorkbook wb, CellStyle style) throws FileNotFoundException, IOException, DataNotFoundException, ParseException {
//
//        int rowCount = 0;
//        for (rowCount = 0; rowCount < excelHeader.length; rowCount++) {
//            Row row = sheet.createRow(rowCount + 1);
//            XSSFFont headerFont = wb.createFont();
//            headerFont.setBold(true);
//            headerFont.setFontHeightInPoints((short) 14);
//            style.setFont(headerFont);
//            Cell cell = row.createCell(0);
//            cell.setCellValue(excelHeader[rowCount]);
//            cell.setCellStyle(style);
//            sheet.autoSizeColumn(0);
//            sheet.setColumnWidth(0, 500 * 20 + 1000);
//            Cell cell2 = row.createCell(1);
//            cell2.setCellValue(exceldata[rowCount].toString());
//            cell2.setCellStyle(style);
//            sheet.autoSizeColumn(1);
//            sheet.setColumnWidth(1, 500 * 50 + 1000);
//            sheet.autoSizeColumn(2);
//            sheet.setColumnWidth(2, 500 * 20 + 1000);
//            sheet.autoSizeColumn(3);
//            sheet.setColumnWidth(3, 500 * 20 + 1000);
//            sheet.autoSizeColumn(4);
//            sheet.setColumnWidth(4, 500 * 20 + 1000);
//            sheet.autoSizeColumn(5);
//            sheet.setColumnWidth(5, 500 * 20 + 1000);
//            sheet.autoSizeColumn(6);
//            sheet.setColumnWidth(6, 500 * 20 + 1000);
//            sheet.autoSizeColumn(7);
//            sheet.setColumnWidth(7, 500 * 20 + 1000);
//        }
//
//        this.generateBodySPEEDREPORT(sheet, rowCount, excelHeaderR, even, style, wb);
//
//        Util.generateDirIfDoesntExist(xlsPath);
//        Util.generateExcelReportFile(wb, uuid, xlsPath);
//
//    }
//
//    private void generateBodySPEEDREPORT(XSSFSheet sheet, int rowCount,
//                                         String[] excelHeaderR, List<EventSpeedDto> listSpeedevent, CellStyle style, XSSFWorkbook wb) {
//
//        rowCount = rowCount + 2;
//        Row rowe = sheet.createRow(rowCount++);
//        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelHeaderR, style);
//        for (int i = 0; i < listSpeedevent.size(); i++) {
//            rowe = sheet.createRow(rowCount);
//            EventSpeedDto event = listSpeedevent.get(i);
//            int odometer = event.getOdometer() != null ? event.getOdometer() : 0;
//            Util.generateBodyType(wb, rowe, Util.convertDateUtcGmtPeruString(event.getGps_Date()),
//                    String.valueOf(event.getLatitude()), String.valueOf(event.getLongitude()), String.valueOf(event.getSpedd()), String.valueOf(odometer), null, null, null, null, null, Util.SPEED_REPORT);
//            rowCount++;
//        }
//
//    }
//
//    private void generateTravelExcelReport(String uuid, String[] excelHeader, Object[] exceldata,
//                                           String[] excelHeaderR, String xlsPath, List<EventTravelDto> events, XSSFSheet sheet,
//                                           XSSFWorkbook wb, CellStyle style, CreationHelper travelReportHelper) throws FileNotFoundException, IOException, DataNotFoundException, ParseException {
//        int rowCount = 0;
//        for (rowCount = 0; rowCount < excelHeader.length; rowCount++) {
//            Row row = sheet.createRow(rowCount + 1);
//            XSSFFont headerFont = wb.createFont();
//            headerFont.setBold(true);
//            headerFont.setFontHeightInPoints((short) 14);
//            style.setFont(headerFont);
//            travelReportHelper = wb.getCreationHelper();
//            Cell cell = row.createCell(0);
//            cell.setCellValue(excelHeader[rowCount]);
//            cell.setCellStyle(style);
//            sheet.autoSizeColumn(0);
//            sheet.setColumnWidth(0, 500 * 20 + 1000);
//            Cell cell2 = row.createCell(1);
//            cell2.setCellValue(exceldata[rowCount].toString());
//            cell2.setCellStyle(style);
//            sheet.autoSizeColumn(1);
//            sheet.setColumnWidth(1, 500 * 50 + 1000);
//            sheet.autoSizeColumn(2);
//            sheet.setColumnWidth(2, 500 * 20 + 1000);
//            sheet.autoSizeColumn(3);
//            sheet.setColumnWidth(3, 500 * 20 + 1000);
//            sheet.autoSizeColumn(4);
//            sheet.setColumnWidth(4, 500 * 20 + 1000);
//            sheet.autoSizeColumn(5);
//            sheet.setColumnWidth(5, 500 * 20 + 1000);
//            sheet.autoSizeColumn(6);
//            sheet.setColumnWidth(6, 500 * 20 + 1000);
//            sheet.autoSizeColumn(7);
//            sheet.setColumnWidth(7, 500 * 20 + 1000);
//        }
//
//        nvecesCantidadViajes = this.generateBodyTRAVELREPORT(sheet, rowCount, excelHeaderR, events, style, wb);
//        Row row = sheet.createRow(10);
//        Cell cell = row.createCell(0);
//        cell.setCellValue("Cantidad de Viajes");
//        cell.setCellStyle(style);
//        Cell cell2 = row.createCell(1);
//        cell2.setCellValue(nvecesCantidadViajes);
//        cell2.setCellStyle(style);
//        nvecesCantidadViajes = 0;
//
//        Util.generateDirIfDoesntExist(xlsPath);
//        Util.generateExcelReportFile(wb, uuid, xlsPath);
//
//    }
//
//
//    private int generateBodyTRAVELREPORT(XSSFSheet sheet, int rowCount, String[] excelHeaderR, List<EventTravelDto> even, CellStyle style, XSSFWorkbook wb) {
//        rowCount = rowCount + 3;
//        Row rowe = sheet.createRow(rowCount++);
//        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelHeaderR, style);
//        int rowv2 = 0;
//        int rowv3 = 0;
//        List<Object[]> listTravels = new ArrayList<>();
//        int nvecesCantidadViajes = 0;
//        for (int i = 0; i < even.size(); i++) {
//            rowe = sheet.createRow(rowCount);
//            if (rowv2 < even.size()) {
//                if (even.get(rowv2).getType().equals("ENDIN")) {
//                    if (rowv2 + 1 < even.size()) {
//                        if (even.get(rowv2).getType().equals("ENDIN") && even.get(rowv2 + 1).getType().equals("STARTOUT")) {
//                            int odometer = even.get(rowv2).getOdometer() != null ? even.get(rowv2).getOdometer() : 0;
//                            listTravels.add(new Object[]{even.get(rowv2).getId_Travel(), even.get(rowv2).getName_Travel(),
//                                    String.valueOf(even.get(rowv2).getLatitude() + "," + even.get(rowv2).getLongitude()),
//                                    Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", "null", "null", even.get(rowv2).getSpedd(), odometer});
//                        }
//                        if (even.get(rowv2).getType().equals("ENDIN") && even.get(rowv2 + 1).getType().equals("ENDIN")) {
//                            int odometer = even.get(rowv2).getOdometer() != null ? even.get(rowv2).getOdometer() : 0;
//                            listTravels.add(new Object[]{even.get(rowv2).getId_Travel(), even.get(rowv2).getName_Travel(),
//                                    String.valueOf(even.get(rowv2).getLatitude() + "," + even.get(rowv2).getLongitude()),
//                                    Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", "null", "null", even.get(rowv2).getSpedd(), odometer});
//                        }
//                    } else {
//                        int odometer = even.get(rowv2).getOdometer() != null ? even.get(rowv2).getOdometer() : 0;
//                        listTravels.add(new Object[]{even.get(rowv2).getId_Travel(), even.get(rowv2).getName_Travel(),
//                                String.valueOf(even.get(rowv2).getLatitude() + "," + even.get(rowv2).getLongitude()),
//                                Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", "null", "null", even.get(rowv2).getSpedd(), odometer});
//                    }
//                }
//                if (even.get(rowv2).getType().equals("STARTOUT")) {
//                    if (rowv2 + 1 < even.size()) {
//                        if (even.get(rowv2).getType().equals("STARTOUT") && even.get(rowv2 + 1).getType().equals("ENDIN")) {
//                            Date gpsdate = even.get(rowv2 + 1).getGps_Date();
//                            Date recievedate = even.get(rowv2).getGps_Date();
//                            double timestap = recievedate.getTime() - gpsdate.getTime();
//                            String formatDetenido = formatDuration(Double.valueOf(timestap).longValue());
//                            double kmdistance = Util.calculateDistance(even.get(rowv2 + 1).getLatitude(), even.get(rowv2 + 1).getLongitude(),
//                                    even.get(rowv2).getLatitude(), even.get(rowv2).getLongitude());
//                            nvecesCantidadViajes++;
//                            int odometer = even.get(rowv2).getOdometer() != null ? even.get(rowv2).getOdometer() : 0;
//                            listTravels.add(new Object[]{even.get(rowv2 + 1).getId_Travel(), even.get(rowv2 + 1).getName_Travel(),
//                                    String.valueOf(even.get(rowv2 + 1).getLatitude() + "," + even.get(rowv2 + 1).getLongitude()),
//                                    Util.convertDateUtcGmtPeruString(even.get(rowv2 + 1).getGps_Date()), String.valueOf(even.get(rowv2).getLatitude() + "," + even.get(rowv2).getLongitude())
//                                    , Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), formatDetenido, String.valueOf(kmdistance), even.get(rowv2).getSpedd(), odometer});
//                            rowv3++;
//                        }
//                        if (even.get(rowv2).getType().equals("STARTOUT") && even.get(rowv2 + 1).getType().equals("STARTOUT")) {
//                            int odometer = even.get(rowv2).getOdometer() != null ? even.get(rowv2).getOdometer() : 0;
//                            listTravels.add(new Object[]{even.get(rowv2).getId_Travel(), even.get(rowv2).getName_Travel(),
//                                    "null", "null", String.valueOf(even.get(rowv2).getLatitude() + "," + even.get(rowv2).getLongitude()),
//                                    Util.convertDateUtcGmtPeruString(even.get(rowv2 + 1).getGps_Date()), "null", "null", even.get(rowv2).getSpedd(), odometer});
//                        }
//                    } else {
//                        int odometer = even.get(rowv2).getOdometer() != null ? even.get(rowv2).getOdometer() : 0;
//                        listTravels.add(new Object[]{even.get(rowv2).getId_Travel(), even.get(rowv2).getName_Travel(),
//                                "null", "null", String.valueOf(even.get(rowv2).getLatitude() + "," + even.get(rowv2).getLongitude()),
//                                Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", even.get(rowv2).getSpedd(), odometer});
//
//                    }
//                }
//            } else {
//                break;
//            }
//            if (rowv3 > 0) {
//                rowv2 += 2;
//                rowv3 = 0;
//            } else {
//                rowv2++;
//            }
//            int l = 0;
//            Util.generateBodyType(wb, rowe, listTravels.get(l)[0].toString(), listTravels.get(l)[1].toString(), listTravels.get(l)[2].toString(),
//                    listTravels.get(l)[3].toString(), listTravels.get(l)[4].toString(), listTravels.get(l)[5].toString(),
//                    listTravels.get(l)[6].toString(), listTravels.get(l)[7].toString(), listTravels.get(l)[8].toString(), listTravels.get(l)[9].toString(), Util.TRAVEL_REPORT);
//            listTravels.clear();
//            rowCount++;
//        }
//        return nvecesCantidadViajes;
//    }
//
//
//    private void generateStoppedExcelReport(String uuid, String[] excelHeader, Object[] exceldata,
//                                            String[] excelHeaderR, String xlsPath, List<EventStoppedDto> events, XSSFSheet sheet,
//                                            XSSFWorkbook wb, CellStyle style, CreationHelper excelReportHelper) throws FileNotFoundException, IOException, DataNotFoundException, ParseException {
//        int rowCount = 0;
//        for (rowCount = 0; rowCount < excelHeader.length; rowCount++) {
//            Row row = sheet.createRow(rowCount + 1);
//            XSSFFont headerFont = wb.createFont();
//            headerFont.setBold(true);
//            headerFont.setFontHeightInPoints((short) 14);
//            style.setFont(headerFont);
//            excelReportHelper = wb.getCreationHelper();
//            Cell cell = row.createCell(0);
//            cell.setCellValue(excelHeader[rowCount]);
//            cell.setCellStyle(style);
//            sheet.autoSizeColumn(0);
//            sheet.setColumnWidth(0, 500 * 20 + 1000);
//            Cell cell2 = row.createCell(1);
//            cell2.setCellValue(exceldata[rowCount].toString());
//            cell2.setCellStyle(style);
//            sheet.autoSizeColumn(1);
//            sheet.setColumnWidth(1, 500 * 50 + 1000);
//            sheet.autoSizeColumn(2);
//            sheet.setColumnWidth(2, 500 * 20 + 1000);
//            sheet.autoSizeColumn(3);
//            sheet.setColumnWidth(3, 500 * 20 + 1000);
//            sheet.autoSizeColumn(4);
//            sheet.setColumnWidth(4, 500 * 20 + 1000);
//            sheet.autoSizeColumn(5);
//            sheet.setColumnWidth(5, 500 * 20 + 1000);
//            sheet.autoSizeColumn(6);
//            sheet.setColumnWidth(6, 500 * 20 + 1000);
//            sheet.autoSizeColumn(7);
//            sheet.setColumnWidth(7, 500 * 20 + 1000);
//        }
//
//        nvecesStopped = this.generateBodySTOPPEDCISTERN(sheet, rowCount, excelHeaderR, events, style, wb);
//        Row row = sheet.createRow(10);
//        Cell cell = row.createCell(0);
//        cell.setCellValue("Cantidad de detenciones");
//        cell.setCellStyle(style);
//        Cell cell2 = row.createCell(1);
//        cell2.setCellValue(nvecesStopped);
//        cell2.setCellStyle(style);
//        nvecesStopped = 0;
//        Util.generateDirIfDoesntExist(xlsPath);
//        Util.generateExcelReportFile(wb, uuid, xlsPath);
//
//    }
//
//    private int generateBodySTOPPEDCISTERN(XSSFSheet sheet, int rowCount, String[] excelHeaderR, List<EventStoppedDto> even, CellStyle style, XSSFWorkbook wb) {
//        rowCount = rowCount + 3;
//        Row rowe = sheet.createRow(rowCount++);
//        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelHeaderR, style);
//        int rowv2 = 0;
//        int rowv3 = 0;
//        List<Object[]> listStopped = new ArrayList<>();
//        List<Object[]> listStoppedData = new ArrayList<>();
//        int nvecesStopped = 0;
//        for (int i = 0; i < even.size(); i++) {
//            rowe = sheet.createRow(rowCount);
//            if (rowv2 < even.size()) {
//
//                int odometer = even.get(rowv2).getOdometer() != null ? even.get(rowv2).getOdometer() : 0;
//                listStopped.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getCreation_Date()), String.valueOf(even.get(rowv2).getLatitude()),
//                        String.valueOf(even.get(rowv2).getLongitude()), even.get(rowv2).getSpedd(), even.get(rowv2).getOdometer()});
//                if (even.get(rowv2).getType().equals("STOPPED")) {
//                    if (rowv2 + 1 < even.size()) {
//                        if (even.get(rowv2).getType().equals("STOPPED") && even.get(rowv2 + 1).getType().equals("MOVEMENT")) {
//
//                            listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", even.get(rowv2).getSpedd(), odometer});
//                        }
//                        if (even.get(rowv2).getType().equals("STOPPED") && even.get(rowv2 + 1).getType().equals("STOPPED")) {
//                            listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", even.get(rowv2).getSpedd(), odometer});
//                        }
//                    } else {
//                        listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", even.get(rowv2).getSpedd(), odometer});
//                    }
//                }
//                if (even.get(rowv2).getType().equals("MOVEMENT")) {
//
//                    if (rowv2 + 1 < even.size()) {
//
//                        if (even.get(rowv2).getType().equals("MOVEMENT") && even.get(rowv2 + 1).getType().equals("STOPPED")) {
//                            Date gpsdate = even.get(rowv2 + 1).getGps_Date();
//                            Date recievedate = even.get(rowv2).getGps_Date();
//                            double timestap = recievedate.getTime() - gpsdate.getTime();
//                            String formatDetenido = formatDuration(Double.valueOf(timestap).longValue());
//                            nvecesStopped++;
//                            rowv3++;
//                            listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2 + 1).getGps_Date()),
//                                    Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), formatDetenido, even.get(rowv2).getSpedd(), odometer});
//                        }
//                        if (even.get(rowv2).getType().equals("MOVEMENT") && even.get(rowv2 + 1).getType().equals("MOVEMENT")) {
//                            listStoppedData.add(new Object[]{"null", Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", even.get(rowv2).getSpedd(), odometer});
//                        }
//                    } else {
//                        listStoppedData.add(new Object[]{"null", Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", even.get(rowv2).getSpedd(), odometer});
//                    }
//                }
//            } else {
//                break;
//            }
//            if (rowv3 > 0) {
//                rowv2 += 2;
//                rowv3 = 0;
//            } else {
//                rowv2++;
//            }
//            int l = 0;
//            Util.generateBodyType(wb, rowe, listStopped.get(l)[0].toString(), listStopped.get(l)[1].toString(), listStopped.get(l)[2].toString(),
//                    listStoppedData.get(l)[0].toString(), listStoppedData.get(l)[1].toString(), listStoppedData.get(l)[2].toString(), listStoppedData.get(l)[3].toString(), listStoppedData.get(l)[4].toString(), null, null, Util.STOPPED_REPORT);
//            listStopped.clear();
//            listStoppedData.clear();
//            rowCount++;
//        }
//
//        return nvecesStopped;
//    }

    private List<StoppedListDto> generateBodySTOPPEDCISTERNList(List<EventStoppedDto> even,String type , String typeEnd) {
        int rowv2 = 0;
        int rowv3 = 0;
        List<Object[]> listStopped = new ArrayList<>();
        List<Object[]> listStoppedData = new ArrayList<>();
        List<StoppedListDto> listStoppedDataList = new ArrayList<>();
        int nvecesStopped = 0;
        for (int i = 0; i < even.size(); i++) {
            if (rowv2 < even.size()) {

                int odometer = even.get(rowv2).getOdometer() != null ? even.get(rowv2).getOdometer() : 0;
                listStopped.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getCreation_Date()), String.valueOf(even.get(rowv2).getLatitude()),
                        String.valueOf(even.get(rowv2).getLongitude()), even.get(rowv2).getSpedd(), even.get(rowv2).getOdometer()});
                if (even.get(rowv2).getType().equals(type)) {
                    if (rowv2 + 1 < even.size()) {
                        if (even.get(rowv2).getType().equals(type) && even.get(rowv2 + 1).getType().equals(typeEnd)) {

                            listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", even.get(rowv2).getSpedd(), odometer});
                        }
                        if (even.get(rowv2).getType().equals(type) && even.get(rowv2 + 1).getType().equals(type)) {
                            listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", even.get(rowv2).getSpedd(), odometer});
                        }
                    } else {
                        listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", even.get(rowv2).getSpedd(), odometer});
                    }
                }
                if (even.get(rowv2).getType().equals(typeEnd)) {

                    if (rowv2 + 1 < even.size()) {

                        if (even.get(rowv2).getType().equals(typeEnd) && even.get(rowv2 + 1).getType().equals(type)) {
                            Date gpsdate = even.get(rowv2 + 1).getGps_Date();
                            Date recievedate = even.get(rowv2).getGps_Date();
                            double timestap = recievedate.getTime() - gpsdate.getTime();
                            String formatDetenido = formatDuration(Double.valueOf(timestap).longValue());
                            nvecesStopped++;
                            rowv3++;
                            listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2 + 1).getGps_Date()),
                                    Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), formatDetenido, even.get(rowv2).getSpedd(), odometer});
                        }
                        if (even.get(rowv2).getType().equals(typeEnd) && even.get(rowv2 + 1).getType().equals(typeEnd)) {
                            listStoppedData.add(new Object[]{"null", Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", even.get(rowv2).getSpedd(), odometer});
                        }
                    } else {
                        listStoppedData.add(new Object[]{"null", Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", even.get(rowv2).getSpedd(), odometer});
                    }
                }
            } else {
                break;
            }
            if (rowv3 > 0) {
                rowv2 += 2;
                rowv3 = 0;
            } else {
                rowv2++;
            }
            int l = 0;
            listStoppedDataList.add(new StoppedListDto(listStopped.get(l)[0].toString(),
                    listStopped.get(l)[1].toString(), listStopped.get(l)[2].toString(),
                    listStoppedData.get(l)[0].toString(), listStoppedData.get(l)[1].toString(),
                    listStoppedData.get(l)[2].toString(), listStoppedData.get(l)[3].toString(),
                    listStoppedData.get(l)[4].toString(), String.valueOf(nvecesStopped)));
            listStopped.clear();
            listStoppedData.clear();
        }
        //listStoppedDataList.add(new StoppedListDto("null","null","null","null","null","null","null","null",String.valueOf(nvecesStopped)));
        return listStoppedDataList;
    }
//
//    private void generateStoppedFourExcelReport(String uuid, String[] excelHeader, Object[] exceldata,
//                                                String[] excelHeaderR, String xlsPath, List<EventStoppedDto> events, XSSFSheet sheet,
//                                                XSSFWorkbook wb, CellStyle style, CreationHelper excelReportHelper) throws FileNotFoundException, IOException, DataNotFoundException, ParseException {
//        int rowCount = 0;
//        for (rowCount = 0; rowCount < excelHeader.length; rowCount++) {
//            Row row = sheet.createRow(rowCount + 1);
//            XSSFFont headerFont = wb.createFont();
//            headerFont.setBold(true);
//            headerFont.setFontHeightInPoints((short) 14);
//            style.setFont(headerFont);
//            excelReportHelper = wb.getCreationHelper();
//            Cell cell = row.createCell(0);
//            cell.setCellValue(excelHeader[rowCount]);
//            cell.setCellStyle(style);
//            sheet.autoSizeColumn(0);
//            sheet.setColumnWidth(0, 500 * 20 + 1000);
//            Cell cell2 = row.createCell(1);
//            cell2.setCellValue(exceldata[rowCount].toString());
//            cell2.setCellStyle(style);
//            sheet.autoSizeColumn(1);
//            sheet.setColumnWidth(1, 500 * 50 + 1000);
//            sheet.autoSizeColumn(2);
//            sheet.setColumnWidth(2, 500 * 20 + 1000);
//            sheet.autoSizeColumn(3);
//            sheet.setColumnWidth(3, 500 * 20 + 1000);
//            sheet.autoSizeColumn(4);
//            sheet.setColumnWidth(4, 500 * 20 + 1000);
//            sheet.autoSizeColumn(5);
//            sheet.setColumnWidth(5, 500 * 20 + 1000);
//            sheet.autoSizeColumn(6);
//            sheet.setColumnWidth(6, 500 * 20 + 1000);
//            sheet.autoSizeColumn(7);
//            sheet.setColumnWidth(7, 500 * 20 + 1000);
//        }
//
//        nvecesStoppedFour = this.generateBodySTOPPEDCISTERNFOUR(sheet, rowCount, excelHeaderR, events, style, wb);
//        Row row = sheet.createRow(10);
//        Cell cell = row.createCell(0);
//        cell.setCellValue("Cantidad de detenciones");
//        cell.setCellStyle(style);
//        Cell cell2 = row.createCell(1);
//        cell2.setCellValue(nvecesStoppedFour);
//        cell2.setCellStyle(style);
//        nvecesStoppedFour = 0;
//        Util.generateDirIfDoesntExist(xlsPath);
//        Util.generateExcelReportFile(wb, uuid, xlsPath);
//    }
//
//    private int generateBodySTOPPEDCISTERNFOUR(XSSFSheet sheet, int rowCount, String[] excelHeaderR, List<EventStoppedDto> even, CellStyle style, XSSFWorkbook wb) {
//        rowCount = rowCount + 3;
//        Row rowe = sheet.createRow(rowCount++);
//        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelHeaderR, style);
//        int rowv2 = 0;
//        int rowv3 = 0;
//        List<Object[]> listStopped = new ArrayList<>();
//        List<Object[]> listStoppedData = new ArrayList<>();
//        nvecesStoppedFour = 0;
//        for (int i = 0; i < even.size(); i++) {
//            rowe = sheet.createRow(rowCount + i);
//            if (rowv2 < even.size()) {
//                int odometer = even.get(rowv2).getOdometer() != null ? even.get(rowv2).getOdometer() : 0;
//                listStopped.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getCreation_Date()), String.valueOf(even.get(rowv2).getLatitude())
//                        , String.valueOf(even.get(rowv2).getLongitude()), String.valueOf(even.get(rowv2).getSpedd()), odometer});
//                if (even.get(rowv2).getType().equals("STOPPEDFOUR")) {
//                    if (rowv2 + 1 < even.size()) {
//                        if (even.get(rowv2).getType().equals("STOPPEDFOUR") && even.get(rowv2 + 1).getType().equals("MOVEMENTFOUR")) {
//                            listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", String.valueOf(even.get(rowv2).getSpedd()), odometer});
//                        }
//                        if (even.get(rowv2).getType().equals("STOPPEDFOUR") && even.get(rowv2 + 1).getType().equals("STOPPEDFOUR")) {
//                            listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", String.valueOf(even.get(rowv2).getSpedd()), odometer});
//                        }
//                    } else {
//                        listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", "null", String.valueOf(even.get(rowv2).getSpedd()), odometer});
//                    }
//                }
//                if (even.get(rowv2).getType().equals("MOVEMENTFOUR")) {
//                    if (rowv2 + 1 < even.size()) {
//                        if (even.get(rowv2).getType().equals("MOVEMENTFOUR") && even.get(rowv2 + 1).getType().equals("STOPPEDFOUR")) {
//                            Date gpsdate = even.get(rowv2 + 1).getGps_Date();
//                            Date recievedate = even.get(rowv2).getGps_Date();
//                            double timestap = recievedate.getTime() - gpsdate.getTime();
//                            String formatDetenido = formatDuration(Double.valueOf(timestap).longValue());
//                            nvecesStoppedFour++;
//                            rowv3++;
//                            listStoppedData.add(new Object[]{Util.convertDateUtcGmtPeruString(even.get(rowv2 + 1).getGps_Date()),
//                                    Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), formatDetenido, String.valueOf(even.get(rowv2).getSpedd()), odometer});
//                        }
//                        if (even.get(rowv2).getType().equals("MOVEMENTFOUR") && even.get(rowv2 + 1).getType().equals("MOVEMENTFOUR")) {
//                            listStoppedData.add(new Object[]{"null", Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", String.valueOf(even.get(rowv2).getSpedd()), odometer});
//                        }
//                    } else {
//                        listStoppedData.add(new Object[]{"null", Util.convertDateUtcGmtPeruString(even.get(rowv2).getGps_Date()), "null", String.valueOf(even.get(rowv2).getSpedd()), odometer});
//                    }
//                }
//            } else {
//                break;
//            }
//            if (rowv3 > 0) {
//                rowv2 += 2;
//                rowv3 = 0;
//            } else {
//                rowv2++;
//            }
//            int l = 0;
//            Util.generateBodyType(wb, rowe, listStopped.get(l)[0].toString(), listStopped.get(l)[1].toString(), listStopped.get(l)[2].toString(),
//                    listStoppedData.get(l)[0].toString(), listStoppedData.get(l)[1].toString(), listStoppedData.get(l)[2].toString(), listStoppedData.get(l)[3].toString(), listStoppedData.get(l)[4].toString(), null, null, Util.STOPPED_REPORTFOUR);
//            listStopped.clear();
//            listStoppedData.clear();
//            rowCount++;
//        }
//        return nvecesStoppedFour;
//    }
//
//
//    private void generateGeozoneExcelReport(String uuid, String[] excelHeader, Object[] exceldata,
//                                            String[] excelHeaderR, String xlsPath, List<EventGeozoneDto> events, XSSFSheet sheet,
//                                            XSSFWorkbook wb, CellStyle style, CreationHelper excelReportHelper) throws FileNotFoundException, IOException, DataNotFoundException, ParseException {
//        int rowCount = 0;
//        for (rowCount = 0; rowCount < excelHeader.length; rowCount++) {
//            Row row = sheet.createRow(rowCount + 1);
//            XSSFFont headerFont = wb.createFont();
//            headerFont.setBold(true);
//            headerFont.setFontHeightInPoints((short) 14);
//            style.setFont(headerFont);
//            excelReportHelper = wb.getCreationHelper();
//            Cell cell = row.createCell(0);
//            cell.setCellValue(excelHeader[rowCount]);
//            cell.setCellStyle(style);
//            sheet.autoSizeColumn(0);
//            sheet.setColumnWidth(0, 500 * 20 + 1000);
//            Cell cell2 = row.createCell(1);
//            cell2.setCellValue(exceldata[rowCount].toString());
//            cell2.setCellStyle(style);
//            sheet.autoSizeColumn(1);
//            sheet.setColumnWidth(1, 500 * 50 + 1000);
//            sheet.autoSizeColumn(2);
//            sheet.setColumnWidth(2, 500 * 20 + 1000);
//            sheet.autoSizeColumn(3);
//            sheet.setColumnWidth(3, 500 * 20 + 1000);
//            sheet.autoSizeColumn(4);
//            sheet.setColumnWidth(4, 500 * 20 + 1000);
//            sheet.autoSizeColumn(5);
//            sheet.setColumnWidth(5, 500 * 20 + 1000);
//            sheet.autoSizeColumn(6);
//            sheet.setColumnWidth(6, 500 * 20 + 1000);
//            sheet.autoSizeColumn(7);
//            sheet.setColumnWidth(7, 500 * 20 + 1000);
//        }
//
//        this.generateBodyGEOZONESREPORT(sheet, rowCount, excelHeaderR, events, style, wb);
//
//        Util.generateDirIfDoesntExist(xlsPath);
//        Util.generateExcelReportFile(wb, uuid, xlsPath);
//    }
//
//    private void generateBodyGEOZONESREPORT(XSSFSheet sheet, int rowCount,
//                                            String[] excelHeaderR, List<EventGeozoneDto> even, CellStyle style, XSSFWorkbook wb) throws DataNotFoundException {
//        rowCount = rowCount + 2;
//        Row rowe = sheet.createRow(rowCount++);
//        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelHeaderR, style);
//        for (int i = 0; i < even.size(); i++) {
//            rowe = sheet.createRow(rowCount);
//            EventGeozoneDto event = even.get(i);
//
//            Geozone geo = geozoneService.findById(event.getGeozone_id());
//            String tipo_evento = event.getType().equals(Util.TYPEINCOMEREPORT) ? "ENTRADA" : "SALIDA";
//            int odometer = event.getOdometer() != null ? event.getOdometer() : 0;
//            Util.generateBodyType(wb, rowe, Util.convertDateUtcGmtPeruString(event.getGps_Date()),
//                    tipo_evento, geo.getName(), String.valueOf(event.getSpedd()), String.valueOf(odometer)
//                    , null, null, null, null, null, Util.GEOZONES_REPORT);
//            rowCount++;
//        }
//
//    }
//
//
//    private void generateNoTransmissionExcelReport(String uuid, String[] excelHeader, Object[] exceldata,
//                                                   String[] excelHeaderR, String xlsPath, List<EventNoTransmissionDto> events, XSSFSheet sheet,
//                                                   XSSFWorkbook wb, CellStyle style, CreationHelper excelReportHelper) throws FileNotFoundException, IOException, DataNotFoundException, ParseException {
//        int rowCount = 0;
//        for (rowCount = 0; rowCount < excelHeader.length; rowCount++) {
//            Row row = sheet.createRow(rowCount + 1);
//            XSSFFont headerFont = wb.createFont();
//            headerFont.setBold(true);
//            headerFont.setFontHeightInPoints((short) 14);
//            style.setFont(headerFont);
//            excelReportHelper = wb.getCreationHelper();
//            Cell cell = row.createCell(0);
//            cell.setCellValue(excelHeader[rowCount]);
//            cell.setCellStyle(style);
//            sheet.autoSizeColumn(0);
//            sheet.setColumnWidth(0, 500 * 20 + 1000);
//            Cell cell2 = row.createCell(1);
//            cell2.setCellValue(exceldata[rowCount].toString());
//            cell2.setCellStyle(style);
//            sheet.autoSizeColumn(1);
//            sheet.setColumnWidth(1, 500 * 50 + 1000);
//            sheet.autoSizeColumn(2);
//            sheet.setColumnWidth(2, 500 * 20 + 1000);
//            sheet.autoSizeColumn(3);
//            sheet.setColumnWidth(3, 500 * 20 + 1000);
//            sheet.autoSizeColumn(4);
//            sheet.setColumnWidth(4, 500 * 20 + 1000);
//            sheet.autoSizeColumn(5);
//            sheet.setColumnWidth(5, 500 * 20 + 1000);
//            sheet.autoSizeColumn(6);
//            sheet.setColumnWidth(6, 500 * 20 + 1000);
//            sheet.autoSizeColumn(7);
//            sheet.setColumnWidth(7, 500 * 20 + 1000);
//        }
//
//        this.generateBodyNOTRASMISSIONREPORT(sheet, rowCount, excelHeaderR, events, style, wb);
//
//        Util.generateDirIfDoesntExist(xlsPath);
//        Util.generateExcelReportFile(wb, uuid, xlsPath);
//    }
//
//    private void generateBodyNOTRASMISSIONREPORT(XSSFSheet sheet, int rowCount,
//                                                 String[] excelHeaderR, List<EventNoTransmissionDto> even, CellStyle style, XSSFWorkbook wb) {
//
//        rowCount = rowCount + 2;
//        Row rowe = sheet.createRow(rowCount++);
//        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelHeaderR, style);
//        for (int i = 0; i < even.size(); i++) {
//            rowe = sheet.createRow(rowCount);
//            EventNoTransmissionDto event = even.get(i);
//            String tipo = event.getType().equals(Util.TYPERECOVERYTRASMISSION) ? "RECUPERACION" : "PERDIDA";
//            int odometer = event.getOdometer() != null ? event.getOdometer() : 0;
//            Util.generateBodyType(wb, rowe, Util.convertDateUtcGmtPeruString(event.getGps_Date()),
//                    tipo, String.valueOf(event.getLatitude()), String.valueOf(event.getLongitude()), String.valueOf(event.getSpedd()), String.valueOf(odometer), null, null, null, null, Util.NO_TRANSMISSION_REPORT);
//            rowCount++;
//        }
//
//    }
//
//    private void generateTimeLagExcelReport(String uuid, String[] excelHeader, Object[] exceldata,
//                                            String[] excelHeaderR, String xlsPath, List<EventTimeLagDto> events, XSSFSheet sheet,
//                                            XSSFWorkbook wb, CellStyle style, CreationHelper excelReportHelper) throws FileNotFoundException, IOException, DataNotFoundException, ParseException {
//        int rowCount = 0;
//        for (rowCount = 0; rowCount < excelHeader.length; rowCount++) {
//            Row row = sheet.createRow(rowCount + 1);
//            XSSFFont headerFont = wb.createFont();
//            headerFont.setBold(true);
//            headerFont.setFontHeightInPoints((short) 14);
//            style.setFont(headerFont);
//            excelReportHelper = wb.getCreationHelper();
//            Cell cell = row.createCell(0);
//            cell.setCellValue(excelHeader[rowCount]);
//            cell.setCellStyle(style);
//            sheet.autoSizeColumn(0);
//            sheet.setColumnWidth(0, 500 * 20 + 1000);
//            Cell cell2 = row.createCell(1);
//            cell2.setCellValue(exceldata[rowCount].toString());
//            cell2.setCellStyle(style);
//            sheet.autoSizeColumn(1);
//            sheet.setColumnWidth(1, 500 * 50 + 1000);
//            sheet.autoSizeColumn(2);
//            sheet.setColumnWidth(2, 500 * 20 + 1000);
//            sheet.autoSizeColumn(3);
//            sheet.setColumnWidth(3, 500 * 20 + 1000);
//            sheet.autoSizeColumn(4);
//            sheet.setColumnWidth(4, 500 * 20 + 1000);
//            sheet.autoSizeColumn(5);
//            sheet.setColumnWidth(5, 500 * 20 + 1000);
//            sheet.autoSizeColumn(6);
//            sheet.setColumnWidth(6, 500 * 20 + 1000);
//            sheet.autoSizeColumn(7);
//            sheet.setColumnWidth(7, 500 * 20 + 1000);
//        }
//
//        this.generateBodyTIMELAGREPORT(sheet, rowCount, excelHeaderR, events, style, wb);
//
//        Util.generateDirIfDoesntExist(xlsPath);
//        Util.generateExcelReportFile(wb, uuid, xlsPath);
//    }
//
//
//    private void generateBodyTIMELAGREPORT(XSSFSheet sheet, int rowCount,
//                                           String[] excelHeaderR, List<EventTimeLagDto> even, CellStyle style, XSSFWorkbook wb) {
//        rowCount = rowCount + 2;
//        Row rowe = sheet.createRow(rowCount++);
//        sheet = Util.generateHeaderBodyTypev2(rowe, sheet, excelHeaderR, style);
//        for (int i = 0; i < even.size(); i++) {
//            rowe = sheet.createRow(rowCount);
//            EventTimeLagDto event = even.get(i);
//            int odometer = event.getOdometer() != null ? event.getOdometer() : 0;
//            Util.generateBodyType(wb, rowe, Util.convertDateUtcGmtPeruString(event.getReceive_Date()),
//                    Util.convertDateUtcGmtPeruString(event.getGps_Date()), String.valueOf(event.getLatitude()), String.valueOf(event.getLongitude()), String.valueOf(event.getSpedd()), String.valueOf(odometer), null, null, null, null, Util.REPORTTIMELAG);
//            rowCount++;
//
//        }
//    }

//    private String translateTypeEventToSpanish(String typeEvent) {
//        String type = null;
//        switch (typeEvent) {
////            case Util.TYPERECOVERYTRASMISSION:
////                type = "RECUPERACIÓN";
////                break;
////            case Util.TYPELOSTTRASMISSION:
////                type = "PÉRDIDA";
////                break;
////            case Util.TYPETIMELAG:
////                type = "DEFASE DE TIEMPO";
////                break;
////            case Util.TYPEMAXIMUNSPEED:
////                type = "EXCESO DE VELOCIDAD";
////                break;
////            case Util.TYPESTARTOUT:
////                type = "EMPIEZA VIAJE";
////                break;
////            case Util.TYPEENDIN:
////                type = "TERMINA VIAJE";
////                break;
//            case Util.TYPESTOPPED:
//                type = "DETENIDO";
//                break;
////            case Util.TYPESTOPPEDFOUR:
////                type = "DETENIDO POR 4 HORAS";
////                break;
//            case Util.TYPEMOVEMENT:
//                type = "EN MOVIMIENTO";
//                break;
////            case Util.TYPEMOVEMENTFOUR:
////                type = "EN MOVIMIENTO POR 4 HORAS";
////                break;
////            case Util.TYPEINCOMEREPORT:
////                type = "DENTRO DE LA GEOZONA";
////                break;
////            case Util.TYPEEXITREPORT:
////                type = "FUERA DE LA GEOZONA";
////                break;
//        }
//
//        return type;
//    }


}