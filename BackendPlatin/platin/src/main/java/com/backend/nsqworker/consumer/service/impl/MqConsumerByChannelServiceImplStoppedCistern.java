package com.backend.nsqworker.consumer.service.impl;

import com.backend.ingest.dtos.EventInsDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.dtos.TramaNsqDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.producer.Model.NsqMessage;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.services.VehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.NsqChannelConst;
import com.backend.platin.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplStoppedCistern implements MqConsumerService {

    @Autowired(required = true)
    private EventRedisHsetService eventRedis;

    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;

    //
    @Autowired(required = true)
    private VehicleService vehicleService;

    @Autowired(required = true)
    private IEventService eventService;
    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    int tried = 0;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Value("${rest.uri.alertEvent}")
    String alertEvent;

    @Value("${token.Internal}")
    String tokenInternal;

    @Value("${time.stoppedcistern}")
    double timeStoppedcistern;

    @Value("${time.stoppedcisternFour}")
    double timeStoppedcisternFour;

    public MqConsumerByChannelServiceImplStoppedCistern() {
    }

    public MqConsumerByChannelServiceImplStoppedCistern(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);
        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.CHANNEL_STOPPEDCISTERN, (message) -> {
//            message = null;
            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();
                try {
                    nsqMessage.setAction(NsqChannelConst.CHANNEL_STOPPEDCISTERN);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker StoppedCistern: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }
                try {
                    if (tried >= 3) {
                        tried = 0;
                        message.finished();
                        return;
                    }
                    boolean sameCoordsFour = false;
                    boolean changeFour = true;
                    List<TramaInsDto> castedListFour = null;
                    boolean sameCoords = false;
                    boolean change = true;
                    TramaInsDto tramaNew = null;
                    tramaNew = new Gson().fromJson(nsqMessage.getBody(), TramaInsDto.class);
                    double latitudeTrama = tramaNew.getPosition().getLatitude();
                    double longitudeTrama = tramaNew.getPosition().getLongitude();
                    List<TramaInsDto> castedList = null;
                    String hsetkey = "stopped:vehicle:" + tramaNew.getPlate();
                    String hsetkeyFour = "stoppedFour:vehicle:" + tramaNew.getPlate();
                    String idvehicle = com.backend.platin.consumer.util.Util.VehicleSet(tramaNew,eventRedis,vehicleService,message);
                    String redisKey = "tramas:" + tramaNew.getPlate();
                    // convert ZADD
                    castedList = eventRedis.findStoppedZrange(redisKey, tramaNew, "MINUTE", -10);
                    if (!castedList.isEmpty()) {
                        double resta=timeRestGpsDate(castedList);
                        double minuto = timeStoppedcistern;
                        if (resta >= minuto) {
                            sameCoords = stoppedTrueFalse(castedList, latitudeTrama, longitudeTrama, change, sameCoords);
                            message.touch();
                            String timelagGet = eventRedis.findGetTypeWorkersSet(hsetkey);
                            if (sameCoords == true) {
                                if (timelagGet == null) {
                                    eventRedis.createSetTypeWorkers(hsetkey, tramaNew.getGpsDate().getTime());
                                    EventInsDto eventNew = saveEventStopped(Util.TYPESTOPPED, tramaNew, latitudeTrama, longitudeTrama, idvehicle);
                                    com.backend.platin.util.Util.sendAlertEventInst(eventNew, this.alertEvent, tokenInternal);
                                }
                            } else {
                                if (timelagGet != null) {
                                    eventRedis.DeleteTypeDelWorkers(hsetkey);
                                    EventInsDto eventNew = saveEventStopped(Util.TYPEMOVEMENT, tramaNew, latitudeTrama, longitudeTrama, idvehicle);
                                    com.backend.platin.util.Util.sendAlertEventInst(eventNew, this.alertEvent, tokenInternal);
                                }
                            }

                        }
                    }

                    castedListFour = eventRedis.findStoppedZrange(redisKey, tramaNew, "HOUR", -4);
                    if (!castedListFour.isEmpty()) {
                        double restaFour=timeRestGpsDate(castedListFour);
                        double minutoFour = timeStoppedcisternFour;
                        if (restaFour >= minutoFour) {
                            sameCoordsFour = stoppedTrueFalse(castedListFour, latitudeTrama, longitudeTrama, changeFour, sameCoordsFour);
                            message.touch();
                            String timelagGetFour = eventRedis.findGetTypeWorkersSet(hsetkeyFour);
                            if (sameCoordsFour == true) {
                                if (timelagGetFour == null) {
                                    eventRedis.createSetTypeWorkers(hsetkeyFour, tramaNew.getGpsDate().getTime());
                                    EventInsDto eventNew = saveEventStopped(Util.TYPESTOPPEDFOUR, tramaNew, latitudeTrama, longitudeTrama, idvehicle);
                                    com.backend.platin.util.Util.sendAlertEventInst(eventNew, this.alertEvent, tokenInternal);
                                }
                            } else {
                                if (timelagGetFour != null) {
                                    eventRedis.DeleteTypeDelWorkers(hsetkeyFour);
                                    EventInsDto eventNew = saveEventStopped(Util.TYPEMOVEMENTFOUR, tramaNew, latitudeTrama, longitudeTrama, idvehicle);
                                    com.backend.platin.util.Util.sendAlertEventInst(eventNew, this.alertEvent, tokenInternal);
                                }
                            }
                        }
                    }

                    log.info("Consumo de mensajes específicos CHANNEL_STOPPEDCISTERN: " + nsqMessage.getBody());
                    // Mensaje de confirmacion
                    message.finished();
                    return;
                } catch (DataNotFoundException ex) {
                    log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.info("DataNotFoundException Worker StoppedCistern:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker StoppedCistern:" + ex.getMessage(), "400"));
                    message.requeue();
                    return;
                } catch (Exception e) {
                    tried++;
                    log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
                    log.info("Error Worker StoppedCistern:" + e.getMessage());
                    e.printStackTrace();
                    iErrorRepository.save(new ErrorLogs("Worker StoppedCistern:" + e.getMessage(), "Error"));
                    message.requeue();
                    return;
                }
            }
            return;
        });
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By CHANNEL_STOPPEDCISTERN El consumidor comenzó con éxito!");
    }
public double timeRestGpsDate(List<TramaInsDto> castedListFour){
    Date gpsInicioFour = castedListFour.get(0).getGpsDate();
    Date gpsFinalFour = castedListFour.get(castedListFour.size() - 1).getGpsDate();
    double starFour = gpsInicioFour.getTime();
    double finFour = gpsFinalFour.getTime();
    double restaFour = finFour - starFour;
    return restaFour;

}
    public EventInsDto saveEventStopped(String type, TramaInsDto tramaNew, double latitudeTrama, double longitudeTrama, String idvehicle) throws DataNotFoundException {
        EventInsDto eventNew = null;
        eventNew = new EventInsDto(type,
                idvehicle,
                "null",
                latitudeTrama,
                longitudeTrama,
                tramaNew.getReceiveDate(),
                tramaNew.getGpsDate(),
                tramaNew.getSpeed(), "null", "null", (int) tramaNew.getOdometer());

        //Vehicle vehicledat = vehicleService.findById(idvehicle);
        //eventNew.setVehicle_id(vehicledat);
        log.info("TYPESTOPPED" + ":" + type);
        eventNew = eventService.save(eventNew);
        return eventNew;
    }

    public boolean stoppedTrueFalse(List<TramaInsDto> castedList, double latitudeTrama, double longitudeTrama, boolean change, boolean sameCoords) {
        for (TramaInsDto trama : castedList) {
            double latitudeBd = trama.getPosition().getLatitude();
            double longitudeBd = trama.getPosition().getLongitude();
            if (latitudeTrama == latitudeBd && longitudeTrama == longitudeBd) {
                if (change) {
                    sameCoords = true;
                }
            } else {
                sameCoords = false;
                change = false;
                break;
            }
        }
        return sameCoords;
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerByReportsChannel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
