package com.backend.nsqworker.consumer.service.impl;

import com.backend.ingest.dtos.EventInsDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.dtos.TramaNsqDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.producer.Model.NsqMessage;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.services.VehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.NsqChannelConst;
import com.backend.platin.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplTimeLag implements MqConsumerService {

    @Autowired(required = true)
    private EventRedisHsetService eventRedis;

    @Autowired(required = true)
    private VehicleService vehicleService;

    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;

    @Autowired(required = true)
    private IEventService eventService;

    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    int tried = 0;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Value("${rest.uri.alertEvent}")
    String alertEvent;

    @Value("${time.lag}")
    double timelag;

    @Value("${token.Internal}")
    String tokenInternal;

    @Autowired
    EventRedisHsetService redisService;

    public MqConsumerByChannelServiceImplTimeLag() {
    }

    public MqConsumerByChannelServiceImplTimeLag(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

//
    @Override
    public void mqConsumeAndSendReportTimeLag() {
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);
        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.CHANNEL_REPORTTIMELAG, (message) -> {
//            message = null;
            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();
                try {
                    nsqMessage.setAction(NsqChannelConst.CHANNEL_REPORTTIMELAG);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker TimeLag: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }

                try {
                    if (tried >= 3) {
                        tried = 0;

                        message.finished();
                        return;
                    }
                    // //guardar la trama a la base de datos

                    TramaInsDto tramaNew = null;

                    tramaNew = new Gson().fromJson(nsqMessage.getBody(), TramaInsDto.class);
                    String hsetkey = "timeLag:" + tramaNew.getPlate();
                    String idVehicle = com.backend.platin.consumer.util.Util.VehicleSet(tramaNew,eventRedis,vehicleService,message);
                    Date gpsdate = tramaNew.getGpsDate();
                    Date recievedate = tramaNew.getReceiveDate();
                    double timestap = recievedate.getTime() - gpsdate.getTime();
                    double minuto = timelag;
                    String timelagGet = eventRedis.findGetTypeWorkersSet(hsetkey);
                    if (timestap >= minuto) {
                        if (timelagGet == null) {
                            log.info("TYPETIMELAG");
                            EventInsDto eventNew = null;
                            eventNew = saveEventTimeLag(tramaNew,idVehicle);
                            eventRedis.createSetTypeWorkers(hsetkey, tramaNew.getGpsDate().getTime());
                            com.backend.platin.util.Util.sendAlertEventInst(eventNew, this.alertEvent, tokenInternal);
                        }
                    } else {
                        String time = eventRedis.findGetTypeWorkersSet(hsetkey);
                        if (time != null) {
                            eventRedis.DeleteTypeDelWorkers(hsetkey);
                            log.info("TYPERECOVERY");
                        }

                    }
                    log.info("Consumo CHANNEL_REPORTTIMELAG de mensajes específicos: " + nsqMessage.getBody());
                    // Mensaje de confirmacion
                    message.finished();
                    return;
                } catch (DataNotFoundException ex) {
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.warn("DataNotFoundException Worker TimeLag:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker TimeLag:" + ex.getMessage(), "400"));
                    message.finished();
                    return;
                } catch (Exception e) {
                    tried++;
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
                    log.warn("Error Worker TimeLag:" + e.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker TimeLag:" + e.getMessage(), "Error"));
                    message.requeue();
                    return;
                }
            }
            return;
        }
        );
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);

        consumer.start();

        log.info(
                "nsq By CHANNEL_REPORTTIMELAG El consumidor comenzó con éxito!");
    }
    public EventInsDto saveEventTimeLag(TramaInsDto tramaNew, String idvehicle) throws DataNotFoundException {
       EventInsDto eventNew = null;
        eventNew = new EventInsDto(Util.TYPETIMELAG,
                idvehicle,
                "null",
                tramaNew.getPosition().getLatitude(), tramaNew.getPosition().getLongitude(),
                tramaNew.getReceiveDate(), tramaNew.getGpsDate(), tramaNew.getSpeed(),
                "null", "null",(int)tramaNew.getOdometer());

        //Vehicle vehicledate = vehicleService.findById(idvehicle);
        //eventNew.setVehicle_id(vehicledate);
        eventNew = eventService.save(eventNew);
        return eventNew;
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerByReportsChannel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static String formatDuration(long duration) {
        long hours = TimeUnit.MILLISECONDS.toHours(duration);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) % 60;
//        long milliseconds = duration % 1000;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
