package com.backend.nsqworker.consumer.service.impl;

import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.util.GsonUTCDateAdapter;
import com.backend.platin.util.NsqChannelConst;
import com.backend.nsqworker.consumer.model.NsqMessage;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.dtos.TramaNsqDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Trama;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.interfaces.ITramaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;
import org.springframework.context.annotation.ComponentScan;

@Service
@Slf4j

@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplTrama implements MqConsumerService {

    @Autowired
    private ITramaService tramaService;

    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;

    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    private final Logger log = LogManager.getLogger(MqConsumerByChannelServiceImplTrama.class);
    int tried = 0;

    public MqConsumerByChannelServiceImplTrama() {
    }

    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        System.out.println("Spring boot application running in UTC timezone :" + new Date());
    }

    public MqConsumerByChannelServiceImplTrama(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

    public void mqConsumerSaveDatabaseTrama() {
        log.info("CORRECTO CONSUMO TRAMA : ");
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);
        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.INGEST_CHANNEL, (message) -> {
//            message = null;
            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();
                try {
                    nsqMessage.setAction(NsqChannelConst.INGEST_CHANNEL);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());

                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker Trama: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }

                try {
                    if (tried >= 3) {
                        tried = 0;
                        message.finished();
                        return;
                    }
                    Trama trama = null;
                    TramaNsqDto tramaNew = null;
                    tramaNew = new Gson().fromJson(nsqMessage.getBody(), TramaNsqDto.class);
                    trama = new Trama(tramaNew.getEvent(), tramaNew.getPlate(),
                            tramaNew.getPosition(), tramaNew.getGpsDate(), tramaNew.getSpeed(), tramaNew.getReceiveDate(),
                            tramaNew.getTokenTrama(), tramaNew.getOdometer());
                    long gps_dateMili = Long.parseLong(tramaNew.getRu_gps_date());
//                    gps_dateMili= gps_dateMili+18000000;
                    Calendar calendarR = Calendar.getInstance();
                    calendarR.setTimeInMillis(gps_dateMili);
                    Date dategpsDateMili = calendarR.getTime();
                    trama.setGpsDate(dategpsDateMili);
                    trama = tramaService.create(trama);
                    log.info("Consumo de mensajes específicos INGEST_CHANNEL: " + nsqMessage.getBody());
                    // Mensaje de confirmacion
                    message.finished();
                    return;
                } catch (Exception e) {
                    tried++;
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
                    log.warn("Error Worker INGEST_CHANNEL:" + e.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker Trama:" + e.getMessage(), "Error"));
                    message.requeue();
                    return;
                }
            }
//			message.finished();
            return;
        });
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By INGEST_CHANNEL El consumidor comenzó con éxito!");
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerByReportsChannel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
