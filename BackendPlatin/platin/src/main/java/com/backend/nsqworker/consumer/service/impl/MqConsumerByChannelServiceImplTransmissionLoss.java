package com.backend.nsqworker.consumer.service.impl;

import com.backend.ingest.dtos.EventInsDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.dtos.TramaNsqDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Event;
import com.backend.ingest.entities.Vehicle;
import com.backend.ingest.producer.Model.NsqMessage;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.service.interfaces.IEventService;
import com.backend.ingest.service.services.VehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.util.NsqChannelConst;
import com.backend.platin.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;

import io.lettuce.core.ScoredValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@ComponentScan("com.backend.ingest.*")
public class MqConsumerByChannelServiceImplTransmissionLoss implements MqConsumerService {

    @Autowired(required = true)
    private EventRedisHsetService eventRedis;
    //
    @Autowired(required = true)
    private VehicleService vehicleService;

    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;

    @Autowired(required = true)
    private IEventService eventService;

    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;
    int tried = 0;
    int nveces = 0;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Value("${rest.uri.alertEvent}")
    String alertEvent;

    @Value("${token.Internal}")
    String tokenInternal;

    @Value("${time.transmission}")
    double timeTransmission;

    @Autowired
    EventRedisHsetService redisService;

    public MqConsumerByChannelServiceImplTransmissionLoss() {
    }

    public MqConsumerByChannelServiceImplTransmissionLoss(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);
        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.CHANNEL_REPORTTRASMISSION, (message) -> {
//            message = null;
            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();
                try {
                    nsqMessage.setAction(NsqChannelConst.CHANNEL_REPORTTRASMISSION);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker TransmissionLoss: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }
                try {
                    if (tried >= 3) {
                        tried = 0;
                        message.finished();
                        return;
                    }
                    // //guardar la trama a la base de datos
                    TramaInsDto tramaNew = null;
                    tramaNew = new Gson().fromJson(nsqMessage.getBody(), TramaInsDto.class);
                    eventRedis.createReportTrasmission(tramaNew);
                    String hsetkey = "lost_transmission:" + tramaNew.getPlate();
                    String idvehicle = com.backend.platin.consumer.util.Util.VehicleSet(tramaNew,eventRedis,vehicleService,message);
                    List<ScoredValue<String>> listeventTras = eventRedis.findGetTrasmission(tramaNew);
                    message.touch();
                    String gps = eventRedis.findGetTypeWorkersSet(hsetkey);
                    if (!listeventTras.isEmpty() && listeventTras.size() > 1) {
                        double gpsUltimo = listeventTras.get(listeventTras.size()-1).getScore();
                        double gpsPenultimo = listeventTras.get(listeventTras.size()-2).getScore();
                        double resta = gpsUltimo - gpsPenultimo;
                        double minuto = timeTransmission;
                        if (resta >= minuto) {
                            if (gps == null) {
                                log.info("TYPELOSTTRASMISSION");
                                message.touch();
                                EventInsDto eventNew = saveEventTrasmission(Util.TYPELOSTTRASMISSION, tramaNew, idvehicle);
                                eventRedis.createSetTypeWorkers(hsetkey, gpsUltimo);
                                com.backend.platin.util.Util.sendAlertEventInst(eventNew, this.alertEvent, tokenInternal);
                            }
                        } else {
                            if (gps != null) {
                                eventRedis.DeleteTypeDelWorkers(hsetkey);
                                log.info("TYPERECOVERYTRASMISSION");
                                message.touch();
                                EventInsDto eventNew = saveEventTrasmission(Util.TYPERECOVERYTRASMISSION, tramaNew, idvehicle);
                                com.backend.platin.util.Util.sendAlertEventInst(eventNew, this.alertEvent, tokenInternal);
                            }

                        }
                    }

                    log.info("Consumo CHANNEL_REPORTTRASMISSION de mensajes específicos: " + nsqMessage.getBody());
                    // Mensaje de confirmacion
                    message.finished();
                    return;
                } catch (DataNotFoundException ex) {
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(ex));
                    log.warn("DataNotFoundException Worker TransmissionLoss:" + ex.getMessage());
                    iErrorRepository.save(new ErrorLogs("Worker TransmissionLoss:" + ex.getMessage(), "400"));
                    message.finished();
                    return;
                } catch (Exception e) {
                    tried++;
                    log.warn(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
                    log.warn("Error Worker TransmissionLoss:" + e.getMessage());
                    e.printStackTrace();
                    iErrorRepository.save(new ErrorLogs("Worker TransmissionLoss:" + e.getMessage(), "Error"));
                    message.requeue();
                    return;
                }
            }
//			message.finished();
            return;
        });
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By testChannel El consumidor comenzó con éxito!");
    }

    public EventInsDto saveEventTrasmission(String type, TramaInsDto tramaNew, String idvehicle) throws DataNotFoundException {
        EventInsDto eventNew = new EventInsDto(type,
                idvehicle,
                "null",
                tramaNew.getPosition().getLatitude(), tramaNew.getPosition().getLongitude(),
                tramaNew.getReceiveDate(), tramaNew.getGpsDate(),
                tramaNew.getSpeed(), "null", "null", (int) tramaNew.getOdometer());

        //Vehicle vehicledate = vehicleService.findById(idvehicle);
        //eventNew.setVehicle_id(vehicledate);
        eventNew = eventService.save(eventNew);
        return eventNew;
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerByReportsChannel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
