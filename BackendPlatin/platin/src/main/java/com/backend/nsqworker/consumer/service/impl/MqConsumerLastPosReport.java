/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.nsqworker.consumer.service.impl;

import com.backend.ingest.dtos.LastPosReportDto;
import com.backend.ingest.dtos.PositionDto;
import com.backend.ingest.dtos.Reports.jsons.ReportResponseDto;
import com.backend.ingest.entities.ErrorLogs;
import com.backend.ingest.entities.Trama;
import com.backend.ingest.repositories.IErrorLogsRepository;
import com.backend.ingest.service.MqConsumerService;
import com.backend.ingest.service.interfaces.ICompanyService;
import com.backend.ingest.service.interfaces.IReportService;
import com.backend.ingest.service.interfaces.IUserService;
import com.backend.ingest.service.interfaces.IVehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.nsqworker.consumer.model.NsqMessage;
import com.backend.platin.util.NsqChannelConst;
import com.backend.platin.util.Util;
import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.google.gson.Gson;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;


/**
 *
 * @author FAMLETO
 */
public class MqConsumerLastPosReport implements MqConsumerService {

    @Value("${geojson.path}")
    private String geojsonPath;

    @Value("${xls.path}")
    private String xlsPath;

    @Value("${url.map}")
    private String urlMap;

    @Value("${rest.uri.report}")
    String uri;

    @Value("${token.Internal}")
    String tokenInternal;

    private String topic;
    private String nsqAddress;
    private Integer nsqPort;
    private Integer nsqThreadCount;

    int tried = 0;

//    @Autowired
//    private ITramaService tramaService;

    @Autowired(required = true)
    private IVehicleService vehicleService;

    @Autowired(required = true)
    private IErrorLogsRepository iErrorRepository;

    @Autowired
    private IReportService reportService;

    @Autowired
    private ICompanyService companyService;

    @Autowired
    private IUserService userService;

    @Autowired
    EventRedisHsetService redisService;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    private final Logger log = LogManager.getLogger(MqConsumerLastPosReport.class);

    public MqConsumerLastPosReport() {
    }

    public MqConsumerLastPosReport(String topic, String nsqAddress, Integer nsqPort, Integer nsqThreadCount) {
        this.topic = topic;
        this.nsqAddress = nsqAddress;
        this.nsqPort = nsqPort;
        this.nsqThreadCount = nsqThreadCount;
    }

    @Override
    public void mqConsumeLastPosReportMessage() {
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(nsqAddress, nsqPort);

        // Consumo específico channel
        NSQConsumer consumer = new NSQConsumer(lookup, topic, NsqChannelConst.REPORTS_CHANNEL, (message) -> {
            if (message != null) {
                String msg = new String(message.getMessage());
                NsqMessage nsqMessage = new NsqMessage();
                try {
                    nsqMessage.setAction(NsqChannelConst.REPORTS_CHANNEL);
                    nsqMessage.setBody(msg);
                    nsqMessage.setId(UUID.randomUUID().getLeastSignificantBits());
                } catch (Exception e) {
                    iErrorRepository.save(new ErrorLogs("Worker PosReport: El mensaje no se pudo convertir, hay un problema", "Error"));
                    log.error("El mensaje no se pudo convertir, hay un problema");
                    message.finished();
                    return;
                }

                try {
                    if (tried >= 3) {
                        tried = 0;
                        ReportResponseDto reportResponse = new ReportResponseDto();
                        reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_ERROR);
                        this.sendReport(reportResponse);
                        message.finished();
                        return;
                    }
                    LastPosReportDto report = new Gson().fromJson(nsqMessage.getBody(), LastPosReportDto.class);

                   
                    if (report != null
                            && report.getStartDate() != null
                            && report.getCompany_id() != null && !report.getCompany_id().isEmpty()) {

//                        List<VehicleDataDto> vehicles = vehicleService.plateFindByStatus();
//                        List<Trama> tramas = new ArrayList<>() ;
//                        for(VehicleDataDto vehicle: vehicles)
//                        {
//                           Trama trama= redisService.findLastTramaWhileTenMinutes(vehicle.getPlate(),report.getStartDate());
//                           
//                           tramas.add(trama);
//                        }
//                        if (tramas.isEmpty()) 
//                            throw new DataNotFoundException(Util.TRAMA_NOT_FOUND);
//                        
//                            for (Trama trama : tramas) {
//                                System.out.println(trama.getPlate() + ": " + trama.getGpsDate());
//                            }
//                            com.backend.platin.util.Util.generateDirIfDoesntExist(geojsonPath);
//
//                            //generate geojson file
//                            GeoJsonDto geoJsonDto = new GeoJsonDto();
//
//                            geoJsonDto.setHeader(Util.generateGeneralHeadersGeoJson(Util.STANDARD_LAST_POS_REPORT));
//
//                            String[][] body = null;
//
//                            String uuid = UUID.randomUUID().toString();
//                            String geoUrlMap = urlMap + uuid + ".geojson";
//
//                            MetaStandardLasPosDto meta = new MetaStandardLasPosDto();
//                            geoJsonDto.setMeta(meta);
//
//                            body = new String[tramas.size()][12];
//                            for (int i = 0; i < tramas.size(); i++) {
//                                int columnCount = 0;
//
//                                body[i][columnCount++] = tramas.get(i).getPlate();
//                                IVehicleCompanyDto vehicle = vehicleService.vehiclefindByPlate(tramas.get(i).getPlate());
//                                body[i][columnCount++] = vehicle.getCompanyName();
//                                body[i][columnCount++] = vehicle.getNameclient();
//                                body[i][columnCount++] = vehicle.getcode_osinergmin();
//                                body[i][columnCount++] = vehicle.getStatus() ? Util.PARAM_STATUS_ACTIVE : Util.PARAM_STATUS_INACTIVE;
//                                body[i][columnCount++] = geoUrlMap;
//                                body[i][columnCount++] = Util.parseDateToPrinterFormatString(new Date());
//                                body[i][columnCount++] = Util.parseDateToPrinterFormatString(tramas.get(0).getGpsDate());
//                                body[i][columnCount++] = Util.parseDateToPrinterFormatString(tramas.get(tramas.size() - 1).getGpsDate());
//
//                                //GENERATING GPS DATE
//                                body[i][columnCount++] = String.valueOf(tramas.get(i).getGpsDate());
//
//                                //GENERATING LATITUDE AND LONGITUDE
//                                PositionDto position = new Gson().fromJson(tramas.get(i).getPosition(), PositionDto.class);
//                                body[i][columnCount++] = String.valueOf(position.getLatitude());
//                                body[i][columnCount++] = String.valueOf(position.getLongitude());
//                            }
//
//                            //GENERATING TOTAL DISTANCE  (STANDARD_LAST_POS_REPORT) 
//                            double totalDistance = this.getTotalDistance(tramas);
//
//                            int rowCount = tramas.size() - 1;
//
//                            body[rowCount][10] = String.valueOf(totalDistance);
//                            System.out.println("================================================   BODY");
//                            for (int i = 0; i < body.length; i++) {
//                                System.out.println(body[i][0] + ": " + body[i][9]);
//                            }
//                            geoJsonDto.setBody(body);
//                            String jsonTramas = new Gson().toJson(geoJsonDto);
//                            Util.generateGeoJsonReportFile(uuid, geojsonPath, jsonTramas);
//
//                            String xlsUrlMap = urlMap + uuid + ".xls";
//
//                            String[] urls = new String[2];
//
//                            urls[0] = geoUrlMap;
//                            urls[1] = xlsUrlMap;
//
//                            ReportResponseDto reportResponse = new ReportResponseDto();
//
//                            reportResponse.setUrls(urls);
//                            reportResponse.setResponse(com.backend.platin.util.Util.RESPONSE_OK);
//                            this.sendReport(reportResponse);
//
//                            //Create a new row in Report Table in Database
//                            ReportInDto reportObject = new ReportInDto(geoUrlMap, xlsUrlMap, Util.LAST_POS_VEHICLES_REPORT,"Disponible",report.getStartDate(),null,true, report.getCompany_id());
//                            reportService.create(reportObject);
//                        
                        log.info("Consumo de mensajes REPORTS_CHANNEL específicos: " + nsqMessage.getBody());
                        // Mensaje de confirmacion
                        message.finished();
                        return;
                    } else {

                        log.info("Empty Fields");
                    }

                } catch (Exception e) {

                    tried++;
                    iErrorRepository.save(new ErrorLogs("Worker PosReport:" + e.getMessage(), "Error"));
                    e.printStackTrace();
                    message.requeue();
                    return;
                }

            }
            return;
        }
        );
        consumer.setExecutor(Executors.newFixedThreadPool(nsqThreadCount));
        consumer.setMessagesPerBatch(nsqThreadCount);
        consumer.start();
        log.info("nsq By REPORTS_CHANNEL El consumidor mqConsumerByReportsChannel comenzó con éxito!");

    }

    private double getTotalDistance(List<Trama> tramas) {
        int j = 1;
        double totalDistance = 0;
        if (tramas.size() > 0) {
            for (int i = 0; i < tramas.size(); i++) {
                String stPositionString = tramas.get(i).getPosition();
                PositionDto stPosition = new Gson().fromJson(stPositionString, PositionDto.class);
                String enPositionString = tramas.get(j).getPosition();
                PositionDto enPosition = new Gson().fromJson(enPositionString, PositionDto.class);
                if (j < tramas.size() - 1) {
                    j++;
                }
                double distance = Util.calculateDistance(stPosition.getLatitude(), stPosition.getLongitude(), enPosition.getLatitude(), enPosition.getLongitude());
                totalDistance = totalDistance + distance;
            }
        }

        return totalDistance;
    }

    @Override
    public void mqConsumerSaveDatabaseTrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerCalculateGeocerca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportSpedd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerReportTravels() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendTramaWebSocket() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendStoppedCisternFour() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTransmissionLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumeAndSendReportTimeLag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mqConsumerByReportsChannel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void sendReport(ReportResponseDto responseReport) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String requestJson = new Gson().toJson(responseReport);

        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
        RestTemplate restTemplate = restTemplate();
        restTemplate.postForObject(uri + "/" + tokenInternal, entity, String.class);
    }

    @Override
    public void mqConsumeAndSendStoppedCistern() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
