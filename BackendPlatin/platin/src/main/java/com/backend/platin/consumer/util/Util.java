package com.backend.platin.consumer.util;

import com.backend.ingest.dtos.CoordInDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.service.services.VehicleService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.github.brainlag.nsq.NSQMessage;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import java.awt.geom.Path2D;
import java.util.ArrayList;

public class Util {

//    public void coordPolygon(NSQMessage message, ArrayList<Coordinate> points, JsonArray jsonArrayy) {
//        int i = 0;
//        if (i == 0) {
//            jsonArrayy.add(jsonArrayy.get(0));
//        }
//        CoordInDto coordi = null;
//        message.touch();
//        for (JsonElement obj : jsonArrayy) {
//            coordi = new Gson().fromJson(obj, CoordInDto.class);
//            points.add(new Coordinate(coordi.getLatitude(), coordi.getLongitude()));
//            i++;
//        }
//        message.touch();
//    }

    public void coordPolygon(ArrayList<Coordinate> points, JsonArray jsonArrayy) {
        int i = 0;
        if (i == 0) {
            jsonArrayy.add(jsonArrayy.get(0));
        }
        CoordInDto coordi;
        for (JsonElement obj : jsonArrayy) {
            coordi = new Gson().fromJson(obj, CoordInDto.class);
            points.add(new Coordinate(coordi.getLatitude(), coordi.getLongitude()));
            i++;
        }
    }

    public boolean coordPolygonPath2D(JsonArray jsonArrayy, Coordinate coord) {
        int i = 0;
//        if (i == 0) {
//            jsonArrayy.add(jsonArrayy.get(0));
//        }
        CoordInDto coordi;
        Path2D path = new Path2D.Double();
        for (JsonElement obj : jsonArrayy) {
            coordi = new Gson().fromJson(obj, CoordInDto.class);
            //Definimos el primer punto como el punto de inicio con moveTo
            if (i == 0) {
                path.moveTo(coordi.getLatitude(), coordi.getLongitude());
            }
            // Utilizamos el método lineTo para ir definiendo las líneas
            if (i > 0) {
                path.lineTo(coordi.getLatitude(), coordi.getLongitude());
            }
            //Cerramos la figura geométrica en caso los puntos proporcionados no lo hagan.
            i++;
        }
        path.closePath();
        return path.contains(coord.x, coord.y);
    }

    public CoordInDto coordCircle(String loc) {
        CoordInDto coordi = null;
        JsonArray jsonArray = new JsonParser().parse(loc).getAsJsonArray();
        for (JsonElement jsonElement : jsonArray) {
            coordi = new Gson().fromJson(jsonElement, CoordInDto.class);
        }
        return coordi;
    }

    public boolean isPointPolygon(ArrayList<Coordinate> points, Coordinate coord) {
        GeometryFactory gf = new GeometryFactory();
        Point point;
        Polygon polygon;
        polygon = gf.createPolygon(new LinearRing(
                        new CoordinateArraySequence(points.toArray(new Coordinate[points.size()])), gf),
                null);
        point = gf.createPoint(coord);
        return point.within(polygon);
    }

    boolean isInRectangle(double centerX, double centerY, double radius,
                          double x, double y) {
        return x >= centerX - radius && x <= centerX + radius
                && y >= centerY - radius && y <= centerY + radius;
    }

    public boolean isPointInCircle(double centerX, double centerY,
                                   double radius, double x, double y) {
        if (isInRectangle(centerX, centerY, radius, x, y)) {
            double dx = centerX - x;
            double dy = centerY - y;
            dx *= dx;
            dy *= dy;
            double distanceSquared = dx + dy;
            double radiusSquared = radius * radius;
            return distanceSquared <= radiusSquared;
        }
        return false;
    }

    public static String ExceptionClassMethodLine(Exception e) {
        int currentLine = e.getStackTrace()[0].getLineNumber();
        String metodo = e.getStackTrace()[0].getMethodName();
        String clas = e.getStackTrace()[0].getClassName();
        return "Error en la clase:" + clas + " en el metodo:" + metodo + " y en la linea:" + currentLine;
    }

    public boolean contains(ArrayList<CoordInDto> points, JsonArray jsonArrayy, double latitude, double longitude) {
        int i;
        int j;
        boolean result = false;
        //coordPolygonv1(points, jsonArrayy);
        for (i = 0, j = points.size() - 1; i < points.size(); j = i++) {
            if ((points.get(i).getLongitude() > longitude) != (points.get(j).getLongitude() > longitude) &&
                    (latitude < (points.get(j).getLatitude() - points.get(i).getLatitude()) *
                            (longitude - points.get(i).getLongitude())
                            / (points.get(j).getLongitude() - points.get(i).getLongitude()) + points.get(i).getLatitude())) {
                result = !result;
            }
        }
        return result;
    }

    public static String VehicleSet(TramaInsDto tramaNew, EventRedisHsetService eventRedis, VehicleService vehicleService, NSQMessage message) throws Exception {

        String keySetVehicle = "VehicleSet" + tramaNew.getPlate();
        String idvehicle = eventRedis.findGetTypeWorkersSet(keySetVehicle);

        if (idvehicle == null) {

            message.touch();
            idvehicle = vehicleService.findByPlateWorker(tramaNew.getPlate());
            message.touch();

            if (idvehicle != null) {
                eventRedis.createSetTypeWorkers(keySetVehicle, idvehicle);
            }
        }

        if (idvehicle == null) {
            throw new DataNotFoundException(com.backend.platin.util.Util.VEHICLE_NOT_FOUND);
        }
        return idvehicle;

    }


}
