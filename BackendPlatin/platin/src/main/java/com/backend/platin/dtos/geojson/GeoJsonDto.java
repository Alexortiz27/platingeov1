/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;

import java.util.HashMap;

/**
 *
 * @author FAMLETO
 */
public class GeoJsonDto {

    private MetaDto meta;
    
    private String[] header;
    
    private String[][] body;
    
    
     /**
     * @return the meta
     */
    public MetaDto getMeta() {
        return meta;
    }

    /**
     * @param meta the meta to set
     */
    public void setMeta(MetaDto meta) {
        this.meta = meta;
    }

    /**
     * @return the header
     */
    public String[] getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(String[] header) {
        this.header = header;
    }

    /**
     * @return the body
     */
    public String[][] getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(String[][] body) {
        this.body = body;
    }
}
