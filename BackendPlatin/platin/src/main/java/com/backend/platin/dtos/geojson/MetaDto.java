/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;

/**
 *
 * @author FAMLETO
 */
public class MetaDto
{

    private String placa;
    private String emv;
    private String cliente;
    private String codigo_osinerming;
    private String estado_vehículo;
    private String Url_de_mapa;
    private String fecha_generacion;
    private String desde;
    private String hasta;

    public MetaDto() {
        this.placa = "Placa del vehículo a hacer consultar";
        this.emv = "Nombre de la empresa";
        this.cliente = "Nombre de la empresa a dar el servicio";
        this.codigo_osinerming = "Código proporcionado por osinergmin";
        this.estado_vehículo ="Estado del vehículo";
        this.Url_de_mapa = "Url donde se mostrará el mapa";
        this.fecha_generacion = "Fecha de genereación del reporte";
        this.desde = "Fecha de la consulta desde";
        this.hasta = "Fecha de la consulta hasta";
    }
    
    
    
    /**
     * @return the plate
     */
    public String getPlate() {
        return placa;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(String plate) {
        this.placa = plate;
    }

    /**
     * @return the emv
     */
    public String getEmv() {
        return emv;
    }

    /**
     * @param emv the emv to set
     */
    public void setEmv(String emv) {
        this.emv = emv;
    }

    /**
     * @return the cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the codigo_osinerming
     */
    public String getCodigo_osinerming() {
        return codigo_osinerming;
    }

    /**
     * @param codigo_osinerming the codigo_osinerming to set
     */
    public void setCodigo_osinerming(String codigo_osinerming) {
        this.codigo_osinerming = codigo_osinerming;
    }

    /**
     * @return the estado_vehículo
     */
    public String getEstado_vehículo() {
        return estado_vehículo;
    }

    /**
     * @param estado_vehículo the estado_vehículo to set
     */
    public void setEstado_vehículo(String estado_vehículo) {
        this.estado_vehículo = estado_vehículo;
    }

    /**
     * @return the Url_de_mapa
     */
    public String getUrl_de_mapa() {
        return Url_de_mapa;
    }

    /**
     * @param Url_de_mapa the Url_de_mapa to set
     */
    public void setUrl_de_mapa(String Url_de_mapa) {
        this.Url_de_mapa = Url_de_mapa;
    }

    /**
     * @return the fecha_generacion
     */
    public String getFecha_generacion() {
        return fecha_generacion;
    }

    /**
     * @param fecha_generacion the fecha_generacion to set
     */
    public void setFecha_generacion(String fecha_generacion) {
        this.fecha_generacion = fecha_generacion;
    }

    /**
     * @return the desde
     */
    public String getDesde() {
        return desde;
    }

    /**
     * @param desde the desde to set
     */
    public void setDesde(String desde) {
        this.desde = desde;
    }

    /**
     * @return the hasta
     */
    public String getHasta() {
        return hasta;
    }

    /**
     * @param hasta the hasta to set
     */
    public void setHasta(String hasta) {
        this.hasta = hasta;
    }
    
    
}
