/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;

/**
 *
 * @author FAMLETO
 */
public class MetaGeozone extends MetaDto {

    private String fechaGeozone;
    private String type;

    private String typeGeozone;
    private String speed;

    public MetaGeozone() {
        super();
        this.fechaGeozone = "fecha_trasmission de la trama enviada por GPS";
        this.type = "type de la trama enviada por GPS";
        this.typeGeozone = "typeGeozone de la trama enviada por GPS";
        this.speed="Velocidad a la que va el vehículo";
    }

    public String getFechaGeozone() {
        return fechaGeozone;
    }

    public void setFechaGeozone(String fechaGeozone) {
        this.fechaGeozone = fechaGeozone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the typeGeozone
     */
    public String getTypeGeozone() {
        return typeGeozone;
    }

    /**
     * @param typeGeozone the typeGeozone to set
     */
    public void setTypeGeozone(String typeGeozone) {
        this.typeGeozone = typeGeozone;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }


}
