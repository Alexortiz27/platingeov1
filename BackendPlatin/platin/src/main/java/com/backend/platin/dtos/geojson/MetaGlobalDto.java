/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;

/**
 *
 * @author FAMLETO
 */
public class MetaGlobalDto extends MetaDto 
{

    private String latitud;
    private String longitud;
    private String altitud;
    private String velocidad;
    private String fechaGps;
    private String evento;
    private String geozona;
    private String FechaMensaje;

    public MetaGlobalDto() {
        this.latitud = "Latitud del punto donde se ubicó la transmisión";
        this.longitud = "Longitud del punto donde se ubicó la transmisión";
        this.altitud = "Altitud referente al nivel del mar";
        this.velocidad = "Velocidad de la unidad (Km/h)";
        this.fechaGps = "Fecha y hora de la transmisión de la unidad";
        this.evento = "Descripción del evento";
        this.geozona = "Geocerca";
        this.FechaMensaje = "Fecha y hora de envío del mensaje de la trama";
    }
    
    
    
    
    
    /**
     * @return the latitud
     */
    public String getLatitud() {
        return latitud;
    }

    /**
     * @param latitud the latitud to set
     */
    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    /**
     * @return the longitud
     */
    public String getLongitud() {
        return longitud;
    }

    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    /**
     * @return the altitud
     */
    public String getAltitud() {
        return altitud;
    }

    /**
     * @param altitud the altitud to set
     */
    public void setAltitud(String altitud) {
        this.altitud = altitud;
    }

    /**
     * @return the velocidad
     */
    public String getVelocidad() {
        return velocidad;
    }

    /**
     * @param velocidad the velocidad to set
     */
    public void setVelocidad(String velocidad) {
        this.velocidad = velocidad;
    }

    /**
     * @return the fechaGps
     */
    public String getFechaGps() {
        return fechaGps;
    }

    /**
     * @param fechaGps the fechaGps to set
     */
    public void setFechaGps(String fechaGps) {
        this.fechaGps = fechaGps;
    }

    /**
     * @return the evento
     */
    public String getEvento() {
        return evento;
    }

    /**
     * @param evento the evento to set
     */
    public void setEvento(String evento) {
        this.evento = evento;
    }

    /**
     * @return the geozona
     */
    public String getGeozona() {
        return geozona;
    }

    /**
     * @param geozona the geozona to set
     */
    public void setGeozona(String geozona) {
        this.geozona = geozona;
    }

    /**
     * @return the FechaMensaje
     */
    public String getFechaMensaje() {
        return FechaMensaje;
    }

    /**
     * @param FechaMensaje the FechaMensaje to set
     */
    public void setFechaMensaje(String FechaMensaje) {
        this.FechaMensaje = FechaMensaje;
    }
    
    
}
