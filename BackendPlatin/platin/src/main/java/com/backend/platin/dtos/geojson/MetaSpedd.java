/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;

/**
 *
 * @author FAMLETO
 */
public class MetaSpedd extends MetaDto {

    private String fecha_spedd;

    private String latitude;
    private String longitude;
    private String spedd;
   

    public MetaSpedd() {
        super();
        this.fecha_spedd = "fecha_spedd de la trama enviada por GPS";
        this.latitude = "Latitud de la trama enviada por GPS";
        this.longitude = "Longitud de la trama enviada por GPS";
        this.spedd = "spedd de la trama enviada por GPS";
       
    }

    public String getFecha_spedd() {
        return fecha_spedd;
    }

    public void setFecha_spedd(String fecha_spedd) {
        this.fecha_spedd = fecha_spedd;
    }

    public String getSpedd() {
        return spedd;
    }

    public void setSpedd(String spedd) {
        this.spedd = spedd;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
