/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;


/**
 *
 * @author FAMLETO
 */
public class MetaStandardLasPosDto extends MetaDto {

    private String latitude ;
    private String longitude;
    private String kmsRecorridos;
    private String gps_date;
    private String speed;
    public MetaStandardLasPosDto()
    {
        super();
        
        this.latitude="Latitud de la trama enviada por GPS";
        this.longitude="Longitud de la trama enviada por GPS";
        this.gps_date="Fecha que envía el gps";
        this.speed="Velocidad del vehículo";
        this.kmsRecorridos = "kmsRecorridos de la trama por el intervalo de fecha";
    }
    
    
    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    
     /**
     * @return the gps_date
     */
    public String getGps_date() {
        return gps_date;
    }

    /**
     * @param gps_date the gps_date to set
     */
    public void setGps_date(String gps_date) {
        this.gps_date = gps_date;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
