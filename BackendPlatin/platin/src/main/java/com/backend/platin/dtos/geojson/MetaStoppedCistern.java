/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;

/**
 *
 * @author FAMLETO
 */
public class MetaStoppedCistern extends MetaDto {

    private String fecha;
    private String latitude;
    private String longitude;
    private String InicioDetenido;
    private String FinalDetenido;
    private String TiempoDetenido;
    private String speed ;

    public MetaStoppedCistern() {
        super();
        this.fecha = "fecha_trasmission de la trama enviada por GPS";
        this.latitude = "latitude de la trama enviada por GPS";
        this.longitude = "longitude de la trama enviada por GPS";
        this.InicioDetenido = "InicioDetenido de la trama enviada por GPS";
        this.FinalDetenido = "FinalDetenido de la trama enviada por GPS";
        this.TiempoDetenido = "TiempoDetenido de la trama enviada por GPS";
        this.speed="Velocidad del vehículo";

    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getInicioDetenido() {
        return InicioDetenido;
    }

    public void setInicioDetenido(String InicioDetenido) {
        this.InicioDetenido = InicioDetenido;
    }

    public String getFinalDetenido() {
        return FinalDetenido;
    }

    public void setFinalDetenido(String FinalDetenido) {
        this.FinalDetenido = FinalDetenido;
    }

    public String getTiempoDetenido() {
        return TiempoDetenido;
    }

    public void setTiempoDetenido(String TiempoDetenido) {
        this.TiempoDetenido = TiempoDetenido;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }



}
