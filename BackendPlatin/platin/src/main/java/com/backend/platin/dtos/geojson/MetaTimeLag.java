/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;

/**
 *
 * @author FAMLETO
 */
public class MetaTimeLag extends MetaDto {

    private String fechaRecepcion;
    private String fechaGpsDate;

    private String latitude;
    private String longitude;
    private String speed;

    public MetaTimeLag() {
        super();
        this.fechaRecepcion = "fechaRecepcion de la trama enviada por GPS";
        this.fechaGpsDate = "fechaGpsDate de la trama enviada por GPS";
        this.latitude = "Latitud de la trama enviada por GPS";
        this.longitude = "Longitud de la trama enviada por GPS";
        this.speed="Velocidad del vehículo";
    }

    public String getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(String fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public String getFechaGpsDate() {
        return fechaGpsDate;
    }

    public void setFechaGpsDate(String fechaGpsDate) {
        this.fechaGpsDate = fechaGpsDate;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
