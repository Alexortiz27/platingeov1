/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;

/**
 *
 * @author FAMLETO
 */
public class MetaTrasmission extends MetaDto {

    private String fecha_trasmission;
    private String type;

    private String latitude;
    private String longitude;
    private String speed ;

    
    public MetaTrasmission() {
        super();
        this.fecha_trasmission = "fecha_trasmission de la trama enviada por GPS";
        this.type = "type de la trama enviada por GPS";
        this.latitude = "Latitud de la trama enviada por GPS";
        this.longitude = "Longitud de la trama enviada por GPS";
        this.speed= "Velocidad del vehículo";

    }

    public String getFecha_trasmission() {
        return fecha_trasmission;
    }

    public void setFecha_trasmission(String fecha_trasmission) {
        this.fecha_trasmission = fecha_trasmission;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    
}
