/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.dtos.geojson;

/**
 *
 * @author FAMLETO
 */
public class MetaTravelsReport extends MetaDto {

    private String idViaje;
    private String nombre;
    private String InicioCoordenadas;
    private String InicioHora;
    private String FinalCoordenadas;
    private String FinalHora;
    private String tiempo;
    private String Kilometros;

    private String speed;

    public MetaTravelsReport() {
        super();
        this.idViaje = "idViaje de la trama enviada por GPS";
        this.nombre = "nombre de la trama enviada por GPS";
        this.InicioCoordenadas = "InicioCoordenadas de la trama enviada por GPS";
        this.InicioHora = "InicioHora de la trama enviada por GPS";
        this.FinalCoordenadas = "FinalCoordenadas de la trama enviada por GPS";
        this.FinalHora = "FinalHora de la trama enviada por GPS";
        this.tiempo = "tiempo de la trama enviada por GPS";
        this.Kilometros = "Kilometros de la trama enviada por GPS";
      
        this.speed="Velocidad del vehículo";
       
    }

    public String getIdViaje() {
        return idViaje;
    }

    public void setIdViaje(String idViaje) {
        this.idViaje = idViaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getInicioCoordenadas() {
        return InicioCoordenadas;
    }

    public void setInicioCoordenadas(String InicioCoordenadas) {
        this.InicioCoordenadas = InicioCoordenadas;
    }

    public String getInicioHora() {
        return InicioHora;
    }

    public void setInicioHora(String InicioHora) {
        this.InicioHora = InicioHora;
    }

    public String getFinalCoordenadas() {
        return FinalCoordenadas;
    }

    public void setFinalCoordenadas(String FinalCoordenadas) {
        this.FinalCoordenadas = FinalCoordenadas;
    }

    public String getFinalHora() {
        return FinalHora;
    }

    public void setFinalHora(String FinalHora) {
        this.FinalHora = FinalHora;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getKilometros() {
        return Kilometros;
    }

    public void setKilometros(String Kilometros) {
        this.Kilometros = Kilometros;
    }

    /**
     * @return the speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }
  
}
