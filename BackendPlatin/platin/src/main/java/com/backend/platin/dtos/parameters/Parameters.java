package com.backend.platin.dtos.parameters;

import java.util.Date;

public class Parameters {

    private String nameOrder;

    private Integer page;

    private Integer size;
    private Boolean orderAsc;
    private String busca;
    private String user_id;
    private String type;

    public Parameters() {
    }

    public Parameters(String nameOrder, Integer page, Integer size, Boolean orderAsc) {

        this.nameOrder = nameOrder;
        this.page = page;
        this.size = size;
        this.orderAsc = orderAsc;
    }

    public Parameters(String nameOrder, Integer page, Integer size, Boolean orderAsc,String busca) {
        this.nameOrder = nameOrder;
        this.page = page;
        this.size = size;
        this.orderAsc = orderAsc;
        this.busca = busca;
    }
    
    public Parameters(String nameOrder, Integer page, Integer size, Boolean orderAsc,String busca,String user_id) {
        this.nameOrder = nameOrder;
        this.page = page;
        this.size = size;
        this.orderAsc = orderAsc;
        this.busca = busca;
        this.user_id=user_id;
    }
    
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBusca() {
        return busca;
    }

    public void setBusca(String busca) {
        this.busca = busca;
    }

    public String getNameOrder() {
        return nameOrder;
    }

    public void setNameOrder(String nameOrder) {
        this.nameOrder = nameOrder;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Boolean getOrderAsc() {
        return orderAsc;
    }

    public void setOrderAsc(Boolean orderAsc) {
        this.orderAsc = orderAsc;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }


}
