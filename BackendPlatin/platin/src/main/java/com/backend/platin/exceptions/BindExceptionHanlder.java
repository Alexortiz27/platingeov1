/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.exceptions;

import com.backend.platin.helper.Response;
import io.netty.handler.codec.http.HttpContentEncoder.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class BindExceptionHanlder {

    private static final Logger logger = LoggerFactory.getLogger(BindExceptionHanlder.class);

    @ExceptionHandler(BindException.class)
    public Response handleBindException(BindException ex) {
        // ex.getFieldError (): aleatoriamente devuelve información de excepción para una propiedad de objeto. Si desea devolver toda la información de excepción de propiedad de objeto a la vez, llame a ex.getAllErrors ()
        FieldError fieldError = ex.getFieldError();
                    StringBuilder sb = new StringBuilder();
            sb.append(fieldError.getField()).append("=[").append(fieldError.getRejectedValue()).append("]")
                    .append(fieldError.getDefaultMessage());
        // generar resultado de retorno
        Response errorResult = new Response(sb.toString(),400);
        return errorResult;
    }
}
