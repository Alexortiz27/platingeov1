/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.exceptions;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.validation.BindingResult;

/**
 *
 * @author FAMLETO
 */
public class EmptyFieldsException  extends Exception{
       
    public EmptyFieldsException(String message)
    {
        super(message);    
    }
    
}
