/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.exceptions;

/**
 *
 * @author Joshua Ly
 */
public class GeneralException extends Exception {
    
    public GeneralException(String message)
    {
        super(message);
    }
}
