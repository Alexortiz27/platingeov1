/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.exceptions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author Propietario
 */
public class MyJsonDateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(com.fasterxml.jackson.core.JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Date fecha = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String date = jsonParser.getText();
        try {
            return format.parse(date);
        } catch (ParseException e) {
            try {
                fecha = format.parse("2000-01-01T00:01:00.000Z");
            } catch (ParseException ex) {
            }
            return fecha;
        }

    }

}
