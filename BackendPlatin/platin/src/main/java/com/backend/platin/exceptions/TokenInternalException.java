/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.exceptions;

import com.backend.platin.util.Util;

/**
 *
 * @author FAMLETO
 */
public class TokenInternalException extends Exception {
    
    public TokenInternalException()
    {
        super(Util.TOKEN_FALLIDO);
    }
    
}
