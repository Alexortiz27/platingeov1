package com.backend.platin.util;

import com.google.gson.GsonBuilder;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class JSONParseUtils {
	 private static Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss").disableHtmlEscaping().create();


	    public JSONParseUtils() {
	    }

	    public static String object2JsonString(Object obj) {
	        return obj == null ? null : gson.toJson(obj);
	    }


	    public static <T> T json2Object(String jsonString, Class<T> resultType) {
	        return Strings.isNullOrEmpty(jsonString) ? null : gson.fromJson(jsonString, resultType);
	    }

	    public static <T> T json2GenericObject(String jsonString, TypeToken<T> typeToken) {
	        return gson.fromJson(jsonString, typeToken.getType());
	    }

}
