package com.backend.platin.util;

public class NsqChannelConst {
    //public final static String DEFAULT_CHANNEL = "default";

    /**
     * The constant INGEST_CHANNEL.
     */
    public final static String INGEST_CHANNEL = "ingestChannel";

    public final static String CHANNEL_CAL_GEOZONA = "ChannelCalGeozona";
    public final static String CHANNEL_REPORT_SPEED = "ChannelReportSpeed";
    public final static String WEBSOCKET_WORKER_CHANNEL = "WebSocketChannel";
    public final static String CHANNEL_STOPPEDCISTERN = "ChannelStoppedCistern";
    public final static String CHANNEL_STOPPEDCISTERNFOUR = "ChannelStoppedCisternFour";
    public final static String CHANNEL_REPORTTRAVELS = "ChannelReportTravels";
    public final static String CHANNEL_REPORTTRASMISSION = "ChannelReportTrasmission";
    public final static String CHANNEL_REPORTTIMELAG = "ChannelReportTimeLag";
    public final static String REPORTS_CHANNEL = "ReportsChannel";
   
}
