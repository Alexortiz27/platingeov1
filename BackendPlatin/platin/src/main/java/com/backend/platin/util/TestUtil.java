package com.backend.platin.util;

import com.backend.ingest.dtos.CoordInDto;
import com.backend.ingest.dtos.PositionDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.service.services.GeozoneService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.google.gson.*;
import com.vividsolutions.jts.geom.Coordinate;
import io.lettuce.core.api.sync.RedisCommands;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.awt.geom.Path2D;
import java.io.*;
import java.net.InetAddress;
import java.text.ParseException;
import java.util.*;

public class TestUtil {
    private final Logger log = LogManager.getLogger(TestUtil.class);
    @Autowired(required = true)
    private GeozoneService geozoneService;

    @Autowired(required = true)
    private EventRedisHsetService eventRedis;
    @Autowired
    RedisCommands<String, String> jedis;

    //Util u = new Util();
    public boolean isNull(Object valueField) {
        if (valueField == null || (valueField.getClass().equals(String.class) && ((String) valueField).isEmpty())) {
            return true;
        }
        return false;
    }

    @Test
    @DisplayName("Test!")
    public void validarEmail() {
        Util u = new Util();
        boolean exis = u.validEmail("ortizvega@gmail.com");
        Assertions.assertEquals(true, exis, "El Correo es Valido");
        Assertions.assertNotEquals(false, exis, "El Correo no es Valido");
    }
    @Test
    @DisplayName("calculate!")
    public void calculate() {

        double numero1 = 2;
        double numero2 = 4;

        double totalMulti = numero1 * numero2;

        System.out.println("La Multiplicacion de esos 2 numeros es :"+totalMulti);

    }
    @Test
    @DisplayName("TestString!")
    public void validarString() {
        String key = null;
        boolean exis = isNull(key);

        if (!exis) {
            System.out.println("El key no es null");
        }
        if (exis) {
            System.out.println("El key es null");
        }
        System.out.println("El key es:" + exis);

        Assertions.assertEquals(true, exis, "El Correo es Valido");
        Assertions.assertNotEquals(false, exis, "El Correo no es Valido");
    }

    @Test
    @DisplayName("TramaStringAndObject!")
    public void TramaStringAndObject() {
        Util u = new Util();
        TramaInsDto tr = null;
        try {
            tr = u.parseJsonToTramaDto("{\"event\":\"TestREDISj\",\"plate\":\"MERC001\",\"position\":{\"latitude\":-12.054126219274595,\"longitude\":-77.14071750640869,\"altitude\":44.2222},\"gpsDate\":\"Nov 24, 2021, 5:09:14 AM\",\"speed\":250.0,\"receiveDate\":\"Nov 24, 2021, 5:09:15 AM\",\"tokenTrama\":\"1511FA4C-8806-496E-B9F8-A4AED00B5B4A\",\"odometer\":200.0,\"ru_gps_date\":\"1637730554943\"}");
            System.out.println(tr.getPlate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Assertions.assertNotEquals(false, tr, "El Trama no es Valido");
        Assertions.assertEquals(true, tr, "La trama es valida");
    }

    public interface Task {
        void doSomething(JsonObject jsonResult, Map<Object, Object> addres);
    }

    @Test
    @DisplayName("TestIfOpti!")
    public void TestIfOpti() {
        Map<String, Task> map = new HashMap<String, Task>();
        map.put("someString", new Task() {
            @Override
            public void doSomething(JsonObject jsonResult, Map<Object, Object> addres) {
                if (jsonResult.has("display_name") && !jsonResult.get("display_name").isJsonNull()) {
                    addres.put("display_name", jsonResult.get("display_name").getAsString());
                }
            }
        });
        map.put("someOtherString", new Task() {
            @Override
            public void doSomething(JsonObject jsonResult, Map<Object, Object> addres) {
                if (jsonResult.has("name") && !jsonResult.get("name").isJsonNull()) {
                    addres.put("name", jsonResult.get("name").getAsString());
                }
            }
        });

        //String vuelto =   map.get("someOtherString").doSomething();
    }

    public void agregar(JsonObject jsonResult, Map<Object, Object> addres, String type) {

        if (jsonResult.has(type) && !jsonResult.get(type).isJsonNull()) {
            addres.put(type, jsonResult.get(type).getAsString());
        }
    }

    @Test
    @DisplayName("TramaStringAndObject!")
    public void TramaStringAndObjectv1() {


        Map<Object, Object> addres = new HashMap<Object, Object>();
        PositionDto position = new PositionDto(-11.122760509290623, -77.61051177978517, 22);
        try {
            if (position != null && position.getLatitude() != 0 && position.getLongitude() != 0) {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                String uriWithParameters = "https://nominatim.openstreetmap.org/reverse?lat=" + position.getLatitude() + "&lon=" + position.getLongitude() + "&format=jsonv2&addressdetails=1&extratags=1";
                RestTemplate restTemplate = restTemplate();
                ResponseEntity<String> result = restTemplate.getForEntity(uriWithParameters, String.class);

                if (result != null && result.getBody() != null) {
                    String jsonAddressString = result.getBody();
                    JsonObject jsonResult = new JsonParser().parse(jsonAddressString).getAsJsonObject();
                    JsonObject jsonAddress = jsonResult.get("address").getAsJsonObject();

                    if (jsonResult != null && jsonResult.has("address")) {
                        String[] types = new String[]{"name", "display_name", "house_number", "road", "city", "region", "postcode", "country",
                                "country_code", "state", "town"};
                        for (String type : types) {
                            switch (type) {
                                case "name":
                                    agregar(jsonResult, addres, type);
                                    break;
                                case "display_name":
                                    agregar(jsonResult, addres, type);
                                    break;
                                default:
                                    agregar(jsonAddress, addres, type);
                            }


                        }


                    } else {
                        System.out.println("error2");
                    }

                } else {
                    System.out.println("error3");
                }
            } else {
                System.out.println("error4");
            }
        } catch (Exception ex) {
            System.out.println("errortry" + ex);

        }
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public int go() {
        int[] myNumbers = {1, 2, 3};
        //System.out.println(myNumbers[10]);
        return myNumbers[10];
    }

    @Test
    @DisplayName("TRY!")
    public void TryCatch() {
        try {
            boolean isDifferent = false;
            String[] permitted_events = {"none", "acc_on", "acc_off", "battery_ct", "battery_dc", "sos"};

            for (String permitted_event : permitted_events) {
                if (permitted_event.equals("acc_on")) {
                    isDifferent = true;
                    break;
                }
            }
            if(!isDifferent){
                System.out.println ("EL EVENTO NOO SI EXISTE");
            }

//            String ip = InetAddress.getLocalHost().getHostAddress();
//            // Obtener el nombre de la computadora
//            String name = InetAddress.getLocalHost().getHostName();
//            // imprimir
//            System.out.println ("Dirección IP:" + ip);
//            System.err.println ("Nombre del equipo:" + name);
//            go();

        } catch (Exception e) {
            int currentLine = e.getStackTrace()[0].getLineNumber();
            String metodo = e.getStackTrace()[0].getMethodName();
            String clas = e.getStackTrace()[0].getClassName();
            System.out.println("Error en la clase:" + clas + " en el metodo:" + metodo + " y en la linea:" + currentLine);
            log.info("Error Worker WEBSOCKET_WORKER_CHANNEL:" + e.getMessage());
            log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
            e.printStackTrace();
        }

//        Assertions.assertNotEquals(false, tr, "El Trama no es Valido");
//        Assertions.assertEquals(true, tr, "La trama es valida");
    }


    @Test
    @DisplayName("repeated")
    public void repeated() {
        Path2D path = new Path2D.Double();
        //Definimos el primer punto como el punto de inicio con moveTo
        path.moveTo(-38.68750711967124, -72.68457327069606);
        // Utilizamos el método lineTo para ir definiendo las líneas
        path.lineTo(-38.68813914788199, -72.68296848437275);
        path.lineTo(-38.688780103816896, -72.68343011690975);
        path.lineTo(-38.688168934781345, -72.68492519454888);

        //Cerramos la figura geométrica en caso los puntos proporcionados no lo hagan.
        path.closePath();

        // Verificamos si las coordenadas están dentro con el método contains
        System.out.println(path.contains(-38.68811913797079, -72.68355687984929));
        System.out.println(path.contains(-38.68675112946928, -72.68677040517952));
    }

    @Test
    @DisplayName("calculategeo")
    public void calculategeo() throws IOException {
        com.backend.platin.consumer.util.Util u = new com.backend.platin.consumer.util.Util();
        ArrayList<Coordinate> points = new ArrayList<Coordinate>();
        double latitud = -12.054126219274595;
        double longitude = -77.14071750640869;
        Coordinate coordTrama = new Coordinate(latitud, longitude);
        Gson gson = new Gson();
        double radius;
        try {
            BufferedReader br = new BufferedReader(new FileReader(
                    "src/test/list.json"));
            JsonArray jsonArray = new JsonParser().parse(br).getAsJsonArray();
            for (JsonElement jsonElement : jsonArray) {
                String listCoords = jsonElement.getAsJsonObject().get("coords").getAsString();
                radius = Double.parseDouble(jsonElement.getAsJsonObject().get("radius").getAsString());
                points.clear();
                String category = jsonElement.getAsJsonObject().get("category").getAsString();
                if (category.equals("polygon")) {
                    JsonArray coords = new JsonParser().parse(listCoords).getAsJsonArray();
                    u.coordPolygon(points, coords);
                    boolean outPolygon = u.isPointPolygon(points, coordTrama);
                    if (outPolygon) {
                        log.info("LAS COORDENADAS ESTA DENTRO DEL POLIGONO");
                    } else {
                        log.info("LAS COORDENADAS NO ESTA DENTRO DEL POLIGONO");
                    }
                } else {
                    CoordInDto coordi = u.coordCircle(listCoords);
                    double y1 = longitude;
                    double x1 = latitud;
                    double y2 = coordi.getLatitude();
                    double x2 = coordi.getLongitude();
                    boolean outCircle = u.isPointInCircle(y2, x2, radius, x1, y1);
                    if (outCircle == true) {
                        log.info("ENTRO DENTRO CIRCLE DE LA GEOZONA:");

                    } else {
                        log.info("SALIO CIRCLE DE LA GEOZONA:");
                    }
                }
            }
        } catch (IOException e) {

        }
    }

}
