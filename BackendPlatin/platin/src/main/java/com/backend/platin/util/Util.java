    package com.backend.platin.util;

import com.backend.ingest.dtos.*;
import com.backend.ingest.entities.*;
import com.backend.ingest.service.services.GeozoneService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.backend.platin.exceptions.EmptyFieldsException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import static org.apache.poi.ss.util.CellUtil.createCell;

import org.apache.poi.xssf.usermodel.XSSFFont;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.client.RestTemplate;

/**
 * @author usuario
 */
public class Util {

    public static boolean ACTIVE_STATUS = true;
    public static Integer ACTIVE_STATUS_BIT = 1;
    public static Integer INACTIVE_STATUS_BIT = 0;

    public static boolean INACTIVE_STATUS = false;

    public static String DATA_NO_FOUND = "no existe en la base de datos!";
    public static String TOKEN_FALLIDO = "INCORRECT CREDENTIALS!";

    public static Integer size = 10;
    public static Integer page = 0;
    public static String nameOrder = "status";

    public static String A_STATUS = "1";
    public static String I_SATUS = "0";
    public static String POLYGON = "polygon";
    public static String CIRCLE = "circle";

    public static String PARAM_STATUS_ACTIVE = "Habilitado";
    public static String PARAM_STATUS_INACTIVE = "Inhabilitado";
    public static String PARAM_ACTIVE = "EN MOVIMIENTO";
    public static String PARAM_INACTIVE = "DETENIDO";
    //TYPE REPORT'S VARIABLES
    public final static String GENERAL_REPORT = "GENERALREPORT";
    public final static String STANDARD_LAST_POS_REPORT = "STANDARD_LAST_POS";
    public final static String TRAVEL_REPORT = "TRAVEL";
    public final static String STOPPED_REPORT = "STOPPED";
    public final static String STOPPED_REPORTFOUR = "STOPPEDFOUR";
    public final static String GEOZONES_REPORT = "GEOZONES";
    public final static String NO_TRANSMISSION_REPORT = "NO_TRANSMISSION";
    public final static String SPEED_REPORT = "SPEED";
    public final static String TIMELAG_REPORT = "TIMELAG";
    public final static String TYPEREPORTALL = "TYPEREPORTALL";

    //TYPE VEHICLE MOVEMENT EVENT VARIABLES
    public static String TYPE_EVENT_CISTERN_STOPPED = "STOPPED";
    public static String TYPE_EVENT_CISTERN_MOVEMENT = "MOVEMENT";

    // WEBSOCKET REPORT RESPONSE
    public static String RESPONSE_OK = "Reporte generado con éxito";
    public static String RESPONSE_ERROR = "No se ha podido generar el reporte";

    //WORKER TRASSMISION PUBLIC VARIABLES
    public static String NOTIFYNTDATE = "notifyNTDate";
    public static final String TYPELOSTTRASMISSION = "LOSTTRASMISSION";
    public static final String TYPERECOVERYTRASMISSION = "RECOVERYTRASMISSION";
    //WORKER DESFASEDETIEMPO PUBLIC VARIABLES
    public static final String TYPETIMELAG = "TIMELAG";
    //WORKER STOPPEDCISTERN PUBLIC VARIABLES
    public static int nvecesStopped = 0;
    public static int nvecesStoppedFour = 0;
    public static int nvecesCantidadViajes = 0;
    public static final String TYPESTOPPED = "STOPPED";
    public static final String TYPEMOVEMENT = "MOVEMENT";
    public static final String TYPESTOPPEDFOUR = "STOPPEDFOUR";
    public static final String TYPEMOVEMENTFOUR = "MOVEMENTFOUR";
    public static final String TYPEMOVEMENTSTATUS = "movementStatus";
    public static final String TYPEMOVEMENTSTATUSFOUR = "movementStatusFour";
    //WORKER TRAVELS PUBLIC VARIABLES
    public static final String TYPESTARTOUT = "STARTOUT";
    public static final String TYPEENDIN = "ENDIN";
    //WORKER SPEDD PUBLIC VARIABLES
    public static final String TYPEMAXIMUNSPEED = "MAXIMUNSPEED";
    //WORKER CALCULATEGEOCERCA PUBLIC VARIABLES
    public static final String TYPEINCOMEREPORT = "incomeReport";
    public static final String TYPEEXITREPORT = "exitReport";

    //    //variable of the last position of vehicles into a report
    public final static String LAST_POS_VEHICLES_REPORT = "LAST_POS_VEHICLES";

    public final static String USER_COMPLETE_NAMES_ORDER = "userCompleteNames";

    public static String USER_ID = "";

    public static String BEARER_TOKEN = "";

    //GLOBAL REPORT
    public final static String GLOBAL_REPORT = "GLOBAL";

    // ENTITIES TRANSALATED IN SPANISH
    public static String COMPANY_NOT_FOUND = "La EMV ";
    public static String COMPANY_CLIENT_NOT_FOUND = "La empresa ";
    public static String EVENT_NOT_FOUND = "El evento ";
    public static String GEOZONE_NOT_FOUND = "La geozona ";
    public static String MAP_NOT_FOUND = "El mapa ";
    public static String MARKER_NOT_FOUND = "El marker ";
    public static String PERMIT_NOT_FOUND = "El permiso ";
    public static String REPORT_NOT_FOUND = "El reporte ";
    public static String ROL_NOT_FOUND = "El rol ";
    public static String SESSION_NOT_FOUND = "La sesión ";
    public static String TRAVEL_NOT_FOUND = "El viaje ";
    public static String USER_NOT_FOUND = "El usuario ";
    public static String VEHICLE_NOT_FOUND = "El vehículo ";
    public static String TRAMA_NOT_FOUND = "La trama ";
    public static String TRANSMITTING_STATUS = "1";
    public static String NO_TRANSMITTING_STATUS = "0";

    //PARAMETER ALL
    public static String PARAM_ALL = "ALL";
    
    public  static String SESSION_ID= null;
    
    // ACTIONS OF ACTION TABLE 
    public static String ACTION_CREATE="REGISTRO";
    public static String ACTION_UPDATE="ACTUALIZACION";
    public static String ACTION_DELETE="ELIMINACION";
    
    //TABLES OF ACTION
    public static String GEOZONE_TABLE_NAME="GEOZONA";
    public static String MAP_TABLE_NAME="MAPA";
    public static String MARKER_TABLE_NAME="MARKER";
    
    // REPORT STATUS
    public static String REQUESTED_STATUS= "Solicitado";
    public static String PROCESSING_STATUS="Procesando";
    public static String COMPLETED_STATUS="Completado";
    public static String FAILED_STATUS="Fallido";
    
    public final static String EMPTY_FIELD_MESSAGE="no puede estar vacío(a)";
    
    //TYPE OF LAST POSITION OF VEHICLES
    public final static String LAST_POSITION_NOW ="LAST_POS_NOW";
    public final static String LAST_FIVE_MINUTES_OF_POSITION="LAST_FIVE_POS";
    public final static String LAST_TEN_MINUTES_OF_POSITION ="LAST_TEN_POS";
    public final static String LAST_MORE_TEN_MINUTES_POSITION="LAST_MORE_POS";
    
    @Bean
    public static RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
    public static final Integer NSQ_MSG_TIMEOUT = 1800;

    public final static String AMENITY_ADDRESS="amenity";
    public final static String ROAD_ADDRESS="road";
    public final static String CITY_ADDRESS="city";
    public final static  String STATE_ADDRESS="state";

    public static void isEmptyField(BindingResult result) throws EmptyFieldsException {

        if (result.hasErrors()) {

            List<String> errors = result.getFieldErrors().stream()
                    .map(err -> "El Campo'" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            throw new EmptyFieldsException(errors.toString());
        }

    }

    public static XSSFSheet writeHeaderLine(XSSFWorkbook workbook, String typeReport, String plate, CellStyle style) throws ParseException {
        XSSFSheet sheet;
        sheet = workbook.createSheet("Tramas");
        int rowCount = 1;
        Row row = sheet.createRow(rowCount);

        CreationHelper creationHelper = workbook.getCreationHelper();
        int columnCount = 0;
        if (!plate.equals(PARAM_ALL)) {
            createCell(row, 0, "Placa", style);
            rowCount++;
            row = sheet.createRow(rowCount);

            createCell(row, 0, "EMV", style);
            rowCount++;
            row = sheet.createRow(rowCount);

            createCell(row, 0, "Cliente", style);
            rowCount++;
            row = sheet.createRow(rowCount);

            createCell(row, 0, "Codigo de Osinergmin", style);
            rowCount++;
            row = sheet.createRow(rowCount);

            createCell(row, 0, "Estado de Vehículo", style);
            rowCount++;
            row = sheet.createRow(rowCount);

            createCell(row, 0, "Url del Mapa", style);
            rowCount++;
            row = sheet.createRow(rowCount);

            createCell(row, 0, "Fecha de Generación", style);
            rowCount++;
            row = sheet.createRow(rowCount);

            createCell(row, 0, "Desde", style);
            rowCount++;
            row = sheet.createRow(rowCount);

            createCell(row, 0, "Hasta", style);
        } else {

            createCell(row, columnCount++, "Placa", style);
            rowCount++;
            row = sheet.createRow(rowCount);

            createCell(row, columnCount++, "EMV", style);

            createCell(row, columnCount++, "Cliente", style);

            createCell(row, columnCount++, "Codigo de Osinergmin", style);

            createCell(row, columnCount++, "Estado de Vehículo", style);

            createCell(row, columnCount++, "Url del Mapa", style);

            createCell(row, columnCount++, "Fecha de Generación", style);

            createCell(row, columnCount++, "Desde", style);

            createCell(row, columnCount++, "Hasta", style);

        }
        if (typeReport.equals(STANDARD_LAST_POS_REPORT) && !plate.equals(PARAM_ALL)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            writeCell(row, 0, "Kms Recorridos", style, sheet, creationHelper);

            rowCount = rowCount + 2;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Fecha", style);
            createCell(row, 1, "Latitud", style);
            createCell(row, 2, "Longitud", style);
        } else if (typeReport.equals(STANDARD_LAST_POS_REPORT)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            writeCell(row, columnCount++, "Kms Recorridos", style, sheet, creationHelper);

            createCell(row, columnCount++, "Fecha", style);
            createCell(row, columnCount++, "Latitud", style);
            createCell(row, columnCount++, "Longitud", style);

        }

        if (typeReport.equals(SPEED_REPORT) && !plate.equals(PARAM_ALL)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            writeCell(row, 0, "Total de excesos", style, sheet, creationHelper);

            rowCount++;
            row = sheet.createRow(rowCount);

            writeCell(row, 0, "Valor configurado", style, sheet, creationHelper);
//            rowCount++;
//            row = sheet.createRow(rowCount);

            rowCount = rowCount + 2;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Fecha Spedd", style);
            createCell(row, 1, "Latitud", style);

            createCell(row, 2, "Longitud", style);
            createCell(row, 3, "Spedd", style);
        } else if (typeReport.equals(SPEED_REPORT)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, columnCount++, "Total de excesos", style);
            createCell(row, columnCount++, "Valor configurado", style);

            createCell(row, columnCount++, "Fecha Spedd", style);
            createCell(row, columnCount++, "Latitud", style);

            createCell(row, columnCount++, "Longitud", style);
            createCell(row, columnCount++, "Spedd", style);

        }

        if (typeReport.equals(NO_TRANSMISSION_REPORT) && !plate.equals(PARAM_ALL)) {

            rowCount = rowCount + 2;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Fecha de Transmisión", style);
            createCell(row, 1, "Tipo", style);

            createCell(row, 2, "Latitud", style);
            createCell(row, 3, "Longitud", style);
        } else if (typeReport.equals(NO_TRANSMISSION_REPORT)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, columnCount++, "Fecha de Transmisión", style);
            createCell(row, columnCount++, "Tipo", style);

            createCell(row, columnCount++, "Latitud", style);
            createCell(row, columnCount++, "Longitud", style);
        }

        if (typeReport.equals(TIMELAG_REPORT) && !plate.equals(PARAM_ALL)) {
            rowCount++;

            rowCount = rowCount + 1;
            row = sheet.createRow(rowCount);

            createCell(row, 0, "Fecha Recepción", style);
            createCell(row, 1, "Fecha GpsDate", style);

            createCell(row, 2, "Latitud", style);
            createCell(row, 3, "Longitud", style);
        } else if (typeReport.equals(TIMELAG_REPORT)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, columnCount++, "Fecha Recepción", style);

            createCell(row, columnCount++, "Fecha GpsDate", style);
            createCell(row, columnCount++, "Latitud", style);
            createCell(row, columnCount++, "Longitud", style);
        }

        if ((typeReport.equals(STOPPED_REPORT) || typeReport.equals(STOPPED_REPORTFOUR)) && !plate.equals(PARAM_ALL)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Cantidad de detenciones", style);

            rowCount = rowCount + 2;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Fecha", style);
            createCell(row, 1, "Latitud", style);
            createCell(row, 2, "Longitud", style);
            createCell(row, 3, "Inicio Detenido", style);
            createCell(row, 4, "Final detenido", style);
            createCell(row, 5, "Tiempo detenido", style);

        } else if (typeReport.equals(STOPPED_REPORT)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, columnCount++, "Cantidad de detenciones", style);

            createCell(row, columnCount++, "Fecha", style);
            createCell(row, columnCount++, "Latitud", style);
            createCell(row, columnCount++, "Longitud", style);
            createCell(row, columnCount++, "Inicio Detenido", style);
            createCell(row, columnCount++, "Final detenido", style);
            createCell(row, columnCount++, "Tiempo detenido", style);

        }

        if (typeReport.equals(GEOZONES_REPORT) && !plate.equals(PARAM_ALL)) {
            rowCount = rowCount + 2;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Fecha", style);
            createCell(row, 1, "Tipo de evento", style);
            createCell(row, 2, "Tipo de geozona", style);
            createCell(row, 3, "Geozona", style);
        } else if (typeReport.equals(GEOZONES_REPORT)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, columnCount++, "Fecha", style);

            createCell(row, columnCount++, "Tipo de evento", style);
            createCell(row, columnCount++, "Tipo de geozona", style);
            createCell(row, columnCount++, "Geozona", style);

        }

        if (typeReport.equals(TRAVEL_REPORT) && !plate.equals(PARAM_ALL)) {

            //VERTICAL HEADERS
            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Cantidad de viajes", style);

            //HORIZONTAL HEADERS
            rowCount = rowCount + 2;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "ViajeId", style);
            createCell(row, 1, "Nombre", style);
            createCell(row, 2, "Inicio Coordenadas", style);
            createCell(row, 3, "Inicio(Hora)", style);
            createCell(row, 4, "Final Coordenadas", style);
            createCell(row, 5, "Final(Hora)", style);
            createCell(row, 6, "Tiempo", style);
            createCell(row, 7, "Kilometros Recorridos", style);

        } else if (typeReport.equals(TRAVEL_REPORT)) {
            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, columnCount++, "Cantidad de viajes", style);

            createCell(row, columnCount++, "ViajeId", style);
            createCell(row, columnCount++, "Nombre", style);
            createCell(row, columnCount++, "Inicio Coordenadas", style);
            createCell(row, columnCount++, "Inicio(Hora)", style);
            createCell(row, columnCount++, "Final Coordenadas", style);
            createCell(row, columnCount++, "Final(Hora)", style);
            createCell(row, columnCount++, "Tiempo", style);
            createCell(row, columnCount++, "Kilometros Recorridos", style);
        }

        if (typeReport.equals(GLOBAL_REPORT) && !plate.equals(PARAM_ALL)) {

            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Dirección", style);

            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Distrito", style);

            rowCount = rowCount + 2;
            row = sheet.createRow(rowCount);
            createCell(row, 0, "Latitud", style);
            createCell(row, 1, "Longitud", style);
            createCell(row, 2, "Altitud", style);
            createCell(row, 3, "Velocidad", style);
            createCell(row, 4, "Fecha GPS", style);
            createCell(row, 5, "Evento", style);
            createCell(row, 6, "Geozona", style);
            createCell(row, 7, "Fecha y Hora del Mensaje", style);

        } else {
            rowCount++;
            row = sheet.createRow(rowCount);
            createCell(row, columnCount++, "Dirección", style);

            createCell(row, columnCount++, "Distrito", style);

            createCell(row, columnCount++, "Latitud", style);
            createCell(row, columnCount++, "Longitud", style);
            createCell(row, columnCount++, "Altitud", style);
            createCell(row, columnCount++, "Velocidad", style);
            createCell(row, columnCount++, "Fecha GPS", style);
            createCell(row, columnCount++, "Evento", style);
            createCell(row, columnCount++, "Geozona", style);
            createCell(row, columnCount++, "Fecha y Hora del Mensaje", style);
        }

        return sheet;
    }

    public static void generateDirIfDoesntExist(String path) {

        File theDir = new File(path);

        if (!theDir.exists()) {
            theDir.mkdirs();
        }
    }

    public static void writeCell(Row row, int columnCount, Object value, CellStyle style, XSSFSheet sheet, CreationHelper creationHelper) throws ParseException {

        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof String) {
            cell.setCellValue((String) value);
        } else if (value instanceof Boolean) {
            Boolean status = (Boolean) value;
            if (status) {
                cell.setCellValue(Util.PARAM_STATUS_ACTIVE);
            } else {
                cell.setCellValue(Util.PARAM_STATUS_INACTIVE);
            }
        } else if (value instanceof Date) {
            cell.setCellValue((Date) value);
            style.setDataFormat(creationHelper.createDataFormat().getFormat(
                    "dd/mm/yyyy HH:mm:ss"));
        }//else 
//        {
//            cell.setCellValue((double) value);
//        }
        cell.setCellStyle(style);
    }

    public static String[] generateGeneralHeadersGeoJson(String typeReport) {
        String[] headers = null;
        switch (typeReport) {
            case GENERAL_REPORT:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta"};
                break;

            case STANDARD_LAST_POS_REPORT:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Latitud", "Longitud", "Kms Recorridos","Velocidad","Fecha GPS"};
                break;

            case SPEED_REPORT:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Fecha de Velocidad", "Latitud", "Longitud", "Velocidad"};
                break;

            case NO_TRANSMISSION_REPORT:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Fecha Trasmission", "Tipo", "Latitud", "Longitud","Velocidad"};
                break;

            case TIMELAG_REPORT:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Fecha de Recepción", "Fecha de Gps", "Latitud", "Longitud","Velocidad"};
                break;

            case GEOZONES_REPORT:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Fecha Geozona", "Tipo", "Geozona","Velocidad"};
                break;

            case STOPPED_REPORT:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Fecha", "Latitud", "Longitud", "Inicio detenido", "Final detenido", "Tiempo definido","Velocidad","Fecha GPS"};
                break;

            case STOPPED_REPORTFOUR:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Fecha", "Latitud", "Longitud", "Inicio detenido", "Final detenido", "Tiempo definido","Velocidad","Fecha GPS"};
                break;

            case TRAVEL_REPORT:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "ViajeId", "Nombre", "Inicio Coordenadas", "Inicio(Hora)", "Final Coordenadas", "Final(Hora)", "Tiempo", "Kilometros Recorridos","Velocidad","Odometro"};
                break;
            case GLOBAL_REPORT:
                headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Latitud", "Longitud", "Altitud", "Velocidad", "Fecha Gps", "Evento", "Geozona", "Fecha y Hora del mensaje"};
                break;

        }
        return headers;
    }

    public static String[][] generateGeneralBodyGeoJson(List<Trama> tramas, String geoUrlMap) throws DataNotFoundException {
        String[][] body = new String[tramas.size()][9];
        int columnCount = 0;
        for (int rowCount = 0; rowCount < tramas.size(); rowCount++) {

            body[rowCount][columnCount++] = tramas.get(rowCount).getPlate();

            Vehicle vehicle = new Vehicle();
//            vehicle=vehicle.findByPlate(tramas.get(rowCount).getPlate());
//            body[rowCount][columnCount++]=vehicle.getCompany().getName();
//            body[rowCount][columnCount++]=vehicle.getCompany().getCompanyClient().get(0).getName();
//            body[rowCount][columnCount++]=vehicle.getcode_osinergmin();
//            body[rowCount][columnCount++]=vehicle.getStatus()?PARAM_STATUS_ACTIVE:PARAM_STATUS_INACTIVE;

            body[rowCount][columnCount++] = geoUrlMap;
            body[rowCount][columnCount++] = parseDateToPrinterFormatString(new Date());
            body[rowCount][columnCount++] = parseDateToPrinterFormatString(tramas.get(0).getGpsDate());
            body[rowCount][columnCount++] = parseDateToPrinterFormatString(tramas.get(tramas.size() - 1).getGpsDate());
        }

        return body;
    }

    public static void generateExcelReportFile(Workbook workbook, String uuid, String xlsPath) throws FileNotFoundException, IOException {
        String xlsname = xlsPath + "/" + uuid + ".xls";

        FileOutputStream fileOut = new FileOutputStream(xlsname);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }

    public static void generateGeoJsonReportFile(String uuid, String geojsonPath) throws FileNotFoundException, IOException {
        String geoname = geojsonPath + "/" + uuid + ".geojson";
        FileWriter myWriter = new FileWriter(geoname);
//        myWriter.write(tramasJson);
//        myWriter.close();

    }

    public static String parseDatetimeToString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        return simpleDateFormat.format(date);
    }

    public static Date parseStringToDatetime(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.parse(date);
    }
    public static Date parseStringToDatetimev1(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.parse(date);
    }
    public static String parseDateToPrinterFormatString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(date);
    }

    public static String parseDatetoNsqFormatString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(date);
    }

    public static String parseDatetoJpaFormatString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return simpleDateFormat.format(date);
    }

    public static double calculateDistance(double latPoint1, double lngPoint1,
                                           double latPoint2, double lngPoint2) {
        if (latPoint1 == latPoint2 && lngPoint1 == lngPoint2) {
            return 0d;
        }

        final double EARTH_RADIUS = 6371.0; //km value;

        latPoint1 = Math.toRadians(latPoint1);
        lngPoint1 = Math.toRadians(lngPoint1);
        latPoint2 = Math.toRadians(latPoint2);
        lngPoint2 = Math.toRadians(lngPoint2);

        double distance = Math.pow(Math.sin((latPoint2 - latPoint1) / 2.0), 2)
                + Math.cos(latPoint1) * Math.cos(latPoint2)
                * Math.pow(Math.sin((lngPoint2 - lngPoint1) / 2.0), 2);
        distance = 2.0 * EARTH_RADIUS * Math.asin(Math.sqrt(distance));

        return distance; //km value
    }

    public static void generateGeoJsonReportFile(String uuid, String geojsonPath, String tramasJson) throws FileNotFoundException, IOException {
        String geoname = geojsonPath + "/" + uuid + ".geojson";
        FileWriter myWriter = new FileWriter(geoname);
        myWriter.write(tramasJson);
        myWriter.close();

    }

    public static void sendAlertEvent(Event event, String uri, String bearerToken) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        EventDto eventDto = new EventDto(event.getType(), event.getVehicle_id().getId(), event.getGeozone_id(), event.getLatitude(),
                event.getLongitude(), event.getReceiveDate(), event.getGpsDate(), event.getSpedd(), event.getIdTravel(), event.getNameTravel());
        String requestJson = new Gson().toJson(eventDto);

        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
        RestTemplate restTemplate = restTemplate();
        restTemplate.postForObject(uri + "/" + bearerToken, entity, String.class);
    }
    public static void sendAlertEventInst(EventInsDto event, String uri, String bearerToken) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        EventDto eventDto = new EventDto(event.getType(), event.getVehicle_id(), event.getGeozone_id(), event.getLatitude(),
                event.getLongitude(), event.getReceiveDate(), event.getGpsDate(), event.getSpedd(), event.getIdTravel(), event.getNameTravel());
        String requestJson = new Gson().toJson(eventDto);

        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
        RestTemplate restTemplate = restTemplate();
        restTemplate.postForObject(uri + "/" + bearerToken, entity, String.class);
    }
    public static String parseDatetoRestClientFormatString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        return simpleDateFormat.format(date);
    }

    public static CompanyShowDto parseCompanyToShowDto(Company company) {
        List<CompanyClientShowDto> companiesClientDto = new ArrayList<>();
        for (Historial_Company historial : company.getHistorial()) {
            CompanyClientShowDto companyClientDto = new CompanyClientShowDto(historial.getCompanyClient().getId(),
                    historial.getCompanyClient().getName(), historial.getCompanyClient().getEmail(),
                    historial.getCompanyClient().getRuc(), historial.getCompanyClient().getAddress(),
                    historial.getCompanyClient().getPhone(), historial.getCompanyClient().getStatus(),
                    historial.getCompanyClient().getCreationDate());
            companiesClientDto.add(companyClientDto);
        }

        CompanyShowDto companyDto = new CompanyShowDto(company.getId(), company.getName(), company.getIngestToken(),
                company.getEmail(), company.getAddress(), company.getPhone(), company.getCity(), company.getCountry(), company.getStatus(), company.getCreationDate(), companiesClientDto, company.getUser());
        return companyDto;
    }

    public static CompanyClientListDto parseCompanyToShowDto(CompanyClient companyClient) {
        List<CompanyBasicDto> companies = new ArrayList<>();
        for (Historial_Company historial : companyClient.getHistorial()) {

            companies.add(parseCompanyToBasicDto(historial.getCompany()));
        }

        CompanyClientListDto companyClientDto = new CompanyClientListDto(companyClient.getId(), companyClient.getName(), companyClient.getEmail(),
                companyClient.getRuc(), companyClient.getAddress(), companyClient.getPhone(), companies, companyClient.getStatus(), companyClient.getCreationDate());
        return companyClientDto;
    }

    public static CompanyBasicDto parseCompanyToBasicDto(Company company) {

        CompanyBasicDto companyDto = new CompanyBasicDto(company.getId(), company.getName(), company.getIngestToken(),
                company.getEmail(), company.getAddress(), company.getPhone(), company.getCity(), company.getCountry(), company.getStatus(), company.getCreationDate(), company.getUser());
        return companyDto;
    }

    public static boolean validEmail(String email) {
        boolean arroba = false;
        for (int i = 0; i < email.length(); i++) {
            if (email.charAt(i) == '@') {
                arroba = true;
            }
        }
        return arroba;
    }

    public static Map<Object, Object> totalPagesElemnt(long total, int size) {
        Map<Object, Object> map = new HashMap<Object, Object>();
        double npage = Math.ceil(total / size);
        int totalpage = (int) npage;
        map.put("totalElements", total);
        map.put("totalPages", totalpage);
        return map;
    }
    public static Date convertDateUtcPeru(Date gpsdate) {
        Calendar calendarR = Calendar.getInstance();
        calendarR.setTime(gpsdate);
        calendarR.add(Calendar.HOUR, 5);
//        calendarR.setTimeInMillis(gpsdate.getTime() - 18000000);
        Date fechaSalidaR = calendarR.getTime();
        return fechaSalidaR;
    }
    public static Date convertDateUtcGmtPeru(Date gpsdate) {
        Calendar calendarR = Calendar.getInstance();
        calendarR.setTime(gpsdate);
        calendarR.add(Calendar.HOUR, -5);
//        calendarR.setTimeInMillis(gpsdate.getTime() - 18000000);
        Date fechaSalidaR = calendarR.getTime();
        return fechaSalidaR;
    }

    public static String convertDateUtcGmtPeruString(Date gpsdate) {
        Calendar calendarR = Calendar.getInstance();
        calendarR.setTime(gpsdate);
        calendarR.add(Calendar.HOUR, -5);
        Date fechaSalidaR = calendarR.getTime();
        return parseDatetoJpaFormatString(fechaSalidaR);
    }
    public static void agregar(JsonObject jsonResult, Map<Object, Object> addres,String type){

        if (jsonResult.has(type) && !jsonResult.get(type).isJsonNull()) {
            addres.put(type, jsonResult.get(type).getAsString());
        }
    }

    public static XSSFSheet generateBodyGLOVALREPORT(XSSFSheet sheet,CreationHelper creationHelper,GeozoneService geozoneService,
                                                     String[] excelHeaderR, List<Trama> tramas, List<Event> listEvent, CellStyle style, XSSFWorkbook wb) throws DataNotFoundException, ParseException {
        sheet = generateHeaderBodyType(13, sheet, excelHeaderR, style);
        Row rowe =null;
        int rowCount = 14;
        CellStyle stylee = wb.createCellStyle();
        XSSFFont headerFont = wb.createFont();
        headerFont.setFontHeightInPoints((short) 13);
        stylee.setFont(headerFont);
        for (Trama tramaObject : tramas) {
            int columnCount = 0;
            PositionDto position = new Gson().fromJson(tramaObject.getPosition(), PositionDto.class);
            rowe = sheet.createRow(rowCount);
            com.backend.platin.util.Util.writeCell(rowe, columnCount++, String.valueOf(position.getLatitude()), stylee, sheet, creationHelper);
            com.backend.platin.util.Util.writeCell(rowe, columnCount++, String.valueOf(position.getLongitude()), stylee, sheet, creationHelper);
            com.backend.platin.util.Util.writeCell(rowe, columnCount++, String.valueOf(position.getAltitude()), stylee, sheet, creationHelper);
            com.backend.platin.util.Util.writeCell(rowe, columnCount++, String.valueOf(tramaObject.getSpeed()), stylee, sheet, creationHelper);
            com.backend.platin.util.Util.writeCell(rowe, columnCount++, Util.convertDateUtcGmtPeruString(tramaObject.getGpsDate()), stylee, sheet, creationHelper);
            rowCount++;
        }
        rowCount = rowCount - tramas.size();
        int length = 0;
        if (listEvent.size() > tramas.size()) {
            length = tramas.size();
        } else {
            length = listEvent.size();
        }
        int counter = 0;
        if (listEvent.size() > 0) {
            int i = 0;
            length = counter + length;
            if (listEvent.size() < tramas.size()) {
                while (counter < length) {
                    int columnCount = 5;
                    rowe = sheet.getRow(rowCount);
                    com.backend.platin.util.Util.writeCell(rowe, columnCount++, translateTypeEventToSpanish(listEvent.get(i).getType()), stylee, sheet, creationHelper);
                    if (listEvent.get(i).getGeozone_id() != null && !listEvent.get(i).getGeozone_id().equals("")
                            && !listEvent.get(i).getGeozone_id().equals("null")) {
                        Geozone geozone = geozoneService.findById(listEvent.get(i).getGeozone_id());
                        if (geozone == null) {
                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
                        }
                        com.backend.platin.util.Util.writeCell(rowe, columnCount++, geozone.getName(), stylee, sheet, creationHelper);
                    } else {
                        com.backend.platin.util.Util.writeCell(rowe, columnCount++, " ", stylee, sheet, creationHelper);
                    }
                    com.backend.platin.util.Util.writeCell(rowe, columnCount++, Util.convertDateUtcGmtPeruString(listEvent.get(i).getCreationDate()), stylee, sheet, creationHelper);
                    rowCount++;
                    counter++;
                    i++;
                }
            } else {
                int a = 0;
                for (a = 0; a < tramas.size(); a++) {
                    int columnCount = 5;
                    rowe = sheet.getRow(rowCount);
                    com.backend.platin.util.Util.writeCell(rowe, columnCount++, translateTypeEventToSpanish(listEvent.get(a).getType()), stylee, sheet, creationHelper);
                    if (listEvent.get(a).getGeozone_id() != null && !listEvent.get(a).getGeozone_id().equals("")
                            && !listEvent.get(a).getGeozone_id().equals("null")) {
                        Geozone geozone = geozoneService.findById(listEvent.get(a).getGeozone_id());
                        if (geozone == null) {
                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
                        }
                        com.backend.platin.util.Util.writeCell(rowe, columnCount++, geozone.getName(), stylee, sheet, creationHelper);
                    } else {
                        com.backend.platin.util.Util.writeCell(rowe, columnCount++, " ", stylee, sheet, creationHelper);
                    }
                    com.backend.platin.util.Util.writeCell(rowe, columnCount++, Util.convertDateUtcGmtPeruString(listEvent.get(a).getCreationDate()), stylee, sheet, creationHelper);
                    rowCount++;
                }
                int substract = listEvent.size() - tramas.size();
                for (int x = a + 1; x < substract; x++) {
                    int columnCount = 5;
                    rowe = sheet.createRow(rowCount);
                    com.backend.platin.util.Util.writeCell(rowe, columnCount++, translateTypeEventToSpanish(listEvent.get(x).getType()), stylee, sheet, creationHelper);
                    if (listEvent.get(i).getGeozone_id() != null && !listEvent.get(i).getGeozone_id().equals("")
                            && !listEvent.get(i).getGeozone_id().equals("null")) {
                        Geozone geozone = geozoneService.findById(listEvent.get(i).getGeozone_id());
                        if (geozone == null) {
                            throw new DataNotFoundException(Util.GEOZONE_NOT_FOUND);
                        }
                        com.backend.platin.util.Util.writeCell(rowe, columnCount++, geozone.getName(), stylee, sheet, creationHelper);
                    } else {
                        com.backend.platin.util.Util.writeCell(rowe, columnCount++, " ", stylee, sheet, creationHelper);
                    }
                    com.backend.platin.util.Util.writeCell(rowe, columnCount++, Util.convertDateUtcGmtPeruString(listEvent.get(i).getCreationDate()), stylee, sheet, creationHelper);
                    rowCount++;
                }
            }
        }
        return sheet;
    }

    private static String translateTypeEventToSpanish(String typeEvent) {
        String type = null;
        switch (typeEvent) {
            case Util.TYPERECOVERYTRASMISSION:
                type = "RECUPERACIÓN";
                break;
            case Util.TYPELOSTTRASMISSION:
                type = "PÉRDIDA";
                break;
            case Util.TYPETIMELAG:
                type = "DEFASE DE TIEMPO";
                break;
            case Util.TYPEMAXIMUNSPEED:
                type = "EXCESO DE VELOCIDAD";
                break;
            case Util.TYPESTARTOUT:
                type = "EMPIEZA VIAJE";
                break;
            case Util.TYPEENDIN:
                type = "TERMINA VIAJE";
                break;
            case Util.TYPESTOPPED:
                type = "DETENIDO";
                break;
            case Util.TYPESTOPPEDFOUR:
                type = "DETENIDO POR 4 HORAS";
                break;
            case Util.TYPEMOVEMENT:
                type = "EN MOVIMIENTO";
                break;
            case Util.TYPEMOVEMENTFOUR:
                type = "EN MOVIMIENTO POR 4 HORAS";
                break;
            case Util.TYPEINCOMEREPORT:
                type = "DENTRO DE LA GEOZONA";
                break;
            case Util.TYPEEXITREPORT:
                type = "FUERA DE LA GEOZONA";
                break;
        }

        return type;
    }


    public static String[] generateGeneralHeadersExcel(String typeReport) {
        String[] headers = null;
        if(typeReport.equals(GENERAL_REPORT))
        {
            headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta"};
        }else if(typeReport.equals(STANDARD_LAST_POS_REPORT))
        {
             headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Kms Recorridos", "Dirección", "Distrito","Ciudad"};
        }else if(typeReport.equals(TRAVEL_REPORT) || typeReport.equals(STOPPED_REPORT) || typeReport.equals(STOPPED_REPORTFOUR)|| typeReport.equals(GEOZONES_REPORT) || typeReport.equals(NO_TRANSMISSION_REPORT)|| typeReport.equals(TIMELAG_REPORT))
        {
             headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta"};
        }else if(typeReport.equals(SPEED_REPORT))
        {
            headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta", "Total de excesos", "Valor Configurado"};
        }else if(typeReport.equals(GLOBAL_REPORT))
        {
            headers = new String[]{"Placa,", "EMV", "Cliente", "Codigo Osignergmin", "Estado de Vehículo", "Url del Mapa", "Fecha de Generación", "Desde", "Hasta"};
        }
          
        return headers;
    }

    public static Row generateBodyType(XSSFWorkbook wb, Row rowe, String cellData1, String cellData2, String cellData3,
                                       String cellData4, String cellData5, String cellData6, String cellData7, String cellData8,String cellData9, String cellData10,String typeReport) {
        CellStyle stylee = wb.createCellStyle();
        XSSFFont headerFont = wb.createFont();
        headerFont.setFontHeightInPoints((short)15 );
        stylee.setFont(headerFont);
        Cell cell = rowe.createCell(0);
        cell.setCellValue(cellData1);
        cell.setCellStyle(stylee);

        Cell cell2 = rowe.createCell(1);
        cell2.setCellValue(cellData2);
        cell2.setCellStyle(stylee);

        Cell cell3 = rowe.createCell(2);
        cell3.setCellValue(String.valueOf(cellData3));
        cell3.setCellStyle(stylee);

       
        if (typeReport.equals(Util.GEOZONES_REPORT)||typeReport.equals(Util.STANDARD_LAST_POS_REPORT)) {
            Cell cell4 = rowe.createCell(3);
            cell4.setCellValue(String.valueOf(cellData4));
            cell4.setCellStyle(stylee);
            Cell cell5 = rowe.createCell(4);
            cell5.setCellValue(String.valueOf(cellData5));
            cell5.setCellStyle(stylee);
        }  
            
        if (typeReport.equals(Util.NO_TRANSMISSION_REPORT) || typeReport.equals(Util.TRAVEL_REPORT) || typeReport.equals(Util.STOPPED_REPORTFOUR) || typeReport.equals(Util.STOPPED_REPORT) || typeReport.equals(Util.SPEED_REPORT) || typeReport.equals(Util.TIMELAG_REPORT)) {
            Cell cell4 = rowe.createCell(3);
            cell4.setCellValue(String.valueOf(cellData4));
            cell4.setCellStyle(stylee);
            Cell cell5 = rowe.createCell(4);
            cell5.setCellValue(String.valueOf(cellData5));
            cell5.setCellStyle(stylee);
           
        }
        
        if (typeReport.equals(Util.NO_TRANSMISSION_REPORT) || typeReport.equals(Util.TRAVEL_REPORT) || typeReport.equals(Util.STOPPED_REPORTFOUR) || typeReport.equals(Util.STOPPED_REPORT) || typeReport.equals(Util.TIMELAG_REPORT)) {
            Cell cell6 = rowe.createCell(5);
            cell6.setCellValue(String.valueOf(cellData6));
            cell6.setCellStyle(stylee);
        }
        
        if (typeReport.equals(Util.STOPPED_REPORT) || typeReport.equals(Util.STOPPED_REPORTFOUR) || typeReport.equals(Util.TRAVEL_REPORT)) {
            Cell cell5 = rowe.createCell(4);
            cell5.setCellValue(String.valueOf(cellData5));
            cell5.setCellStyle(stylee);
            Cell cell6 = rowe.createCell(5);
            cell6.setCellValue(String.valueOf(cellData6));
            cell6.setCellStyle(stylee);
            Cell cell7 = rowe.createCell(6);
            cell7.setCellValue(String.valueOf(cellData7));
            cell7.setCellStyle(stylee);
            Cell cell8 = rowe.createCell(7);
            cell8.setCellValue(String.valueOf(cellData8));
            cell8.setCellStyle(stylee);
            
        }
        
        if( typeReport.equals(Util.TRAVEL_REPORT))
        {
            Cell cell9 = rowe.createCell(8);
            cell9.setCellValue(String.valueOf(cellData9));
            cell9.setCellStyle(stylee);
            Cell cell10 = rowe.createCell(9);
            cell10.setCellValue(String.valueOf(cellData10));
            cell10.setCellStyle(stylee);
        }
      
        
        return rowe;
    }

    public static XSSFSheet generateHeaderBodyType(int nRow, XSSFSheet sheet, String[] excelHeaderR, CellStyle style) {
        Row row = sheet.createRow(nRow);
        for (int i = 0; i < excelHeaderR.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(excelHeaderR[i]);
            cell.setCellStyle(style);
        }
        return sheet;
    }
    
    public static XSSFSheet generateHeaderBodyTypev2(Row row, XSSFSheet sheet, List<String> excelHeaderR, CellStyle style) {
        
        for (int i = 0; i < excelHeaderR.size(); i++) {
            Cell cell = row.createCell(i);           
            cell.setCellValue(excelHeaderR.get(i));
            cell.setCellStyle(style);
        }
        return sheet;
    }

    private static String formatDuration(long duration) {
        long hours = TimeUnit.MILLISECONDS.toHours(duration);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) % 60;
//        long milliseconds = duration % 1000;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static PositionDto parseJsonToPositionDto(JsonObject jsonPosition )
    {
        PositionDto position = new PositionDto();
        position.setAltitude(jsonPosition.get("altitude").getAsDouble());
        position.setLatitude(jsonPosition.get("latitude").getAsDouble());
        position.setLongitude(jsonPosition.get("longitude").getAsDouble());

        return position;
    }


    public static TramaInsDto parseJsonToTramaDto(String tramaString) throws ParseException
    {
        JsonParser parser = new JsonParser();
        JsonElement jsonElemente = parser.parse(tramaString);
        JsonObject rootObject = jsonElemente.getAsJsonObject();
        JsonObject childObject = rootObject.getAsJsonObject("position"); // get place object
        double latitude = Double.parseDouble(childObject.get("latitude").getAsString());
        double longitude = Double.parseDouble(childObject.get("longitude").getAsString());
        double altitude = Double.parseDouble(childObject.get("altitude").getAsString());

        String event = rootObject.get("event").getAsString();
        String plate = rootObject.get("plate").getAsString();
        String tokenTrama = rootObject.get("tokenTrama").getAsString();
        double speed = Double.parseDouble(rootObject.get("speed").getAsString());
        double odometer = Double.parseDouble(rootObject.get("odometer").getAsString());
        String gpsDateString = rootObject.get("gpsDate").getAsString();
        String receiveDateString = rootObject.get("receiveDate").getAsString();
       
        SimpleDateFormat formatter = new SimpleDateFormat("MMM d, yyyy, h:mm:ss a", Locale.ENGLISH);
       
        Date gpsDate = formatter.parse(gpsDateString);
        Date receiveDate= formatter.parse(receiveDateString);

        PositionDto po = new PositionDto(latitude,longitude,altitude);
        TramaInsDto trama = new TramaInsDto(event, plate,
                po,gpsDate , speed, receiveDate,
                tokenTrama, odometer);
        
        Date utcGpsDate=  trama.getGpsDate();
        trama.setGpsDate(utcGpsDate);
        
        return trama;
    }

    public static String convertStringUtcGmtPeruString(String gpsdate) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date gps_date = simpleDateFormat.parse(gpsdate);
        Calendar calendarR = Calendar.getInstance();
        calendarR.setTime(gps_date);
        calendarR.add(Calendar.HOUR, -5);
        Date fechaSalidaR = calendarR.getTime();
        return parseDatetoJpaFormatString(fechaSalidaR);
    }

    public static Date convertStringUtcGmtPeruDate(String gpsdate) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date gps_date = simpleDateFormat.parse(gpsdate);
        Calendar calendarR = Calendar.getInstance();
        calendarR.setTime(gps_date);
        calendarR.add(Calendar.HOUR, -5);
        Date fechaSalidaR = calendarR.getTime();
        return fechaSalidaR;
    }
}