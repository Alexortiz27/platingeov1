/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.util;

import com.backend.ingest.dtos.LastPositionVehicleDto;
import com.backend.ingest.entities.Trama;

import java.util.Comparator;

/**
 *
 * @author FAMLETO
 */
public class VehiclesSortByDate implements Comparator<LastPositionVehicleDto>{

    @Override
    public int compare(LastPositionVehicleDto arg0, LastPositionVehicleDto arg1) {
        return arg0.getGpsDate().compareTo(arg1.getGpsDate());
    }
    
}
