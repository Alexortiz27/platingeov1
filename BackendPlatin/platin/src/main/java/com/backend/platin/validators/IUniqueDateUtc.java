/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.validators;

import com.backend.platin.exceptions.MyJsonDateDeserializer;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UniqueDateValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IUniqueDateUtc {
    String message() default "No es válida y tiene que ser formato UTC.";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
