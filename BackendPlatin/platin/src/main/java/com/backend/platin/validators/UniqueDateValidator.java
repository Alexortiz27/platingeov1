/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.validators;

import com.backend.ingest.repositories.ITravelRepository;
import com.backend.platin.exceptions.MyJsonDateDeserializer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueDateValidator implements ConstraintValidator<IUniqueDateUtc, Date> {

    @Autowired
    ITravelRepository travelRepository;

    @Override
    public boolean isValid(Date name, ConstraintValidatorContext context) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        Date fecha = null;
        try {
            fecha = sdf.parse("2000-01-01T00:01:00.000Z");
        } catch (ParseException ex) {
        }
        if (name == null) {
            return true;
        }
        return !name.equals(fecha);
    }
}
