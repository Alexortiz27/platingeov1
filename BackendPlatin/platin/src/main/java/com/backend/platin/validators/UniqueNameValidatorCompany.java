/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.validators;

import com.backend.ingest.repositories.ICompanyRepository;
import com.backend.platin.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueNameValidatorCompany implements ConstraintValidator<IUniqueNameCompany, String> {

    @Autowired
    ICompanyRepository emvRepository;

    @Override
    public boolean isValid(String name, ConstraintValidatorContext context) {
        boolean exis = false;
        Integer exi = emvRepository.existsByNameAll(name, Util.ACTIVE_STATUS);
        if (exi == 0) {
            exis = true;
        }
        return exis;
    }
}
