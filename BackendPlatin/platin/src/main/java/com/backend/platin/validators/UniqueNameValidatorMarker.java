/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.platin.validators;

import com.backend.ingest.repositories.IMarkerRepository;
import com.backend.ingest.repositories.ITravelRepository;
import com.backend.platin.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueNameValidatorMarker implements ConstraintValidator<IUniqueNameMarker, String> {

    @Autowired 
    IMarkerRepository markerRepository;

    @Override
    public boolean isValid(String name, ConstraintValidatorContext context) {
        boolean exis = false;
        Integer exi = markerRepository.findByNameAll(name, Util.ACTIVE_STATUS);
        if (exi == 0) {
            exis = true;
        }
        return exis;
    }
}
