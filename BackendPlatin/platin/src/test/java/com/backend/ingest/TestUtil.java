package com.backend.ingest;

import com.backend.platin.util.*;
import com.backend.WorkerConsoleTramaWebSocket.MqConsumerByChannelServiceImplWebSocket;
import com.backend.ingest.dtos.CoordInDto;
import com.backend.ingest.dtos.GeozoneNsqDto;
import com.backend.ingest.dtos.PositionDto;
import com.backend.ingest.dtos.TramaInsDto;
import com.backend.ingest.entities.Geozone;
import com.backend.ingest.entities.Trama;
import com.backend.ingest.service.services.GeozoneService;
import com.backend.ingest.services.redis.EventRedisHsetService;
import com.backend.platin.exceptions.DataNotFoundException;
import com.google.gson.*;
import io.lettuce.core.api.sync.RedisCommands;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.swing.text.Position;
import java.text.ParseException;
import java.util.*;

public class TestUtil {
    private final Logger log = LogManager.getLogger(TestUtil.class);
    @Autowired(required = true)
    private GeozoneService geozoneService;

    @Autowired(required = true)
    private EventRedisHsetService eventRedis;
    @Autowired
    RedisCommands<String, String> jedis;
    //Util u = new Util();
    public boolean isNull(Object valueField) {
        if (valueField == null || (valueField.getClass().equals(String.class) && ((String) valueField).isEmpty())) {
            return true;
        }
        return false;
    }

    @Test
    @DisplayName("Test!")
    public void validarEmail() throws DataNotFoundException {
        List<Map<String, Object>> gg = geozoneService.findByStatusAll();
        Util u = new Util();
        boolean exis = u.validEmail("ortizvega@gmail.com");
        Assertions.assertEquals(true, exis, "El Correo es Valido");
        Assertions.assertNotEquals(false, exis, "El Correo no es Valido");
    }

    @Test
    @DisplayName("TestString!")
    public void validarString() {
        String key = null;
        boolean exis = isNull(key);

        if (!exis) {
            System.out.println("El key no es null");
        }
        if (exis) {
            System.out.println("El key es null");
        }
        System.out.println("El key es:" + exis);

        Assertions.assertEquals(true, exis, "El Correo es Valido");
        Assertions.assertNotEquals(false, exis, "El Correo no es Valido");
    }

    @Test
    @DisplayName("TramaStringAndObject!")
    public void TramaStringAndObject() {
        Util u = new Util();
        TramaInsDto tr = null;
        try {
            tr = u.parseJsonToTramaDto("{\"event\":\"TestREDISj\",\"plate\":\"MERC001\",\"position\":{\"latitude\":-12.054126219274595,\"longitude\":-77.14071750640869,\"altitude\":44.2222},\"gpsDate\":\"Nov 24, 2021, 5:09:14 AM\",\"speed\":250.0,\"receiveDate\":\"Nov 24, 2021, 5:09:15 AM\",\"tokenTrama\":\"1511FA4C-8806-496E-B9F8-A4AED00B5B4A\",\"odometer\":200.0,\"ru_gps_date\":\"1637730554943\"}");
            System.out.println(tr.getPlate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Assertions.assertNotEquals(false, tr, "El Trama no es Valido");
        Assertions.assertEquals(true, tr, "La trama es valida");
    }

    @Test
    @DisplayName("TramaStringAndObject!")
    public void TramaStringAndObjectv1() {
        Map<Object, Object> addres = new HashMap<Object, Object>();
        PositionDto position = new PositionDto(-11.122760509290623, -77.61051177978517, 22);
        try {
            if (position != null && position.getLatitude() != 0 && position.getLongitude() != 0) {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                String uriWithParameters = "https://nominatim.openstreetmap.org/reverse?lat=" + position.getLatitude() + "&lon=" + position.getLongitude() + "&format=jsonv2&addressdetails=1&extratags=1";
                RestTemplate restTemplate = restTemplate();
                ResponseEntity<String> result = restTemplate.getForEntity(uriWithParameters, String.class);

                if (result != null && result.getBody() != null) {
                    String jsonAddressString = result.getBody();
                    JsonObject jsonResult = new JsonParser().parse(jsonAddressString).getAsJsonObject();
                    JsonObject jsonAddress = jsonResult.get("address").getAsJsonObject();

                    if (jsonResult != null && jsonResult.get("address") != null) {
                        if (jsonResult.has("display_name")) {
                            addres.put("display_name", jsonResult.get("display_name").getAsString());
                        }
                        if (jsonResult.has("name")) {
                            addres.put("name", jsonResult.get("name").getAsString());
                        }
                        if (jsonAddress.has("house_number")) {
                            addres.put("house_number", jsonAddress.get("house_number").getAsString());

                        }
                        if (jsonAddress.has("road")) {
                            addres.put("road", jsonAddress.get("road").getAsString());

                        }
                        if (jsonAddress.has("city")) {
                            addres.put("city", jsonAddress.get("city").getAsString());

                        }
                        if (jsonAddress.has("region")) {
                            addres.put("region", jsonAddress.get("region").getAsString());

                        }
                        if (jsonAddress.has("postcode")) {
                            addres.put("postcode", jsonAddress.get("postcode").getAsString());

                        }
                        if (jsonAddress.has("country")) {
                            addres.put("country", jsonAddress.get("country").getAsString());

                        }
                        if (jsonAddress.has("country_code")) {
                            addres.put("country_code", jsonAddress.get("country_code").getAsString());

                        }
                        if (jsonAddress.has("state")) {
                            addres.put("state", jsonAddress.get("state").getAsString());

                        }
                        if (jsonAddress.has("town")) {
                            addres.put("town", jsonAddress.get("town").getAsString());

                        }

                    } else {
                        System.out.println("error2");
                    }

                } else {
                    System.out.println("error3");
                }
            } else {
                System.out.println("error4");
            }
        } catch (Exception ex) {
            System.out.println("errortry" + ex);

        }
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public int go() {
        int[] myNumbers = {1, 2, 3};
        //System.out.println(myNumbers[10]);
        return myNumbers[10];
    }

    @Test
    @DisplayName("TRY!")
    public void TryCatch() {
        try {
            go();

        } catch (Exception e) {
            int currentLine = e.getStackTrace()[0].getLineNumber();
            String metodo = e.getStackTrace()[0].getMethodName();
            String clas = e.getStackTrace()[0].getClassName();
            System.out.println("Error en la clase:" + clas + " en el metodo:" + metodo + " y en la linea:" + currentLine);
            log.info("Error Worker WEBSOCKET_WORKER_CHANNEL:" + e.getMessage());
            log.info(com.backend.platin.consumer.util.Util.ExceptionClassMethodLine(e));
            e.printStackTrace();
        }

//        Assertions.assertNotEquals(false, tr, "El Trama no es Valido");
//        Assertions.assertEquals(true, tr, "La trama es valida");
    }


    @Test
    @DisplayName("repeated")
    public void repeated() {
        Util u = new Util();
        System.out.println("Hello Param");
    }


}
